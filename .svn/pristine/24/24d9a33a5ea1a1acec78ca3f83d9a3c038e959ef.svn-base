package com.chefonline.online;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.customview.MsmEditText;
import com.chefonline.datamodel.UserData;
import com.chefonline.gps.GPSTracker;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UtilityMethod;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

//from home
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static String TAG = "LoginActivity";
    private Button btnRegistration;
    private Button btnLogin;
    private MsmEditText editTextUserName;
    private MsmEditText editTextPassword;
    private PreferenceUtil preferenceUtil;
    private TextView txtViewForgotPassword;
    private Dialog dialogForgot;
    Toolbar toolBar;

    UserData userData;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        setUiListener();
    }

    private void setUiListener() {
        btnRegistration.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        txtViewForgotPassword.setOnClickListener(this);
    }

    private void initView() {

        toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle("Login");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        preferenceUtil = new PreferenceUtil(this);
        txtViewForgotPassword = (TextView) findViewById(R.id.text_view_forgot_password);
        btnRegistration = (Button) findViewById(R.id.btnRegistration);
        btnLogin = (Button) findViewById(R.id.btn_login);
        editTextUserName = (MsmEditText) findViewById(R.id.edit_text_user);
        editTextPassword = (MsmEditText) findViewById(R.id.edit_text_password);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    ProgressDialog progress;

    /**
     * api for login api
     */
    private void callLoginApi() {
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        final GPSTracker gps = new GPSTracker(this);
        String url = ConstantValues.BASE_API_URL;

        StringRequest myReq = new StringRequest(Request.Method.POST, url, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "3");
                params.put("username", editTextUserName.getText().toString().trim());
                params.put("password", editTextPassword.getText().toString().trim());
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

        progress = new ProgressDialog(LoginActivity.this);
        progress.setMessage("Please wait....");
        progress.show();
    }

    /**
     * api for forgot password
     */
    private void callForgotPasswordApi(final String email) {
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        String url = ConstantValues.BASE_API_URL;

        StringRequest myReq = new StringRequest(Request.Method.POST, url, createForgotPassReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "4");
                params.put("email", email);
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

        progress = new ProgressDialog(LoginActivity.this);
        progress.setMessage("Please wait....");
        progress.show();
    }

    /**
     * Response listener for login api
     */
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Login_response", response);
                    progress.dismiss();

                    userData = new UserData();
                    userData = JsonParser.parserUserData(response);
                    AppData.getInstance().setUserDatas(userData);

                    if (userData.getLoginStatus().equalsIgnoreCase("Success")) {
                        if (getIntent().getExtras().getInt(ConstantValues.NAVIGATION_KEY) == ConstantValues.FRAGMENT_CART) {
                            ConstantValues.NAVIGATION_TO = ConstantValues.FRAGMENT_CART;

                        } else if (getIntent().getExtras().getInt(ConstantValues.NAVIGATION_KEY) == ConstantValues.ACTIVITY_RESERVATION) {
                            ConstantValues.NAVIGATION_TO = ConstantValues.ACTIVITY_RESERVATION;

                        } else if (getIntent().getExtras().getInt(ConstantValues.NAVIGATION_KEY) == ConstantValues.ACTIVITY_SETTINGS) {
                            ConstantValues.NAVIGATION_TO = ConstantValues.ACTIVITY_SETTINGS;
                        }

                        preferenceUtil.setLogInStatus(1); // set login status 1
                        preferenceUtil.setUserID(userData.getUserId());
                        preferenceUtil.setUserTitle(userData.getTitle());
                        preferenceUtil.setUserFirstName(userData.getFirstName());
                        preferenceUtil.setUserSurName(userData.getSurName());
                        preferenceUtil.setUserLastName(userData.getLastName());
                        preferenceUtil.setUserAddress1(userData.getAddress1());
                        preferenceUtil.setUserAddress2(userData.getAddress2());
                        preferenceUtil.setUserMobile(userData.getMobileNo());
                        preferenceUtil.setUserEmail(userData.getEmailId());
                        preferenceUtil.setUserDob(userData.getDob());
                        preferenceUtil.setUserDoa(userData.getDoa());
                        preferenceUtil.setUserGroupId(userData.getGroupId());
                        preferenceUtil.setUserPostCode(userData.getPostCode());
                        preferenceUtil.setUserTel(userData.getTelNo());
                        preferenceUtil.setUserTown(userData.getCity());
                        preferenceUtil.setUserCountry(userData.getCountry());

                        Intent intent = new Intent();
                        intent.putExtra("data", 1);
                        setResult(RESULT_OK, intent);
                        new CustomToast(LoginActivity.this, getResources().getString(R.string.login_message_success), "", true);
                        finish();
                    } else {
                        new CustomToast(LoginActivity.this, getResources().getString(R.string.login_message_failed), "", false);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        new CustomToast(LoginActivity.this, "Parsing error encountered", "", false);
                    } catch (Exception e1) {

                    }
                }

            }

        };
    }

    /**
     * Response listener for forgot password api
     */
    private Response.Listener<String> createForgotPassReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progress.dismiss();
                    Log.e("Forget_pass_response", response);

                    JSONObject jsonObject = new org.json.JSONObject(response);
                    JSONObject jsonAppObject = jsonObject.getJSONObject("app");
                    Log.e(TAG, jsonAppObject.toString());

                    if ("Success".equalsIgnoreCase(jsonAppObject.getString("status"))) {
                        new CustomToast(LoginActivity.this, jsonAppObject.getString("msg"), "", true);
                        dialogForgot.dismiss();

                    } else {
                        new CustomToast(LoginActivity.this, jsonAppObject.getString("msg"), "", false);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        new CustomToast(LoginActivity.this, "Parsing error encountered.", "", false);
                    } catch (Exception e1) {

                    }
                }

            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                    new CustomToast(LoginActivity.this, "Server Response Error.", "", false);
                } catch (Exception e) {
                    Log.e("After volley execution", e.getMessage());
                }
            }
        };
    }

    private void openDilaog() {
        dialogForgot = new Dialog(this);
        dialogForgot.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogForgot.setContentView(R.layout.dialog_forgot_password);

        final MsmEditText editTextEmail = (MsmEditText) dialogForgot.findViewById(R.id.editTextEmail);
        // editTextEmail.setErrorMsgColor(getResources().getColor(R.color.theme_red_dark));
        ImageButton dialogButton = (ImageButton) dialogForgot.findViewById(R.id.imgBtnClose);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogForgot.dismiss();
            }
        });

        Button btnResend = (Button) dialogForgot.findViewById(R.id.btnResend);
        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                if (!editTextEmail.getText().toString().trim().equalsIgnoreCase("")) {
                    if (UtilityMethod.isEmailValid(editTextEmail.getText().toString().trim())) {
                        callForgotPasswordApi(editTextEmail.getText().toString().trim());
                        AppData.getInstance().hideKeyboard(LoginActivity.this, v);
                        dialogForgot.dismiss();
                    } else {
                        editTextEmail.setErrorTextVisible(true);
                        editTextEmail.setErrorMsg("Please enter valid email.");
                    }

                } else {
                    editTextEmail.setErrorTextVisible(true);
                    editTextEmail.setErrorMsg("Please enter your email.");
                }

            }
        });

        dialogForgot.show();
    }

    public boolean loginValidation() {
        /*if (!editTextUserName.getText().toString().trim().equalsIgnoreCase("")) {
            if (!UtilityMethod.isEmailValid(editTextUserName.getText().toString().trim())) {
                editTextUserName.setTypingFocus();
                editTextUserName.setErrorTextVisible(true);
                editTextUserName.setErrorMsg("Please enter valid email.");
                return false;
            }
        } else {
            editTextUserName.setTypingFocus();
            editTextUserName.setErrorTextVisible(true);
            editTextUserName.setErrorMsg("Please enter valid email.");
            return false;
        }*/

        if (editTextUserName.getText().toString().trim().equalsIgnoreCase("")) {
            editTextUserName.setTypingFocus();
            editTextUserName.setErrorTextVisible(true);
            editTextUserName.setErrorMsg("Please enter valid email.");
            return false;
        }

        //When password is empty enter
        if (editTextPassword.getText().toString().trim().equalsIgnoreCase("")) {
            editTextPassword.setTypingFocus();
            editTextPassword.setErrorTextVisible(true);
            editTextPassword.setErrorMsg("Please enter valid password.");
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        } else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        switch (v.getId()) {
            case R.id.btnRegistration:
                Intent intent = new Intent(this, RegistrationActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.btn_login:
                if (loginValidation()) {
                    callLoginApi();
                    AppData.getInstance().hideKeyboard(LoginActivity.this, v);
                }
                break;

            case R.id.text_view_forgot_password:
                openDilaog();
                break;
        }
    }

}
