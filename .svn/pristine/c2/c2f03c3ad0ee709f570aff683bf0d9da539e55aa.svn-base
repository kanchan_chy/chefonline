package com.chefonline.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.customview.MsmEditText;
import com.chefonline.datamodel.UserData;
import com.chefonline.gps.GPSTracker;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.online.R;
import com.chefonline.online.RegistrationActivity;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UtilityMethod;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

//from home

public class LoginFragment extends Fragment implements View.OnClickListener {
    private static String TAG = "LoginActivity";
    private Button btnRegistration;
    private Button btnLogin;
    private MsmEditText editTextUserName;
    private MsmEditText editTextPassword;
    private PreferenceUtil preferenceUtil;
    private TextView txtViewForgotPassword;
    private ImageButton imgBtnBack;
    private long mLastClickTime = 0;

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Login");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Order");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        initView(rootView);
        setUiListener();
        return rootView;
    }

    private void setUiListener() {
        btnRegistration.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        txtViewForgotPassword.setOnClickListener(this);
    }

    private void initView(View rootView) {
        preferenceUtil = new PreferenceUtil(getActivity());
        imgBtnBack = (ImageButton) rootView.findViewById(R.id.imgBtnBack);
        txtViewForgotPassword = (TextView) rootView.findViewById(R.id.text_view_forgot_password);
        btnRegistration = (Button) rootView.findViewById(R.id.btnRegistration);
        btnLogin = (Button) rootView.findViewById(R.id.btn_login);
        editTextUserName = (MsmEditText) rootView.findViewById(R.id.edit_text_user);
        editTextPassword = (MsmEditText) rootView.findViewById(R.id.edit_text_password);
    }

    private void loadOffersFragment() {
        android.app.Fragment fragment = null;
        fragment = new OffersCalculationFragment();
        Bundle args = new Bundle();
        args.putInt("selected_service", getArguments().getInt("selected_service"));
        args.putString("selected_policy_id", getArguments().getString("selected_policy_id"));
        args.putString("selected_policy_time", getArguments().getString("selected_policy_time"));
        args.putInt("selected_offer", 0);

        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.addToBackStack(null);
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_left);
        ft.add(R.id.content_frame, fragment);
        ft.commit();
    }

    private void loadPaymentSelectFragment() {
        android.app.Fragment fragment = null;
        fragment = new CheckoutFragment();
        Bundle args = new Bundle();
        args.putInt("selected_service",getArguments().getInt("selected_service"));
        args.putString("selected_policy_id", getArguments().getString("selected_policy_id"));
        args.putString("selected_policy_time", getArguments().getString("selected_policy_time"));
        args.putString("grand_sub_total", getArguments().getString("grand_sub_total"));
        args.putString("discount_amount", getArguments().getString("discount_amount"));
        args.putString("total_amount", getArguments().getString("total_amount"));
        args.putString("offer_text", "");
        args.putInt("selected_offer", 0);

        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.addToBackStack(null);
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_left);
        ft.add(R.id.content_frame, fragment);
        ft.commit();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    ProgressDialog progress;

    /** api for login api */
    private void callLoginApi() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final GPSTracker gps = new GPSTracker(getActivity());
        String url = ConstantValues.BASE_API_URL;

        StringRequest myReq = new StringRequest(Request.Method.POST, url, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "3");
                params.put("username", editTextUserName.getText().toString().trim());
                params.put("password", editTextPassword.getText().toString().trim());
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

        progress = new ProgressDialog(getActivity());
        progress.setMessage("Please wait....");
        progress.show();
    }

    /** api for forgot password */
    private void callForgotPasswordApi(final String email) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = ConstantValues.BASE_API_URL;

        StringRequest myReq = new StringRequest(Request.Method.POST, url, createForgotPassReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "4");
                params.put("email", email);
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

        progress = new ProgressDialog(getActivity());
        progress.setMessage("Please wait....");
        progress.show();
    }

    /** Response listener for login api */
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progress.dismiss();
                    UserData userData = new UserData();
                    userData = JsonParser.parserUserData(response);
                    AppData.getInstance().setUserDatas(userData);

                    if (userData.getLoginStatus().equalsIgnoreCase("Success")) {
                        preferenceUtil.setLogInStatus(1); // set login status 1
                        preferenceUtil.setUserID(userData.getUserId());
                        preferenceUtil.setUserFirstName(userData.getFirstName());
                        Log.i(TAG, "******* " + preferenceUtil.getUserFirstName());
                        preferenceUtil.setUserSurName(userData.getSurName());
                        preferenceUtil.setUserLastName(userData.getLastName());
                        preferenceUtil.setUserAddress1(userData.getAddress1());
                        preferenceUtil.setUserAddress2(userData.getAddress2());
                        preferenceUtil.setUserMobile(userData.getMobileNo());
                        preferenceUtil.setUserEmail(userData.getEmailId());
                        preferenceUtil.setUserDob(userData.getDob());
                        preferenceUtil.setUserDoa(userData.getDoa());
                        preferenceUtil.setUserGroupId(userData.getGroupId());
                        preferenceUtil.setUserPostCode(userData.getPostCode());
                        preferenceUtil.setUserTel(userData.getTelNo());
                        preferenceUtil.setUserTown(userData.getCity());
                        preferenceUtil.setUserCountry(userData.getCountry());

                        Intent intent = new Intent();
                        intent.putExtra("data", 1);
                        getActivity().setResult(getActivity().RESULT_OK, intent);
                        Toast.makeText(getActivity(), getResources().getString(R.string.login_message_success), Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();

                        if (getArguments().getInt("is_navigate_to_offers") == 0) {
                            loadPaymentSelectFragment();
                        } else {
                            loadOffersFragment();
                        }

                    } else {
                        new CustomToast(getActivity(), getResources().getString(R.string.login_message_failed), "", false);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    new CustomToast(getActivity(), "Parsing error encountered.", "", false);
                }

            }

        };
    }

    /** Response listener for forgot password api */
    private Response.Listener<String> createForgotPassReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progress.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonAppObject = jsonObject.getJSONObject("app");
                    Log.e(TAG, jsonAppObject.toString());
                    jsonAppObject.getString("status");

                    if ("Success".equalsIgnoreCase(jsonAppObject.getString("status"))) {
                        new CustomToast(getActivity(), jsonAppObject.getString("msg"), "", false);
                    } else {
                        new CustomToast(getActivity(), jsonAppObject.getString("msg"), "", false);
                    }

                } catch (Exception e) {
                    try{
                        e.printStackTrace();
                        new CustomToast(getActivity(), "Parsing error encountered", "", false);
                    }
                    catch (Exception e1){
                        Log.e("After volley execution",e1.getMessage());
                    }
                }

            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                    new CustomToast(getActivity(), "Server Response Error Splash", "", false);
                }
                catch (Exception e) {
                    Log.e(TAG, "After volley request *** " + e.getMessage());
                }
            }
        };
    }

    private void openDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_forgot_password);

        final MsmEditText editTextEmail = (MsmEditText) dialog.findViewById(R.id.editTextEmail);
        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dialog.dismiss();
            }
        });

        Button btnResend = (Button) dialog.findViewById(R.id.btnResend);
        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppData.getInstance().hideKeyboard(getActivity(), v);
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (!editTextEmail.getText().toString().trim().equalsIgnoreCase("")) {
                    if (UtilityMethod.isEmailValid(editTextEmail.getText().toString().trim())) {
                        callForgotPasswordApi(editTextEmail.getText().toString().trim());
                        dialog.dismiss();

                    } else {
                        editTextEmail.setErrorTextVisible(true);
                        editTextEmail.setErrorMsg("Please enter valid email.");
                    }

                } else {
                    editTextEmail.setErrorTextVisible(true);
                    editTextEmail.setErrorMsg("Please enter your email.");
                }

                //callForgotPasswordApi(editTextEmail.getText().toString());

            }
        });

        dialog.show();
    }

    public boolean loginValidation() {
        /*if (!editTextUserName.getText().toString().trim().equalsIgnoreCase("")) {
            if (!UtilityMethod.isEmailValid(editTextUserName.getText().toString().trim())) {
                editTextUserName.setTypingFocus();
                editTextUserName.setErrorTextVisible(true);
                editTextUserName.setErrorMsg("Please enter valid email.");
                return false;
            }
        } else {
            editTextUserName.setTypingFocus();
            editTextUserName.setErrorTextVisible(true);
            editTextUserName.setErrorMsg("Please enter valid email.");
            return false;
        }*/

        if (editTextUserName.getText().toString().trim().equalsIgnoreCase("")) {
            editTextUserName.setTypingFocus();
            editTextUserName.setErrorTextVisible(true);
            editTextUserName.setErrorMsg("Please enter valid email.");
            return false;
        }

        //When password is empty enter
        if (editTextPassword.getText().toString().trim().equalsIgnoreCase("")) {
            editTextPassword.setTypingFocus();
            editTextPassword.setErrorTextVisible(true);
            editTextPassword.setErrorMsg("Please enter valid password.");
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegistration:
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                Intent intent = new Intent(getActivity(), RegistrationActivity.class);
                startActivity(intent);
                getActivity().onBackPressed();
                break;
            case R.id.btn_login:
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }

                if(loginValidation()) {
                    callLoginApi();
                    AppData.getInstance().hideKeyboard(getActivity(), v);
                }

                break;

            case R.id.text_view_forgot_password:
                openDialog();
            break;
            case R.id.imgBtnBack:
                getActivity().onBackPressed();
                break;
        }
    }

}
