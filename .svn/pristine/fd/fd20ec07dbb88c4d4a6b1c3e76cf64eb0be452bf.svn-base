package com.chefonline.online;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.customview.MsmEditText;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UtilityMethod;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    private static String TAG = "ResetPasswordActivity";
    private MsmEditText editTextEmail;
    private MsmEditText editTextPrevPassword;
    private MsmEditText editTextNewPassword;
    private MsmEditText editTextNewPasswordConfirm;
    private Button btnSend;
    Toolbar toolBar;
    PreferenceUtil preferenceUtil;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        initView();
        setUiListener();
    }

    private void setUiListener() {
        btnSend.setOnClickListener(this);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void initView() {

        toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle("Reset Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        preferenceUtil = new PreferenceUtil(this);

        editTextEmail = (MsmEditText) findViewById(R.id.editTextEmail);
        editTextEmail.setEnable(false);
        editTextPrevPassword = (MsmEditText) findViewById(R.id.editTextPrevPassword);
        editTextNewPassword = (MsmEditText) findViewById(R.id.editTextNewPassword);
        editTextNewPasswordConfirm = (MsmEditText) findViewById(R.id.editTextNewPasswordConfirm);
        btnSend = (Button) findViewById(R.id.btnSend);

        if (isLoggedIn()) {
            editTextEmail.setText(preferenceUtil.getUserEmail());
        }

    }

    public boolean isLoggedIn() {
        return preferenceUtil.getLogInStatus() == 1;
    }

    ProgressDialog progress;
    private void callResetPassword() {
        RequestQueue queue = Volley.newRequestQueue(ResetPasswordActivity.this);
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "10");
                params.put("email", editTextEmail.getText().toString().trim());
                params.put("previouspassword", editTextPrevPassword.getText().toString().trim());
                params.put("newpassword", editTextNewPassword.getText().toString().trim());

                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);
        progress = new ProgressDialog(ResetPasswordActivity.this);
        progress.setMessage("Please wait....");
        progress.show();
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                    String x = error.getMessage();
                    new CustomToast(ResetPasswordActivity.this, "Something went worng, please try again.", "", false);
                }
                catch (Exception e) {
                    Log.e("After volley execution",e.getMessage());
                }
            }
        };
    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progress.dismiss();
                    Log.i(TAG, "****" + response);

                    JSONObject jsonObject = new org.json.JSONObject(response);
                    if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        new CustomToast(ResetPasswordActivity.this, "Your password reset successfully.", "", true);
                        finish();
                    } else {
                        new CustomToast(ResetPasswordActivity.this, jsonObject.getString("msg"), "", true);
                    }

                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                        new CustomToast(ResetPasswordActivity.this, "Something went wrong, please try again.", "", true);
                    }
                    catch (Exception e1){
                        Log.e("After volley execution",e1.getMessage());
                    }
                }

            }

        };
    }

    private boolean inputValidation() {
        if (!UtilityMethod.isEmailValid(editTextEmail.getText().toString().trim())
                || editTextEmail.getText().toString().trim().equalsIgnoreCase("")) {
            editTextEmail.setErrorTextVisible(true);
            editTextEmail.setErrorMsg("Please enter valid email.");
            editTextEmail.setTypingFocus();
            return false;
        }

        /*if (UtilityMethod.isWhiteSpaces(editTextPrevPassword.getText().toString())) {
            editTextPrevPassword.setErrorTextVisible(true);
            editTextPrevPassword.setErrorMsg("White Space is not accepted.");
            editTextPrevPassword.setTypingFocus();
            return false;
        }*/

        if (editTextPrevPassword.getText().toString().trim().equalsIgnoreCase("")) {
            editTextPrevPassword.setErrorTextVisible(true);
            editTextPrevPassword.setErrorMsg("Please enter valid password.");
            editTextPrevPassword.setTypingFocus();
            return false;
        }


        if (UtilityMethod.isWhiteSpaces(editTextNewPassword.getText().toString())) {
            editTextNewPassword.setErrorTextVisible(true);
            editTextNewPassword.setErrorMsg("White Space is not accepted.");
            editTextNewPassword.setTypingFocus();
            return false;
        }

        if (editTextNewPassword.getText().toString().length() < 6 || editTextNewPassword.getText().toString().length() > 16) {
            editTextNewPassword.setErrorTextVisible(true);
            editTextNewPassword.setErrorMsg("Password should be between 6 to 16 character.");
            editTextNewPassword.setTypingFocus();
            return false;
        }

        /*if (editTextNewPassword.getText().toString().length() > 16) {
            editTextNewPassword.setErrorTextVisible(true);
            editTextNewPassword.setErrorMsg("Password length limit is exceeded.");
            editTextNewPassword.setTypingFocus();
            return false;
        }*/

        if (editTextNewPassword.getText().toString().trim().equalsIgnoreCase("")) {
            editTextNewPassword.setErrorTextVisible(true);
            editTextNewPassword.setErrorMsg("Please enter valid password.");
            editTextNewPassword.setTypingFocus();
            return false;
        }

        if (UtilityMethod.isWhiteSpaces(editTextNewPasswordConfirm.getText().toString())) {
            editTextNewPasswordConfirm.setErrorTextVisible(true);
            editTextNewPasswordConfirm.setErrorMsg("White Space is not accepted.");
            editTextNewPasswordConfirm.setTypingFocus();
            return false;
        }

        if (editTextNewPasswordConfirm.getText().trim().equalsIgnoreCase("")) {
            editTextNewPasswordConfirm.setErrorTextVisible(true);
            editTextNewPasswordConfirm.setErrorMsg("Your confirm password is required.");
            editTextNewPasswordConfirm.setTypingFocus();
            return false;
        }

        if (!editTextNewPasswordConfirm.getText().trim().equalsIgnoreCase(editTextNewPassword.getText().toString().trim())) {
            editTextNewPasswordConfirm.setErrorTextVisible(true);
            editTextNewPasswordConfirm.setErrorMsg("Password mismatch.");
            editTextNewPasswordConfirm.setTypingFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        switch (v.getId()){
            case R.id.btnSend:
                if (inputValidation()) {
                    callResetPassword();
                }

                break;

        }
    }
}
