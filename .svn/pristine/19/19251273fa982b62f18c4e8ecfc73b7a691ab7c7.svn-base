package com.chefonline.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.adapter.OrderHistoryDetailsAdapter;
import com.chefonline.customview.CustomToast;
import com.chefonline.datamodel.CollectionDeliveryData;
import com.chefonline.datamodel.DishData;
import com.chefonline.datamodel.HotRestaurantData;
import com.chefonline.gps.GPSTracker;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.online.OrderHostActivity;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UnsafeOkHttpClient;
import com.chefonline.utility.UtilityMethod;
import com.custom.dateform.DateFormatter;
import com.google.android.gms.maps.model.LatLng;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class OrderDetailsFragment extends Fragment implements View.OnClickListener{
    public static String TAG = "OrderDetailsFragment";
    ProgressBar progressBar;
    ListView listViewOrderDetails;
    TextView textViewTotal;
    TextView txtViewOrderId;
    TextView txtViewOrderDate;
    TextView txtViewConfirmation;
    Button btnReOrder;
    ImageView imageViewLogo;
    RelativeLayout relative_main;
    private boolean dialogOpened=false;
    ArrayList<HotRestaurantData> tempHotRestaurantDatas = new ArrayList<>();
    private long mLastClickTime = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_order_details, container, false);
        initView(rootView);
        setUiListener();
        callOrderHistoryApi();
        return rootView;
    }

    private void setUiListener() {
        btnReOrder.setOnClickListener(this);
    }

    private void initView(View rootView) {
       textViewTotal = (TextView) rootView.findViewById(R.id.textViewTotal);
       txtViewOrderId = (TextView) rootView.findViewById(R.id.txtViewOrderId);
       txtViewOrderDate = (TextView) rootView.findViewById(R.id.txtViewOrderDate);
       txtViewConfirmation = (TextView) rootView.findViewById(R.id.txtViewConfirmation);
       btnReOrder = (Button) rootView.findViewById(R.id.btnReOrder);
       progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar );
       listViewOrderDetails = (ListView) rootView.findViewById(R.id.listViewOrderDetails);
       imageViewLogo = (ImageView) rootView.findViewById(R.id.imageViewLogo);
       relative_main = (RelativeLayout) rootView.findViewById(R.id.relative_main);
    }

    /** Call restaurant category item api*/
    ProgressDialog progress;
    private void callOrderHistoryApi() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "14");
                params.put("order_id", getArguments().getString("order_id"));
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

        progress = new ProgressDialog(getActivity());
        progress.setMessage("Please wait....");
        //progress.show();
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progress.dismiss();
                try{
                    progressBar.setVisibility(View.GONE);
                    new CustomToast(getActivity(), "Server Response Error", "", false);
                }
                catch (Exception e){
                    Log.e("After volley execution",e.getMessage());
                }
            }
        };
    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //progress.dismiss();
                try {
                    progressBar.setVisibility(View.GONE);
                    relative_main.setVisibility(View.VISIBLE);
                    Log.e(TAG, "Order History Data**** " + response);

                    GPSTracker gpsTracker = new GPSTracker(getActivity());
                    LatLng latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                    ArrayList<HotRestaurantData> hotRestaurantDatas = JsonParser.getRestaurantOrderDetails(response, latLng);
                    if(hotRestaurantDatas != null) tempHotRestaurantDatas = hotRestaurantDatas;
                    /*Set to common singleton for restaurant information */
                    /*hotRestaurantDatas.get(0).setDeliveryTime();
                    hotRestaurantDatas.get(0).setCollectionTime();*/
                    CollectionDeliveryData collectionDeliveryData = new CollectionDeliveryData();
                    collectionDeliveryData = UtilityMethod.getCollectionDeliveryTIme(hotRestaurantDatas.get(0));
                    hotRestaurantDatas.get(0).setDeliveryTime(collectionDeliveryData.getDelivery());
                    hotRestaurantDatas.get(0).setCollectionTime(collectionDeliveryData.getCollection());

                    AppData.getInstance().setRestaurantInfoData(hotRestaurantDatas.get(0));
                    OrderHistoryDetailsAdapter orderHistoryDetailsAdapter = new OrderHistoryDetailsAdapter(getActivity(), hotRestaurantDatas.get(0).getDishDatas());
                    listViewOrderDetails.setAdapter(orderHistoryDetailsAdapter);

                    textViewTotal.setText("Total " + getActivity().getResources().getString(R.string.pound_sign) + hotRestaurantDatas.get(0).getGrandTotal());
                    txtViewOrderId.setText("Order Id: " + hotRestaurantDatas.get(0).getOrderId());
                    DateFormatter dateFormatter = new DateFormatter();
                    txtViewOrderDate.setText("Date: " + dateFormatter.doFormat(hotRestaurantDatas.get(0).getOrderDate(), DateFormatter.DD_MM_YYYY2, DateFormatter.MMM_dd_YYYY));
                    txtViewConfirmation.setText(getArguments().getString("confirmation_status"));

                    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View footerView  = inflater.inflate(R.layout.footer_order_history_details, null);
                    TextView textViewSubTotal = (TextView) footerView.findViewById(R.id.txtViewSubTotal);
                    TextView textViewDiscountAmount = (TextView) footerView.findViewById(R.id.txtViewDiscount);
                    TextView textViewDeliveryCharge = (TextView) footerView.findViewById(R.id.textViewDeliveryChargeAmount);
                    TextView textViewGrandTotal = (TextView) footerView.findViewById(R.id.textViewGrandTotal);
                    String discountAmount = hotRestaurantDatas.get(0).getDiscountAmount().trim();

                    if(discountAmount.equalsIgnoreCase("0.00") || discountAmount.equalsIgnoreCase("0.0") || discountAmount.equalsIgnoreCase("0")) {
                        textViewSubTotal.setText("Total: " + getActivity().getResources().getString(R.string.pound_sign) + hotRestaurantDatas.get(0).getSubTotal());
                        textViewDiscountAmount.setVisibility(View.GONE);
                        textViewGrandTotal.setVisibility(View.GONE);

                    } else {
                        textViewDiscountAmount.setVisibility(View.VISIBLE);
                        textViewGrandTotal.setVisibility(View.VISIBLE);
                        textViewSubTotal.setText("Sub Total: " + getActivity().getResources().getString(R.string.pound_sign) + hotRestaurantDatas.get(0).getSubTotal());
                        textViewDiscountAmount.setText("Discount: -" + getActivity().getResources().getString(R.string.pound_sign) + hotRestaurantDatas.get(0).getDiscountAmount());
                        textViewGrandTotal.setText("Grand Total: " + getActivity().getResources().getString(R.string.pound_sign) + hotRestaurantDatas.get(0).getGrandTotal());

                    }

                    if (hotRestaurantDatas.get(0).getDeliveryCharge()!= null) {
                        if(hotRestaurantDatas.get(0).getDeliveryCharge() > 0) {
                            textViewDeliveryCharge.setVisibility(View.VISIBLE);
                            textViewGrandTotal.setVisibility(View.VISIBLE);
                            textViewDeliveryCharge.setText("Delivery Charge: " + getActivity().getResources().getString(R.string.pound_sign) + hotRestaurantDatas.get(0).getDeliveryCharge());
                            textViewSubTotal.setText("Sub Total: " + getActivity().getResources().getString(R.string.pound_sign) + hotRestaurantDatas.get(0).getSubTotal());
                            textViewGrandTotal.setText("Grand Total: " + getActivity().getResources().getString(R.string.pound_sign) + hotRestaurantDatas.get(0).getGrandTotal());
                        } else {
                            textViewDeliveryCharge.setVisibility(View.GONE);
                        }


                    }

                    listViewOrderDetails.addFooterView(footerView);

                    Picasso.Builder builder = new Picasso.Builder(getActivity());
                    OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient(getActivity());
                    builder.downloader(new OkHttp3Downloader(okHttpClient));
                    Picasso picasso = builder.build();
                    picasso.load(hotRestaurantDatas.get(0).getRestaurantLogo().trim()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).noFade().fit().into(imageViewLogo);

                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                        Log.e("Order History Details", ""+e);
                        progressBar.setVisibility(View.GONE);
                    }
                    catch (Exception e1){
                        Log.e("Afetr volley execution",e1.getMessage());
                    }
                }

            }

        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openDialog(String message, String okButton) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_message);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(okButton);

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialogOpened = false;
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialogOpened = false;
            }
        });

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                dialogOpened = true;
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private boolean checkRestaurantOpenStatus()
    {
        String currentDay = new SimpleDateFormat("EEEE").format(new Date());
        int shiftCount = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().size();
        for (int i = 0; i < shiftCount; i++) {
            if (AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getDayName().equalsIgnoreCase(currentDay)) {
                if(AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas() == null || AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().size() == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnReOrder:
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }

                mLastClickTime = SystemClock.elapsedRealtime();

                if("1".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getComingSoon())) {
                    openDialog("This restaurant is coming soon. Ordering is not possible right now.","OK");
                    return;
                }

                if(!checkRestaurantOpenStatus())
                {
                    if(!dialogOpened) openDialog("Restaurant is closed today. Ordering is not possible.","OK");
                    return;
                }

                if(UtilityMethod.isBusinessTimeOver()) {
                    openDialog("Business time is over today. Ordering is not possible right now.","OK");
                    return;
                }

                if(tempHotRestaurantDatas == null || tempHotRestaurantDatas.size() == 0) {
                    openDialog("No item found. Reorder is not possible.", "OK");
                    return;
                }
                ArrayList<DishData> dishDatas = tempHotRestaurantDatas.get(0).getDishDatas();
                if(dishDatas == null || dishDatas.size() == 0) {
                    openDialog("No item found. Reorder is not possible.", "OK");
                    return;
                }
                Intent intent = new Intent(getActivity(), OrderHostActivity.class);
                intent.putExtra(ConstantValues.NAVIGATION_KEY, ConstantValues.FRAGMENT_CATEGORY);
                intent.putExtra("restaurant_id", Integer.toString(AppData.getInstance().getRestaurantInfoData().getRestaurantId()));
                startActivity(intent);
                getActivity().getFragmentManager().beginTransaction().remove(this).commit();
                getActivity().onBackPressed();
                break;
        }
    }

}
