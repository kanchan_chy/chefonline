package com.chefonline.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.adapter.HotRestaurantsAdapter;
import com.chefonline.customview.CustomToast;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.gps.GPSTracker;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.online.MainActivity;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;

public class SearchFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener, AbsListView.OnScrollListener{
    private static int SELECTED_BUTTON = 0;
    private ListView listViewHotRestaurants;

    private ImageButton imgBtnMapMarker;
    private Button imgBtnReservation;
    private Button imgBtnCollection;
    private Button imgBtnDelivery;
    private Button btnSearch;
    private EditText edtTextSearch;
    private View headerLayout;
    private RelativeLayout relativeContainer;

    private DataBaseUtil db;
    private int lastTopValue = 0;
    private long mLastClickTime = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        initView(rootView);
        SELECTED_BUTTON=0;
        setUiItemListener();
        loadData();
        return  rootView;

    }

    public void initView(View rootView) {

        listViewHotRestaurants = (ListView) rootView.findViewById(R.id.listViewHotRestaurants);

        LayoutInflater inflater=LayoutInflater.from(getActivity());
        headerLayout = inflater.inflate(R.layout.header_restaurant_list,null);

        relativeContainer=(RelativeLayout)headerLayout.findViewById(R.id.relativeContainer);
        imgBtnMapMarker = (ImageButton) headerLayout.findViewById(R.id.imgBtnMapMarker);
        imgBtnReservation = (Button) headerLayout.findViewById(R.id.imgBtnReservation);
        imgBtnCollection = (Button) headerLayout.findViewById(R.id.imgBtnCollection);
        imgBtnDelivery = (Button) headerLayout.findViewById(R.id.imgBtnDelivery);

        btnSearch = (Button) headerLayout.findViewById(R.id.btnSearch);
        edtTextSearch = (EditText) headerLayout.findViewById(R.id.edtTextSearch);

    }

    private void setUiItemListener() {
        imgBtnMapMarker.setOnClickListener(this);
        imgBtnReservation.setOnClickListener(this);
        imgBtnCollection.setOnClickListener(this);
        imgBtnDelivery.setOnClickListener(this);

        btnSearch.setOnClickListener(this);
        listViewHotRestaurants.setOnItemClickListener(this);

      //  listViewHotRestaurants.setOnScrollListener(this);

        edtTextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        AppData.getInstance().hideKeyboard(getActivity(), view);
                        searchRestaurant();
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        //searchRestaurant();
                        break;
                }

                return false;
            }
        });

    }

    /***
     * Set adapter to hot listview  **/
    private void loadData() {
        db = new DataBaseUtil(getActivity());
        db.open();
        HotRestaurantsAdapter adapter = new HotRestaurantsAdapter(getActivity(),
                AppData.getInstance().getHotRestaurantDataArrayList(),
                ConstantValues.NO_POLICY_BUTTON_SELECTION, ConstantValues.FEATURED_RESULT);
        listViewHotRestaurants.addHeaderView(headerLayout, null, false);
        listViewHotRestaurants.setAdapter(adapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null && item.getItemId() == android.R.id.home) {
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private boolean validation () {
        if (edtTextSearch.getText().toString().trim().equalsIgnoreCase("")) {
            //new CustomToast(getActivity(), "Please enter a postcode or street or city.", "", false);
            openDialog("Please enter a postcode or street or city.", "OK");
            return false;
        }

        /*if (!UtilityMethod.postcodeValidation(edtTextSearch.getText().toString().trim().toUpperCase())) {
            new CustomToast(getActivity(), "Please enter valid postcode.", "", false);
            return false;
        }*/

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        ConstantValues.SELECTED_BUTTON = 0;
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getActivity().findViewById(R.id.toolbar_title1).setVisibility(View.VISIBLE);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void makeSelection(int itemNo) {
        if (itemNo == ConstantValues.RESERVATION) {
            SELECTED_BUTTON = itemNo;
            imgBtnReservation.setBackgroundResource(R.drawable.left_round_button_white_selected);
            imgBtnCollection.setBackgroundResource(R.drawable.solid_button_white_default);
            imgBtnDelivery.setBackgroundResource(R.drawable.right_round_button_white_default);

        } else if (itemNo == ConstantValues.COLLECTION) {
            SELECTED_BUTTON = itemNo;
            imgBtnReservation.setBackgroundResource(R.drawable.left_round_button_white_default);
            imgBtnCollection.setBackgroundResource(R.drawable.solid_button_white_selected);
            imgBtnDelivery.setBackgroundResource(R.drawable.right_round_button_white_default);

        } else if (itemNo == ConstantValues.DELIVERY) {
            SELECTED_BUTTON = itemNo;
            imgBtnReservation.setBackgroundResource(R.drawable.left_round_button_white_default);
            imgBtnCollection.setBackgroundResource(R.drawable.solid_button_white_default);
            imgBtnDelivery.setBackgroundResource(R.drawable.right_round_button_white_selected);

        }
    }

    private void makeDefaultSelection() {
        SELECTED_BUTTON = 0;
        imgBtnReservation.setBackgroundResource(R.drawable.left_round_button_white_default);
        imgBtnCollection.setBackgroundResource(R.drawable.solid_button_white_default);
        imgBtnDelivery.setBackgroundResource(R.drawable.right_round_button_white_default);
    }

    ProgressDialog progress;
    private void callPostCodeAPI() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        GPSTracker gps = new GPSTracker(getActivity());
        String url = ConstantValues.POST_CODE_API + "lon=" + gps.getLongitude() + "&lat=" + gps.getLatitude();
       //String url = "http://api.postcodes.io/postcodes?lon=-0.175287&lat=51.631891";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progress.dismiss();
                            if (JsonParser.getPostCode(response).equalsIgnoreCase("")) {
                                new CustomToast(getActivity(), "Postcode not found from your current location.", "", false);
                            } else {
                                edtTextSearch.setText(JsonParser.getPostCode(response));
                                edtTextSearch.setSelection(edtTextSearch.getText().length());
                            }

                        } catch (Exception e) {
                            Log.e("After Volley execution",e.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                    new CustomToast(getActivity(), "Something went wrong. Response error.", "", false);
                } catch (Exception e) {
                    Log.e("After Volley execution",e.getMessage());
                }

            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

        progress = new ProgressDialog(getActivity());
        progress.setMessage("Please wait....");
        progress.show();
    }

    private void openDialog(final String restaurant_id, final int position) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_restuarant_override);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                loadCategoryFragment(position);
                AppData.getInstance().setRestaurantInfoData(AppData.getInstance().getHotRestaurantDataArrayList().get(position));
                db.emptyCartData();
                MainActivity.txtViewBubble.setText("" + db.sumCartData());
                dialog.dismiss();
            }

        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        ImageButton btnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private void loadCategoryFragment(int position) {
        android.app.Fragment fragment = null;
        fragment = new TakeawayCategoryFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putInt("is_searched_list", 0);
        args.putString("restaurant_id", Integer.toString(AppData.getInstance().getHotRestaurantDataArrayList().get(position).getRestaurantId()));

        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.add(R.id.content_frame, fragment, "CatFrame");
        ft.addToBackStack(null);
        ft.commit();
    }

    private void loadRestaurantsListFragment() {
        android.app.Fragment fragment = null;
        fragment = new RestaurantsFragment();
        Bundle args = new Bundle();

        args.putString("key", edtTextSearch.getText().toString().trim());
        args.putInt("criteria", SELECTED_BUTTON);

        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.addToBackStack(null);
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.add(R.id.content_frame, fragment, "RESTAURANT_LIST");
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }

        mLastClickTime = SystemClock.elapsedRealtime();

        int position = pos - 1;
        String x = Integer.toString(AppData.getInstance().getHotRestaurantDataArrayList().get(position).getRestaurantId());
        if (db.sumCartData() == 0) {
            loadCategoryFragment(position);
            AppData.getInstance().setRestaurantInfoData(AppData.getInstance().getHotRestaurantDataArrayList().get(position));
            return;
        }

        if (db.checkRestaurant(x) == 1) {
            loadCategoryFragment(position);
            AppData.getInstance().setRestaurantInfoData(AppData.getInstance().getHotRestaurantDataArrayList().get(position));

        } else {
            //Toast.makeText(SearchActivity.this, "Your current cart will be lost while select different restaurant", Toast.LENGTH_SHORT).show();
            openDialog(Integer.toString(AppData.getInstance().getHotRestaurantDataArrayList().get(position).getRestaurantId()), position);

        }

    }

    private void searchRestaurant() {
        if (validation ()) {
            loadRestaurantsListFragment();
        }
    }

    public void showSettingsAlert(){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Location Service Disabled");
        builder.setMessage("Please turn on location services to get best user experience.");

        builder.setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        // on pressing cancel button
        builder.setNegativeButton("IGNORE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

        });

        android.support.v7.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void openDialog(String message, String okButton) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_message);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        imgBtnClose.setVisibility(View.INVISIBLE);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(okButton);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBtnMapMarker:
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }

                final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                if ( !manager.isProviderEnabled(LocationManager.GPS_PROVIDER) ) {
                    showSettingsAlert();
                    return;
                }

                callPostCodeAPI();
                break;
            case R.id.imgBtnReservation:
                if (SELECTED_BUTTON == ConstantValues.RESERVATION) {
                    makeDefaultSelection();
                } else {
                    makeSelection(1);
                }

                break;
            case R.id.imgBtnCollection:
                if (SELECTED_BUTTON == ConstantValues.COLLECTION) {
                    makeDefaultSelection();
                } else {
                    makeSelection(2);
                }

                break;
            case R.id.imgBtnDelivery:
                if (SELECTED_BUTTON == ConstantValues.DELIVERY) {
                    makeDefaultSelection();
                } else {
                    makeSelection(3);
                }

                break;
            case R.id.btnSearch:
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                AppData.getInstance().hideKeyboard(getActivity(), v);
                searchRestaurant();
                break;

        }
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int i, int i1, int i2) {
      /*  Rect rect = new Rect();
        relativeContainer.getLocalVisibleRect(rect);
        if (lastTopValue != rect.top) {
            lastTopValue = rect.top;
            relativeContainer.setY((float) (rect.top / 2.0));
        }  */

    }
}

