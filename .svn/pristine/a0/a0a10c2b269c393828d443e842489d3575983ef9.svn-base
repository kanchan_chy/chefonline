package com.chefonline.online;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.datamodel.HotRestaurantData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.gps.GPSTracker;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UtilityMethod;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends AppCompatActivity {
    PreferenceUtil preferenceUtil;
    ArrayList<HotRestaurantData> hotRestaurantDataArrayList;
    GPSTracker gps;
    private long mLastClickTime = 0;
    boolean doInitialWork;
    String playStoreVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        doInitialWork = true;
        gps = new GPSTracker(SplashActivity.this);

        Picasso.Builder picassoBuilder = new Picasso.Builder(this);
        Picasso picasso = picassoBuilder.build();
        try {
            Picasso.setSingletonInstance(picasso);
        } catch (IllegalStateException ignored) {
            // Picasso instance was already set
            // cannot set it after Picasso.with(Context) was already in use
        }

    }

    private void openDialog(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_restuarant_override);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        //imgBtnClose.setVisibility(View.INVISIBLE);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(okButton);

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                onBackPressed();
            }
        });

        // if button is clicked, close the custom dialog
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                initialWork();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }

        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setText(cancelButton);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }

        });

        dialog.show();
    }

    private void callPostCodeAPI() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = ConstantValues.POST_CODE_API + "lon=" + gps.getLongitude() + "&lat=" + gps.getLatitude();
        //String url = "http://api.postcodes.io/postcodes?lon=-0.175287&lat=51.631891";
        // Request a string response from the provided URL.

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            getHotRestaurant(JsonParser.getPostCode(response));

                        } catch (Exception e) {
                            showMessageDialog("Something went wrong. Please re-open the app.");
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try{
                  //  new CustomToast(SplashActivity.this, "" + error.getMessage().toString() + error.getMessage(),"" , false);
                    showMessageDialog("Something went wrong. Please re-open the app.");
                } catch (Exception e) {

                }

            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    //Get Restaurant list by current location address
    private void getHotRestaurant(final String postCode) {
        RequestQueue queue = Volley.newRequestQueue(SplashActivity.this);
        String url = ConstantValues.BASE_API_URL;
        StringRequest myReq = new StringRequest(Request.Method.POST, url, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("funId", "5");
                params.put("postcode", postCode.trim());
                //params.put("postcode", "SE1 2TH");
                params.put("lat", Double.toString(gps.getLatitude()));
                params.put("lng", Double.toString(gps.getLongitude()));
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    //new CustomToast(SplashActivity.this, "Server Response Error Splash","" , false);
                    showMessageDialog("Something went wrong. Please re-open the app.");
                } catch (Exception e){
                    Log.e("After volley execution",e.getMessage());
                }
            }
        };
    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i("resp", "" + response);
                    LatLng latLngCurrent = new LatLng(gps.getLatitude(), gps.getLongitude());

                    /*Geocoder gcd = new Geocoder(SplashActivity.this, Locale.getDefault());
                    List<Address> addresses = gcd.getFromLocation(gps.getLatitude(), gps.getLongitude(), 1);

                    if (addresses.size() > 0){
                        String countryName = addresses.get(0).getCountryName();
                        Log.i("Country Name", "" + countryName);
                        if("United Kingdom".equalsIgnoreCase(countryName) || "UK".equalsIgnoreCase(countryName) || "GB".equalsIgnoreCase(countryName)) {
                            ConstantValues.COUNTRY_FLAG = "1";
                        } else {
                            ConstantValues.COUNTRY_FLAG = "2";
                        }
                    }*/

                    hotRestaurantDataArrayList = new ArrayList<>();
                    hotRestaurantDataArrayList =  JsonParser.getHotRestaurant(response, latLngCurrent);
                    AppData.getInstance().setHotRestaurantDataArrayList(hotRestaurantDataArrayList);
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {
                    try {
                        Log.e("Exception", e.getMessage());
                        e.printStackTrace();
                        showMessageDialog("Something went wrong. Please re-open the app.");
                    }
                    catch (Exception e1){
                        Log.e("After volley execution", e1.getMessage());
                    }
                }

            }

        };
    }


    //Get Playstore version
    private void callPlaystoreVersionAPI() {
        RequestQueue queue = Volley.newRequestQueue(SplashActivity.this);
        String url = ConstantValues.BASE_API_URL;
        StringRequest myReq = new StringRequest(Request.Method.POST, url, createPlayVersionSuccessListener(), createPlayVersionErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("funId", "67");
                params.put("platform_id", "2");
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

    }

    private Response.ErrorListener createPlayVersionErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    setPlayStoreVersion("-1");
                }catch (Exception e) {
                }
            }
        };
    }

    private Response.Listener<String> createPlayVersionSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("PlayVersion", response);

                    if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        setPlayStoreVersion(jsonObject.getString("version"));
                        //setPlayStoreVersion("1.2.2");
                    } else {
                        setPlayStoreVersion("-1");
                    }

                } catch (Exception e) {
                    try {
                        setPlayStoreVersion("-1");
                    }catch (Exception e1) {
                    }
                }
            }

        };
    }


    @Override
    protected void onResume() {
        if(doInitialWork) {
            doInitialWork = false;
            initialWork();
        }
        super.onResume();
    }

    private void showMessageDialog(String message) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_message);
        dialog.setCancelable(false);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }

        });

        dialog.show();
    }

    public void showSettingsAlert(){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);

        // Setting Dialog Title
        builder.setTitle("Location Service Disabled");

        // Setting Dialog Message
        builder.setMessage("Please turn on location services to get best user experience.");

        // On pressing Settings button
        builder.setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                doInitialWork = true;
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        // on pressing cancel button
        builder.setNegativeButton("IGNORE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                initialWorkWithoutGps();

            }

        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                initialWorkWithoutGps();
            }
        });

        // Showing Alert Message
        android.support.v7.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    private void showUpdateDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_restuarant_override);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText("An updated version of this application is available in Google Play Store. Please update this application.");
        final Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText("UPDATE");

        // if button is clicked, close the custom dialog
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Uri uri = Uri.parse("market://details?id=" + getPackageName());
                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(myAppLinkToMarket);
                    finish();
                } catch (ActivityNotFoundException e) {
                    fetchRequiredData();
                }
            }

        });

        final Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setText("CANCEL");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }

        });

        dialog.show();
    }


    public void initialWork() {
        if(!UtilityMethod.isConnectedToInternet(getApplicationContext())) {
            openDialog("Please check your internet connection.", "RETRY", "CANCEL");
            return;
        }

        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        if ( !manager.isProviderEnabled(LocationManager.GPS_PROVIDER) ) {
            showSettingsAlert();
            return;
        }

        getPlayStoreVersion();
    }

    public void initialWorkWithoutGps() {
        if(!UtilityMethod.isConnectedToInternet(getApplicationContext())) {
            openDialog("Please check your internet connection.", "RETRY", "CANCEL");
            return;
        }

        getPlayStoreVersion();
    }


    private String getVersionName()
    {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }


    public void setPlayStoreVersion(String version) {
        playStoreVersion = version;
        if(playStoreVersion == null) {
            fetchRequiredData();
        } else {
            checkUpdate();
        }
    }


    private void getPlayStoreVersion() {
        if(playStoreVersion == null) {
            callPlaystoreVersionAPI();
        } else {
            checkUpdate();
        }
    }


    private void checkUpdate() {
        Log.e("Version", getVersionName());
        //fetchRequiredData();
        if(playStoreVersion.equalsIgnoreCase(getVersionName()) || playStoreVersion.equalsIgnoreCase("-1")) {
            fetchRequiredData();
        } else {
            showUpdateDialog();
        }
    }


    private void fetchRequiredData() {
        preferenceUtil = new PreferenceUtil(SplashActivity.this);
        DataBaseUtil db = new DataBaseUtil(SplashActivity.this);
        db.open();
        db.emptyCartData();

        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    callPostCodeAPI();
                }
            }
        };
        timer.start();
    }




}
