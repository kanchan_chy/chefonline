package com.chefonline.presenter;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.chefonline.customview.CustomBottomToast;
import com.chefonline.datamodel.LocationData;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.modelinterface.PlaceOrderView;
import com.chefonline.modelinterface.RequestCompleteInterface;
import com.chefonline.modelinterface.VolleyApiInterface;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.volleyapicalls.NetPayChargeApiCallback;
import com.chefonline.volleyapicalls.PlaceOrderNewApiCallback;
import com.chefonline.volleyapicalls.UpdatePaymentStatusApiCallback;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by masum on 29/11/2015.
 */
public class PlaceOrderPresenter {
    PlaceOrderView restaurantView;
    public PlaceOrderPresenter(PlaceOrderView restaurantView) {
        this.restaurantView = restaurantView;
    }

    public void getRealIPAddress(final Context context, String f) {
        restaurantView.setLoadingVisible();
        new AsyncTaskIPTracking(context, f, new RequestCompleteInterface() {
            @Override
            public void onCompleted(String result) {
                try {
                    restaurantView.setLoadingHide();
                    restaurantView.visiblePayPalButton();
                    LocationData locationData = JsonParser.getRealIP(result);
                    if(locationData != null) {
                        restaurantView.setLocationData(locationData);
                    }
                }
                catch (Exception e){

                }
            }

            @Override
            public void onErrorCompleted(String result) {
                try {
                    restaurantView.setLoadingHide();
                }
                catch (Exception e){

                }
            }
        });
    }


    public void onClickPayCashOrPaypal(final Context context, final String paymentOption) {
        restaurantView.setLoadingVisible();
        new PlaceOrderNewApiCallback(context, paymentOption, new VolleyApiInterface() {

            @Override
            public void onRequestSuccess(String response) {
                try {
                    Log.e("placeorder_cash", response);
                    AppData.getInstance().getOrderPlaceData().setVerificationCode("0");
                    restaurantView.setLoadingHide();
                    JSONObject jsonObject = new JSONObject(response);
                    if("sms_sent".equalsIgnoreCase(jsonObject.getString("status"))) {
                        if (jsonObject.getBoolean("is_varification_required")) {
                            AppData.getInstance().getOrderPlaceData().setIsVerificationCodeRequired("1");
                        } else {
                            AppData.getInstance().getOrderPlaceData().setIsVerificationCodeRequired("0");
                        }
                        if (jsonObject.getBoolean("is_special_message_required")) {
                            AppData.getInstance().getOrderPlaceData().setIsSpecialMessageRequired("1");
                        } else {
                            AppData.getInstance().getOrderPlaceData().setIsSpecialMessageRequired("0");
                        }
                        if(paymentOption.equalsIgnoreCase(ConstantValues.PAYMENT_PAYPAL)) {
                            restaurantView.verifyCode(jsonObject.getString("code"), true);
                        } else {
                            restaurantView.verifyCode(jsonObject.getString("code"), false);
                        }
                    } else if("Success".equalsIgnoreCase(jsonObject.getString("status"))){
                        if(paymentOption.equalsIgnoreCase(ConstantValues.PAYMENT_PAYPAL)) {
                            restaurantView.orderCompleted(jsonObject.getString("order_ID"), jsonObject.getString("transaction_id"), true);
                        } else {
                            restaurantView.orderCompleted(jsonObject.getString("order_ID"), jsonObject.getString("transaction_id"), false);
                        }
                    } else if("Failed".equalsIgnoreCase(jsonObject.getString("status"))){
                        AppData.getInstance().getOrderPlaceData().setCardFee("0");
                        AppData.getInstance().getOrderPlaceData().setDeliveryCharge("0");
                        AppData.getInstance().getOrderPlaceData().setIsSpecialMessageRequired("0");
                        AppData.getInstance().getOrderPlaceData().setIsVerificationCodeRequired("0");
                        AppData.getInstance().getOrderPlaceData().setUserAddressExtId("0");
                        if(paymentOption.equalsIgnoreCase(ConstantValues.PAYMENT_PAYPAL)) {
                            restaurantView.onVerificationFailed(true);
                        } else {
                            restaurantView.onVerificationFailed(false);
                        }
                    } else {
                        if(paymentOption.equalsIgnoreCase(ConstantValues.PAYMENT_PAYPAL)) {
                            restaurantView.retryPayment(true);
                        } else {
                            restaurantView.retryPayment(false);
                        }
                    }
                } catch (Exception e) {
                    Log.e("success_error", "" + e);
                    try {
                        restaurantView.setLoadingHide();
                        if(paymentOption.equalsIgnoreCase(ConstantValues.PAYMENT_PAYPAL)) {
                            restaurantView.retryPayment(true);
                        } else {
                            restaurantView.retryPayment(false);
                        }
                    }catch (Exception e1) {
                    }
                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    Log.e("Volley_error", "" + error);
                    restaurantView.setLoadingHide();
                    if(paymentOption.equalsIgnoreCase(ConstantValues.PAYMENT_PAYPAL)) {
                        restaurantView.retryPayment(true);
                    } else {
                        restaurantView.retryPayment(false);
                    }
                }catch (Exception e) {
                    Log.e("error", "" + e);
                }
            }

        });

    }




    public void updatePaymentStatus(final Context context, final String orderId, final String paymentStatus, final String transactionId) {
        restaurantView.setLoadingVisible();
        new UpdatePaymentStatusApiCallback(context, orderId, paymentStatus, transactionId, new VolleyApiInterface() {

            @Override
            public void onRequestSuccess(String response) {
                try {
                    Log.e("update_status", response);
                    restaurantView.setLoadingHide();
                    JSONObject jsonObject = new JSONObject(response);
                    if("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        restaurantView.paymentStatusUpdated(true);
                    } else{
                        restaurantView.paymentStatusUpdated(false);
                    }
                } catch (Exception e) {
                    try {
                        restaurantView.setLoadingHide();
                        restaurantView.showToast("Something went wrong", "", false);
                    }catch (Exception e1) {
                    }
                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    restaurantView.setLoadingHide();
                    restaurantView.showToast("Something went wrong", "", false);
                }catch (Exception e) {
                }
            }

        });

    }




    public void onClickGetNetPayCharge(final Context context, final String restId, final String paymentSettingsId, final String amount) {
        restaurantView.setLoadingVisible();
        new NetPayChargeApiCallback(context, restId, paymentSettingsId, amount, new VolleyApiInterface() {

            @Override
            public void onRequestSuccess(String response) {
                try {
                    Log.e("netpay_charge_response", response);
                    restaurantView.setLoadingHide();
                    boolean flag = false;
                    JSONObject jsonObject = new JSONObject(response);
                    if("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        JSONArray jMethods = jsonObject.getJSONArray("payment_methods");
                        if(jMethods != null && jMethods.length() > 0) {
                            for (int i = 0; i < jMethods.length(); i++) {
                                JSONObject jObjMethod = jMethods.getJSONObject(i);
                                if(jObjMethod.getString("payment_settings_id").equalsIgnoreCase(paymentSettingsId)) {
                                    AppData.getInstance().getRestaurantInfoData().setNetpayChargeType(jObjMethod.getJSONObject("charge").getString("charge_type"));
                                    AppData.getInstance().getRestaurantInfoData().setNetpayCustomerCharge(jObjMethod.getJSONObject("charge").getString("customer_charge"));
                                    flag = true;
                                    break;
                                }
                            }
                        }
                    }
                    if(flag) {
                        restaurantView.isNetpayChargeFound(true);
                    } else {
                        restaurantView.showToast("Sorry, Netpay customer charge is not found", "", false);
                    }
                } catch (Exception e) {
                    try {
                        restaurantView.setLoadingHide();
                        restaurantView.showToast("Something went wrong", "", false);
                    }catch (Exception e1) {
                    }
                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    restaurantView.setLoadingHide();
                    restaurantView.showToast("Something went wrong", "", false);
                }catch (Exception e) {
                }
            }

        });

    }



}
