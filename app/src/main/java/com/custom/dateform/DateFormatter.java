package com.custom.dateform;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by masum on 06/05/2015.
 */
public class DateFormatter {
    public String YYYY_MM_DD = "yyyy-MM-dd";
    public String YYYY_MMM_DD_EEEE = "yyyy-MMM-dd, EEEE";
    public String DD_MMM_YYYY = "DD-MMM-YYYY";
    public String DD_MM_YYYY = "dd-MM-yyyy";

    public static String DD_MM_YYYY2 = "dd/MM/yyyy";

    public static String MMM_dd_YYYY = "MMM d, yyyy"; //july 5, 2015

    public String yyyy_mm_dd_hh_mm_ss = "yyyy-mm-dd hh:mm:ss";
    public static String EEE_MMM_dd_yy = "EEE, MMM d, ''yyyy";
    public static String EEE_dd_MMM_yy = "EEEE, d MMM yyyy";
    public static String EE_dd_MMM_yy = "EEE, d MMM yyyy"; // wed, 5 july 2014

    public String doFormat(String getDate, String fromDate, String toDate) throws ParseException {
        String date_s = getDate;
        // *** note that it's "yyyy-MM-dd hh:mm:ss" not "yyyy-mm-dd hh:mm:ss"
        SimpleDateFormat dt = new SimpleDateFormat(fromDate);
        Date date = dt.parse(date_s);
        // *** same for the format String below
        SimpleDateFormat dt1 = new SimpleDateFormat(toDate);
        System.out.println(dt1.format(date));

        return dt1.format(date);
    }
}
