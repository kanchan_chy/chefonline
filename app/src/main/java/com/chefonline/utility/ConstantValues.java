package com.chefonline.utility;

import android.widget.Toast;

import com.chefonline.datamodel.DishData;
import java.util.ArrayList;

/**
 * Created by masum on 04/04/2015.
 */
public class ConstantValues {
    //public static String BASE_API_URL = "http://testing.smartrestaurantsolutions.com/mobileapi-v1/Tigger.php";
    //public static String BASE_API_URL = "http://chefonline.co.uk/mobileapi-v2/Tigger.php";
    //public static String BASE_API_URL = "http://smartrestaurantsolutions.com/mobileapi-v2/Tigger.php"; //Main

  //  public static String BASE_API_URL = "http://smartrestaurantsolutions.com/mobileapi-v2/v2/Tigger.php"; //Main new

    public static String BASE_API_URL = "http://smartrestaurantsolutions.com/mobileapi-test/Tigger.php"; //test Main  *****
  //  public static String BASE_API_URL = "http://srsapi.smartrestaurantsolutions.com/chefonline_mobile/Tigger.php"; //test Main new
    //public static String BASE_API_URL = "http://172.16.1.38/api/mobileapi-v2/Tigger.php"; //test local
    //public static String BASE_API_URL = "http://10.0.0.64/api/mobileapi-v2/Tigger.php"; //test local last
    //public static String BASE_API_URL = "http://172.16.1.226:8080/mobileapi-v2/Tigger.php"; //Raji
 //   public static String BASE_API_URL = "http://10.0.0.144/mobileapi-v2/Tigger.php"; //Local Host Raji Vai

    public static String GOOGLE_API_URL = "https://maps.googleapis.com/maps/api/geocode/json?";
    public static String POST_CODE_API = "http://api.postcodes.io/postcodes?";

  //  public static String NETPAY_API_URL = "http://10.0.0.144:8080/netpay_api/process.php";   // Local
    public static String NETPAY_API_URL = "http://smartrestaurantsolutions.com/mobileapi-test/netpay_api/process.php";   // Test

    public static String API_KEY = "AIzaSyD13mQmG8owTc1NgbSbk5Fedvnm7I7v6GY";
    public static String IP_CHECK_URL= "https://api.ipify.org?format=json";

    public static String COUNTRY_FLAG = "2";
    public static int FRAGMENT_CART = 1;
    public static int FRAGMENT_CATEGORY = 2;
    public static int FRAGMENT_SETTINGS = 3;
    public static int FRAGMENT_SEARCH = 4;
    public static int FRAGMENT_OFFERS = 5;
    public static int NAVIGATION_TO = 0;
    public static int ACTIVITY_RESERVATION = 2;
    public static int ACTIVITY_SETTINGS = 3;
    public static boolean IS_CART_ADDED = false;
    public static String NAVIGATION_KEY = "navigation_to";

    public static int SERVICE_DELIVERY = 1;
    public static int SERVICE_COLLECTION = 2;

    public static String PAYMENT_CASH = "0";
    public static String PAYMENT_PAYPAL = "1";
    public static String PAYMENT_PAY_OVER_PHONE = "2";
    public static String PAYMENT_NETPAY = "11";
    public static boolean IS_REORDER = false;

    public static int RESERVATION = 1;
    public static int COLLECTION = 2;
    public static int DELIVERY = 3;
    public static int NO_POLICY_BUTTON_SELECTION = 0;
    public static int SELECTED_BUTTON = 0;

    public static int FEATURED_RESULT = 0;
    public static int SEARCHING_RESULT = 1;

    public static ArrayList<DishData> DISH_DATA = new ArrayList<>();

    public static ArrayList<Toast> DISH_MENU_TOASTS = new ArrayList<>();

    public static final String[] CUISINE_LIST = new String[] {"FEEL LIKE HAVING",
            "CHINESE",
            "FRIED CHICKEN & GRILL HOUSE",
            "INDIAN",
            "JAPANESE - SUSHI",
            "MEDITERRANEAN",
            "NEPALESE",
            "SUSHI, JAPANESE",
            "THAI"
    };

    public static String CHARGE_TYPE_ACTUAL = "Actual";
    public static String CHARGE_TYPE_PERCENT = "Percent";
    public static int PAYMENT_TYPE_MASTER_CARD = 0;
    public static int PAYMENT_TYPE_VISA = 1;
    public static int PAYMENT_TYPE_AMERICAN_EXPRESS = 2;
    public static int PAYMENT_TYPE_MAESTRO = 3;
    public static int PAYMENT_TYPE_MASTER_CARD_DEBIT = 4;
    public static int PAYMENT_TYPE_VISA_DEBIT = 5;
    public static int PAYMENT_TYPE_VISA_ELECTRON = 6;
    public static int PAYMENT_TYPE_DINERS_CLUB_INTERNATIONAL = 7;

}
