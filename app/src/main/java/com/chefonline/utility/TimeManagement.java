package com.chefonline.utility;

import android.util.Log;

import com.chefonline.datamodel.CollectionDeliveryData;
import com.chefonline.datamodel.HotRestaurantData;
import com.chefonline.datamodel.OpenEndTimeData;
import com.chefonline.datamodel.ScheduleData;
import com.chefonline.pattern.AppData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by masum on 05/05/2015.
 */
public class TimeManagement {
    private static String TAG = "TimeManagement";

    public boolean checkRestaurantOpenStatus(HotRestaurantData hotRestaurantData)
    {
        String currentDay = new SimpleDateFormat("EEEE").format(new Date());
        int shiftCount = hotRestaurantData.getScheduleDatas().size();
        for (int i = 0; i < shiftCount; i++) {
            if (hotRestaurantData.getScheduleDatas().get(i).getDayName().equalsIgnoreCase(currentDay)) {
                if(hotRestaurantData.getScheduleDatas().get(i).getReservationDatas() != null && hotRestaurantData.getScheduleDatas().get(i).getReservationDatas().size() > 0) {
                    return true;
                } else {
                    if(hotRestaurantData.getScheduleDatas().get(i).getOpenEndTimeDatas() == null || hotRestaurantData.getScheduleDatas().get(i).getOpenEndTimeDatas().size() == 0) {
                        return false;
                    } else {
                        for (int j = 0; j < hotRestaurantData.getScheduleDatas().get(i).getOpenEndTimeDatas().size(); j++) {
                            String collectionTime = hotRestaurantData.getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getCollection();
                            String deliveryTime = hotRestaurantData.getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getDelivery();
                            if((collectionTime != null && !"".equalsIgnoreCase(collectionTime)) || (deliveryTime != null && !"".equalsIgnoreCase(deliveryTime))) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }


    public boolean checkOrderingOpenStatus()
    {
        String currentDay = new SimpleDateFormat("EEEE").format(new Date());
        int shiftCount = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().size();
        for (int i = 0; i < shiftCount; i++) {
            if (AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getDayName().equalsIgnoreCase(currentDay)) {
                if(AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas() == null || AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().size() == 0) {
                    return false;
                } else {
                    for (int j = 0; j < AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().size(); j++) {
                        String collectionTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getCollection();
                        String deliveryTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getDelivery();
                        if((collectionTime != null && !"".equalsIgnoreCase(collectionTime)) || (deliveryTime != null && !"".equalsIgnoreCase(deliveryTime))) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }


    public String getTimeFrom12To24(String time12) {
        if(time12.contains(" ")) {
            try {
                String hour = "", minute = "", a = "";
                int flag = 0;
                for (int i = 0; i < time12.length(); i++) {
                    if(time12.charAt(i) == ':' || time12.charAt(i) == ' ') {
                        flag ++;
                        continue;
                    } else {
                        if(flag == 0) {
                            hour = hour + time12.charAt(i);
                        } else if(flag == 1) {
                            minute = minute + time12.charAt(i);
                        } else {
                            a = a + time12.charAt(i);
                        }
                    }
                }
                int hh = Integer.valueOf(hour);
                if(hh == 12) {
                    hh = 0;
                }
                if(a.equalsIgnoreCase("PM") || a.equalsIgnoreCase("P.M.")) {
                    hh = hh + 12;
                }
                hour = String.valueOf(hh);
                if(hour.length() == 1) {
                    hour = "0" + hour;
                }
                String time24 = hour + ":" + minute;
                return time24;
            } catch (Exception e) {
                return time12;
            }
        } else {
            return time12;
        }
    }

    public String getTimeFrom24To12(String time24) {
        if(!time24.contains(" ")) {
            try {
                String hour = "", minute = "", a = "";
                int flag = 0;
                for (int i = 0; i < time24.length(); i++) {
                    if(time24.charAt(i) == ':') {
                        flag ++;
                        continue;
                    } else {
                        if(flag == 0) {
                            hour = hour + time24.charAt(i);
                        } else {
                            minute = minute + time24.charAt(i);
                        }
                    }
                }
                int hh = Integer.valueOf(hour);
                if(hh == 0) {
                    hh = 24;
                }
                if(hh > 12 && hh < 24) {
                    hh = hh - 12;
                    a = "PM";
                } else {
                    if(hh == 12) {
                        a = "PM";
                    } else if(hh == 24) {
                        hh = 12;
                        a = "AM";
                    } else {
                        a = "AM";
                    }
                }
                hour = String.valueOf(hh);
                if(hour.length() == 1) {
                    hour = "0" + hour;
                }
                String time12 = hour + ":" + minute + " " + a;
                return time12;
            } catch (Exception e) {
                return time24;
            }
        } else {
            return time24;
        }
    }


    public static String setEstimatedTime(int extraTime) {
        String newTimeMinus = null;
        //SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss a");
        // SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
        String currentDateTime = sdf24.format(new Date());
        //  Log.i(TAG, "CurrentTIme*** " + currentDateTime);

        String my_time = currentDateTime;
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss a");
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a");
        try {
            Date startTime = sdf24.parse(my_time);
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(startTime);
            cal1.add(Calendar.MINUTE, extraTime);
            newTimeMinus = sdf24.format(cal1.getTime());
            TimeManagement timeManagement = new TimeManagement();
            newTimeMinus = timeManagement.getTimeFrom24To12(newTimeMinus);
        } catch (Exception e) {
            newTimeMinus = "";
        }

        return newTimeMinus;
    }


    public static boolean isUserMiddleOnShift(String getFirstShiftEndTime, String  getSecondShiftStartTime) {
        TimeManagement timeManagement = new TimeManagement();
        String start_Time = timeManagement.getTimeFrom12To24(getFirstShiftEndTime);
        String end_time = timeManagement.getTimeFrom12To24(getSecondShiftStartTime);

       // SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
        String currentUserTime = sdf24.format(new Date());
        Log.i(TAG, "isUserMiddleOnShift CurrentTIme*** " + currentUserTime);

        String my_time = currentUserTime;

      //  SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        try {
            Date startTime = sdf24.parse(start_Time);
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(startTime);
            cal1.add(Calendar.MINUTE, -1);
            String newTimeMinus = sdf24.format(cal1.getTime());

            Date startTimeNew = sdf24.parse(newTimeMinus);
            Date endTime = sdf24.parse(end_time);

            Date userTime = sdf24.parse(my_time);

            if (userTime.after(startTimeNew) && userTime.before(endTime)) {
                //System.out.println(" Yes");
                return true;
            }

        } catch (ParseException ex) {
            System.out.println("Exception " + ex);
        }

        return false;
    }


    public static boolean isShiftTimeRemaining(String getStartTime, String getEndTime, String lastTimeStr, String policyTime) {
        TimeManagement timeManagement = new TimeManagement();
        String start_Time = timeManagement.getTimeFrom12To24(getStartTime);
        String end_time = timeManagement.getTimeFrom12To24(getEndTime);
        int lastTime = 0;
        if(lastTimeStr != null && !lastTimeStr.equalsIgnoreCase("")) {
            lastTime = Integer.valueOf(lastTimeStr);
        }

        if(policyTime != null && !"".equalsIgnoreCase(policyTime)) {
            int tempTime = Integer.valueOf(policyTime);
            lastTime += tempTime;
        }

      //  SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
        String currentDateAndTime = sdf24.format(new Date());
        Log.i(TAG, "CurrentTIme*** " + currentDateAndTime);

        String my_time = currentDateAndTime;

      //  SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");

        try {
            Date startTime = sdf24.parse(start_Time);
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(startTime);
            cal1.add(Calendar.MINUTE, -1);
            String newTimeMinus = sdf24.format(cal1.getTime());

            Date startTimeNew = sdf24.parse(newTimeMinus);
            Date endTime = sdf24.parse(end_time);

            lastTime = 0 - lastTime;
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(endTime);
            cal2.add(Calendar.MINUTE, lastTime);
            String endTimeNewStr = sdf24.format(cal2.getTime());
            Date endTimeNew = sdf24.parse(endTimeNewStr);

            Date myTime = sdf24.parse(my_time);

            if (myTime.after(startTimeNew) && (myTime.before(endTimeNew) || myTime.equals(endTimeNew))) {
                return true;
            }

        } catch (ParseException ex) {
            System.out.println("Exception " + ex);
        }

        return false;
    }


    public static boolean isShiftTimeOver(String getEndTime, String lastTimeStr, String policyTime) {
        TimeManagement timeManagement = new TimeManagement();
        String end_time = timeManagement.getTimeFrom12To24(getEndTime);
        int lastTime = 0;
        if(lastTimeStr != null && !lastTimeStr.equalsIgnoreCase("")) {
            lastTime = Integer.valueOf(lastTimeStr);
        }

        if(policyTime != null && !"".equalsIgnoreCase(policyTime)) {
            int tempTime = Integer.valueOf(policyTime);
            lastTime += tempTime;
        }

        //  SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
        String currentDateAndTime = sdf24.format(new Date());
        Log.i(TAG, "CurrentTIme*** " + currentDateAndTime);

        String my_time = currentDateAndTime;

        //  SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");

        try {
            Date endTime = sdf24.parse(end_time);

            lastTime = 0 - lastTime;
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(endTime);
            cal2.add(Calendar.MINUTE, lastTime);
            String endTimeNewStr = sdf24.format(cal2.getTime());
            Date endTimeNew = sdf24.parse(endTimeNewStr);

            Date myTime = sdf24.parse(my_time);

            if (myTime.before(endTimeNew) || myTime.equals(endTimeNew)) {
                return false;
            }

        } catch (ParseException ex) {
            System.out.println("Exception " + ex);
        }

        return true;
    }


    public static boolean isInMiddleTime(String getStartTime, String getEndTime) {
        TimeManagement timeManagement = new TimeManagement();
        String start_Time = timeManagement.getTimeFrom12To24(getStartTime);
        String end_time = timeManagement.getTimeFrom12To24(getEndTime);

        SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
       // SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
       // String currentDateAndTime = sdf.format(new Date());
      //  Log.i(TAG, "CurrentTIme*** " + currentDateAndTime);

        String currentDateAndTime = sdf24.format(new Date());
        String my_time = currentDateAndTime;

      //  SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");

        try {
            Date startTime = sdf24.parse(start_Time);
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(startTime);
            cal1.add(Calendar.MINUTE, -1);
            String newTimeMinus = sdf24.format(cal1.getTime());

            Date startTimeNew = sdf24.parse(newTimeMinus);
            Date endTime = sdf24.parse(end_time);

            Date myTime = sdf24.parse(my_time);

            if ((myTime.after(startTimeNew) || myTime.equals(startTimeNew)) && myTime.before(endTime)) {
                return true;
            }

        } catch (ParseException ex) {
            System.out.println("Exception " + ex);
        }

        return false;
    }


    public static boolean isBeforeStartingShift(String getStartTime) {
        TimeManagement timeManagement = new TimeManagement();
        String start_time = timeManagement.getTimeFrom12To24(getStartTime);
        SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");

        String currentDateAndTime = sdf24.format(new Date());
        String my_time = currentDateAndTime;

        try {
            Date startTime = sdf24.parse(start_time);

            Date myTime = sdf24.parse(my_time);

            if (myTime.before(startTime)) {
                return true;
            }

        } catch (ParseException ex) {
            System.out.println("Exception " + ex);
        }

        return false;
    }


    public boolean isOrderingTimeCorrect(String selectedTime12, int selectedPolicy) {
        TimeManagement timeManagement = new TimeManagement();
        int extraDay = 0;
        String currentDay = new SimpleDateFormat("EEEE").format(new Date());
        int dayCount = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().size();
        for(int i = 0; i <dayCount; i++) {
            if (AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getDayName().equalsIgnoreCase(currentDay)) {
                int timeListLength = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().size();
                try {
                    //  SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
                    SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
                    String selectedTime24 = timeManagement.getTimeFrom12To24(selectedTime12);
                    // String currentTimeStr = sdf24.format(new Date());
                    Date selectedTime = sdf24.parse(selectedTime24);
                    for(int j = 0; j < timeListLength; j++){
                        String policyTime = "";
                        String lastSubmitTime = "";
                        if(selectedPolicy == ConstantValues.SERVICE_COLLECTION) {
                            policyTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getCollection();
                            lastSubmitTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getLastCollectionSubmission();
                        } else {
                            policyTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getDelivery();
                            lastSubmitTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getLastDeliverySubmission();
                        }

                        if(policyTime == null || "".equalsIgnoreCase(policyTime)) {
                            continue;
                        }
                        String openingTimeStr1 = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getOpeningTime();
                        String closingTimeStr1 = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getEndTime();
                        String openingTimeStr = timeManagement.getTimeFrom12To24(openingTimeStr1);
                        String closingTimeStr = timeManagement.getTimeFrom12To24(closingTimeStr1);
                        Date openingTime = sdf24.parse(openingTimeStr);
                        Date closingTime = sdf24.parse(closingTimeStr);
                        if(openingTime.after(closingTime)) {
                            extraDay = 1;
                        }
                        int lastTime = 0;
                        if(lastSubmitTime != null && !lastSubmitTime.equalsIgnoreCase("")) {
                            lastTime = Integer.valueOf(lastSubmitTime);
                        }
                        lastTime = 0 - lastTime;
                        Calendar calendar = Calendar.getInstance(Locale.getDefault());
                        calendar.setTime(closingTime);
                        calendar.add(Calendar.DATE, extraDay);
                        calendar.add(Calendar.MINUTE, lastTime);
                        String lastTimeStr = sdf24.format(calendar.getTime());
                        Date lastTimeDate = sdf24.parse(lastTimeStr);
                        if((selectedTime.after(openingTime) || selectedTime.equals(openingTime)) && (selectedTime.before(lastTimeDate) || selectedTime.equals(lastTimeDate))) {
                            String currentTimeStr = sdf24.format(new Date());
                            Date currentTime = sdf24.parse(currentTimeStr);
                            Calendar calCurrent = Calendar.getInstance(Locale.getDefault());
                            calCurrent.setTime(currentTime);
                            calCurrent.add(Calendar.MINUTE, Integer.valueOf(policyTime));
                            String currentNewTimeStr = sdf24.format(calCurrent.getTime());
                            Date currentNewTime = sdf24.parse(currentNewTimeStr);
                            //  if(currentNewTime.before(selectedTime) || currentNewTime.equals(selectedTime)) {
                            if(currentNewTime.before(lastTimeDate) || currentNewTime.equals(lastTimeDate)) {
                                return true;
                            }
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
        return false;
    }


    public boolean isCollectionTimeOver() {
        TimeManagement timeManagement = new TimeManagement();
        int extraDay = 0;
        String currentDay = new SimpleDateFormat("EEEE").format(new Date());
        int dayCount = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().size();
        for(int i = 0; i <dayCount; i++) {
            if (AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getDayName().equalsIgnoreCase(currentDay)) {
                int timeListLength = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().size();
                try {
                    //  SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
                    SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
                    String currentTimeStr = sdf24.format(new Date());
                    Date currentTime = sdf24.parse(currentTimeStr);
                    for(int j = 0; j < timeListLength; j++){
                        String collectionTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getCollection();
                        if(collectionTime == null || "".equalsIgnoreCase(collectionTime)) {
                            continue;
                        }
                        String openingTimeStr1 = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getOpeningTime();
                        String closingTimeStr1 = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getEndTime();
                        String openingTimeStr = timeManagement.getTimeFrom12To24(openingTimeStr1);
                        String closingTimeStr = timeManagement.getTimeFrom12To24(closingTimeStr1);
                        Date tempOpeningDate = sdf24.parse(openingTimeStr);
                        Date tempClosingDate = sdf24.parse(closingTimeStr);
                        if(tempOpeningDate.after(tempClosingDate)) {
                            extraDay = 1;
                        }
                        String lastCollectionTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getLastCollectionSubmission();
                        Date closingTime = sdf24.parse(closingTimeStr);
                        int lastTime = 0;
                        if(lastCollectionTime != null && !lastCollectionTime.equalsIgnoreCase("")) {
                            lastTime = Integer.valueOf(lastCollectionTime);
                        }
                        if(collectionTime != null && !collectionTime.equalsIgnoreCase("")) {
                            int tempCollection = Integer.valueOf(collectionTime);
                            lastTime += tempCollection;
                        }
                        lastTime = 0 - lastTime;
                        Calendar calendar = Calendar.getInstance(Locale.getDefault());
                        calendar.setTime(closingTime);
                        calendar.add(Calendar.DATE, extraDay);
                        calendar.add(Calendar.MINUTE, lastTime);
                        String lastTimeStr = sdf24.format(calendar.getTime());
                        Date lastTimeDate = sdf24.parse(lastTimeStr);
                        if(currentTime.before(lastTimeDate) || currentTime.equals(lastTimeDate)) {
                            return false;
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
        return true;
    }



    public boolean isDeliveryTimeOver() {
        TimeManagement timeManagement = new TimeManagement();
        int extraDay = 0;
        String currentDay = new SimpleDateFormat("EEEE").format(new Date());
        int dayCount = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().size();
        for(int i = 0; i <dayCount; i++) {
            if (AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getDayName().equalsIgnoreCase(currentDay)) {
                int timeListLength = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().size();
                try {
                    //  SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
                    SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
                    String currentTimeStr = sdf24.format(new Date());
                    Date currentTime = sdf24.parse(currentTimeStr);
                    for(int j = 0; j < timeListLength; j++){
                        String deliveryTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getDelivery();
                        if(deliveryTime == null || "".equalsIgnoreCase(deliveryTime)) {
                            continue;
                        }
                        String openingTimeStr1 = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getOpeningTime();
                        String closingTimeStr1 = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getEndTime();
                        String openingTimeStr = timeManagement.getTimeFrom12To24(openingTimeStr1);
                        String closingTimeStr = timeManagement.getTimeFrom12To24(closingTimeStr1);
                        Date tempOpeningDate = sdf24.parse(openingTimeStr);
                        Date tempClosingDate = sdf24.parse(closingTimeStr);
                        if(tempOpeningDate.after(tempClosingDate)) {
                            extraDay = 1;
                        }
                        String lastDeliveryTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getLastDeliverySubmission();
                        Date closingTime = sdf24.parse(closingTimeStr);
                        int lastTime = 0;
                        if(lastDeliveryTime != null && !lastDeliveryTime.equalsIgnoreCase("")) {
                            lastTime = Integer.valueOf(lastDeliveryTime);
                        }
                        if(deliveryTime != null && !deliveryTime.equalsIgnoreCase("")) {
                            int tempDelivery = Integer.valueOf(deliveryTime);
                            lastTime += tempDelivery;
                        }
                        lastTime = 0 - lastTime;
                        Calendar calendar = Calendar.getInstance(Locale.getDefault());
                        calendar.setTime(closingTime);
                        calendar.add(Calendar.DATE, extraDay);
                        calendar.add(Calendar.MINUTE, lastTime);
                        String lastTimeStr = sdf24.format(calendar.getTime());
                        Date lastTimeDate = sdf24.parse(lastTimeStr);
                        if(currentTime.before(lastTimeDate) || currentTime.equals(lastTimeDate)) {
                            return false;
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
        return true;
    }


    public CollectionDeliveryData getCollectionDeliveryTIme(HotRestaurantData hotRestaurantDatas) {
        CollectionDeliveryData collectionDeliveryData = new CollectionDeliveryData();
        String delivery = "";
        String collection = "";
        String[] weekDays = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        int daySerial = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        String today = weekDays[daySerial];
        ArrayList<ScheduleData> scheduleDatas = hotRestaurantDatas.getScheduleDatas();
        for (int i = 0; i < scheduleDatas.size() ; i++) {
            String tempDay = scheduleDatas.get(i).getDayName();
            if(tempDay.equalsIgnoreCase(today)) {
                ArrayList<OpenEndTimeData> openEndTimeDatas = scheduleDatas.get(i).getOpenEndTimeDatas();
                for(int j=0; j < openEndTimeDatas.size(); j++){
                    if("".equalsIgnoreCase(collection)) {
                        if(openEndTimeDatas.get(j).getCollection() != null && !"".equalsIgnoreCase(openEndTimeDatas.get(j).getCollection())) {
                            //    if(TimeManagement.isShiftTimeRemaining(openEndTimeDatas.get(j).getOpeningTime(), openEndTimeDatas.get(j).getEndTime(), openEndTimeDatas.get(j).getLastCollectionSubmission(), openEndTimeDatas.get(j).getCollection())) {
                            if(TimeManagement.isInMiddleTime(openEndTimeDatas.get(j).getOpeningTime(), openEndTimeDatas.get(j).getEndTime())) {
                                collection = openEndTimeDatas.get(j).getCollection();
                            } else if(TimeManagement.isBeforeStartingShift(openEndTimeDatas.get(j).getOpeningTime())) {
                                collection = openEndTimeDatas.get(j).getCollection();
                            }
                        }
                    }

                    if("".equalsIgnoreCase(delivery)) {
                        if(openEndTimeDatas.get(j).getDelivery() != null && !"".equalsIgnoreCase(openEndTimeDatas.get(j).getDelivery())) {
                            //  if(TimeManagement.isShiftTimeRemaining(openEndTimeDatas.get(j).getOpeningTime(), openEndTimeDatas.get(j).getEndTime(), openEndTimeDatas.get(j).getLastDeliverySubmission(), openEndTimeDatas.get(j).getDelivery())) {
                            if(TimeManagement.isInMiddleTime(openEndTimeDatas.get(j).getOpeningTime(), openEndTimeDatas.get(j).getEndTime())) {
                                delivery = openEndTimeDatas.get(j).getDelivery();
                            } else if(TimeManagement.isBeforeStartingShift(openEndTimeDatas.get(j).getOpeningTime())) {
                                delivery = openEndTimeDatas.get(j).getDelivery();
                            }
                        }
                    }

                    if(!"".equalsIgnoreCase(collection) && !"".equalsIgnoreCase(delivery)) {
                        break;
                    }

                }

                if("".equalsIgnoreCase(collection)) {
                    int k = 0;
                    while (k < openEndTimeDatas.size()) {
                        if(openEndTimeDatas.get(k).getCollection() != null && !"".equalsIgnoreCase(openEndTimeDatas.get(k).getCollection())) {
                            collection = openEndTimeDatas.get(k).getCollection();
                            break;
                        }
                        k++;
                    }
                }

                if("".equalsIgnoreCase(delivery)) {
                    int k = 0;
                    while (k < openEndTimeDatas.size()) {
                        if(openEndTimeDatas.get(k).getDelivery() != null && !"".equalsIgnoreCase(openEndTimeDatas.get(k).getDelivery())) {
                            delivery = openEndTimeDatas.get(k).getDelivery();
                            break;
                        }
                        k++;
                    }
                }
                break;
            }
        }

        collectionDeliveryData.setDelivery(delivery);
        collectionDeliveryData.setCollection(collection);
        return collectionDeliveryData;
    }



    public static List<String> makeTimeListForReservation (String getStartTime1, String getEndTime1, int policyTime) {
        List<String> timeList = new ArrayList<>();

       // SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
        try {
            TimeManagement timeManagement = new TimeManagement();
            String getStartTime = timeManagement.getTimeFrom12To24(getStartTime1);
            String getEndTime = timeManagement.getTimeFrom12To24(getEndTime1);
            Date startTime = sdf24.parse(getStartTime); // main start time
            Date endTime = sdf24.parse(getEndTime);

            Calendar calInit = Calendar.getInstance();
            calInit.setTime(startTime);
            calInit.add(Calendar.MINUTE, policyTime);
            String newTimeInit = sdf24.format(calInit.getTime());

          //  timeList.add(timeFormatWithoutSecond(newTimeInit));
            timeList.add(timeManagement.getTimeFrom24To12(newTimeInit));

            // time instance for interval increment
            Date startForIntervalTime = sdf24.parse(newTimeInit);
            Calendar cal = Calendar.getInstance();
            cal.setTime(startForIntervalTime);

            boolean isTrue = true;
            while (isTrue) {
                cal.add(Calendar.MINUTE, 15);
                String newTime = sdf24.format(cal.getTime());

                Date increasedTime = sdf24.parse(newTime);
                if ((increasedTime.before(endTime) || increasedTime.equals(endTime))) {
                   // timeList.add(timeFormatWithoutSecond(newTime));
                    timeList.add(timeManagement.getTimeFrom24To12(newTime));
                } else {
                    isTrue = false;
                }

            }

        } catch (Exception ex) {
            System.out.println("Exception " + ex);
        }

        return timeList;
    }

    public static List<String> makeTimeList (String getStartTime, String getEndTime, int policyTime, String lastTimePolicy) {
        TimeManagement timeManagement = new TimeManagement();
        String start_Time_string = timeManagement.getTimeFrom12To24(getStartTime);
        String end_time_string = timeManagement.getTimeFrom12To24(getEndTime);

        List<String> timeList = new ArrayList<>();

       // SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
        try {
            Date startTime = sdf24.parse(start_Time_string); // main start time
            String newTimeMinus = timeAddOrRemove(startTime, -1);

            // new start time minus 1 minus
            Date startTimeNew = sdf24.parse(newTimeMinus);
            Date endTime = sdf24.parse(end_time_string);

            end_time_string = timeAddOrRemove(endTime, -Integer.parseInt(lastTimePolicy));
            Log.i("NEW TIME", "++> " + end_time_string);
            Log.i("NEW TIME res", "++> " + -Integer.parseInt(lastTimePolicy));
            endTime = sdf24.parse(end_time_string);


            String newTimeInit = timeAddOrRemove(startTime, policyTime);
          //  timeList.add(timeFormatWithoutSecond(newTimeInit)); // add first time item
            timeList.add(timeManagement.getTimeFrom24To12(newTimeInit)); // add first time item

            // time instance for interval increment
            Date startForIntervalTime = sdf24.parse(newTimeInit);
            Calendar cal = Calendar.getInstance();
            cal.setTime(startForIntervalTime);

            boolean isTrue = true;
            while (isTrue) {
                cal.add(Calendar.MINUTE, 15);
                String newTime = sdf24.format(cal.getTime());

                Date increasedTime = sdf24.parse(newTime);

                if ((increasedTime.after(startTimeNew) && increasedTime.before(endTime)) || (increasedTime.after(startTimeNew) && increasedTime.equals(endTime))) {
                    //System.out.println("NEW TIME : " + newTime);
                  //  timeList.add(timeFormatWithoutSecond(newTime));
                    timeList.add(timeManagement.getTimeFrom24To12(newTime));
                    //System.out.println(" Yes");
                } else {
                    isTrue = false;
                }

            }

        } catch (ParseException ex) {
            System.out.println("Exception " + ex);
        }

        return timeList;
    }


    // generate next time slot when previous times over
    public static List<String> reGenerateTimeList(List<String> timeArrayList) {
        TimeManagement timeManagement = new TimeManagement();
        List<String> stringList = new ArrayList<>();
        try {
            // SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");
            SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
            String currentDateTime = sdf24.format(new Date());
            Date startTime = sdf24.parse(currentDateTime);

            boolean isNeedTimeReGenerate = false;
            for (int k = 0; k < timeArrayList.size(); k++) {

                try {
                    Date start = sdf24.parse(timeManagement.getTimeFrom12To24(timeArrayList.get(k).toString()));

                    if (Integer.toString(start.compareTo(startTime)).equalsIgnoreCase("-1")) {
                        isNeedTimeReGenerate = true;
                    } else {
                        stringList.add(timeArrayList.get(k).toString());
                    }

                } catch (ParseException e) {
                    // Invalid date was entered
                }
            }

            if(isNeedTimeReGenerate) {
                timeArrayList.clear();
                timeArrayList = stringList;
            }

        } catch (Exception e) {
            Log.e("Ex", "" + e.getMessage());
        }

        return timeArrayList;
    }


    public static String timeFormatWithoutSecond(String newTime) {
        final String NEW_FORMAT = " hh:mm a";
        final String OLD_FORMAT = "hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
        Date d = null;
        try {
            d = sdf.parse(newTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        sdf.applyPattern(NEW_FORMAT);
        newTime = sdf.format(d);

        return newTime;
    }

    public static String timeAddOrRemove(Date startTime, int policyTime) {
        Calendar calInit = Calendar.getInstance();
        calInit.setTime(startTime);
        calInit.add(Calendar.MINUTE, policyTime);
      //  SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
        String newTimeInit = sdf24.format(calInit.getTime());
        return newTimeInit;
    }

}
