package com.chefonline.utility;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.chefonline.adapter.OffersDialogAdapter;
import com.chefonline.datamodel.CartData;
import com.chefonline.datamodel.CollectionDeliveryData;
import com.chefonline.datamodel.HotRestaurantData;
import com.chefonline.datamodel.OffersData;
import com.chefonline.datamodel.OpenEndTimeData;
import com.chefonline.datamodel.ScheduleData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;
import com.custom.dateform.DateFormatter;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by masum on 09/04/2015.
 */
public class UtilityMethod {
    public static String TAG = "UtilityMethod";
    public static double getMiles(LatLng latLngCurrent, LatLng latB) {
        Location locationA = new Location("point A");
        locationA.setLatitude(latLngCurrent.latitude);
        locationA.setLongitude(latLngCurrent.longitude);
        Location locationB = new Location("point B");
        locationB.setLatitude(latB.latitude);
        locationB.setLongitude(latB.longitude);

        float distance = locationA.distanceTo(locationB);

        return distance * 0.000621371192;
    }

    public static boolean isWhiteSpaces( String s ) {
        if (s.contains(" ")) {
            return true;
        }
        return s != null && s.matches("\\s+");
    }

    public static String getRealCriteriaString(int selected_button) {
        String criteria = null;
        switch (selected_button) {
            case 1:
                criteria =  "Reservation";
                break;
            case 2:
                criteria =  "Collection";
                break;
            case 3:
                criteria =  "Delivery";
                break;
            default:
                criteria = "";
        }

        return criteria;
    }

 /*   public static JSONObject createOrderListJson(ArrayList<CartData> groups) throws JSONException {
        JSONObject jResult = new JSONObject();
        JSONArray jArray = new JSONArray();

        for (int i = 0; i < groups.size(); i++) {
            JSONObject jGroup = new JSONObject();
            jGroup.put("DishId", groups.get(i).getItemId());
            jGroup.put("DishName", groups.get(i).getItemName());
            jGroup.put("DishPrice", groups.get(i).getPrice());
            jGroup.put("DishCount", groups.get(i).getItemQty());
            jArray.put(jGroup);
        }

        jResult.put("OrderList", jArray);
        return jResult;
    }

    public static String CartJsonToString() {
        String data = null;
        try {
            JSONObject jsonObject = createOrderListJson(AppData.getInstance().getCartDatas());
            data = jsonObject.toString();
        } catch (Exception e) {
            data = "";
        }

        return data;
    }  */

    public static String createOrderListJsonNew() {
        try {
            ArrayList<CartData> cartDatas = AppData.getInstance().getCartDatas();
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < cartDatas.size(); i++) {
                JSONObject jGroup = new JSONObject();
                jGroup.put("DishId", cartDatas.get(i).getItemId());
                jGroup.put("quantity", cartDatas.get(i).getItemQty());

                if(cartDatas.get(i).getItemPizzaIds() != null && !"".equalsIgnoreCase(cartDatas.get(i).getItemPizzaIds())) {

                    JSONArray jPizzaDetails = new JSONArray();

                    String tempIds = cartDatas.get(i).getItemPizzaIds();
                    String id = "";
                    for (int j = 0; j < tempIds.length(); j++) {
                        if(tempIds.charAt(j) != ',') {
                            id += tempIds.charAt(j);
                        }
                        if(j == (tempIds.length() - 1) || tempIds.charAt(j) == ',') {
                            JSONObject jItemId = new JSONObject();
                            jItemId.put("pizza_group_item_id", id);
                            jPizzaDetails.put(jItemId);
                            id = "";
                        }
                    }

                   jGroup.put("pizza_details", jPizzaDetails);

                }

                jsonArray.put(jGroup);
            }
            return jsonArray.toString();
        } catch (Exception e) {
            return "";
        }
    }


 /*   public static Double getTotalCartPrice(Activity activity) {
        ArrayList<CartData> cartDataForTotal = new ArrayList<>();
        Double totalSubAmount = 0.0;
        DataBaseUtil db;
        db = new DataBaseUtil(activity);
        db.open();
        cartDataForTotal = db.fetchCartData();
        for (int i = 0; i < cartDataForTotal.size(); i++) {
            totalSubAmount += (cartDataForTotal.get(i).getItemQty() * cartDataForTotal.get(i).getPrice());
        }
        return totalSubAmount;
    }  */

    public static String formattedCurrentDate() {
        DateFormatter formatter = new DateFormatter();
        SimpleDateFormat sdf = new SimpleDateFormat(formatter.DD_MM_YYYY);
        String currentDate = sdf.format(new Date());
        try {
            return formatter.doFormat(currentDate, formatter.DD_MM_YYYY, DateFormatter.MMM_dd_YYYY);
        } catch (Exception e) {
            Log.i(TAG, "Current Date EXP *****" + e.getMessage());
        }

        return currentDate;
    }

    public static void cancelAllToast(){
        for (int i=0;i<ConstantValues.DISH_MENU_TOASTS.size();i++){
            ConstantValues.DISH_MENU_TOASTS.get(i).cancel();
        }
    }

    public static void serviceButtonSelection(Button selectedButton, Button unselectedButton, Context context) {
        selectedButton.setTextColor(context.getResources().getColor(R.color.white));
        selectedButton.setTypeface(null, Typeface.BOLD);
        unselectedButton.setTextColor(context.getResources().getColor(R.color.black));
        unselectedButton.setTypeface(null, Typeface.NORMAL);

    }

    public static boolean isEmailValid(String email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public static boolean isMobileNoValid(String mobileNo) {
        String expr = "^(\\+44\\s?7\\d{3}|\\(?07\\d{3}\\)?)\\s?\\d{3}\\s?\\d{3}$";
        String exprBd = "^(?:\\+?88)?01[15-9]\\d{8}$";

        CharSequence inputStr = mobileNo.toString();

        Pattern pattern = Pattern.compile(expr, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        Pattern patternBd = Pattern.compile(exprBd, Pattern.CASE_INSENSITIVE);
        Matcher matcherBd = patternBd.matcher(inputStr);

        return matcher.matches() || matcherBd.matches();
    }



    public static boolean isNumberValid(String number) {
        boolean isValid = false;

        String expression = "^[0-9]*$";
        CharSequence inputStr = number;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean connected = (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable() && conMgr
                .getActiveNetworkInfo().isConnected());

        return connected;

    }

    public static boolean postcodeValidation(String postCode) {

        if (postCode.indexOf(" ") == -1) {
            if (postCode.length() == 5) {
                postCode = postCode.substring(0, 2) + " " + postCode.substring(2, postCode.length());
            } else if (postCode.length() == 6) {
                postCode = postCode.substring(0, 3) + " " + postCode.substring(3, postCode.length());
            } else if (postCode.length() == 7) {
                postCode = postCode.substring(0, 4) + " " + postCode.substring(4, postCode.length());
            }
        }

        System.out.println("AFTER " + postCode);

       /* List<String> zips = new ArrayList<String>();

        //Valid ZIP codes
        zips.add("SW1W 0NY");
        zips.add("PO16 7GZ");
        zips.add("GU16 7HF");
        zips.add("L1 8JQ");

        //Invalid ZIP codes
        zips.add("Z1A 0B1");
        zips.add("A1A 0B11");*/

        String regex = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$";
        //String regex = "/\\\\A\\\\b[A-Z]{1,2}[0-9][A-Z0-9]?[\\s]?[0-9][ABD-HJLNP-UW-Z]{2}\\\\b\\\\z/i";
        //String regex = "(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})";
        //String postMatch = "/[A-z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}/i";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(postCode);
        return matcher.matches();
    }

    public static void openDialog(final HotRestaurantData hotRestaurantData, String dishName, Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_offers);

        TextView text = (TextView) dialog.findViewById(R.id.txtViewOptionName);
        text.setText(dishName);

        ListView listView = (ListView) dialog.findViewById(R.id.listViewOptions);

        ArrayList<OffersData> optionDatas = new ArrayList<>();

        for (int i = 0; i < hotRestaurantData.getDiscountList().size() ; i++) {
            OffersData offersData = new OffersData();
            offersData.setOfferTitle(hotRestaurantData.getDiscountList().get(i).getDiscountTitle());
            offersData.setOfferElegibleAmount(hotRestaurantData.getDiscountList().get(i).getDiscountEligibleAmount());
            offersData.setOfferImage(hotRestaurantData.getDiscountList().get(i).getDiscountImage());
            optionDatas.add(offersData);
        }

        optionDatas.addAll(hotRestaurantData.getOffersDatas());

        OffersDialogAdapter adapter = new OffersDialogAdapter(context, optionDatas, "", dishName);
        listView.setAdapter(adapter);

        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        Button btnContinue = (Button) dialog.findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    public static String priceFormatter(Double price) {
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(price);
    }

    public static int daysInMonth(String monthNumber)
    {
        monthNumber = monthNumber.toUpperCase();
        switch(monthNumber)
        {
            case "FEBRUARY":
                return 29;
            case "APRIL":
            case "JUNE":
            case "SEPTEMBER":
            case "NOVEMBER":// intentional fall-through
                return 30;
            default:
                return 31;
        }// end switch
    }

    public static String getNoOfMonth(String monthNumber)
    {
        monthNumber = monthNumber.toUpperCase();
        switch(monthNumber)
        {
            case "JANUARY":
                return "01";
            case "FEBRUARY":
                return "02";
            case "MARCH":
                return "03";
            case "APRIL":
                return "04";
            case "MAY":
                return "05";
            case "JUNE":
                return "06";

            case "JULY":
                return "07";

            case "AUGUST":
                return "08";
            case "SEPTEMBER":
                return "09";
            case "OCTOBER":
                return "10";
            case "NOVEMBER":
                return "11";
            case "DECEMBER":// intentional fall-through
                return "12";
            default:
                return "Month";
        }// end switch
    }


    public static String getNameOfMonth(String monthNumber)
    {
        monthNumber = monthNumber.toUpperCase();
        switch(monthNumber)
        {
            case "01":
                return "January";
            case "02":
                return "February";
            case "03":
                return "March";
            case "04":
                return "April";
            case "05":
                return "May";
            case "06":
                return "June";
            case "07":
                return "July";
            case "08":
                return "August";
            case "09":
                return "September";
            case "10":
                return "October";
            case "11":
                return "November";
            case "12":// intentional fall-through
                return "December";
            default:
                return "Month";
        }// end switch
    }


}
