package com.chefonline.pattern;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.chefonline.datamodel.CartData;
import com.chefonline.datamodel.CategoryData;
import com.chefonline.datamodel.DishData;
import com.chefonline.datamodel.HotRestaurantData;
import com.chefonline.datamodel.NetpayData;
import com.chefonline.datamodel.OrderHistoryData;
import com.chefonline.datamodel.OrderPlaceData;
import com.chefonline.datamodel.UserData;

import java.util.ArrayList;

/**
 * Created by masum on 11/04/2015.
 */
public class AppData {
    private static AppData ourInstance = new AppData();
    private ArrayList<HotRestaurantData> hotRestaurantDataArrayList = new ArrayList<>();
    private ArrayList<OrderHistoryData> orderHistoryDatas = new ArrayList<>();
    private ArrayList<OrderHistoryData> orderHistoryDetails = new ArrayList<>();
    private ArrayList<HotRestaurantData> RestaurantDataArrayList = new ArrayList<>();
    private HotRestaurantData restaurantInfoData = new HotRestaurantData();
    private ArrayList<CartData> cartDatas = new ArrayList<>();
    private ArrayList<DishData> dishDatas = new ArrayList<>();
    private ArrayList<DishData> dishDatasDuplicate = new ArrayList<>();
    private ArrayList<Activity> savedActivities = new ArrayList<>();
    private UserData userDatas = new UserData();
    private OrderPlaceData orderPlaceData = new OrderPlaceData();
    private NetpayData netpayData = new NetpayData();

    private AppData() {
    }

    public static AppData getInstance() {
        return ourInstance;
    }

    public static void destroyInstance() {
        ourInstance = null;
    }

    public void hideKeyboard(Context context, View v) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public ArrayList<HotRestaurantData> getHotRestaurantDataArrayList() {
        return hotRestaurantDataArrayList;
    }

    public void setHotRestaurantDataArrayList(ArrayList<HotRestaurantData> hotRestaurantDataArrayList) {
        this.hotRestaurantDataArrayList = hotRestaurantDataArrayList;
    }

    public ArrayList<CartData> getCartDatas() {
        return cartDatas;
    }

    public void setCartDatas(ArrayList<CartData> cartDatas) {
        this.cartDatas = cartDatas;
    }

    public NetpayData getNetpayData() {
        return netpayData;
    }

    public void setNetpayData(NetpayData netpayData) {
        this.netpayData = netpayData;
    }

    public ArrayList<HotRestaurantData> getRestaurantDataArrayList() {
        return RestaurantDataArrayList;
    }

    public void setRestaurantDataArrayList(ArrayList<HotRestaurantData> restaurantDataArrayList) {
        RestaurantDataArrayList = restaurantDataArrayList;
    }

    public UserData getUserDatas() {
        return userDatas;
    }

    public void setUserDatas(UserData userDatas) {
        this.userDatas = userDatas;
    }

    public HotRestaurantData getRestaurantInfoData() {
        return restaurantInfoData;
    }

    public void setRestaurantInfoData(HotRestaurantData restaurantInfoData) {
        this.restaurantInfoData = restaurantInfoData;
    }

    public OrderPlaceData getOrderPlaceData() {
        return orderPlaceData;
    }

    public void setOrderPlaceData(OrderPlaceData orderPlaceData) {
        this.orderPlaceData = orderPlaceData;
    }

    public ArrayList<OrderHistoryData> getOrderHistoryDatas() {
        return orderHistoryDatas;
    }

    public void setOrderHistoryDatas(ArrayList<OrderHistoryData> orderHistoryDatas) {
        this.orderHistoryDatas = orderHistoryDatas;
    }

    public ArrayList<OrderHistoryData> getOrderHistoryDetails() {
        return orderHistoryDetails;
    }

    public void setOrderHistoryDetails(ArrayList<OrderHistoryData> orderHistoryDetails) {
        this.orderHistoryDetails = orderHistoryDetails;
    }

    public ArrayList<DishData> getDishDatas() {
        return dishDatas;
    }

    public void setDishDatas(ArrayList<DishData> dishDatas) {
        this.dishDatas = dishDatas;
    }

    public ArrayList<DishData> getDishDatasDuplicate() {
        return dishDatasDuplicate;
    }

    public void setDishDatasDuplicate(ArrayList<DishData> dishDatasDuplicate) {
        this.dishDatasDuplicate = dishDatasDuplicate;
    }

    public ArrayList<Activity> getSavedActivities() {
        return savedActivities;
    }

    public void setSavedActivities(ArrayList<Activity> savedActivities) {
        this.savedActivities = savedActivities;
    }
}
