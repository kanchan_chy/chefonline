package com.chefonline.modelinterface;

import com.chefonline.datamodel.CategoryData;

import java.util.ArrayList;

/**
 * Created by masum on 30/11/2015.
 */
public interface TakeawayCategoryView {
    void setHideLoading();
    void setCategory(ArrayList<CategoryData> categoryList);
    void setEmptyMessage(String message);
}
