package com.chefonline.modelinterface;

import com.chefonline.datamodel.LocationData;

/**
 * Created by masum on 29/11/2015.
 */
public interface NetPayView {
    void setOrderId(String orderId);
    void setTransactionId(String transactionId);
    void orderCompleted(boolean isSuccess);
    void setLoadingVisible();
    void setLoadingHide();
    void showToast(String message, String subMessage, boolean isButtonOk);
}
