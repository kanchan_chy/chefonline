package com.chefonline.modelinterface;

/**
 * Created by masum on 29/11/2015.
 */
public interface RequestCompleteInterface {
    void onCompleted(String result);
    void onErrorCompleted(String result);
}
