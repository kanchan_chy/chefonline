package com.chefonline.modelinterface;

import com.chefonline.datamodel.HotRestaurantData;

import java.util.ArrayList;

/**
 * Created by masum on 29/11/2015.
 */
public interface RestaurantView {
    void setLoadingHide();
    void setListView(ArrayList<HotRestaurantData> hotRestaurantDatas);
    void setEmptyMessage(String msg);
}
