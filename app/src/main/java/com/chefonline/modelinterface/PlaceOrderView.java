package com.chefonline.modelinterface;

import com.chefonline.datamodel.LocationData;

/**
 * Created by masum on 29/11/2015.
 */
public interface PlaceOrderView {
    void setLoadingVisible();
    void setLoadingHide();
    void showToast(String message, String subMessage, boolean isButtonOk);
    void setLocationData(LocationData locationData);
    void visiblePayPalButton();
    void verifyCode(String code, boolean isPaypal);
    void orderCompleted(String orderId, String transactionId, boolean isPaypal);
    void paymentStatusUpdated(boolean isSuccess);
    void onVerificationFailed(boolean isPaypal);
    void retryPayment(boolean isPaypal);
    void isNetpayChargeFound(boolean isFound);
}
