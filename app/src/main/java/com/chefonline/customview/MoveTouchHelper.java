package com.chefonline.customview;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.chefonline.adapter.ManagePaymentMethodsAdapter;

/**
 * Created by user on 5/2/2016.
 */
public class MoveTouchHelper extends ItemTouchHelper.SimpleCallback {
    private ManagePaymentMethodsAdapter mMovieAdapter;

    public MoveTouchHelper(ManagePaymentMethodsAdapter movieAdapter){
        super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        this.mMovieAdapter = movieAdapter;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        //TODO: Not implemented here
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        //Remove item
        mMovieAdapter.remove(viewHolder.getAdapterPosition());
    }
}
