package com.chefonline.customview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chefonline.online.R;
import com.chefonline.utility.ConstantValues;

/**
 * Created by masum on 22/06/2015.
 */
public class CustomBottomToast {

    public CustomBottomToast(Activity context, String title, String subTitle, boolean isSuccess) {
        LayoutInflater inflater = context.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_custom_bottom, (ViewGroup) context.findViewById(R.id.custom_toast_layout_id));

        TextView textViewTitle = (TextView) layout.findViewById(R.id.textViewTitle);
        TextView textViewSubTitle = (TextView) layout.findViewById(R.id.textViewSubTitle);
        ImageView imageView = (ImageView) layout.findViewById(R.id.imageView1);
        textViewTitle.setText(title);
        textViewTitle.setTextSize(14.0f);
        textViewSubTitle.setText(subTitle);

        if (isSuccess) {
            imageView.setBackgroundResource(R.drawable.tick_mark);
        } else {
            imageView.setBackgroundResource(R.drawable.warning);
        }

        // Create Custom Toast
        Toast toast = new Toast(context);
       // toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

}
