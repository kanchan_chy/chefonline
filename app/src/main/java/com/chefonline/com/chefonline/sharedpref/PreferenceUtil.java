package com.chefonline.com.chefonline.sharedpref;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferenceUtil {
	
	Context mContext;
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor spEditor;
	
	
	private final String IS_CART_ACTIVE = "is_cart_active";
    private final String LOGIN_STATUS = "login_status";
    private final String USER_ID = "user_id";

    private final String USER_GROUP_ID = "user_group_id";
    private final String USER_EMAIL = "email";
    private final String TITLE = "title";
    private final String FIRST_NAME = "first_name";
    private final String USER_SUR_NAME = "sur_name";
    private final String USER_LAST_NAME = "last_name";
    private final String USER_POSTCODE = "postcode";
    private final String USER_MOB = "mobile_no";
    private final String USER_ADDRESS = "address1";
    private final String USER_ADDRESS2 = "address2";
    private final String USER_DOB = "date_of_birth";
    private final String USER_DOA = "date_of_anniversery";
    private final String USER_TEL = "telephone_no";
    private final String USER_COUNTRY = "country";
    private final String USER_TOWN = "town";

    private final String VOICE_COMMAND = "voice_command";

	public PreferenceUtil(Context mContext) 
	{
		super();
		this.mContext = mContext;
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
	}

    public void setUserID (String user_id) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_ID, user_id);
        spEditor.commit();

    }

    public String getUserID () {
        return sharedPreferences.getString(USER_ID, "");

    }


    public void setLogInStatus (int status) {
        spEditor = sharedPreferences.edit();
        spEditor.putInt(LOGIN_STATUS, status);
        spEditor.commit();

    }

    public int getLogInStatus () {
        return sharedPreferences.getInt(LOGIN_STATUS, 0);
    }

    public void setUserGroupId(String userGroupId) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_GROUP_ID, userGroupId);
        spEditor.commit();
    }

    public String getUSER_GROUP_ID() {
        return sharedPreferences.getString(USER_GROUP_ID, "");
    }

    public void setUserEmail(String userGroupId) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_EMAIL, userGroupId);
        spEditor.commit();
    }

    public String getUserEmail() {
        return sharedPreferences.getString(USER_EMAIL, "");
    }

    public void setUserTitle(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(TITLE, value);
        spEditor.commit();
    }

    public String getUserTitle() {
        return sharedPreferences.getString(TITLE, "");
    }

    public void setUserFirstName(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(FIRST_NAME, value);
        spEditor.commit();
    }

    public String getUserFirstName() {
        return sharedPreferences.getString(FIRST_NAME, "");
    }

    public void setUserSurName(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_SUR_NAME, value);
        spEditor.commit();
    }

    public String getUserSurName() {
        return sharedPreferences.getString(USER_SUR_NAME, "");
    }

    public void setUserLastName(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_LAST_NAME, value);
        spEditor.commit();
    }

    public String getUserLastName() {
        return sharedPreferences.getString(USER_LAST_NAME, "");
    }

    public void setUserPostCode(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_POSTCODE, value);
        spEditor.commit();
    }

    public String getUserPostCode() {
        return sharedPreferences.getString(USER_POSTCODE, "");
    }

    public void setUserMobile(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_MOB, value);
        spEditor.commit();
    }

    public String getUserMobile() {
        return sharedPreferences.getString(USER_MOB, "");
    }

    public void setUserAddress1(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_ADDRESS, value);
        spEditor.commit();
    }

    public String getUserAddress() {
        return sharedPreferences.getString(USER_ADDRESS, "");
    }

    public void setUserAddress2(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_ADDRESS2, value);
        spEditor.commit();
    }

    public String getUserAddress2() {
        return sharedPreferences.getString(USER_ADDRESS2, "");
    }

    public void setUserDob(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_DOB, value);
        spEditor.commit();
    }

    public String getUserDob() {
        return sharedPreferences.getString(USER_DOB, "");
    }

    public void setUserDoa(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_DOA, value);
        spEditor.commit();
    }

    public String getUserDoa() {
        return sharedPreferences.getString(USER_DOA, "");
    }

    public void setUserTel(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_TEL, value);
        spEditor.commit();
    }

    public String getUserTel() {
        return sharedPreferences.getString(USER_TEL, "");
    }

    public void setUserTown(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_TOWN, value);
        spEditor.commit();
    }

    public String getUserCountry() {
        return sharedPreferences.getString(USER_COUNTRY, "");
    }

    public void setUserCountry(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_COUNTRY, value);
        spEditor.commit();
    }

    public String getUserTown() {
        return sharedPreferences.getString(USER_TOWN, "");
    }

    public void setOnOffVoiceCommand(int value) {
        spEditor = sharedPreferences.edit();
        spEditor.putInt(VOICE_COMMAND, value);
        spEditor.commit();
    }

    public int getOnOffVoiceCommand() {
        return sharedPreferences.getInt(VOICE_COMMAND, 0);
    }

}