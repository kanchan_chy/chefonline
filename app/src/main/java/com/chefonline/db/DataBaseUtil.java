package com.chefonline.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.chefonline.datamodel.CartData;
import com.chefonline.datamodel.NetpayData;
import com.chefonline.pattern.AppData;

import java.util.ArrayList;
import java.util.HashMap;

public class DataBaseUtil {

	//Database Info
	private static final String TAG = "DataBaseUtil";
	private static final String DATABASE_NAME = "db_chefonline";
	private static final String DATABASE_TABLE = "tbl_cart";
	private static final String DATABASE_DISCOUNT_TABLE = "tbl_discount";
	private static final String DATABASE_POLICY_TABLE = "tbl_policy";
	private static final String DATABASE_PLACE_ORDER = "tbl_place_order";
    private static final String TABLE_NETPAY_USER = "tbl_netpay_user";
	private static final int DATABASE_VERSION = 4;

	// Table Columns
	public static final String KEY_ROWID = "_id";
	public static final String KEY_ITEM_RES_ID = "res_id";
	public static final String KEY_ITEM_ID = "item_id";
	public static final String KEY_ITEM_NAME = "item_name";
	public static final String KEY_ITEM_QTY = "item_qty";
	public static final String KEY_ITEM_PRICE = "item_price";
    public static final String KEY_ITEM_PIZZA_IDS = "item_pizza_ids";
    public static final String KEY_ITEM_PIZZA_NAMES = "item_pizza_names";

    /** Discount table column */
    public static final String DISCOUNT_ROW_ID = "_id";
    public static final String DISCOUNT__ID = "discount_id";
    public static final String DISCOUNT_ORDER_TYPE = "order_type";
    public static final String DISCOUNT__TYPE = "discount_type";
    public static final String DISCOUNT__AMOUNT = "discount_amount";
    public static final String DISCOUNT__TITLE = "discount_title";
    public static final String DISCOUNT__DESCRIPTION = "discount_description";
    public static final String DISCOUNT__ELIGIBLE_AMOUNT = "discount_eligible_amount";

    /** Policy table column */
    public static final String POLICY_ROW_ID = "_id";
    public static final String POLICY_ID = "policy_id";
    public static final String POLICY_NAME = "policy_name";
    public static final String POLICY_TIME= "policy_time";
    public static final String POLICY_MIN_ORDER = "policy_min_order";
    public static final String POLICY_STATUS = "policy_status";

    /** Place Order table column */
    public static final String ORDER_ROW_ID = "_id";
    public static final String ORDER_ID = "order_id";
    public static final String ORDER_STATUS = "order_status";
    public static final String ORDER_PAYPAL_TRANSACTION_ID = "transaction_id";
    public static final String IS_CASH_CREDIT = "cash_credit"; //cash=0, credit=1

    /** Netpay User table column */
    public static final String NETPAY_USER_ROW_ID = "_id";
    public static final String NETPAY_USER_ID = "user_id";
    public static final String NETPAY_USER_CARD_TYPE = "card_type";
    public static final String NETPAY_USER_CARD_NUMBER = "card_number";
    public static final String NETPAY_USER_CARD_EXPIRY_MONTH = "exp_month";
    public static final String NETPAY_USER_CARD_EXPIRY_YEAR = "exp_year";
    public static final String NETPAY_USER_CARD_HOLDER_TITLE = "title";
    public static final String NETPAY_USER_CARD_HOLDER_FIRST_NAME = "first_name";
    public static final String NETPAY_USER_CARD_HOLDER_MIDDLE_NAME = "middle_name";
    public static final String NETPAY_USER_CARD_HOLDER_LAST_NAME = "last_name";
    public static final String NETPAY_USER_CARD_HOLDER_FULL_NAME = "full_name";
    public static final String NETPAY_USER_BILLING_ADDRESS = "address";
    public static final String NETPAY_USER_BILLING_POSTCODE = "postcode";
    public static final String NETPAY_USER_BILLING_CITY = "city";
    public static final String NETPAY_USER_BILLING_COUNTY = "county";
    public static final String NETPAY_USER_BILLING_COUNTRY = "country";


    //Column
	public String[] columns = {KEY_ROWID, KEY_ITEM_RES_ID, KEY_ITEM_ID, KEY_ITEM_NAME, KEY_ITEM_QTY, KEY_ITEM_PRICE, KEY_ITEM_PIZZA_IDS, KEY_ITEM_PIZZA_NAMES};
    public String[] columnsNetpayUser = {NETPAY_USER_ROW_ID, NETPAY_USER_ID, NETPAY_USER_CARD_TYPE, NETPAY_USER_CARD_NUMBER, NETPAY_USER_CARD_EXPIRY_MONTH, NETPAY_USER_CARD_EXPIRY_YEAR, NETPAY_USER_CARD_HOLDER_TITLE, NETPAY_USER_CARD_HOLDER_FIRST_NAME, NETPAY_USER_CARD_HOLDER_MIDDLE_NAME, NETPAY_USER_CARD_HOLDER_LAST_NAME, NETPAY_USER_CARD_HOLDER_FULL_NAME, NETPAY_USER_BILLING_ADDRESS, NETPAY_USER_BILLING_POSTCODE, NETPAY_USER_BILLING_CITY, NETPAY_USER_BILLING_COUNTY, NETPAY_USER_BILLING_COUNTRY};

	private static final String CREATE_TABLE_CART = "create table "
			+ DATABASE_TABLE + " (" + KEY_ROWID + " integer primary key autoincrement, "
            + KEY_ITEM_RES_ID + " text not null, "
            + KEY_ITEM_ID + " text not null, "
            + KEY_ITEM_NAME + " text not null, "
            + KEY_ITEM_QTY + " integer not null, "
            + KEY_ITEM_PRICE + " text not null, "
            + KEY_ITEM_PIZZA_IDS + " text, "
			+ KEY_ITEM_PIZZA_NAMES + " text);";

    private static final String CREATE_TABLE_NETPAY_USER = "create table "
            + TABLE_NETPAY_USER + " (" + NETPAY_USER_ROW_ID + " integer primary key autoincrement, "
            + NETPAY_USER_ID + " text not null, "
            + NETPAY_USER_CARD_TYPE + " text not null, "
            + NETPAY_USER_CARD_NUMBER + " text not null, "
            + NETPAY_USER_CARD_EXPIRY_MONTH + " text not null, "
            + NETPAY_USER_CARD_EXPIRY_YEAR + " text not null, "
            + NETPAY_USER_CARD_HOLDER_TITLE + " text not null, "
            + NETPAY_USER_CARD_HOLDER_FIRST_NAME + " text not null, "
            + NETPAY_USER_CARD_HOLDER_MIDDLE_NAME + " text, "
            + NETPAY_USER_CARD_HOLDER_LAST_NAME + " text not null, "
            + NETPAY_USER_CARD_HOLDER_FULL_NAME + " text not null, "
            + NETPAY_USER_BILLING_ADDRESS + " text not null, "
            + NETPAY_USER_BILLING_POSTCODE + " text, "
            + NETPAY_USER_BILLING_CITY + " text not null, "
            + NETPAY_USER_BILLING_COUNTY + " text not null, "
            + NETPAY_USER_BILLING_COUNTRY + " text not null);";

	private final Context context;
	private DatabaseHelper dbHelper;
	private SQLiteDatabase sqlDb;

	public DataBaseUtil(Context ctx) {
		this.context = ctx;
	}

	public DataBaseUtil open() throws SQLException {
		dbHelper = new DatabaseHelper(context);
		sqlDb = dbHelper.getWritableDatabase();
		// Log.i("DB WORK","YES");
		return this;
	}

	public void close() {
		sqlDb.close();
	}

    /** Insert cart data */
    /*  itemPizzaIds are organized in this way 'id1,id2,id3'  and itemPizzaNames are organized as this way 'name1,name2,name3'  */
    public long insertCartData(String restaurantId, String itemId, String itemName, int itemQty, String itemPrice, String itemPizzaIds, String itemPizzaNames) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_ITEM_RES_ID, restaurantId);
        cv.put(KEY_ITEM_ID, itemId);
        cv.put(KEY_ITEM_NAME, itemName);
        cv.put(KEY_ITEM_QTY, itemQty);
        cv.put(KEY_ITEM_PRICE, itemPrice);
        cv.put(KEY_ITEM_PIZZA_IDS, itemPizzaIds);
        cv.put(KEY_ITEM_PIZZA_NAMES, itemPizzaNames);
        return sqlDb.insert(DATABASE_TABLE, null, cv);
    }

    /** Update cart data */
    /*  itemPizzaIds are organized in this way 'id1,id2,id3'  */
    public long updateCartData(String itemId, String itemPizzaIds, int itemQty) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_ITEM_QTY, itemQty);
        if(itemPizzaIds == null || "".equals(itemPizzaIds)) {
            return sqlDb.update(DATABASE_TABLE, cv, KEY_ITEM_ID + "= ?", new String[]{String.valueOf(itemId)});
        } else {
            return sqlDb.update(DATABASE_TABLE, cv, KEY_ITEM_ID + " = " + itemId + " AND " + KEY_ITEM_PIZZA_IDS + " LIKE '" + itemPizzaIds + "'", null);
        }
    }


    /** Delete cart data */
    /*  itemPizzaIds are organized in this way 'id1,id2,id3'  */
    public boolean deleteCartData(String itemId, String itemPizzaIds) {
        if(itemPizzaIds == null || "".equals(itemPizzaIds)) {
            return sqlDb.delete(DATABASE_TABLE, KEY_ITEM_ID + "=" + itemId, null) > 0;
        } else {
            return sqlDb.delete(DATABASE_TABLE, KEY_ITEM_ID + " = " + itemId + " AND " + KEY_ITEM_PIZZA_IDS + " LIKE '" + itemPizzaIds + "'", null) > 0;
        }
    }

    /** Update cart data */
    public int sumCartData() {
        Cursor cur = sqlDb.rawQuery("SELECT SUM(" +  KEY_ITEM_QTY +") FROM " + DATABASE_TABLE, null);
        if(cur.moveToFirst()){
            return cur.getInt(0);
        }
        return 0;
    }


    /** Fetch all cart data */
    public ArrayList<CartData> fetchCartData() {
        int i = 0;
        ArrayList<CartData> cartDatas = new ArrayList<>();
        // query(table, column, where, selection, groupby, having, order)
        Cursor cursor = sqlDb.query(DATABASE_TABLE, columns, null, null, null, null, null, null);

        cursor.moveToFirst();
        String allTaskName[] = new String[cursor.getCount()];

        while (!cursor.isAfterLast()) {
           CartData cartData = new CartData();
            Log.i("ITEM_REST_ID", "* " +  cursor.getString(cursor.getColumnIndex(KEY_ITEM_RES_ID)));
            cartData.setItemId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_ITEM_ID))));
            cartData.setItemName(cursor.getString(cursor.getColumnIndex(KEY_ITEM_NAME)));
            cartData.setItemQty(cursor.getInt(cursor.getColumnIndex(KEY_ITEM_QTY)));
            cartData.setPrice(Float.parseFloat(cursor.getString(cursor.getColumnIndex(KEY_ITEM_PRICE))));
            cartData.setItemPizzaIds(cursor.getString(cursor.getColumnIndex(KEY_ITEM_PIZZA_IDS)));
            cartData.setItemPizzaNames(cursor.getString(cursor.getColumnIndex(KEY_ITEM_PIZZA_NAMES)));
           cartDatas.add(cartData);
           cursor.moveToNext();
        }

        cursor.close();
        return cartDatas;
    }


	public HashMap<String, Integer> checkExistItem (String itemId, String itemPizzaIds) {
        HashMap<String, Integer> map = new HashMap<>();
		// query(table, column, where, selection, groupby, having, order)
        Cursor cursor = null;
        if(itemPizzaIds == null || "".equals(itemPizzaIds)) {
            cursor = sqlDb.query(DATABASE_TABLE, columns, KEY_ITEM_ID + "= ?", new String[] { String.valueOf(itemId) }, null, null, null, null);
        } else {
            cursor = sqlDb.query(DATABASE_TABLE, columns, KEY_ITEM_ID + " = " + itemId + " AND " + KEY_ITEM_PIZZA_IDS + " LIKE '" + itemPizzaIds + "'", null, null, null, null, null);
        }
		cursor.moveToFirst();
		int itemCount = cursor.getCount();

        if (itemCount == 0) {
            map.put("count", new Integer(itemCount));
            map.put("qty", new Integer(0));
            cursor.close();
            return map;
        }

		while (!cursor.isAfterLast()) {
            map.put("count", new Integer(itemCount));
            map.put("qty", new Integer(cursor.getInt(cursor.getColumnIndex(KEY_ITEM_QTY))));
			cursor.moveToNext();
		}

		cursor.close();
		return map;
	}


    public int checkRestaurant(String itemId) {
        // query(table, column, where, selection, groupby, having, order)
        Cursor cursor = sqlDb.query(DATABASE_TABLE, columns, KEY_ITEM_RES_ID + "= ?", new String[] { String.valueOf(itemId) }, null, null, null, null);
        cursor.moveToFirst();
        int itemCount = cursor.getCount();

        if (itemCount > 0) {
            return 1;
        }

        cursor.close();
        return 0;
    }

    public boolean emptyCartData() {
        // query(table, column, where, selection, groupby, having, order)
        return sqlDb.delete(DATABASE_TABLE, null, null) > 0;
    }


    /** Insert Netpay User data */
    public long insertNetpayUserData(NetpayData netpayData, String userId) {

        ContentValues cv = new ContentValues();
        cv.put(NETPAY_USER_ID, userId);
        cv.put(NETPAY_USER_CARD_TYPE, netpayData.getCardType());
        cv.put(NETPAY_USER_CARD_NUMBER, netpayData.getCardNumber());
        cv.put(NETPAY_USER_CARD_EXPIRY_MONTH, netpayData.getExpiryMonth());
        cv.put(NETPAY_USER_CARD_EXPIRY_YEAR, netpayData.getExpiryYear());
        cv.put(NETPAY_USER_CARD_HOLDER_TITLE, netpayData.getTitle());
        cv.put(NETPAY_USER_CARD_HOLDER_FIRST_NAME, netpayData.getFirstName());
        cv.put(NETPAY_USER_CARD_HOLDER_MIDDLE_NAME, netpayData.getMiddleName());
        cv.put(NETPAY_USER_CARD_HOLDER_LAST_NAME, netpayData.getLastName());
        cv.put(NETPAY_USER_CARD_HOLDER_FULL_NAME, netpayData.getFullName());
        cv.put(NETPAY_USER_BILLING_ADDRESS, netpayData.getBillToAddress());
        cv.put(NETPAY_USER_BILLING_POSTCODE, netpayData.getBillToPostCode());
        cv.put(NETPAY_USER_BILLING_CITY, netpayData.getBillToTownCity());
        cv.put(NETPAY_USER_BILLING_COUNTY, netpayData.getBillToCounty());
        cv.put(NETPAY_USER_BILLING_COUNTRY, netpayData.getBillToCountry());

        return sqlDb.insert(TABLE_NETPAY_USER, null, cv);
    }


    /** Update Netpay User data **/
    public int updateNetpayUserData(NetpayData netpayData, String userId) {

        ContentValues cv = new ContentValues();
        cv.put(NETPAY_USER_ID, userId);
        cv.put(NETPAY_USER_CARD_TYPE, netpayData.getCardType());
        cv.put(NETPAY_USER_CARD_NUMBER, netpayData.getCardNumber());
        cv.put(NETPAY_USER_CARD_EXPIRY_MONTH, netpayData.getExpiryMonth());
        cv.put(NETPAY_USER_CARD_EXPIRY_YEAR, netpayData.getExpiryYear());
        cv.put(NETPAY_USER_CARD_HOLDER_TITLE, netpayData.getTitle());
        cv.put(NETPAY_USER_CARD_HOLDER_FIRST_NAME, netpayData.getFirstName());
        cv.put(NETPAY_USER_CARD_HOLDER_MIDDLE_NAME, netpayData.getMiddleName());
        cv.put(NETPAY_USER_CARD_HOLDER_LAST_NAME, netpayData.getLastName());
        cv.put(NETPAY_USER_CARD_HOLDER_FULL_NAME, netpayData.getFullName());
        cv.put(NETPAY_USER_BILLING_ADDRESS, netpayData.getBillToAddress());
        cv.put(NETPAY_USER_BILLING_POSTCODE, netpayData.getBillToPostCode());
        cv.put(NETPAY_USER_BILLING_CITY, netpayData.getBillToTownCity());
        cv.put(NETPAY_USER_BILLING_COUNTY, netpayData.getBillToCounty());
        cv.put(NETPAY_USER_BILLING_COUNTRY, netpayData.getBillToCountry());

      //  String args[] = {netpayData.getCardNumber()};
      //  return sqlDb.update(TABLE_NETPAY_USER, cv, NETPAY_USER_CARD_NUMBER + " = ?", args);
        return sqlDb.update(TABLE_NETPAY_USER, cv, NETPAY_USER_ID + " = " + userId + " AND " + NETPAY_USER_CARD_NUMBER + " = " + netpayData.getCardNumber(), null);

    }


    public int deleteNetPayUserData(String userId, String cardNumber) {
        return sqlDb.delete(TABLE_NETPAY_USER, NETPAY_USER_ID + " = " + userId + " AND " + NETPAY_USER_CARD_NUMBER + " = " + cardNumber, null);
    }


    /** Fetch all cart data */
    public ArrayList<NetpayData> fetchNetpayUserData(String userId) {
        ArrayList<NetpayData> netpayDatas = new ArrayList<>();

        String args[] = {userId};
        Cursor cursor = sqlDb.query(TABLE_NETPAY_USER, columnsNetpayUser, NETPAY_USER_ID + " = ?", args, null, null, null, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            NetpayData netpayData = new NetpayData();
            netpayData.setCardType(cursor.getString(cursor.getColumnIndex(NETPAY_USER_CARD_TYPE)));
            netpayData.setCardNumber(cursor.getString(cursor.getColumnIndex(NETPAY_USER_CARD_NUMBER)));
            netpayData.setExpiryMonth(cursor.getString(cursor.getColumnIndex(NETPAY_USER_CARD_EXPIRY_MONTH)));
            netpayData.setExpiryYear(cursor.getString(cursor.getColumnIndex(NETPAY_USER_CARD_EXPIRY_YEAR)));
            netpayData.setTitle(cursor.getString(cursor.getColumnIndex(NETPAY_USER_CARD_HOLDER_TITLE)));
            netpayData.setFirstName(cursor.getString(cursor.getColumnIndex(NETPAY_USER_CARD_HOLDER_FIRST_NAME)));
            netpayData.setMiddleName(cursor.getString(cursor.getColumnIndex(NETPAY_USER_CARD_HOLDER_MIDDLE_NAME)));
            netpayData.setLastName(cursor.getString(cursor.getColumnIndex(NETPAY_USER_CARD_HOLDER_LAST_NAME)));
            netpayData.setFullName(cursor.getString(cursor.getColumnIndex(NETPAY_USER_CARD_HOLDER_FULL_NAME)));
            netpayData.setBillToAddress(cursor.getString(cursor.getColumnIndex(NETPAY_USER_BILLING_ADDRESS)));
            netpayData.setBillToPostCode(cursor.getString(cursor.getColumnIndex(NETPAY_USER_BILLING_POSTCODE)));
            netpayData.setBillToTownCity(cursor.getString(cursor.getColumnIndex(NETPAY_USER_BILLING_CITY)));
            netpayData.setBillToCounty(cursor.getString(cursor.getColumnIndex(NETPAY_USER_BILLING_COUNTY)));
            netpayData.setBillToCountry(cursor.getString(cursor.getColumnIndex(NETPAY_USER_BILLING_COUNTRY)));
            netpayDatas.add(netpayData);
            cursor.moveToNext();
        }

        cursor.close();

        return netpayDatas;
    }


    /** Database Helper class for creating table */
	private class DatabaseHelper extends SQLiteOpenHelper {

		public DatabaseHelper(Context context) {
			// TODO Auto-generated constructor stub
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_TABLE_CART);
            db.execSQL(CREATE_TABLE_NETPAY_USER);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
            Log.e(TAG, "Upgrading Database from version " + oldVersion + " to " + newVersion + " version.");
            onCreate(db);
		}
	}
}
