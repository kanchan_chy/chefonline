package com.chefonline.volleyapicalls;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.datamodel.NetpayData;
import com.chefonline.modelinterface.VolleyApiInterface;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by masum on 22/08/2015.
 */
public class UpdatePaymentStatusApiCallback {

    VolleyApiInterface volleyApiInterface;

    public UpdatePaymentStatusApiCallback(Context context, String orderId, String paymentStatus, String transactionId, VolleyApiInterface volleyApiInterface) {
        this.volleyApiInterface = volleyApiInterface;
        callUpdatePaymentStatusApi(context, orderId, paymentStatus, transactionId);
    }

    /** api for forgot password */
    private void callUpdatePaymentStatusApi(final Context context, final String orderId, final String paymentStatus, final String transactionId) {
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "76");
                params.put("order_id", orderId);
                params.put("payment_status", paymentStatus);
                params.put("transection_id", transactionId);
                Log.e("update_status_params", params.toString());
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(myReq);


    }

    /** Response listener for login api */
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyApiInterface.onRequestSuccess(response);
            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyApiInterface.onRequestFailed(error);
            }
        };
    }

}
