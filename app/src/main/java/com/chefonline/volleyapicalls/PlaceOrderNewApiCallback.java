package com.chefonline.volleyapicalls;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.datamodel.NetpayData;
import com.chefonline.modelinterface.VolleyApiInterface;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by masum on 22/08/2015.
 */
public class PlaceOrderNewApiCallback {

    VolleyApiInterface volleyApiInterface;

    public PlaceOrderNewApiCallback(Context context, String paymentOption, VolleyApiInterface volleyApiInterface) {
        this.volleyApiInterface = volleyApiInterface;
        callPlaceOrderNewApi(context, paymentOption);
    }

    /** api for forgot password */
    private void callPlaceOrderNewApi(final Context context, final String paymentOption) {
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                NetpayData netpayData = AppData.getInstance().getNetpayData();
                PreferenceUtil preferenceUtil = new PreferenceUtil(context);

                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "77");
                params.put("OrderList", AppData.getInstance().getOrderPlaceData().getOrderList());
                params.put("rest_id", AppData.getInstance().getOrderPlaceData().getRestaurantId());
                params.put("user_id", preferenceUtil.getUserID());
                params.put("order_policy_id", AppData.getInstance().getOrderPlaceData().getPolicyId());
                params.put("post_code", AppData.getInstance().getOrderPlaceData().getPostCode());
                params.put("address", AppData.getInstance().getOrderPlaceData().getAddress());
                params.put("city", AppData.getInstance().getOrderPlaceData().getCity());
                params.put("payment_option", paymentOption);
                params.put("payment_status", AppData.getInstance().getOrderPlaceData().getPaymentStatus());
                params.put("paypal_transection_id", AppData.getInstance().getOrderPlaceData().getPaypalTransactionId());
                params.put("total_amount", AppData.getInstance().getOrderPlaceData().getTotalAmount().substring(1).trim());
                params.put("discount_id", AppData.getInstance().getOrderPlaceData().getDiscountId());
                params.put("grand_total", AppData.getInstance().getOrderPlaceData().getGrandTotal().substring(1).trim());
                params.put("offer_id", AppData.getInstance().getOrderPlaceData().getOfferId());
                params.put("pre_order_delivery_time", AppData.getInstance().getOrderPlaceData().getPreOrderTime());
                params.put("comments", AppData.getInstance().getOrderPlaceData().getComments());
                params.put("verification_code", AppData.getInstance().getOrderPlaceData().getVerificationCode());
                params.put("platform", "2");
                params.put("delivery_charge", AppData.getInstance().getOrderPlaceData().getDeliveryCharge());
                params.put("ip_address", AppData.getInstance().getOrderPlaceData().getIpAddress());
                params.put("user_address_ext_id", AppData.getInstance().getOrderPlaceData().getUserAddressExtId());
                params.put("card_fee", AppData.getInstance().getOrderPlaceData().getCardFee());
                params.put("inside_uk", AppData.getInstance().getOrderPlaceData().getInsideUk());
                params.put("is_varification_required", AppData.getInstance().getOrderPlaceData().getIsVerificationCodeRequired());
                params.put("is_special_message_required", AppData.getInstance().getOrderPlaceData().getIsSpecialMessageRequired());
                params.put("merchant_operation_type", "PURCHASE");
                params.put("transaction_amount", netpayData.getAmount());
                params.put("card_type", netpayData.getCardType());
                params.put("card_number", netpayData.getCardNumber());
                params.put("card_expiry_month", netpayData.getExpiryMonth());
                params.put("card_expiry_year", netpayData.getExpiryYear());
                params.put("card_security_code", netpayData.getSecurityCode());
                params.put("card_holder_title", netpayData.getTitle());
                params.put("card_holder_firstname", netpayData.getFirstName());
                params.put("card_holder_middlename", netpayData.getMiddleName());
                params.put("card_holder_lastname", netpayData.getLastName());
                params.put("card_holder_fullname", netpayData.getFullName());
                params.put("bill_to_company", netpayData.getBillToCompany());
                params.put("bill_to_address", netpayData.getBillToAddress());
                params.put("bill_to_postcode", netpayData.getBillToPostCode());
                params.put("bill_to_town_city", netpayData.getBillToTownCity());
                params.put("bill_to_county", netpayData.getBillToCounty());
                params.put("bill_to_country", netpayData.getBillToCountry());
                Log.e("placeorder_params", params.toString());
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(myReq);


    }

    /** Response listener for login api */
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyApiInterface.onRequestSuccess(response);
            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyApiInterface.onRequestFailed(error);
            }
        };
    }

}
