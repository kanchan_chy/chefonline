package com.chefonline.datamodel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by masum on 07/04/2015.
 */
public class HotRestaurantData implements Serializable{
    private int restaurantId;
    private String restaurantName;
    private String restaurantLogo;
    private String restaurantAddress1;
    private String restaurantAddress2;
    private String restaurantCity;
    private String restaurantDomain;
    private String restaurantLat;
    private String restaurantLon;
    private String businessTel;
    private String reservationTel;
    private String restaurantTelSingle;
    private String restaurantPayPalEmail;
    private String restaurantConcatedCuisineName;
    private int restaurantAcceptReservation;
    private int restaurantAcceptDelivery;
    private int restaurantAcceptCollection;
    private String netpayStatus;
    private String netpayChargeType;
    private String netpayCustomerCharge;
    private String comingSoon;
    private String discountAmount;
    private Double deliveryCharge;
    private String subTotal;
    private String grandTotal;
    private String orderId;
    private String restaurantDistance;
    private String postCode;
    private String orderDate;

    private String deliveryTime;
    private String collectionTime;
    private ArrayList<PolicyData> policyList;
    private ArrayList<DiscountData> discountList;
    private ArrayList<CuisineData> cuisineList;
    private ArrayList<ScheduleData> scheduleDatas;
    private ArrayList<DishData> dishDatas;
    private ArrayList<OffersData> offersDatas;

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getCollectionTime() {
        return collectionTime;
    }

    public void setCollectionTime(String collectionTime) {
        this.collectionTime = collectionTime;
    }


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public HotRestaurantData() {
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantAddress1() {
        return restaurantAddress1;
    }

    public void setRestaurantAddress1(String restaurantAddress1) {
        this.restaurantAddress1 = restaurantAddress1;
    }

    public String getRestaurantAddress2() {
        return restaurantAddress2;
    }

    public void setRestaurantAddress2(String restaurantAddress2) {
        this.restaurantAddress2 = restaurantAddress2;
    }

    public String getRestaurantCity() {
        return restaurantCity;
    }

    public void setRestaurantCity(String restaurantCity) {
        this.restaurantCity = restaurantCity;
    }

    public String getRestaurantDomain() {
        return restaurantDomain;
    }

    public void setRestaurantDomain(String restaurantDomain) {
        this.restaurantDomain = restaurantDomain;
    }

    public String getRestaurantLat() {
        return restaurantLat;
    }

    public void setRestaurantLat(String restaurantLat) {
        this.restaurantLat = restaurantLat;
    }

    public String getRestaurantLon() {
        return restaurantLon;
    }

    public void setRestaurantLon(String restaurantLon) {
        this.restaurantLon = restaurantLon;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getBusinessTel() {
        return businessTel;
    }

    public void setBusinessTel(String businessTel) {
        this.businessTel = businessTel;
    }

    public String getReservationTel() {
        return reservationTel;
    }

    public void setReservationTel(String reservationTel) {
        this.reservationTel = reservationTel;
    }

    public String getNetpayStatus() {
        return netpayStatus;
    }

    public void setNetpayStatus(String netpayStatus) {
        this.netpayStatus = netpayStatus;
    }

    public String getNetpayChargeType() {
        return netpayChargeType;
    }

    public void setNetpayChargeType(String netpayChargeType) {
        this.netpayChargeType = netpayChargeType;
    }

    public String getNetpayCustomerCharge() {
        return netpayCustomerCharge;
    }

    public void setNetpayCustomerCharge(String netpayCustomerCharge) {
        this.netpayCustomerCharge = netpayCustomerCharge;
    }

    public String getComingSoon() {
        return comingSoon;
    }

    public void setComingSoon(String comingSoon) {
        this.comingSoon = comingSoon;
    }

    public int getRestaurantAcceptReservation() {
        return restaurantAcceptReservation;
    }

    public void setRestaurantAcceptReservation(int restaurantAcceptReservation) {
        this.restaurantAcceptReservation = restaurantAcceptReservation;
    }

    public int getRestaurantAcceptDelivery() {
        return restaurantAcceptDelivery;
    }

    public void setRestaurantAcceptDelivery(int restaurantAcceptDelivery) {
        this.restaurantAcceptDelivery = restaurantAcceptDelivery;
    }

    public int getRestaurantAcceptCollection() {
        return restaurantAcceptCollection;
    }

    public void setRestaurantAcceptCollection(int restaurantAcceptCollection) {
        this.restaurantAcceptCollection = restaurantAcceptCollection;
    }

    public ArrayList<PolicyData> getPolicyList() {
        return policyList;
    }

    public void setPolicyList(ArrayList<PolicyData> policyList) {
        this.policyList = policyList;
    }

    public ArrayList<DiscountData> getDiscountList() {
        return discountList;
    }

    public void setDiscountList(ArrayList<DiscountData> discountList) {
        this.discountList = discountList;
    }

    public ArrayList<CuisineData> getCuisineList() {
        return cuisineList;
    }

    public void setCuisineList(ArrayList<CuisineData> cuisineList) {
        this.cuisineList = cuisineList;
    }

    public String getRestaurantDistance() {
        return restaurantDistance;
    }

    public void setRestaurantDistance(String restaurantDistance) {
        this.restaurantDistance = restaurantDistance;
    }

    public String getRestaurantLogo() {
        return restaurantLogo;
    }

    public void setRestaurantLogo(String restaurantLogo) {
        this.restaurantLogo = restaurantLogo;
    }

    public ArrayList<ScheduleData> getScheduleDatas() {
        return scheduleDatas;
    }

    public void setScheduleDatas(ArrayList<ScheduleData> scheduleDatas) {
        this.scheduleDatas = scheduleDatas;
    }

    public ArrayList<DishData> getDishDatas() {
        return dishDatas;
    }

    public void setDishDatas(ArrayList<DishData> dishDatas) {
        this.dishDatas = dishDatas;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public ArrayList<OffersData> getOffersDatas() {
        return offersDatas;
    }

    public void setOffersDatas(ArrayList<OffersData> offersDatas) {
        this.offersDatas = offersDatas;
    }

    public String getRestaurantPayPalEmail() {
        return restaurantPayPalEmail;
    }

    public void setRestaurantPayPalEmail(String restaurantPayPalEmail) {
        this.restaurantPayPalEmail = restaurantPayPalEmail;
    }

    public String getRestaurantTelSingle() {
        return restaurantTelSingle;
    }

    public void setRestaurantTelSingle(String restaurantTelSingle) {
        this.restaurantTelSingle = restaurantTelSingle;
    }

    public String getRestaurantConcatedCuisineName() {
        return restaurantConcatedCuisineName;
    }

    public void setRestaurantConcatedCuisineName(String restaurantConcatedCuisineName) {
        this.restaurantConcatedCuisineName = restaurantConcatedCuisineName;
    }

    public Double getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(Double deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }
}