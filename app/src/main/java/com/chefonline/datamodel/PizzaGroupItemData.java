package com.chefonline.datamodel;

/**
 * Created by user on 10/29/2016.
 */

public class PizzaGroupItemData {
    private String pizzaGroupItemId;
    private String itemName;
    private String itemPrice;
    private int defaultStatus;

    public String getPizzaGroupItemId() {
        return pizzaGroupItemId;
    }

    public void setPizzaGroupItemId(String pizzaGroupItemId) {
        this.pizzaGroupItemId = pizzaGroupItemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(int defaultStatus) {
        this.defaultStatus = defaultStatus;
    }
}
