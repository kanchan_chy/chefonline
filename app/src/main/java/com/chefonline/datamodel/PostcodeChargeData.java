package com.chefonline.datamodel;

/**
 * Created by user on 3/29/2016.
 */
public class PostcodeChargeData {

    private String zoneName;
    private String deliveryChargePerMile;
    private String deliveryRadius;
    private String deliveryCharge;
    private String minDeliveryCharge;
    private String minDeliveryAmount;

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getDeliveryChargePerMile() {
        return deliveryChargePerMile;
    }

    public void setDeliveryChargePerMile(String deliveryChargePerMile) {
        this.deliveryChargePerMile = deliveryChargePerMile;
    }

    public String getDeliveryRadius() {
        return deliveryRadius;
    }

    public void setDeliveryRadius(String deliveryRadius) {
        this.deliveryRadius = deliveryRadius;
    }

    public String getMinDeliveryCharge() {
        return minDeliveryCharge;
    }

    public void setMinDeliveryCharge(String minDeliveryCharge) {
        this.minDeliveryCharge = minDeliveryCharge;
    }

    public String getMinDeliveryAmount() {
        return minDeliveryAmount;
    }

    public void setMinDeliveryAmount(String minDeliveryAmount) {
        this.minDeliveryAmount = minDeliveryAmount;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }
}
