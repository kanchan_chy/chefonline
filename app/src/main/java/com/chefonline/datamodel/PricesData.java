package com.chefonline.datamodel;

/**
 * Created by masum on 26/04/2015.
 */
public class PricesData {
    private String totalAmount;
    private String totalSubAmount;
    private String discountAmunt;

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalSubAmount() {
        return totalSubAmount;
    }

    public void setTotalSubAmount(String totalSubAmount) {
        this.totalSubAmount = totalSubAmount;
    }

    public String getDiscountAmount() {
        return discountAmunt;
    }

    public void setDiscountAmount(String discountAmunt) {
        this.discountAmunt = discountAmunt;
    }

}
