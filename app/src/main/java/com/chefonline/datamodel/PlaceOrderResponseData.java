package com.chefonline.datamodel;

/**
 * Created by masum on 21/12/2015.
 */
public class PlaceOrderResponseData {
    private String deliveryCharge;
    private String ipAddress;
    private String userAddressExtId;
    private String cardFee;
    private String isVarificationRequired;
    private String isSpecialMessageRequired;
    private String isInsideUK;
    private int status;

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserAddressExtId() {
        return userAddressExtId;
    }

    public void setUserAddressExtId(String userAddressExtId) {
        this.userAddressExtId = userAddressExtId;
    }

    public String getCardFee() {
        return cardFee;
    }

    public void setCardFee(String cardFee) {
        this.cardFee = cardFee;
    }

    public String getIsVarificationRequired() {
        return isVarificationRequired;
    }

    public void setIsVarificationRequired(String isVarificationRequired) {
        this.isVarificationRequired = isVarificationRequired;
    }

    public String getIsSpecialMessageRequired() {
        return isSpecialMessageRequired;
    }

    public void setIsSpecialMessageRequired(String isSpecialMessageRequired) {
        this.isSpecialMessageRequired = isSpecialMessageRequired;
    }

    public String getIsInsideUK() {
        return isInsideUK;
    }

    public void setIsInsideUK(String isInsideUK) {
        this.isInsideUK = isInsideUK;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
