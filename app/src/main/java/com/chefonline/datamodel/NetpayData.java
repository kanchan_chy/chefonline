package com.chefonline.datamodel;

import java.util.ArrayList;

/**
 * Created by user on 9/5/2016.
 */
public class NetpayData {

    private String amount = "";
    private String cardType = "";
    private String cardNumber = "";
    private String expiryMonth = "";
    private String expiryYear = "";
    private String securityCode = "";
    private String title = "";
    private String firstName = "";
    private String middleName = "";
    private String lastName = "";
    private String fullName = "";
    private String billToCompany = "";
    private String billToAddress = "";
    private String billToPostCode = "";
    private String billToTownCity = "";
    private String billToCounty = "";
    private String billToCountry = "";


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBillToCompany() {
        return billToCompany;
    }

    public void setBillToCompany(String billToCompany) {
        this.billToCompany = billToCompany;
    }

    public String getBillToAddress() {
        return billToAddress;
    }

    public void setBillToAddress(String billToAddress) {
        this.billToAddress = billToAddress;
    }

    public String getBillToPostCode() {
        return billToPostCode;
    }

    public void setBillToPostCode(String billToPostCode) {
        this.billToPostCode = billToPostCode;
    }

    public String getBillToTownCity() {
        return billToTownCity;
    }

    public void setBillToTownCity(String billToTownCity) {
        this.billToTownCity = billToTownCity;
    }

    public String getBillToCountry() {
        return billToCountry;
    }

    public void setBillToCountry(String billToCountry) {
        this.billToCountry = billToCountry;
    }

    public String getBillToCounty() {
        return billToCounty;
    }

    public void setBillToCounty(String billToCounty) {
        this.billToCounty = billToCounty;
    }

}
