package com.chefonline.datamodel;

/**
 * Created by masum on 13/04/2015.
 */
public class OptionData {
    private String optionParentDishId;
    private String optionSelfId;
    private String optionName;
    private String optionPrice;
    private String optionDescription;

    public String getOptionParentDishId() {
        return optionParentDishId;
    }

    public void setOptionParentDishId(String optionParentDishId) {
        this.optionParentDishId = optionParentDishId;
    }

    public String getOptionSelfId() {
        return optionSelfId;
    }

    public void setOptionSelfId(String optionSelfId) {
        this.optionSelfId = optionSelfId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getOptionPrice() {
        return optionPrice;
    }

    public void setOptionPrice(String optionPrice) {
        this.optionPrice = optionPrice;
    }

    public String getOptionDescription() {
        return optionDescription;
    }

    public void setOptionDescription(String optionDescription) {
        this.optionDescription = optionDescription;
    }


}
