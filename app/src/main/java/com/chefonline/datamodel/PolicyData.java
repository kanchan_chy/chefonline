package com.chefonline.datamodel;

/**
 * Created by masum on 07/04/2015.
 */
public class PolicyData {
    private String policyId;
    private String policyName;
    private String policyTime;
    private String minOrder;
    private String status;

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public String getPolicyTime() {
        return policyTime;
    }

    public void setPolicyTime(String policyTime) {
        this.policyTime = policyTime;
    }

    public String getMinOrder() {
        return minOrder;
    }

    public void setMinOrder(String minOrder) {
        this.minOrder = minOrder;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }
}
