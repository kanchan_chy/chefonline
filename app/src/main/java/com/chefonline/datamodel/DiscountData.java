package com.chefonline.datamodel;

/**
 * Created by masum on 07/04/2015.
 */
public class DiscountData {
    private String discountId;
    private String restaurantOrderPolicyId;
    private String discountImage;
    private String discountTitle;
    private String discountDescription;
    private String discountType;
    private String discountAmount;
    private String discountEligibleAmount;
    private String defaultStatus;
    private int activeStatus;

    public String getDiscountTitle() {
        return discountTitle;
    }

    public void setDiscountTitle(String discountTitle) {
        this.discountTitle = discountTitle;
    }

    public String getDiscountDescription() {
        return discountDescription;
    }

    public void setDiscountDescription(String discountDescription) {
        this.discountDescription = discountDescription;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountEligibleAmount() {
        return discountEligibleAmount;
    }

    public void setDiscountEligibleAmount(String discountEligibleAmount) {
        this.discountEligibleAmount = discountEligibleAmount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getRestaurantOrderPolicyId() {
        return restaurantOrderPolicyId;
    }

    public void setRestaurantOrderPolicyId(String restaurantOrderPolicyId) {
        this.restaurantOrderPolicyId = restaurantOrderPolicyId;
    }

    public String getDiscountId() {
        return discountId;
    }

    public void setDiscountId(String discountId) {
        this.discountId = discountId;
    }

    public String getDiscountImage() {
        return discountImage;
    }

    public void setDiscountImage(String discountImage) {
        this.discountImage = discountImage;
    }

    public String getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(String defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public int getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(int activeStatus) {
        this.activeStatus = activeStatus;
    }

}
