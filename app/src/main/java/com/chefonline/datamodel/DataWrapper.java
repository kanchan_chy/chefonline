package com.chefonline.datamodel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by masum on 08/04/2015.
 */
public class DataWrapper implements Serializable {

    private ArrayList<HotRestaurantData> parliaments;

    public DataWrapper(ArrayList<HotRestaurantData> data) {
        this.parliaments = data;
    }

    public ArrayList<HotRestaurantData> getParliaments() {
        return this.parliaments;
    }

}
