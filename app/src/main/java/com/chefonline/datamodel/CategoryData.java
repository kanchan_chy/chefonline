package com.chefonline.datamodel;

import java.util.ArrayList;

/**
 * Created by masum on 04/04/2015.
 */
public class CategoryData {

    private String categoryId;
    private String categoryName;
    private String subcategoryId;
    private String cuisineId;
    private String cuisineName;
    private boolean isFirstCuisine;
    private boolean isCategory;
    private ArrayList<DishData> dishDatas = new ArrayList<>();

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getCuisineId() {
        return cuisineId;
    }

    public void setCuisineId(String cuisineId) {
        this.cuisineId = cuisineId;
    }

    public String getCuisineName() {
        return cuisineName;
    }

    public void setCuisineName(String cuisineName) {
        this.cuisineName = cuisineName;
    }

    public boolean isCategory() {
        return isCategory;
    }

    public void setCategory(boolean category) {
        isCategory = category;
    }

    public boolean isFirstCuisine() {
        return isFirstCuisine;
    }

    public void setFirstCuisine(boolean firstCuisine) {
        isFirstCuisine = firstCuisine;
    }

    public ArrayList<DishData> getDishDatas() {
        return dishDatas;
    }

    public void setDishDatas(ArrayList<DishData> dishDatas) {
        this.dishDatas = dishDatas;
    }
}
