package com.chefonline.datamodel;

/**
 * Created by masum on 15/03/2016.
 */
public class CollectionDeliveryData {
    private String delivery;
    private String collection;

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }
}
