package com.chefonline.datamodel;

import java.util.ArrayList;

/**
 * Created by user on 10/29/2016.
 */

public class PizzaGroupData {
    private int pizzaGroupId;
    private int minSelection;
    private int maxSelection;
    private ArrayList<PizzaGroupItemData> pizzaGroupItemList;

    public int getPizzaGroupId() {
        return pizzaGroupId;
    }

    public void setPizzaGroupId(int pizzaGroupId) {
        this.pizzaGroupId = pizzaGroupId;
    }

    public int getMinSelection() {
        return minSelection;
    }

    public void setMinSelection(int minSelection) {
        this.minSelection = minSelection;
    }

    public int getMaxSelection() {
        return maxSelection;
    }

    public void setMaxSelection(int maxSelection) {
        this.maxSelection = maxSelection;
    }

    public ArrayList<PizzaGroupItemData> getPizzaGroupItemList() {
        return pizzaGroupItemList;
    }

    public void setPizzaGroupItemList(ArrayList<PizzaGroupItemData> pizzaGroupItemList) {
        this.pizzaGroupItemList = pizzaGroupItemList;
    }
}
