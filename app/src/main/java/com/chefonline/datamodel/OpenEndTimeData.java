package com.chefonline.datamodel;

/**
 * Created by masum on 04/05/2015.
 */
public class OpenEndTimeData {
    private String openingTime;
    private String endTime;
    private String collection;
    private String delivery;
    private int shiftSerial;
    private String collectionPolicyId;
    private String deliveryPolicyId;
    private String lastCollectionSubmission;
    private String lastDeliverySubmission;
    private String type;
    private String minOrder;
    private String minOrderCollection;
    private String timingFor;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMinOrder() {
        return minOrder;
    }

    public void setMinOrder(String minOrder) {
        this.minOrder = minOrder;
    }

    public String getMinOrderCollection() {
        return minOrderCollection;
    }

    public void setMinOrderCollection(String minOrderCollection) {
        this.minOrderCollection = minOrderCollection;
    }

    public String getTimingFor() {
        return timingFor;
    }

    public void setTimingFor(String timingFor) {
        this.timingFor = timingFor;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public int getShiftSerial() {
        return shiftSerial;
    }

    public void setShiftSerial(int shiftSerial) {
        this.shiftSerial = shiftSerial;
    }

    public String getLastCollectionSubmission() {
        return lastCollectionSubmission;
    }

    public void setLastCollectionSubmission(String lastCollectionSubmission) {
        this.lastCollectionSubmission = lastCollectionSubmission;
    }

    public String getLastDeliverySubmission() {
        return lastDeliverySubmission;
    }

    public void setLastDeliverySubmission(String lastDeliverySubmission) {
        this.lastDeliverySubmission = lastDeliverySubmission;
    }

    public String getCollectionPolicyId() {
        return collectionPolicyId;
    }

    public void setCollectionPolicyId(String collectionPolicyId) {
        this.collectionPolicyId = collectionPolicyId;
    }

    public String getDeliveryPolicyId() {
        return deliveryPolicyId;
    }

    public void setDeliveryPolicyId(String deliveryPolicyId) {
        this.deliveryPolicyId = deliveryPolicyId;
    }
}
