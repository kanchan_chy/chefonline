package com.chefonline.datamodel;

import java.util.ArrayList;

/**
 * Created by masum on 04/05/2015.
 */
public class ScheduleData {
    private ArrayList<OpenEndTimeData> openEndTimeDatas = new ArrayList<>();
    private ArrayList<OpenEndTimeData> reservationDatas = new ArrayList<>();
    private String weekDayId;
    private String dayName;

    public ArrayList<OpenEndTimeData> getOpenEndTimeDatas() {
        return openEndTimeDatas;
    }

    public void setOpenEndTimeDatas(ArrayList<OpenEndTimeData> openEndTimeDatas) {
        this.openEndTimeDatas = openEndTimeDatas;
    }

    public ArrayList<OpenEndTimeData> getReservationDatas() {
        return reservationDatas;
    }

    public void setReservationDatas(ArrayList<OpenEndTimeData> reservationDatas) {
        this.reservationDatas = reservationDatas;
    }

    public String getWeekDayId() {
        return weekDayId;
    }

    public void setWeekDayId(String weekDayId) {
        this.weekDayId = weekDayId;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

}
