package com.chefonline.datamodel;

/**
 * Created by masum on 18/05/2015.
 */
public class OffersData {
    private String offerId;
    private String offerStatus;
    private String restaurantId;
    private String offerDescription;
    private String offerImage;
    private String offerTitle;
    private String offerThumb;
    private String offerElegibleAmount;
    private String restaurantOrderPolicyId;
    private String offerPosition;
    private String imageFrom;
    private String offerFor;
    private String defaultStatus;
    private int activeStatus;

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getOfferStatus() {
        return offerStatus;
    }

    public void setOfferStatus(String offerStatus) {
        this.offerStatus = offerStatus;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public String getOfferImage() {
        return offerImage;
    }

    public void setOfferImage(String offerImage) {
        this.offerImage = offerImage;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public String getOfferThumb() {
        return offerThumb;
    }

    public void setOfferThumb(String offerThumb) {
        this.offerThumb = offerThumb;
    }

    public String getOfferElegibleAmount() {
        return offerElegibleAmount;
    }

    public void setOfferElegibleAmount(String offerElegibleAmount) {
        this.offerElegibleAmount = offerElegibleAmount;
    }

    public String getRestaurantOrderPolicyId() {
        return restaurantOrderPolicyId;
    }

    public void setRestaurantOrderPolicyId(String restaurantOrderPolicyId) {
        this.restaurantOrderPolicyId = restaurantOrderPolicyId;
    }

    public String getOfferPosition() {
        return offerPosition;
    }

    public void setOfferPosition(String offerPosition) {
        this.offerPosition = offerPosition;
    }

    public String getImageFrom() {
        return imageFrom;
    }

    public void setImageFrom(String imageFrom) {
        this.imageFrom = imageFrom;
    }

    public String getOfferFor() {
        return offerFor;
    }

    public void setOfferFor(String offerFor) {
        this.offerFor = offerFor;
    }

    public int getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(int activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(String defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

}
