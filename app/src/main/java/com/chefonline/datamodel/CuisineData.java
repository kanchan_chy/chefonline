package com.chefonline.datamodel;

import java.util.ArrayList;

/**
 * Created by masum on 07/04/2015.
 */
public class CuisineData {

    private String cuisineId;
    private String cuisineName;
    private ArrayList<CategoryData> cuisineCategories;

    public String getCuisineId() {
        return cuisineId;
    }

    public void setCuisineId(String cuisineId) {
        this.cuisineId = cuisineId;
    }

    public String getCuisineName() {
        return cuisineName;
    }

    public void setCuisineName(String cuisineName) {
        this.cuisineName = cuisineName;
    }

    public ArrayList<CategoryData> getCuisineCategories() {
        return cuisineCategories;
    }

    public void setCuisineCategories(ArrayList<CategoryData> cuisineCategories) {
        this.cuisineCategories = cuisineCategories;
    }

}
