package com.chefonline.datamodel;

import java.util.ArrayList;

/**
 * Created by masum on 13/04/2015.
 */
public class DishData {
    private String dishId;
    private String dishName;
    private String dishDescription;
    private String dishRating;
    private String dishTotalRating;
    private String dishSpiceLevel;
    private String dishAllergens;
    private String dishPrice;
    private String dishAcmPrice;
    private String dishQty;
    private boolean isPizzaMenu;
    private ArrayList<PizzaGroupData> pizzaGroupList = new ArrayList<>();
    private ArrayList<PizzaGroupItemData> orderedPizzaItemList;
    private ArrayList<OptionData> optionDatas = new ArrayList<>();
    private ArrayList<AlergensData> alergensDatas = new ArrayList<>();

    public DishData() {
    }

    public String getDishDescription() {
        return dishDescription;
    }

    public void setDishDescription(String dishDescription) {
        this.dishDescription = dishDescription;
    }

    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public String getDishRating() {
        return dishRating;
    }

    public void setDishRating(String dishRating) {
        this.dishRating = dishRating;
    }

    public String getDishTotalRating() {
        return dishTotalRating;
    }

    public void setDishTotalRating(String dishTotalRating) {
        this.dishTotalRating = dishTotalRating;
    }

    public String getDishSpiceLevel() {
        return dishSpiceLevel;
    }

    public void setDishSpiceLevel(String dishSpiceLevel) {
        this.dishSpiceLevel = dishSpiceLevel;
    }

    public String getDishAllergens() {
        return dishAllergens;
    }

    public void setDishAllergens(String dishAllergens) {
        this.dishAllergens = dishAllergens;
    }


    public ArrayList<OptionData> getOptionDatas() {
        return optionDatas;
    }

    public void setOptionDatas(ArrayList<OptionData> optionDatas) {
        this.optionDatas = optionDatas;
    }

    public String getDishPrice() {
        return dishPrice;
    }

    public void setDishPrice(String dishPrice) {
        this.dishPrice = dishPrice;
    }

    public String getDishAcmPrice() {
        return dishAcmPrice;
    }

    public void setDishAcmPrice(String dishAcmPrice) {
        this.dishAcmPrice = dishAcmPrice;
    }

    public String getDishQty() {
        return dishQty;
    }

    public void setDishQty(String dishQty) {
        this.dishQty = dishQty;
    }

    public ArrayList<AlergensData> getAlergensDatas() {
        return alergensDatas;
    }

    public void setAlergensDatas(ArrayList<AlergensData> alergensDatas) {
        this.alergensDatas = alergensDatas;
    }

    public boolean isPizzaMenu() {
        return isPizzaMenu;
    }

    public void setPizzaMenu(boolean pizzaMenu) {
        isPizzaMenu = pizzaMenu;
    }

    public ArrayList<PizzaGroupData> getPizzaGroupList() {
        return pizzaGroupList;
    }

    public void setPizzaGroupList(ArrayList<PizzaGroupData> pizzaGroupList) {
        this.pizzaGroupList = pizzaGroupList;
    }

    public ArrayList<PizzaGroupItemData> getOrderedPizzaItemList() {
        return orderedPizzaItemList;
    }

    public void setOrderedPizzaItemList(ArrayList<PizzaGroupItemData> orderedPizzaItemList) {
        this.orderedPizzaItemList = orderedPizzaItemList;
    }

}
