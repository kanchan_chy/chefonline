package com.chefonline.datamodel;

/**
 * Created by masum on 15/04/2015.
 */
public class CartData {
    private int itemId;
    private String itemName;
    private String itemPizzaIds;
    private String itemPizzaNames;
    private int itemQty;
    private float price;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemPizzaIds() {
        return itemPizzaIds;
    }

    public void setItemPizzaIds(String itemPizzaIds) {
        this.itemPizzaIds = itemPizzaIds;
    }

    public String getItemPizzaNames() {
        return itemPizzaNames;
    }

    public void setItemPizzaNames(String itemPizzaNames) {
        this.itemPizzaNames = itemPizzaNames;
    }

    public int getItemQty() {
        return itemQty;
    }

    public void setItemQty(int itemQty) {
        this.itemQty = itemQty;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }


}
