package com.chefonline.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.adapter.SearchItemAdapter;
import com.chefonline.datamodel.CartData;
import com.chefonline.datamodel.DishData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.online.R;
import com.chefonline.online.VoiceRecoActivity;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UtilityMethod;
import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by masum on 07/05/2015.
 */
public class SearchCategoryFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static String TAG = "SearchCategoryFragment";
    public int position = 0;
    public int is_searched_list = 0;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    private TextView txtViewCategoryName;
    public static EditText editTextSearch;
    public static  TextView txtViewBubble;
    private ListView lstViewCategorySearched;
    private FloatingGroupExpandableListView mListViewCategory;
    private ImageButton imgBtnCatMenu, imgBtnBack, imgBtnSearch;
    private RelativeLayout rltvMenuBtnHolder;
    private LinearLayout linear_menu;
    private View headerView;
    private ProgressBar progressBar;

    private SearchItemAdapter searhItemAdapter;
    private DataBaseUtil db;
    View rootView;
    private ArrayList<DishData> dishNewData = new ArrayList<>();
    private long mLastClickTime = 0;

    @Override
    public void onDestroyView() {
        Log.i(TAG, "ON DESTROY");
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        UtilityMethod.cancelAllToast();
        super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "ON CREATE VIEW");
        rootView = inflater.inflate(R.layout.fragment_category_search, container, false);
        getIntentValue();
        ConstantValues.DISH_MENU_TOASTS = new ArrayList<>();
        initView(rootView);
        instantiateObject();
        initTabView(rootView);
        setUiItemListener();
        loadData();
        return rootView;
    }

    private void instantiateObject() {
    }

    private void initTabView(View rootView) {
        txtViewBubble = (TextView) getActivity().findViewById(R.id.txtViewBubble);
    }

    private void setUiItemListener() {
        imgBtnSearch.setOnClickListener(this);

        lstViewCategorySearched.setOnItemClickListener(this);
        mListViewCategory.setOnItemClickListener(this);

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                String text = editTextSearch.getText().toString().toLowerCase(Locale.getDefault());
                Log.i("Text", "** " + text);

                if (text.length() == 0) {
                    //this.dishDatas.addAll(dishDatasTemp);
                    dishNewData = AppData.getInstance().getDishDatas();
                    Log.e("SEARCHED ITEM GET", "SIZE***** " + AppData.getInstance().getDishDatas().size());
                    searhItemAdapter.notifyDataSetChanged();
                    lstViewCategorySearched.setAdapter(searhItemAdapter);

                } else {
                    searhItemAdapter.filter(text);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });

    }

    private void loadData() {
        this.db = new DataBaseUtil(getActivity());
        this.db.open();
        ArrayList<CartData> cartDatas = new ArrayList<>();
        AppData.getInstance().setCartDatas(cartDatas);
        /** End cart data fetching */

        lstViewCategorySearched.invalidateViews();
        dishNewData = AppData.getInstance().getDishDatas();
        Log.e("SEARCHED ITEM GET", "SIZE***** " + AppData.getInstance().getDishDatas().size());
        Log.e(TAG, "Restaurant id ***** " + Integer.toString(AppData.getInstance().getRestaurantInfoData().getRestaurantId()));
        searhItemAdapter = new SearchItemAdapter(getActivity(), AppData.getInstance().getDishDatas(), Integer.toString(AppData.getInstance().getRestaurantInfoData().getRestaurantId()));
        lstViewCategorySearched.setAdapter(searhItemAdapter);

        /*try {
            editTextSearch.setText(getArguments().getString("result"));
            searhItemAdapter.filter(getArguments().getString("result"));
        } catch (Exception e) {

        }*/

       // callDishApi();

    }
    /** Init view method*/
    public void initView(View rootView) {
        txtViewCategoryName = (TextView) rootView.findViewById(R.id.txtViewCategoryName);
        txtViewBubble = (TextView) rootView.findViewById(R.id.txtViewBubble);
        imgBtnBack = (ImageButton) rootView.findViewById(R.id.imgBtnBack);
        imgBtnSearch = (ImageButton) rootView.findViewById(R.id.imgBtnSearch);
        editTextSearch = (EditText) rootView.findViewById(R.id.editTextSearch);

        lstViewCategorySearched = (ListView) rootView.findViewById(R.id.lstViewCategorySearched1);

        mListViewCategory = (FloatingGroupExpandableListView) rootView.findViewById(R.id.lstViewCategory);
        /** Instantiate header item of expandable item list view */
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        headerView = inflater.inflate(R.layout.header_dishes_list, null);

        mListViewCategory.addHeaderView(headerView);

        View footerView = getActivity().getLayoutInflater().inflate(R.layout.footer_fake, null);
        mListViewCategory.addFooterView(footerView);
        mListViewCategory.setVisibility(View.GONE);

        lstViewCategorySearched.setVisibility(View.VISIBLE);
        lstViewCategorySearched.addFooterView(footerView);
        /** End header item load */

        rltvMenuBtnHolder = (RelativeLayout) rootView.findViewById(R.id.relative_top_menu);
        linear_menu = (LinearLayout) rootView.findViewById(R.id.linear_menu);
        rltvMenuBtnHolder.setVisibility(View.VISIBLE);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        /** search view load */
        mListViewCategory.setVisibility(View.GONE);
        lstViewCategorySearched.setVisibility(View.VISIBLE);
        imgBtnSearch.setBackgroundResource(R.drawable.voice_order);
        editTextSearch.setVisibility(View.VISIBLE);
        linear_menu.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        /* End Search view */
    }

    public String getIntentValue() {
        position = getArguments().getInt("position");
        is_searched_list = getArguments().getInt("is_searched_list");
        String x = getArguments().getString("restaurant_id");
        Log.i("Restaurant_Id ", "***** " + x);
        return x;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == getActivity().RESULT_OK && null != data) {
                    Log.i("Final Text", "****" + data.getExtras().getStringArrayList("data"));
                    ArrayList<String> result = new ArrayList<String>();
                    result = data.getExtras().getStringArrayList("data");

                    try {
                        editTextSearch.setText(result.get(0));
                    } catch (Exception e) {

                    }
                }
                break;
            }

        }
    }

    @Override
    public void onResume() {
        txtViewBubble.setText("" + db.sumCartData());
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        switch (v.getId()) {
            case R.id.imgBtnSearch:
                Intent intentSearch = new Intent(getActivity(), VoiceRecoActivity.class);
                startActivityForResult(intentSearch, 100);
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //lastClickedPosition = position;
    }

}