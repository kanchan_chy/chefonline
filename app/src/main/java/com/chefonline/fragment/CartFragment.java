package com.chefonline.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chefonline.adapter.CartAdapter;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.datamodel.CartData;
import com.chefonline.datamodel.OfferListData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.online.MainActivity;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.TimeManagement;
import com.chefonline.utility.UtilityMethod;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CartFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener{
    private static String TAG = "CartFragment";
    private int position = 0;
    private int selectedServiceNo = 0;
    public static String policyId = "";
    private String policyTime = "";
    public static int IS_DISCOUNT_SELECTED = 1;
    public static int IS_OFFER_SELECTED = 2;
    public static int SELECTED_OFFER = 0;
    public static String offerId = "";
    public static String discountId = "";
    private boolean collectionEnabled=false,deliveryEnabled=false;

    private DataBaseUtil db;
    private ListView listViewCart;
    private Button btnPlaceOrder, btnCollection, btnDelivery;
    public static Dialog dialog;

    View rootView;
    ArrayList<CartData> cartDataForTotal = new ArrayList<>();
    ArrayList<OfferListData> offersStrings;

    private TextView txtViewRestaurant, txtViewVat, txtViewDiscount, txtViewDeliveryFee, txtViewTotal, txtViewSubTotal, textViewAddMore;
    public static TextView txtViewOfferText;
    public static TextView txtViewCancelOffer;
    ArrayList<CartData> cartDatas;

    private long mLastClickTime = 0;

        public CartFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Order");
        } catch (Exception e) {
            Log.e("activity_error", "" + e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Order");
    }

    @Override
    public void onDestroyView() {
        SELECTED_OFFER = 0;
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_cart, container, false);
        initView(rootView);
        setUiListener();
        try {
            loadData();
            enableServiceVisibility();
            calculateTotalAmount(rootView);
            setDeliveryOrCollection();
        }
        catch (Exception e3) {
            new CustomToast(getActivity(), "Something went wrong", "", false);
        }
        return rootView;
    }

    private void initView(View rootView) {
        txtViewRestaurant = (TextView) rootView.findViewById(R.id.txtViewRestaurant);
        txtViewDiscount = (TextView) rootView.findViewById(R.id.txtViewDiscount);
        txtViewVat = (TextView) rootView.findViewById(R.id.txtViewVat);
        txtViewDeliveryFee = (TextView) rootView.findViewById(R.id.txtViewDeliveryFee);
        txtViewOfferText = (TextView) rootView.findViewById(R.id.txtViewOfferText);
        txtViewSubTotal = (TextView) rootView.findViewById(R.id.txtViewSubTotal);
        txtViewTotal = (TextView) rootView.findViewById(R.id.txtViewTotal);
        textViewAddMore = (TextView) rootView.findViewById(R.id.textViewAddMore);
        txtViewCancelOffer = (TextView) rootView.findViewById(R.id.txtViewCancelOffer);

        //Service selection Button
        btnCollection = (Button) rootView.findViewById(R.id.btnCollection);
        btnDelivery = (Button) rootView.findViewById(R.id.btnDelivery);

        /** Button place order */
        btnPlaceOrder = (Button) rootView.findViewById(R.id.btnPlaceOrder);

        listViewCart = (ListView) rootView.findViewById(R.id.listViewCart);

    }

    private void setUiListener() {
        listViewCart.setOnItemClickListener(this);
        btnPlaceOrder.setOnClickListener(this);
        textViewAddMore.setOnClickListener(this);
        txtViewCancelOffer.setOnClickListener(this);

        btnCollection.setOnClickListener(this);
        btnDelivery.setOnClickListener(this);
    }

    private void loadData() {
        txtViewRestaurant.setAlpha(0);
        txtViewRestaurant.setText(AppData.getInstance().getRestaurantInfoData().getRestaurantName());
        txtViewRestaurant.animate().alpha(1).start();

        db = new DataBaseUtil(getActivity());
        db.open();

        cartDatas = new ArrayList<>();
        cartDatas = db.fetchCartData();
        CartAdapter cartAdapter = new CartAdapter(getActivity(), getTargetFragment(), cartDatas);
        listViewCart.setAdapter(cartAdapter);

    }

    private void setDeliveryOrCollection() {
        if(ConstantValues.SELECTED_BUTTON != ConstantValues.NO_POLICY_BUTTON_SELECTION) {
          if(ConstantValues.SELECTED_BUTTON == ConstantValues.COLLECTION) {
              btnDelivery.setVisibility(View.GONE);
              chooseService(ConstantValues.SERVICE_COLLECTION);
          } else if(ConstantValues.SELECTED_BUTTON == ConstantValues.DELIVERY) {
              btnCollection.setVisibility(View.GONE);
              chooseService(ConstantValues.SERVICE_DELIVERY);
          }
        } else {
            if(collectionEnabled && deliveryEnabled) {
                //chooseService(ConstantValues.SERVICE_COLLECTION);
            } else {
                if(collectionEnabled) {
                    chooseService(ConstantValues.SERVICE_COLLECTION);
                } else {
                    chooseService(ConstantValues.SERVICE_DELIVERY);
                }
            }
        }
    }

    public void enableServiceVisibility() {

        try {
            int policyListSize = AppData.getInstance().getRestaurantInfoData().getPolicyList().size();
            for (int i = 0; i < policyListSize; i++) {
                if (AppData.getInstance().getRestaurantInfoData().getPolicyList().get(i).getStatus().equalsIgnoreCase("1")
                        && AppData.getInstance().getRestaurantInfoData().getPolicyList().get(i).getPolicyName().equalsIgnoreCase("Collection")) {
                    if(AppData.getInstance().getRestaurantInfoData().getCollectionTime() != null && !"".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getCollectionTime())) {

                        if (policyListSize == 1) {
                            btnCollection.setBackgroundResource(R.drawable.round_red_button_default);
                        }

                        btnCollection.setVisibility(View.VISIBLE);
                        collectionEnabled = true;
                        if(isBetweenAnyShift(true)) {
                            //btnCollection.setText("COLLECTION / " + AppData.getInstance().getRestaurantInfoData().getPolicyList().get(i).getPolicyTime() + " Mins");
                            btnCollection.setText("COLLECTION / " + AppData.getInstance().getRestaurantInfoData().getCollectionTime() + " Mins");
                        } else {
                            btnCollection.setText("COLLECTION");
                        }

                    }
                }

                if (AppData.getInstance().getRestaurantInfoData().getPolicyList().get(i).getStatus().equalsIgnoreCase("1")
                        && AppData.getInstance().getRestaurantInfoData().getPolicyList().get(i).getPolicyName().equalsIgnoreCase("Delivery")) {

                    if(AppData.getInstance().getRestaurantInfoData().getDeliveryTime() != null && !"".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDeliveryTime())) {

                        if (policyListSize == 1) {
                            btnDelivery.setBackgroundResource(R.drawable.round_red_button_default);
                        }
                        btnDelivery.setVisibility(View.VISIBLE);
                        deliveryEnabled = true;
                        if(isBetweenAnyShift(false)) {
                            //btnDelivery.setText("DELIVERY / " + AppData.getInstance().getRestaurantInfoData().getPolicyList().get(i).getPolicyTime() + " Mins");
                            btnDelivery.setText("DELIVERY / " + AppData.getInstance().getRestaurantInfoData().getDeliveryTime() + " Mins");
                        }
                        else btnDelivery.setText("DELIVERY");

                    }

                }
            }

        } catch (Exception e) {
            new CustomToast(getActivity(), "Something went wrong. Please reopen the app", "", false);
        }

    }


    private boolean isBetweenAnyShift(boolean isCollection)
    {
        String OPENING_TIME_ONE, CLOSING_TIME_ONE = "", OPENING_TIME_TWO, CLOSING_TIME_TWO;
        String currentDay = new SimpleDateFormat("EEEE").format(new Date());

        int shiftCount = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().size();
        for (int i = 0; i < shiftCount; i++) {
            if (AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getDayName().equalsIgnoreCase(currentDay)) {
                int timeListLength = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().size();
                for (int j = 0; j < timeListLength; j++) {
                    String collectionTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getCollection();
                    String deliveryTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getDelivery();
                    if(isCollection) {
                        if(collectionTime == null || "".equalsIgnoreCase(collectionTime)) {
                            continue;
                        }
                    } else {
                        if(deliveryTime == null || "".equalsIgnoreCase(deliveryTime)) {
                            continue;
                        }
                    }
                    if (j == 0) {
                        OPENING_TIME_ONE = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getOpeningTime();
                        CLOSING_TIME_ONE = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getEndTime();
                       // timeArrayList = TimeManagement.makeTimeList(OPENING_TIME_ONE, CLOSING_TIME_ONE, Integer.parseInt(policyTime));

                        // When restaurant open
                        if (TimeManagement.isInMiddleTime(OPENING_TIME_ONE, CLOSING_TIME_ONE)) {
                            return true;
                        }
                    }

                    //Second sift
                    if (j == 1) {
                        OPENING_TIME_TWO = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getOpeningTime();
                        CLOSING_TIME_TWO = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getEndTime();

                        if (TimeManagement.isInMiddleTime(OPENING_TIME_TWO, CLOSING_TIME_TWO)) {
                            return true;
                        }

                    }

                    //Log.i("OPENING TIME", "****" + AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getOpeningTime());
                    // Log.i("END TIME", "****" + AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().get(j).getEndTime());
                }
            }
        }
        return false;
    }


    private boolean isCollectionEligible(){
        int listSize = AppData.getInstance().getRestaurantInfoData().getPolicyList().size();
        for (int i = 0; i < listSize; i++) {
            if (AppData.getInstance().getRestaurantInfoData().getPolicyList().get(i).getPolicyName().equalsIgnoreCase("Collection")) {
                position = i;
                policyId = AppData.getInstance().getRestaurantInfoData().getPolicyList().get(i).getPolicyId();
                policyTime = AppData.getInstance().getRestaurantInfoData().getPolicyList().get(i).getPolicyTime();
            }
        }

        if ( Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getPolicyList().get(position).getMinOrder()) > getTotalCartPrice()) {
            return false;
        }
        return true;
    }


    private boolean isDeliveryEligible(){
        int listSize = AppData.getInstance().getRestaurantInfoData().getPolicyList().size();
        for (int i = 0; i < listSize; i++) {
            if (AppData.getInstance().getRestaurantInfoData().getPolicyList().get(i).getPolicyName().equalsIgnoreCase("Delivery")) {
                position = i;
                policyId = AppData.getInstance().getRestaurantInfoData().getPolicyList().get(i).getPolicyId();
                policyTime = AppData.getInstance().getRestaurantInfoData().getPolicyList().get(i).getPolicyTime();
            }

        }

        if ( Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getPolicyList().get(position).getMinOrder())
                > getTotalCartPrice()) {
            return false;
        }
        return true;
    }



    private void chooseService(int getServiceNo) {
        if (getServiceNo == ConstantValues.SERVICE_DELIVERY) {
            int listSize = 0;
            if(AppData.getInstance().getRestaurantInfoData().getPolicyList()!=null) listSize = AppData.getInstance().getRestaurantInfoData().getPolicyList().size();

            selectedServiceNo = ConstantValues.SERVICE_DELIVERY;

            btnCollection.setBackgroundResource(R.drawable.left_round_button_default);
            btnCollection.setTextColor(getResources().getColor(R.color.black));
            btnCollection.setTypeface(null, Typeface.NORMAL);

            if (btnCollection.getVisibility() != View.VISIBLE) {
                btnDelivery.setBackgroundResource(R.drawable.round_red_button_selected);
            } else {
                btnDelivery.setBackgroundResource(R.drawable.right_round_button_selected);
            }

            btnDelivery.setTextColor(getResources().getColor(R.color.white));
            btnDelivery.setTypeface(null, Typeface.BOLD);

        } else if (getServiceNo == ConstantValues.SERVICE_COLLECTION) {
            int listSize = 0;
            if(AppData.getInstance().getRestaurantInfoData().getPolicyList() != null) listSize = AppData.getInstance().getRestaurantInfoData().getPolicyList().size();

            selectedServiceNo = ConstantValues.SERVICE_COLLECTION;

            btnDelivery.setBackgroundResource(R.drawable.right_round_button_default);
            btnDelivery.setTextColor(getResources().getColor(R.color.black));
            btnDelivery.setTypeface(null, Typeface.NORMAL);

            if (btnDelivery.getVisibility() != View.VISIBLE) {
                btnCollection.setBackgroundResource(R.drawable.round_red_button_selected);
            } else {
                btnCollection.setBackgroundResource(R.drawable.left_round_button_selected);
            }

            btnCollection.setTextColor(getResources().getColor(R.color.white));
            btnCollection.setTypeface(null, Typeface.BOLD);

        }


    }

    public Double getTotalCartPrice() {
        Double totalSubAmount = 0.0;
        cartDataForTotal = db.fetchCartData();
        for (int i = 0; i < cartDataForTotal.size(); i++) {
            totalSubAmount += (cartDataForTotal.get(i).getItemQty() * cartDataForTotal.get(i).getPrice());
        }
        return totalSubAmount;
    }

    /** Calculate Total Amount */
    private double calculateTotalAmount(View rootView) {
        Double totalSubAmount = 0.0;
        totalSubAmount = getTotalCartPrice();
        TextView txtViewSubTotal = (TextView)  rootView.findViewById(R.id.txtViewSubTotal);
        txtViewSubTotal.setText("£" + String.format("%.2f", totalSubAmount));

        TextView txtViewTotal = (TextView) rootView.findViewById(R.id.txtViewTotal);
        TextView txtViewDiscount = (TextView) rootView.findViewById(R.id.txtViewDiscount);

        txtViewTotal.setText("£" + String.format("%.2f", totalSubAmount));
        txtViewDiscount.setText("£0.0");

        /** End total amount */
        return totalSubAmount;
    }

    private void offerCalculation () {
        offersStrings = new ArrayList<>();
        int offerSize = AppData.getInstance().getRestaurantInfoData().getOffersDatas().size();
        for (int i = 0; i < offerSize; i++) {

            OfferListData offerListData = new OfferListData();
            Log.i(TAG, "Offers Id***" + AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferTitle());
            if (policyId.equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getRestaurantOrderPolicyId())
                    || "0".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getRestaurantOrderPolicyId())) {

                if(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getActiveStatus() == 1) {
                    offerListData.setOfferText(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferTitle());
                    offerListData.setOfferCode("2");
                    offerListData.setPolicyId(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getRestaurantOrderPolicyId());
                    offerListData.setAmount(0.0f);

                    if (getTotalCartPrice() >= Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferElegibleAmount())) {
                        offersStrings.add(offerListData);
                    }
                }

            }

        }

        int discountSize = AppData.getInstance().getRestaurantInfoData().getDiscountList().size();
        for (int i = 0; i < discountSize; i++) {
            OfferListData offerListData = new OfferListData();
            if (policyId.equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getRestaurantOrderPolicyId())
                    || "0".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getRestaurantOrderPolicyId())) {

                if(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getActiveStatus() == 1) {
                    //offersStrings.add(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountTitle());
                    offerListData.setOfferText(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountTitle());
                    offerListData.setOfferCode("1");
                    offerListData.setPolicyId(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getRestaurantOrderPolicyId());
                    offerListData.setAmount(Float.parseFloat(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount()));

                    if (getTotalCartPrice() >= Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountEligibleAmount())) {
                        offersStrings.add(offerListData);
                    }
                }

            }
        }


    }

    private void loadFragmentManager(Bundle args, android.app.Fragment fragment){
        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.add(R.id.content_frame, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    private void loadLoginFragment(int navigationValue) {
        ArrayList<String>allOfferTexts = new ArrayList<>();
        android.app.Fragment fragment = null;
        fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putInt("selected_service", selectedServiceNo);
        args.putString("selected_policy_id", policyId);
        args.putString("selected_policy_time", policyTime);
        args.putString("grand_sub_total", txtViewTotal.getText().toString());
        args.putString("discount_amount", "£0.0");
        args.putString("total_amount", txtViewSubTotal.getText().toString());
        args.putString("offer_text", "");
        args.putSerializable("all_offer_texts", allOfferTexts);
        args.putString("offer_id", offerId);
        args.putString("discount_id", discountId);
        args.putInt("selected_offer", 0);
        args.putInt("is_navigate_to_offers", navigationValue);
        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        //ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.add(R.id.content_frame, fragment, "LOGIN_FRAGMENT");
        ft.addToBackStack(null);
        ft.commit();
    }

    private void loadOffersFragment() {
        android.app.Fragment fragment = null;
        fragment = new OffersCalculationFragment();
        Bundle args = new Bundle();
        args.putInt("selected_service", selectedServiceNo);
        args.putString("selected_policy_id", policyId);
        args.putString("selected_policy_time", policyTime);
        args.putInt("selected_offer", 0);
        loadFragmentManager(args, fragment); // Call load fragment

    }

    private void loadPaymentSelectFragment() {
        ArrayList<String>allOfferTexts = new ArrayList<>();
        android.app.Fragment fragment = null;
        fragment = new CheckoutFragment();
        Bundle args = new Bundle();
        args.putInt("selected_service", selectedServiceNo);
        args.putString("selected_policy_id", policyId);
        args.putString("selected_policy_time", policyTime);
        args.putString("grand_sub_total", txtViewTotal.getText().toString());
        args.putString("discount_amount", txtViewDiscount.getText().toString());
        args.putString("total_amount", txtViewSubTotal.getText().toString());
        args.putString("offer_text", "");
        args.putSerializable("all_offer_texts", allOfferTexts);
        args.putString("offer_id", offerId);
        args.putString("discount_id", discountId);
        args.putInt("selected_offer", 0);
        loadFragmentManager(args, fragment); // Call load fragment

    }

    private void openDialog(String message, String okButton) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_message);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(okButton);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPlaceOrder:

                if("1".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getComingSoon())) {
                    openDialog("Restaurant is coming soon. Collection or Delivery is not possible right now.", "OK");
                    return;
                }

                if (db.sumCartData() == 0) {
                    new CustomToast(getActivity(), "Your cart is empty.", "", false);
                    return;
                }

                if (selectedServiceNo == 0) {
                    if(collectionEnabled && deliveryEnabled){
                        openDialog("Please select COLLECTION or DELIVERY.", "OK");
                    }
                    else{
                        if(collectionEnabled){
                            openDialog("Please select COLLECTION.", "OK");
                        }
                        else {
                            openDialog("Please select DELIVERY.", "OK");
                        }
                    }
                    return;
                }

                TimeManagement timeManagement = new TimeManagement();
                if(selectedServiceNo == ConstantValues.SERVICE_DELIVERY) {
                    if(timeManagement.isDeliveryTimeOver()) {
                        openDialog("Delivery time is over today.", "OK");
                        return;
                    }
                }

                if(selectedServiceNo == ConstantValues.SERVICE_COLLECTION) {
                    if(timeManagement.isCollectionTimeOver()) {
                        openDialog("Collection time is over today.", "OK");
                        return;
                    }
                }

                if (selectedServiceNo == ConstantValues.SERVICE_DELIVERY){
                    if(!isDeliveryEligible()){
                        openDialog("Minimum order amount for Delivery is £" + AppData.getInstance().getRestaurantInfoData().getPolicyList().get(position).getMinOrder(), "OK");
                        return;
                    }
                }
                else {
                    if(!isCollectionEligible()) {
                        openDialog("Minimum order amount for Collection is £" + AppData.getInstance().getRestaurantInfoData().getPolicyList().get(position).getMinOrder(), "OK");
                        return;
                    }
                }

                //Set all cart item to the singleton class
                offerCalculation ();
                AppData.getInstance().setCartDatas(cartDatas);
                PreferenceUtil preferenceUtil = new PreferenceUtil(getActivity());
                if (preferenceUtil.getLogInStatus() == 1) {
                    cartDatas = db.fetchCartData();
                    if (offersStrings.size() > 0) {
                        loadOffersFragment();
                    } else {
                        loadPaymentSelectFragment();
                    }

                } else {
                    if (offersStrings.size() > 0) {
                        loadLoginFragment(1);
                    } else {
                        loadLoginFragment(0);
                    }

                }

                break;

            case R.id.imgBtnBack:
                getActivity().onBackPressed();
                break;

            case R.id.txtViewCancelOffer:
                if (SELECTED_OFFER == IS_DISCOUNT_SELECTED) {
                    txtViewTotal.setText(txtViewSubTotal.getText().toString());
                    txtViewDiscount.setText("£0.00");
                }

                SELECTED_OFFER = 0;
                txtViewOfferText.setText("");
                txtViewCancelOffer.setVisibility(View.GONE);
                break;

            case R.id.btnDelivery:
                chooseService(ConstantValues.SERVICE_DELIVERY);

                break;
            case R.id.btnCollection:
                 chooseService(ConstantValues.SERVICE_COLLECTION);

                break;
            case R.id.textViewAddMore:
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }

                if (ConstantValues.IS_REORDER) {
                    ConstantValues.IS_REORDER = false;
                    Log.i(TAG, "Rest Id " + AppData.getInstance().getRestaurantInfoData().getRestaurantId());
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(ConstantValues.NAVIGATION_KEY, ConstantValues.FRAGMENT_CATEGORY);
                    intent.putExtra("restaurant_id", Integer.toString(AppData.getInstance().getRestaurantInfoData().getRestaurantId()));
                    intent.putExtra("selected_policy", ConstantValues.NO_POLICY_BUTTON_SELECTION);
                    intent.putExtra("delivery_time", AppData.getInstance().getRestaurantInfoData().getDeliveryTime());
                    intent.putExtra("collection_time", AppData.getInstance().getRestaurantInfoData().getCollectionTime());
                    startActivity(intent);
                    getActivity().finish();

                } else {
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(ConstantValues.NAVIGATION_KEY, ConstantValues.FRAGMENT_CATEGORY);
                    intent.putExtra("restaurant_id", Integer.toString(AppData.getInstance().getRestaurantInfoData().getRestaurantId()));
                    intent.putExtra("selected_policy", ConstantValues.SELECTED_BUTTON);
                    intent.putExtra("delivery_time", AppData.getInstance().getRestaurantInfoData().getDeliveryTime());
                    intent.putExtra("collection_time", AppData.getInstance().getRestaurantInfoData().getCollectionTime());
                    startActivity(intent);
                    getActivity().finish();

                }

                break;
        }
    }

}
