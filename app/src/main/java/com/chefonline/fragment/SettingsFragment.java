package com.chefonline.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.online.LoginActivity;
import com.chefonline.online.MyAccountActivity;
import com.chefonline.online.R;
import com.chefonline.online.ResetPasswordActivity;
import com.chefonline.online.MainActivity;
import com.chefonline.utility.ConstantValues;

import java.util.Calendar;
import java.util.Locale;

public class SettingsFragment extends Fragment implements View.OnClickListener {

    public static Button btnLogin;
    public static Button btnMyAccount;
    public static Button btnResetPassword;
    public static Button btnOrderHistory;

    Button btnTerms;
    Button btnPrivacy;
    Button btnCookiePolicy;
    Button btnAbout;
    Button btnVersion;

    Button btnVoiceCommand;
    PreferenceUtil preferenceUtil;
    private long mLastClickTime = 0;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //btnTabVoiceOrder.setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getActivity().findViewById(R.id.toolbar_title1).setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Settings");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().findViewById(R.id.toolbar_title1).setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        setHasOptionsMenu(true);
        initView(rootView);
        setUiListener();
        loadData();
        setLoginStatus();
        return  rootView;

    }

    private int getCurrentYear() {
        Calendar calendar = Calendar.getInstance(Locale.UK);
        int year = calendar.get(Calendar.YEAR);
        return year;
    }

    private void loadData() {
        String version=getVersionName();
        int year = getCurrentYear();
        if(year < 2016) year = 2016;
        btnVersion.setText("\u00a9 Copyright " + year + " ChefOnline, All rights reserved\nApp Version " + version);
        getSettingsVoiceCommand();
    }

    private String getVersionName()
    {
        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    private void setLoginStatus() {
        if (preferenceUtil.getLogInStatus() == 1) {
            btnLogin.setText("Logout");
            btnLogin.setCompoundDrawablesWithIntrinsicBounds(R.drawable.logout, 0, 0, 0);
            btnMyAccount.setVisibility(View.VISIBLE);
            btnResetPassword.setVisibility(View.VISIBLE);
            btnOrderHistory.setVisibility(View.VISIBLE);

        } else {
            btnLogin.setText("Login");
            btnLogin.setCompoundDrawablesWithIntrinsicBounds(R.drawable.login, 0, 0, 0);
            btnMyAccount.setVisibility(View.GONE);
            btnResetPassword.setVisibility(View.GONE);
            btnOrderHistory.setVisibility(View.GONE);

        }
    }

    private void setUiListener() {
        btnLogin.setOnClickListener(this);
        btnMyAccount.setOnClickListener(this);
        btnOrderHistory.setOnClickListener(this);
        btnResetPassword.setOnClickListener(this);
        btnPrivacy.setOnClickListener(this);
        btnCookiePolicy.setOnClickListener(this);
        btnTerms.setOnClickListener(this);
        btnAbout.setOnClickListener(this);
        btnVoiceCommand.setOnClickListener(this);
    }

    private void initView(View rootView) {
       preferenceUtil = new PreferenceUtil(getActivity());
       btnLogin = (Button) rootView.findViewById(R.id.btnLogin);
       btnMyAccount = (Button) rootView.findViewById(R.id.btnMyAccount);
       btnOrderHistory = (Button) rootView.findViewById(R.id.btnOrderHistory);
       btnResetPassword = (Button) rootView.findViewById(R.id.btnResetPassword);
       btnVoiceCommand = (Button) rootView.findViewById(R.id.btnVoiceCommand);
       btnTerms = (Button) rootView.findViewById(R.id.btnPrivacyPolicy);
       btnPrivacy = (Button) rootView.findViewById(R.id.btnTerms);
        btnCookiePolicy = (Button) rootView.findViewById(R.id.btnCookiePolicy);
       btnAbout = (Button) rootView.findViewById(R.id.btnAbout);
        btnVersion = (Button) rootView.findViewById(R.id.btnVersion);
    }


    private void fragmentProperty(android.app.Fragment fragment) {
        /*Bundle args = new Bundle();
        fragment.setArguments(args);*/
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.addToBackStack(null);
        //ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.add(R.id.content_frame, fragment);
        ft.commit();
    }

    private void loadURLFragment(String URL) {
        android.app.Fragment fragment = new LoadUrlFragment();
        Bundle args = new Bundle();
        args.putString("url", URL);
        fragment.setArguments(args);
        fragmentProperty(fragment);
    }

    private void loadOrderHostoryFragment() {
        android.app.Fragment fragment = new OrderHistoryFragment();
        fragmentProperty(fragment);
    }

    public void settingsVoiceCommand() {
        if (preferenceUtil.getOnOffVoiceCommand() == 1) {
            btnVoiceCommand.setText("Voice Command (ON)");
            btnVoiceCommand.setCompoundDrawablesWithIntrinsicBounds(R.drawable.voice_command, 0, 0, 0);
            preferenceUtil.setOnOffVoiceCommand(0);
        } else {
            btnVoiceCommand.setText("Voice Command (OFF)");
            btnVoiceCommand.setCompoundDrawablesWithIntrinsicBounds(R.drawable.voice_command_selected, 0, 0, 0);
            preferenceUtil.setOnOffVoiceCommand(1);
        }
    }

    public void getSettingsVoiceCommand() {
        if (preferenceUtil.getOnOffVoiceCommand() == 1) {
            btnVoiceCommand.setText("Voice Command (OFF)");
            btnVoiceCommand.setCompoundDrawablesWithIntrinsicBounds(R.drawable.voice_command_selected, 0, 0, 0);
        } else {
            btnVoiceCommand.setText("Voice Command (ON)");
            btnVoiceCommand.setCompoundDrawablesWithIntrinsicBounds(R.drawable.voice_command, 0, 0, 0);
        }
    }

    public boolean isLoggedIn() {
        return preferenceUtil.getLogInStatus() == 1;
    }

    private void openUrl(String url) {
        //String url = "http://www.example.com";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }


    private void showLogoutDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_restuarant_override);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText("Are you sure want to logout?");

        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText("Logout");
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // make logout
                btnLogin.setText("Login");
                btnLogin.setCompoundDrawablesWithIntrinsicBounds(R.drawable.login, 0, 0, 0);
                preferenceUtil.setLogInStatus(0);
                btnMyAccount.setVisibility(View.GONE);
                btnResetPassword.setVisibility(View.GONE);
                btnOrderHistory.setVisibility(View.GONE);

                DataBaseUtil db = new DataBaseUtil(getActivity());
                db.open();
                db.emptyCartData();
                MainActivity.txtViewBubble.setText("0");
                dialog.dismiss();
            }

        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setText("Cancel");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        switch (v.getId()) {
            case R.id.btnLogin:

                if (isLoggedIn()) {
                    //When logout
                    showLogoutDialog();

                } else {
                    Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
                    intentLogin.putExtra(ConstantValues.NAVIGATION_KEY, ConstantValues.ACTIVITY_SETTINGS);
                    startActivityForResult(intentLogin, 100);

                }

                break;

            case R.id.btnAbout:
                loadURLFragment("http://smartrestaurantsolutions.com/mobileapi-v2/chefonline_page/aboutus.html");
                break;

            case R.id.btnPrivacyPolicy:
                loadURLFragment("http://smartrestaurantsolutions.com/mobileapi-v2/chefonline_page/privacy_policy.html");
                break;

            case R.id.btnCookiePolicy:
                loadURLFragment("http://smartrestaurantsolutions.com/mobileapi-v2/chefonline_page/cookie_policy.html");
                break;

            case R.id.btnTerms:
                loadURLFragment("http://smartrestaurantsolutions.com/mobileapi-v2/chefonline_page/terms_conditions.html");
                break;

            case R.id.btnMyAccount:
                if (isLoggedIn()) {
                    startActivity(new Intent(getActivity(), MyAccountActivity.class));
                } else {
                    new CustomToast(getActivity(), "Please login first.", "", false);
                }

                break;

            case R.id.btnOrderHistory:
                if (isLoggedIn()) {
                    loadOrderHostoryFragment();
                } else {
                    new CustomToast(getActivity(), "Please login first.", "", false);
                }

                break;

            case R.id.btnResetPassword:
                Intent intentResetPassword = new Intent(getActivity(), ResetPasswordActivity.class);
                startActivity(intentResetPassword);
                break;

            case R.id.btnVoiceCommand:
                settingsVoiceCommand();
                break;

        }
    }

}
