package com.chefonline.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.chefonline.customview.CustomToast;
import com.chefonline.customview.MsmEditText;
import com.chefonline.datamodel.NetpayData;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by user on 9/3/2016.
 */
public class NetPayBillingAddressFragment extends Fragment{

    private MsmEditText edtTitle;
    private MsmEditText edtFirstName;
    private MsmEditText edtMiddleName;
    private MsmEditText edtLastName;
    private MsmEditText edtBillingAddress;
    private MsmEditText edtPostcode;
    private MsmEditText edtCity;
    private MsmEditText edtCounty;
    private Spinner spinnerCountry;

    private ArrayList<String> countryNames;
    private ArrayList<String> countryCodes;
    public static boolean checkBillingValidation = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_netpay_billing_address, container, false);
        initView(rootView);
        initData();
        setData();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUiData();
        if(checkBillingValidation) {
            checkBillingValidation = false;
            if(isValidated()) {

            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(this.isVisible()) {
            if(isVisibleToUser) {
                // Billing fragment becomes visible
                setUiData();
                if(checkBillingValidation) {
                    checkBillingValidation = false;
                    if(isValidated()) {

                    }
                }
            } else {
                // Billing fragment becomes invisible
                setBillingData();
            }
        }

    }

    private void initView(View view) {
        edtTitle = (MsmEditText) view.findViewById(R.id.edtTitle);
        edtFirstName = (MsmEditText) view.findViewById(R.id.edtFirstName);
        edtMiddleName = (MsmEditText) view.findViewById(R.id.edtMiddleName);
        edtLastName = (MsmEditText) view.findViewById(R.id.edtLastName);
        edtBillingAddress = (MsmEditText) view.findViewById(R.id.edtBillingAddress);
        edtPostcode = (MsmEditText) view.findViewById(R.id.edtBillingPostcode);
        edtCity = (MsmEditText) view.findViewById(R.id.edtCity);
        edtCounty = (MsmEditText) view.findViewById(R.id.edtCounty);
        spinnerCountry = (Spinner) view.findViewById(R.id.spinnerCountry);
    }


    private void initData() {
        countryNames = new ArrayList<>();
        countryCodes = new ArrayList<>();
        String[] locals = Locale.getISOCountries();
        for (int i = 0; i < locals.length; i++) {
            Locale obj = new Locale("", locals[i]);
            String countryName = obj.getDisplayCountry();
            String countryCode = obj.getISO3Country();
            if(countryName.equalsIgnoreCase("United Kingdom") || countryName.equalsIgnoreCase("UK")) {
                countryNames.add(0, countryName);
                countryCodes.add(0, countryCode);
            } else {
                countryNames.add(countryName);
                countryCodes.add(countryCode);
            }
        }
    }

    private void setData() {
        ArrayAdapter<String> adapterSpinnerCountry = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, countryNames);
        adapterSpinnerCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCountry.setAdapter(adapterSpinnerCountry);
    }


    private void setUiData() {
        NetpayData netpayData = AppData.getInstance().getNetpayData();
        edtTitle.setText(netpayData.getTitle());
        edtFirstName.setText(netpayData.getFirstName());
        edtMiddleName.setText(netpayData.getMiddleName());
        edtLastName.setText(netpayData.getLastName());
        edtBillingAddress.setText(netpayData.getBillToAddress());
        edtPostcode.setText(netpayData.getBillToPostCode());
        edtCity.setText(netpayData.getBillToTownCity());
        edtCounty.setText(netpayData.getBillToCounty());
        if(netpayData.getBillToCountry() != null && !"".equalsIgnoreCase(netpayData.getBillToCountry())) {
            if(countryCodes.contains(netpayData.getBillToCountry())) {
                int tempPosition = countryCodes.indexOf(netpayData.getBillToCountry());
                spinnerCountry.setSelection(tempPosition);
            }
        }
    }


    public boolean isValidated() {
        if(edtTitle.getText().toString().trim().equalsIgnoreCase("")) {
            edtTitle.setErrorTextVisible(true);
            edtTitle.setErrorMsg("Title is required.");
            edtTitle.setTypingFocus();
            return false;
        }
        if(edtFirstName.getText().toString().trim().equalsIgnoreCase("")) {
            edtFirstName.setErrorTextVisible(true);
            edtFirstName.setErrorMsg("First Name is required.");
            edtFirstName.setTypingFocus();
            return false;
        }
        if(edtLastName.getText().toString().trim().equalsIgnoreCase("")) {
            edtLastName.setErrorTextVisible(true);
            edtLastName.setErrorMsg("Last Name is required.");
            edtLastName.setTypingFocus();
            return false;
        }
        if(edtBillingAddress.getText().toString().trim().equalsIgnoreCase("")) {
            edtBillingAddress.setErrorTextVisible(true);
            edtBillingAddress.setErrorMsg("Billing Address is required.");
            edtBillingAddress.setTypingFocus();
            return false;
        }
        if(edtCity.getText().toString().trim().equalsIgnoreCase("")) {
            edtCity.setErrorTextVisible(true);
            edtCity.setErrorMsg("City Name is required.");
            edtCity.setTypingFocus();
            return false;
        }
        if(edtCounty.getText().toString().trim().equalsIgnoreCase("")) {
            edtCounty.setErrorTextVisible(true);
            edtCounty.setErrorMsg("County Name is required.");
            edtCounty.setTypingFocus();
            return false;
        }
        return true;
    }

    private boolean isBillingDataValidated() {
        NetpayData netpayData = AppData.getInstance().getNetpayData();
        if("".equalsIgnoreCase(netpayData.getTitle())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getFirstName())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getLastName())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getBillToAddress())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getBillToTownCity())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getBillToCounty())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getBillToCountry())) {
            return false;
        }
        return true;
    }

    public void setBillingData() {
        NetpayData netpayData = AppData.getInstance().getNetpayData();
        netpayData.setTitle(edtTitle.getText().toString().trim());
        netpayData.setFirstName(edtFirstName.getText().toString().trim());
        netpayData.setMiddleName(edtMiddleName.getText().toString().trim());
        netpayData.setLastName(edtLastName.getText().toString().trim());
        netpayData.setBillToAddress(edtBillingAddress.getText().toString().trim());
        if(edtPostcode.getText().toString() != null) {
            netpayData.setBillToPostCode(edtPostcode.getText().toString().trim());
        } else {
            netpayData.setBillToPostCode("");
        }
        netpayData.setBillToTownCity(edtCity.getText().toString().trim());
        netpayData.setBillToCounty(edtCounty.getText().toString().trim());
        netpayData.setBillToCountry(countryCodes.get(spinnerCountry.getSelectedItemPosition()));
        AppData.getInstance().setNetpayData(netpayData);
        if(!isBillingDataValidated()) {
            Intent intent = new Intent("validation_failed");
            intent.putExtra("index", 1);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
            return;
        }
    }

}
