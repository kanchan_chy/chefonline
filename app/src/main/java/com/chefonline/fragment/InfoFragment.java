package com.chefonline.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.chefonline.adapter.ScheduleAdapter;
import com.chefonline.customview.WorkaroundMapFragment;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class InfoFragment extends Fragment implements View.OnClickListener {
    String address = "";
    Button btnCall;
    TextView txtViewRestaurantName, txtViewRestaurantDesc, txtViewRestaurantTel , txtViewRestaurantPostCode, txtViewRestaurantCity;
    public GoogleMap gmap;
    private ListView listViewSchedule;
    private View headerView;
    View rootView;
    ProgressDialog progress;
    private long mLastClickTime = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_info, container, false);
        progress = new ProgressDialog(getActivity());
        progress.setMessage("Please wait....");
        progress.setCanceledOnTouchOutside(false);
        progress.show();

        Handler handler;
        handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    initView(rootView);
                    loadData();
                    loadMap();
                    progress.dismiss();
                } catch (Exception e) {
                    e.getMessage();
                }

            }
        };

        handler.postDelayed(runnable, 1000);
        return  rootView;
    }

    private void loadMap() {
        LatLng userLatLng = new LatLng(Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getRestaurantLat()),
                Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getRestaurantLon()));
        if (gmap != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(userLatLng).zoom(12) // Sets the zoom
                    .tilt(0) // Sets the tilt of the camera to 30 degrees
                    .build(); // Creates a CameraPosition from the builder

            gmap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }

        gmap.addMarker(new MarkerOptions().title(AppData.getInstance().getRestaurantInfoData().getRestaurantName())
                .position(userLatLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
                .snippet(address + ", "
                        + AppData.getInstance().getRestaurantInfoData().getPostCode()));
    }

    private void loadData() {
        /** Instantiate header item of expandable item list view */
        txtViewRestaurantName.setText(AppData.getInstance().getRestaurantInfoData().getRestaurantName());
        String fullAddress;

        if (AppData.getInstance().getRestaurantInfoData().getRestaurantAddress1().contains("<br/>")) {
            fullAddress = AppData.getInstance().getRestaurantInfoData().getRestaurantAddress1().toString().trim().substring(0, AppData.getInstance().getRestaurantInfoData().getRestaurantAddress1().toString().trim().length()-5);
            address = fullAddress;
        } else {
            fullAddress = AppData.getInstance().getRestaurantInfoData().getRestaurantAddress1().toString().trim();
            address = fullAddress;
        }

        if (AppData.getInstance().getRestaurantInfoData().getRestaurantAddress2().toString().trim() != null ||
                !AppData.getInstance().getRestaurantInfoData().getRestaurantAddress2().toString().trim().equalsIgnoreCase("")   ) {
            fullAddress += ", " + AppData.getInstance().getRestaurantInfoData().getRestaurantAddress2().toString().trim();
        }

       /* if (AppData.getInstance().getRestaurantInfoData().getRestaurantTel() != null) {
            fullAddress += "\nTel: " + AppData.getInstance().getRestaurantInfoData().getRestaurantTel() + "";
        }*/

        txtViewRestaurantDesc.setText(fullAddress);

        txtViewRestaurantPostCode.setText("Post Code: " + AppData.getInstance().getRestaurantInfoData().getPostCode());

        if ("".equals(AppData.getInstance().getRestaurantInfoData().getBusinessTel())) {
            txtViewRestaurantTel.setVisibility(View.GONE);
        } else {
            txtViewRestaurantTel.setText("Tel: " + AppData.getInstance().getRestaurantInfoData().getBusinessTel());
        }

        if ("".equals(AppData.getInstance().getRestaurantInfoData().getRestaurantCity())) {
            txtViewRestaurantCity.setVisibility(View.GONE);
        } else {
            txtViewRestaurantCity.setText("City: " + AppData.getInstance().getRestaurantInfoData().getRestaurantCity());
        }


        listViewSchedule.addHeaderView(headerView);
        ScheduleAdapter scheduleAdapter = new ScheduleAdapter(getActivity(), AppData.getInstance().getRestaurantInfoData().getScheduleDatas());
        listViewSchedule.setAdapter(scheduleAdapter);

    }

    private void initView(View rootView) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        headerView = inflater.inflate(R.layout.header_info, null);
        txtViewRestaurantName = (TextView) headerView.findViewById(R.id.txtViewRestaurantName);
        txtViewRestaurantDesc = (TextView) headerView.findViewById(R.id.txtViewRestaurantDesc);
        txtViewRestaurantTel = (TextView) headerView.findViewById(R.id.txtViewRestaurantTel);

        txtViewRestaurantPostCode = (TextView) headerView.findViewById(R.id.txtViewRestaurantPostCode);
        txtViewRestaurantCity = (TextView) headerView.findViewById(R.id.txtViewRestaurantCity);
        btnCall = (Button) headerView.findViewById(R.id.btnCall);
        btnCall.setOnClickListener(this);

        if (AppData.getInstance().getRestaurantInfoData().getRestaurantTelSingle() == null || "".equals(AppData.getInstance().getRestaurantInfoData().getRestaurantTelSingle())) {
            btnCall.setVisibility(View.GONE);
        } else {
            btnCall.setText(AppData.getInstance().getRestaurantInfoData().getRestaurantTelSingle());
        }

        listViewSchedule = (ListView) rootView.findViewById(R.id.listViewSchedule);

        gmap = ((WorkaroundMapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        ((WorkaroundMapFragment) getFragmentManager().findFragmentById(R.id.map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                listViewSchedule.requestDisallowInterceptTouchEvent(true);
            }
        });

    }

    private void showWarningDialog(String message)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_message);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    @Override
    public void onDestroyView() {
        try {
            FragmentManager fm = getActivity().getFragmentManager();
            Fragment fragment = (fm.findFragmentById(R.id.map));
            FragmentTransaction ft = fm.beginTransaction();
            ft.remove(fragment);
            ft.commit();
        } catch (Exception e) {
            e.getMessage();
        }

        super.onDestroyView();
    }


    private String getSpaceLessNumber(String mNumber){
        String tempNumber = "";
        for (int i=0; i<mNumber.length(); i++) {
            if(mNumber.charAt(i) != ' ') {
                tempNumber += mNumber.charAt(i);
            }
        }
        return tempNumber;
    }


    private void makePhoneCall() {
        boolean noProblem = false;
        TelephonyManager telMgr = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                noProblem=false;
                showWarningDialog("SIM card not found. Please insert SIM card.");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                noProblem=false;
                showWarningDialog("Network problem encountered. Please check network settings.");
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                noProblem=false;
                showWarningDialog("Sorry! Call is not possible");
                break;
            case TelephonyManager.SIM_STATE_READY:
                noProblem = true;
                break;
        }
        if(noProblem){
            try{
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + getSpaceLessNumber(AppData.getInstance().getRestaurantInfoData().getRestaurantTelSingle())));
                startActivity(callIntent);
            }
            catch (Exception e){
                showWarningDialog("Unable to deliver the call. "+ e.getMessage());
            }
        }
    }



    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnCall) {
            makePhoneCall();
        }
    }
}
