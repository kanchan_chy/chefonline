package com.chefonline.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.ChefOnlineButton;
import com.chefonline.customview.ChefOnlineTextView;
import com.chefonline.customview.CustomBottomToast;
import com.chefonline.customview.CustomToast;
import com.chefonline.customview.MsmEditText;
import com.chefonline.datamodel.LocationData;
import com.chefonline.datamodel.PlaceOrderResponseData;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.modelinterface.PlaceOrderView;
import com.chefonline.online.ManageNetpayAccountActivity;
import com.chefonline.online.OrderConfirmationActivity;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;
import com.chefonline.presenter.PlaceOrderPresenter;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UtilityMethod;
import com.paypal.android.MEP.CheckoutButton;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalPayment;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PaymentFragment extends Fragment implements View.OnClickListener, PlaceOrderView{
    String code = "";
    private static String TAG = "PaymentFragment";
    private String offerText = "";
    private String transactionId = "";
    public  String placedOrderId = "";
    private String serviceName = "";
    private String verificationCode = "0";
    private String grandTotalStr = "";

    private Button btnPayPal, btnCash, btnNetPay;

    private PayPalAsynchronousTask mPATask;
    public static int PAYPAL_ID = 10001;
    private CheckoutButton mChkButton;
    private ProgressDialog pDialog;
    private static Handler mHandler;
    private PayPalPayment newPayment;
    public View rootView;
    public TextView txtViewPaymentTitle,txtIp;
    public  RelativeLayout rl;

    private String PAYPAL_SANDBOX_APP_ID = "APP-80W284485P519543T";
    private String PAYPAL_LIVE_APP_ID = "APP-0LK43943U05471832";
    private String PAYPAL_TEST_RECIPIENT = "starjahid@yahoo.com";
    private String PAYPAL_CURRENCY = "GBP";
    private LocationData locationData;

    PlaceOrderPresenter placeOrderPresenter;
    PlaceOrderResponseData placeOrderResponseData = new PlaceOrderResponseData();
    private long mLastClickTime = 0;

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Payment");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Order");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_make_payment, container, false);
        intiUI(rootView);
        setListener();
        placeOrderPresenter = new PlaceOrderPresenter(PaymentFragment.this);
        setInitZeroValue();

        Log.i("HELLO", "==" + Double.parseDouble(getArguments().getString("deliveryCharge").toString().trim()));
        if (Double.parseDouble(getArguments().getString("deliveryCharge").toString().trim()) > 0) {
            //plus
            Double grandTotal = Double.parseDouble(AppData.getInstance().getOrderPlaceData().getGrandTotal().trim().substring(1)) +
                    Double.parseDouble(getArguments().getString("deliveryCharge").toString().trim());

            grandTotalStr = getResources().getString(R.string.pound_sign) + UtilityMethod.priceFormatter(grandTotal);
            AppData.getInstance().getOrderPlaceData().setGrandTotal(grandTotalStr);
            openDialog();

        } else {
            grandTotalStr = AppData.getInstance().getOrderPlaceData().getGrandTotal().trim();
            placeOrderPresenter.getRealIPAddress(getActivity(), "44");
        }

        return rootView;
    }

    private void intiUI(View rootView) {
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCanceledOnTouchOutside(false);

        btnPayPal = (Button) rootView.findViewById(R.id.btnPaypal);
        btnCash = (Button) rootView.findViewById(R.id.btnCash);
        btnNetPay = (Button) rootView.findViewById(R.id.btnNetPay);
        txtViewPaymentTitle = (TextView) rootView.findViewById(R.id.txtViewPaymentTitle);
        txtIp = (TextView) rootView.findViewById(R.id.txtIp);
        rl = (RelativeLayout) rootView.findViewById(R.id.paypalButtonHolder);

        if("1".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getNetpayStatus())) {
            btnNetPay.setVisibility(View.VISIBLE);
        } else {
            btnNetPay.setVisibility(View.GONE);
        }
    }

    private void setListener() {
        btnPayPal.setOnClickListener(this);
        btnCash.setOnClickListener(this);
        btnNetPay.setOnClickListener(this);
    }


    private void openDialogPaymentRetry(final boolean isPayPal) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_retry);
        TextView txtViewHelpNo = (TextView) dialog.findViewById(R.id.txtViewHelpNo);
        txtViewHelpNo.setText(AppData.getInstance().getRestaurantInfoData().getBusinessTel());

        Button btnRetry = (Button) dialog.findViewById(R.id.btnRetry);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        ImageButton btnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;

                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                if (isPayPal) {
                    placeOrderPresenter.onClickPayCashOrPaypal(getActivity(), ConstantValues.PAYMENT_PAYPAL);
                } else {
                    placeOrderPresenter.onClickPayCashOrPaypal(getActivity(), ConstantValues.PAYMENT_CASH);
                }

                dialog.dismiss();
            }

        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void openDialogConfirmPayment(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_restuarant_override);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        //imgBtnClose.setVisibility(View.INVISIBLE);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(okButton);
        btnAccept.setBackgroundResource(R.drawable.round_red_button_selector);

        // if button is clicked, close the custom dialog
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                placeOrderPresenter.onClickPayCashOrPaypal(getActivity(), ConstantValues.PAYMENT_CASH);
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setText(cancelButton);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private void openDialogMessage(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_message);

        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);

        btnAccept.setText(okButton);
        txtViewPopupMessage.setText(message);
        imgBtnClose.setVisibility(View.INVISIBLE);
        //btnAccept.setBackgroundResource(R.drawable.round_gray_button_selector);

        // if button is clicked, close the custom dialog
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getActivity().onBackPressed();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    private void openDialogError(String message, String okButton) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_message);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(okButton);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    private void openDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_delivery_charge);
        ChefOnlineTextView txtViewPopupMessage = (ChefOnlineTextView) dialog.findViewById(R.id.txtViewImportantMessage);
        ChefOnlineTextView txtViewSubTotal = (ChefOnlineTextView) dialog.findViewById(R.id.txtViewSubTotal);
        ChefOnlineTextView txtViewDiscountAmount = (ChefOnlineTextView) dialog.findViewById(R.id.txtViewDiscountAmount);
        ChefOnlineTextView txtViewDeliveryChargeAmount = (ChefOnlineTextView) dialog.findViewById(R.id.txtViewDeliveryChargeAmount);
        ChefOnlineTextView txtViewGrandTotalAmount = (ChefOnlineTextView) dialog.findViewById(R.id.txtViewGrandTotalAmount);

        txtViewSubTotal.setText(AppData.getInstance().getOrderPlaceData().getTotalAmount());
        txtViewDiscountAmount.setText(getResources().getString(R.string.pound_sign) + AppData.getInstance().getOrderPlaceData().getDiscount());
        txtViewDeliveryChargeAmount.setText(getResources().getString(R.string.pound_sign) + getArguments().getString("deliveryCharge").toString().trim());
        txtViewGrandTotalAmount.setText(AppData.getInstance().getOrderPlaceData().getGrandTotal());

        com.chefonline.customview.ChefOnlineButton btnCancel = (com.chefonline.customview.ChefOnlineButton) dialog.findViewById(R.id.btnCancel);
        //txtViewPopupMessage.setText(message);
        com.chefonline.customview.ChefOnlineButton btnAccept = (com.chefonline.customview.ChefOnlineButton) dialog.findViewById(R.id.btnRetry);

        //btnAccept.setText("ACCEPT");

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                PlaceOrderPresenter placeOrderPresenter = new PlaceOrderPresenter(PaymentFragment.this);
                placeOrderPresenter.getRealIPAddress(getActivity(), "44");

            }

        });


        //btnCancel.setText("CANCEL");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Order");
                FragmentManager manager = getFragmentManager();
                manager.popBackStack();
                //getActivity().getFragmentManager().beginTransaction().remove(PaymentFragment.this).commit();
            }

        });

        dialog.show();
    }

    private void openDialogCodeVerification(final boolean isPayPal, String message) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_code_verification);

        ChefOnlineTextView txtMessage = (ChefOnlineTextView) dialog.findViewById(R.id.view2);
        txtMessage.setText(message);
        final MsmEditText editTextCode = (MsmEditText) dialog.findViewById(R.id.editTextCode);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        //imgBtnClose.setVisibility(View.INVISIBLE);
        //txtViewPopupMessage.setText(message);

        //editTextCode.setText(code); //open verify code
       // txtMessage.setText(code); //open verify code
        ChefOnlineButton btnAccept = (ChefOnlineButton) dialog.findViewById(R.id.btnResend);
        btnAccept.setText("VERIFY");
        btnAccept.setBackgroundResource(R.drawable.round_red_button_selector);

        // if button is clicked, close the custom dialog
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                verificationCode = editTextCode.getText().toString().trim();
                if (verificationCode.equals("")) {
                    hideKeyboard(editTextCode);
                    new CustomBottomToast(getActivity(), "Verification code required", "", false);
                } else {
                    hideKeyboard(editTextCode);
                    dialog.dismiss();
                    AppData.getInstance().getOrderPlaceData().setVerificationCode(verificationCode);
                    if (isPayPal) {
                        placeOrderPresenter.onClickPayCashOrPaypal(getActivity(), ConstantValues.PAYMENT_PAY_OVER_PHONE);
                    } else {
                        placeOrderPresenter.onClickPayCashOrPaypal(getActivity(), ConstantValues.PAYMENT_CASH);
                    }
                }

            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    public void openConfirmationActivity(boolean isPayPal) {
        String paymentOption = "";
        if (isPayPal) {
            paymentOption = "PayPal";
        } else {
            paymentOption = "Cash";
        }

        Intent intent = new Intent(getActivity(), OrderConfirmationActivity.class);
        intent.putExtra("transaction_id", transactionId);
        intent.putExtra("order_id", placedOrderId);
        intent.putExtra("payment_option", paymentOption);
        intent.putExtra("address", AppData.getInstance().getOrderPlaceData().getAddress());
        intent.putExtra("delivery_option", getArguments().getString("delivery_option"));
        intent.putExtra("delivery_charge", getArguments().getString("deliveryCharge").toString().trim());
        intent.putExtra("offer_text", AppData.getInstance().getOrderPlaceData().getOfferText());
        intent.putExtra("card_fee", AppData.getInstance().getOrderPlaceData().getCardFee());
        intent.putStringArrayListExtra("all_offer_texts", AppData.getInstance().getOrderPlaceData().getAllOfferTexts());
        startActivity(intent);

        transactionId = "";
        placedOrderId = "";

    }

    private void hideKeyboard(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void setLoadingVisible() {
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please wait....");
        pDialog.show();
    }

    @Override
    public void setLoadingHide() {
        if (pDialog.isShowing()) {
            pDialog.dismiss();
        }

    }

    @Override
    public void showToast(String message, String subMessage, boolean isButtonOk) {
        new CustomToast(getActivity(), message, subMessage, isButtonOk);
    }

    @Override
    public void setLocationData(LocationData locationData) {
        this.locationData = locationData;
        Log.i("Country Name", "" + locationData.getCountryName());
        Log.i("IP Address", "" + locationData.getIpAddress());
        String mIP = locationData.getIpAddress() == null ? "" : locationData.getIpAddress();
        if(!mIP.equals("")) {
            txtIp.setVisibility(View.VISIBLE);
            txtIp.setText("Your IP Address " + mIP + ", We Use This IP For Order and Verification Purpose");
        }
        if("UK".equalsIgnoreCase(locationData.getCountryName()) || "GB".equalsIgnoreCase(locationData.getCountryName())) {
            ConstantValues.COUNTRY_FLAG = "1";
        } else {
            ConstantValues.COUNTRY_FLAG = "2";
        }

    }

    public void setInitZeroValue() {
        placeOrderResponseData.setCardFee("0");
        placeOrderResponseData.setIsSpecialMessageRequired("0");
        placeOrderResponseData.setIsVarificationRequired("0");
        placeOrderResponseData.setDeliveryCharge(getArguments().getString("deliveryCharge").toString().trim());
        placeOrderResponseData.setUserAddressExtId(getArguments().getString("selectedAddressExtensionId").toString().trim());


    }

    @Override
    public void visiblePayPalButton() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    setLoadingHide();
                    Intent checkoutIntent = PayPal.getInstance().checkout(newPayment, getActivity());
                    startActivityForResult(checkoutIntent, 2);
                }

            }
        };

        if (AppData.getInstance().getRestaurantInfoData().getRestaurantPayPalEmail().equalsIgnoreCase(".") ||
                AppData.getInstance().getRestaurantInfoData().getRestaurantPayPalEmail().equalsIgnoreCase("")) {

            btnPayPal.setVisibility(View.GONE);
            rl.setVisibility(View.GONE);
            btnCash.setVisibility(View.VISIBLE);
            txtViewPaymentTitle.setVisibility(View.VISIBLE);

        } else {
            mPATask = new PayPalAsynchronousTask();
            mPATask.execute();
        }
    }

    @Override
    public void verifyCode(String codeTemp, boolean isPaypal) {
        code = codeTemp; // for verification code showing as test purpose
      //  AppData.getInstance().getOrderPlaceData().setVerificationCode(code);
        openDialogCodeVerification(isPaypal, "Verification code sent to you via sms. Please check your inbox.");
        new CustomBottomToast(getActivity(),"A verification code sent to you via sms", "", true);
    }

    @Override
    public void orderCompleted(String orderId, String transId, boolean isPaypal) {
        placedOrderId = orderId;
        if(isPaypal) {
            nextScreen();
        } else {
            transactionId = transId;
            openConfirmationActivity(isPaypal);
            getActivity().finish();
        }
    }

    @Override
    public void paymentStatusUpdated(boolean isSuccess) {
        if(isSuccess) {
            openConfirmationActivity(true);
            getActivity().finish();
        } else {
            openDialogError("Sorry, order can't be completed", "OK");
        }
    }

    @Override
    public void onVerificationFailed(boolean isPaypal) {
        openDialogCodeVerification(isPaypal, "Your verification code does not match. Please check your inbox and enter correct code.");
        new CustomToast(getActivity(), "Wrong verification code", "", false);
    }

    @Override
    public void retryPayment(boolean isPaypal) {
        openDialogPaymentRetry(isPaypal);
        new CustomToast(getActivity(), "Your order process has been failed, please try again", "", false);
    }

    @Override
    public void isNetpayChargeFound(boolean isFound) {
        if(isFound) {
            Intent intent = new Intent(getActivity(), ManageNetpayAccountActivity.class);
            startActivity(intent);
        }
    }

    private class PayPalAsynchronousTask extends AsyncTask<Void, Void, String> {
        ProgressDialog pDialog = new ProgressDialog(getActivity());
        PayPal pp;

        @Override
        protected void onPreExecute() {
            setLoadingVisible();

        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                pp = PayPal.getInstance();
                pp = PayPal.initWithAppID(getActivity(), "APP-80W284485P519543T", PayPal.ENV_SANDBOX);
             //   pp = PayPal.initWithAppID(getActivity(), PAYPAL_LIVE_APP_ID, PayPal.ENV_LIVE);
                pp.setLanguage("en_US");
            } catch (Exception e) {
                return e.getMessage();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                setLoadingHide();
                if (!result.equalsIgnoreCase("")) {
                    openDialogMessage(result, "OK", "CANCEL");
                    return;
                }

                mChkButton = pp.getCheckoutButton(getActivity(), PayPal.BUTTON_278x43, CheckoutButton.TEXT_PAY);
                RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                parms.addRule(RelativeLayout.CENTER_IN_PARENT);
                mChkButton.setId(R.id.my_button_1);
                mChkButton.setLayoutParams(parms);
                mChkButton.setOnClickListener(PaymentFragment.this);
                rl.removeAllViews();
                rl.addView(mChkButton);

                txtViewPaymentTitle.setVisibility(View.VISIBLE);
                btnCash.setVisibility(View.VISIBLE);
                pDialog.cancel();

            } catch (Exception e) {

            }

        }

    }



    private void nextScreen() {
        try {
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading PayPal Login Page ...");
            pDialog.show();

            Runnable r = new Runnable() {
                public void run() {
                    newPayment = new PayPalPayment();
                    newPayment.setSubtotal(new BigDecimal(Double.parseDouble(AppData.getInstance().getOrderPlaceData().getGrandTotal().substring(1).trim())));
                    newPayment.setCurrencyType(PAYPAL_CURRENCY);
                    newPayment.setRecipient(PAYPAL_TEST_RECIPIENT);
                    // newPayment.setRecipient(AppData.getInstance().getRestaurantInfoData().getRestaurantPayPalEmail().trim());
                    newPayment.setMerchantName(AppData.getInstance().getRestaurantInfoData().getRestaurantName());
                    mHandler.sendEmptyMessage(1);
                }
            };

            Thread th = new Thread(r);
            th.start();
        } catch (Exception e) {
            Log.e("payment", "" + e);
            openDialogError("Sorry, payment can't be completed. Something went wrong", "OK");
        }
    }


    private void setPlaceOrderData() {
        AppData.getInstance().getOrderPlaceData().setGrandTotal(grandTotalStr);
        AppData.getInstance().getOrderPlaceData().setPolicyId(getArguments().getString("policy_id"));
        AppData.getInstance().getOrderPlaceData().setPreOrderTime(getArguments().getString("preorder_time"));
        AppData.getInstance().getOrderPlaceData().setOrderList(UtilityMethod.createOrderListJsonNew());
        AppData.getInstance().getOrderPlaceData().setPaymentStatus("0");
        AppData.getInstance().getOrderPlaceData().setPaypalTransactionId("");
        AppData.getInstance().getOrderPlaceData().setVerificationCode("0");
        AppData.getInstance().getOrderPlaceData().setDeliveryOption(getArguments().getString("delivery_option"));
        AppData.getInstance().getOrderPlaceData().setDeliveryCharge(getArguments().getString("deliveryCharge").toString().trim());
        AppData.getInstance().getOrderPlaceData().setUserAddressExtId(getArguments().getString("selectedAddressExtensionId").toString().trim());
        AppData.getInstance().getOrderPlaceData().setCardFee(placeOrderResponseData.getCardFee());
        AppData.getInstance().getOrderPlaceData().setIsVerificationCodeRequired(placeOrderResponseData.getIsVarificationRequired());
        AppData.getInstance().getOrderPlaceData().setIsSpecialMessageRequired(placeOrderResponseData.getIsSpecialMessageRequired());
        AppData.getInstance().getOrderPlaceData().setIpAddress(locationData.getIpAddress());
        AppData.getInstance().getOrderPlaceData().setInsideUk(ConstantValues.COUNTRY_FLAG);
    }

    /*
    private void setNetPayCardFee() {
        String totalStr = AppData.getInstance().getOrderPlaceData().getGrandTotal().substring(1);
        if(totalStr != null && !totalStr.equalsIgnoreCase("")) {
            if(AppData.getInstance().getRestaurantInfoData().getNetpayCustomerCharge() != null && !"".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getNetpayCustomerCharge())) {
                String chargeStr = AppData.getInstance().getRestaurantInfoData().getNetpayCustomerCharge();
                if(ConstantValues.CHARGE_TYPE_ACTUAL.equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getNetpayChargeType())) {
                    chargeStr = UtilityMethod.priceFormatter(Double.valueOf(chargeStr));
                    AppData.getInstance().getOrderPlaceData().setCardFee(chargeStr);
                    double totalTemp = Double.valueOf(totalStr) + Double.valueOf(chargeStr);
                    String grandTotal = UtilityMethod.priceFormatter(totalTemp);
                    AppData.getInstance().getOrderPlaceData().setGrandTotal(getResources().getString(R.string.pound_sign) + grandTotal);
                } else {
                    double chargeTemp = (Double)((Double.valueOf(totalStr)*Double.valueOf(chargeStr))/(Double)100.0);
                    chargeStr = UtilityMethod.priceFormatter(chargeTemp);
                    AppData.getInstance().getOrderPlaceData().setCardFee(chargeStr);
                    double totalTemp = Double.valueOf(totalStr) + Double.valueOf(chargeStr);
                    String grandTotal = UtilityMethod.priceFormatter(totalTemp);
                    AppData.getInstance().getOrderPlaceData().setGrandTotal(getResources().getString(R.string.pound_sign) + grandTotal);
                }
            }
        }
    }   */


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            String msg = " req code :" + requestCode;
            msg = msg + " result:" + resultCode;
            if (data.getData() != null) {
                msg = msg + " data :" + data.getData();
            }

            if (resultCode == getActivity().RESULT_OK) {
                transactionId = data.getStringExtra(PayPalActivity.EXTRA_PAY_KEY);
                placeOrderPresenter.updatePaymentStatus(getActivity(), placedOrderId, "1", transactionId);

            } else if (resultCode == getActivity().RESULT_CANCELED) {
                mPATask = new PayPalAsynchronousTask();
                mPATask.execute();

            } else if (resultCode == 2) {
                String errorMessage = data.getStringExtra(PayPalActivity.EXTRA_ERROR_MESSAGE);
                new CustomToast(getActivity(), errorMessage, "", false);
                openDialogMessage(errorMessage, "OK", "CANCEL");

            } else {
                new CustomToast(getActivity(), "Something went wrong, Please contact to customer service.", "", false);
            }

            Log.e("DATA PAYPAL ", " " + data.getDataString() + ": " + msg);
            Log.e("DATA PAYPAL INTENT ", " " + data.getStringExtra(PayPalActivity.EXTRA_PAY_KEY));
            Log.e("DATA PAYPAL INFO ", " " + data.getStringExtra(PayPalActivity.EXTRA_PAYMENT_INFO));
        } catch (Exception e) {
            Log.e("payment", "" + e);
            openDialogError("Sorry, payment can't be completed. Something went wrong", "OK");
        }
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        switch (v.getId()) {
            case R.id.btnPaypal:
                verificationCode = "0";
                setPlaceOrderData();
                placeOrderPresenter.onClickPayCashOrPaypal(getActivity(), ConstantValues.PAYMENT_PAYPAL);
                break;

            case R.id.btnCash:
                verificationCode = "0";
                setPlaceOrderData();
                openDialogConfirmPayment("Do you wish to confirm your order?", "Confirm", "Cancel");
                break;

            case R.id.my_button_1:
                verificationCode = "0";
                setPlaceOrderData();
                placeOrderPresenter.onClickPayCashOrPaypal(getActivity(), ConstantValues.PAYMENT_PAYPAL);
                break;
            case R.id.btnNetPay:
                setPlaceOrderData();
               // setNetPayCardFee();
                placeOrderPresenter.onClickGetNetPayCharge(getActivity(), String.valueOf(AppData.getInstance().getRestaurantInfoData().getRestaurantId()), ConstantValues.PAYMENT_NETPAY, AppData.getInstance().getOrderPlaceData().getGrandTotal().substring(1));

        }

    }
}
