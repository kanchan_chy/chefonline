package com.chefonline.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.adapter.SelectionOfferAdapter;
import com.chefonline.datamodel.CartData;
import com.chefonline.datamodel.OfferListData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.UtilityMethod;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class OffersCalculationFragment extends Fragment implements View.OnClickListener {
    private static String TAG = "OffersCalculationFragment";
    public static int SELECTED_OFFER = 0;
    private Button btnPlaceOrder;
    private int selectedServiceNo = 0;
    public String policyId = "";
    private String policyTime = "";
    private ListView listViewOffers;
    public static TextView txtViewOfferText;
    public static ImageButton txtViewCancelOffer;
    private TextView txtViewDefaultOfferText;
    private TextView txtViewSubTotal;
    private TextView txtViewGrandTotal;
    private TextView txtViewRestaurantName;
    private TextView txtViewDeliveryOption;
    private TextView txtViewDiscount;
    private RelativeLayout relativeOfferText;
    private ArrayAdapter<String> modeAdapter;
    private SelectionOfferAdapter selectionOfferAdapter;
    private ArrayList<OfferListData> offersStrings;
    private ArrayList<String> receivedOffersId;
    public static ArrayList<String> allOfferTexts;
    private long mLastClickTime = 0;
    private String defaultOfferText = "";
    public boolean isDefaultDiscount = false;

    public OffersCalculationFragment() {

    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_offers_calculation, container, false);
        initView(rootView);
        getFragmentParameter();
        setData();

        calculateTotalAmount(rootView);
        offerCalculation();
        setUiListener();
        return rootView;
    }

    private void setData() {
        txtViewRestaurantName.setText(AppData.getInstance().getRestaurantInfoData().getRestaurantName());

        if(selectedServiceNo == 1) {
            txtViewDeliveryOption.setText("Delivery Option: Delivery");
        } else {
            txtViewDeliveryOption.setText("Delivery Option: Collection");
        }

    }

    private void setUiListener() {
       txtViewCancelOffer.setOnClickListener(this);
       btnPlaceOrder.setOnClickListener(this);
    }

    private void initView(View rootView) {
        btnPlaceOrder = (Button) rootView.findViewById(R.id.btnPlaceOrder);
        listViewOffers = (ListView) rootView.findViewById(R.id.listViewOffers);
        txtViewOfferText = (TextView) rootView.findViewById(R.id.txtViewOfferText);
        txtViewCancelOffer = (ImageButton) rootView.findViewById(R.id.txtViewCancelOffer);
        txtViewDefaultOfferText = (TextView) rootView.findViewById(R.id.txtViewDefaultOfferText);
        relativeOfferText = (RelativeLayout) rootView.findViewById(R.id.relativeOfferText);

        txtViewSubTotal = (TextView)  rootView.findViewById(R.id.txtViewSubTotal1);
        txtViewGrandTotal = (TextView) rootView.findViewById(R.id.txtViewTotal1);
        txtViewDiscount = (TextView) rootView.findViewById(R.id.txtViewDiscount1);

        txtViewRestaurantName = (TextView) rootView.findViewById(R.id.txtViewRestaurantName);
        txtViewDeliveryOption = (TextView) rootView.findViewById(R.id.txtViewDeliveryOption);
    }

    public void getFragmentParameter() {
        selectedServiceNo = getArguments().getInt("selected_service");
        policyId = getArguments().getString("selected_policy_id");
        policyTime = getArguments().getString("selected_policy_time");

    }

    public Double getTotalCartPrice() {
        DataBaseUtil db = new DataBaseUtil(getActivity());
        db.open();

        ArrayList<CartData> cartDataForTotal = new ArrayList<>();
        Double totalSubAmount = 0.0;
        cartDataForTotal = db.fetchCartData();
        for (int i = 0; i < cartDataForTotal.size(); i++) {
            totalSubAmount += (cartDataForTotal.get(i).getItemQty() * cartDataForTotal.get(i).getPrice());
        }
        return totalSubAmount;
    }

    /** Calculate Total Amount */
    private double calculateTotalAmount(View rootView) {
        Double totalSubAmount = 0.0;
        totalSubAmount = getTotalCartPrice();

        txtViewSubTotal.setText(getResources().getString(R.string.pound_sign) + String.format("%.2f", totalSubAmount));
        txtViewGrandTotal.setText(getResources().getString(R.string.pound_sign) + String.format("%.2f", totalSubAmount));
        txtViewDiscount.setText(getResources().getString(R.string.pound_sign) + "0.00");

        /** End total amount */
        return totalSubAmount;
    }

    private void offerCalculation () {
        offersStrings = new ArrayList<>();
        receivedOffersId = new ArrayList<>();
        allOfferTexts = new ArrayList<>();
        isDefaultDiscount = false;
        int discountSize = 0;
        try {
            discountSize = AppData.getInstance().getRestaurantInfoData().getDiscountList().size();
        } catch (Exception e) {
        }

        for (int i = 0; i < discountSize; i++) {
            OfferListData offerListData = new OfferListData();
            if (policyId.equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getRestaurantOrderPolicyId())
                    || "0".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getRestaurantOrderPolicyId())) {

                if(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getActiveStatus() == 1) {

                    if("1".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDefaultStatus())) {
                        if (getTotalCartPrice() >= Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountEligibleAmount())) {
                            CartFragment.discountId = AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountId();
                            isDefaultDiscount = true;
                            calculateTotalAmount(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getRestaurantOrderPolicyId(), AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountId());
                            String tempOfferText = "";
                            if(!"Percentage".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountType())) {
                                tempOfferText = "£"+AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount() + " Discount";
                            } else {
                                tempOfferText = AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount() + "% Discount";
                            }
                            if(defaultOfferText.equalsIgnoreCase("")) {
                                defaultOfferText += "You have received the following offer(s):\n" + tempOfferText;
                            } else {
                                defaultOfferText += "\n" + tempOfferText;
                            }
                        }
                    } else {
                        offerListData.setOfferId(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountId());
                        if(!"Percentage".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountType())) {
                            offerListData.setOfferText("£"+AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount() + " Discount");
                        } else {
                            offerListData.setOfferText(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount() + "% Discount");
                        }
                        offerListData.setOfferCode("1");
                        offerListData.setEligibleAmount(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountEligibleAmount());
                        offerListData.setPolicyId(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getRestaurantOrderPolicyId());
                        offerListData.setAmount(Float.parseFloat(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount()));

                        if (getTotalCartPrice() >= Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountEligibleAmount())) {
                            offersStrings.add(offerListData);
                        }
                    }

                }

            }
        }

        int offerSize = AppData.getInstance().getRestaurantInfoData().getOffersDatas().size();
        for (int i = 0; i < offerSize; i++) {
            OfferListData offerListData = new OfferListData();
            //Log.i(TAG, "Offers Id***" + AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferTitle());
            if (policyId.equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getRestaurantOrderPolicyId())
                    || "0".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getRestaurantOrderPolicyId())) {

                if(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getActiveStatus() == 1) {

                    if("1".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getDefaultStatus())) {
                        if (getTotalCartPrice() >= Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferElegibleAmount())) {
                            receivedOffersId.add(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferId());
                            if(defaultOfferText.equalsIgnoreCase("")) {
                                defaultOfferText += "You have received the following offer(s):\n" + AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferTitle();
                            } else {
                                defaultOfferText += "\n" + AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferTitle();
                            }
                            allOfferTexts.add(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferTitle());
                        }
                    } else {
                        offerListData.setOfferId(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferId());
                        offerListData.setOfferText(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferTitle());
                        offerListData.setEligibleAmount(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferElegibleAmount());
                        offerListData.setOfferCode("2");
                        offerListData.setPolicyId(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getRestaurantOrderPolicyId());
                        offerListData.setAmount(0.0f);

                        if (getTotalCartPrice() >= Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getOffersDatas().get(i).getOfferElegibleAmount())) {
                            offersStrings.add(offerListData);
                        }
                    }

                }

            }

        }

        if(receivedOffersId.size() > 0) {
            try {
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < receivedOffersId.size(); i++) {
                    JSONObject jObj = new JSONObject();
                    jObj.put("offer_id", receivedOffersId.get(i));
                    jsonArray.put(jObj);
                }
                CartFragment.offerId = jsonArray.toString();
            } catch (Exception e) {
                CartFragment.offerId = "";
            }
        } else {
            CartFragment.offerId = "";
        }

        if(!"".equalsIgnoreCase(defaultOfferText)) {
            txtViewDefaultOfferText.setText(defaultOfferText);
            txtViewDefaultOfferText.setVisibility(View.VISIBLE);
            txtViewOfferText.setText("Other offers available for you.");
        } else {
            txtViewDefaultOfferText.setVisibility(View.GONE);
            txtViewOfferText.setText("Offers available for you.");
        }

        if (offersStrings.size() > 0) {
            relativeOfferText.setVisibility(View.VISIBLE);
            selectionOfferAdapter = new SelectionOfferAdapter(getActivity(), offersStrings, receivedOffersId);
            listViewOffers.setAdapter(selectionOfferAdapter);
        } else {
            relativeOfferText.setVisibility(View.GONE);
        }
    }


    private void calculateTotalAmount(String policyId, String offerId) {
        /** Calculate Total Amount */
        Double totalSubAmout = 0.00;
        ArrayList<CartData> cartDataForTotal = new ArrayList<>();
        DataBaseUtil db = new DataBaseUtil(getActivity());

        try {
            db.open();
            cartDataForTotal = db.fetchCartData();
            for (int i = 0; i < cartDataForTotal.size(); i++) {
                totalSubAmout += (cartDataForTotal.get(i).getItemQty() * cartDataForTotal.get(i).getPrice());
            }

            txtViewSubTotal.setText("£" + UtilityMethod.priceFormatter(totalSubAmout));

            try {
                OffersCalculationFragment.SELECTED_OFFER = 1;
                for (int i = 0; i < AppData.getInstance().getRestaurantInfoData().getDiscountList().size(); i++) {
                    if (offerId.equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountId())) {
                        //Log.i("OFFER_ID", "Selected " + offerId);
                        //if (totalSubAmout >  Integer.parseInt(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountEligibleAmount())) {
                        if ("Percentage".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountType())) {
                            Double discount = Double.parseDouble(UtilityMethod.priceFormatter((totalSubAmout * Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount()) / 100)));
                            txtViewDiscount.setText("£" +  UtilityMethod.priceFormatter(discount));
                            txtViewGrandTotal.setText("£" + UtilityMethod.priceFormatter((Double.parseDouble(UtilityMethod.priceFormatter(totalSubAmout)) - discount)));
                            Log.i("OFFER_ID_IN", "Selected " + offerId);
                        } else {
                            txtViewDiscount.setText("£" +  UtilityMethod.priceFormatter(Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount())));
                            txtViewGrandTotal.setText("£" + UtilityMethod.priceFormatter(totalSubAmout - Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount())));
                        }

						/*} else {
							txtViewTotal.setText("£" + String.format("%.2f", totalSubAmout));
							txtViewDiscount.setText("£0.0");
						}*/
                        break;
                    }
                }
            } catch (Exception e) {
                txtViewGrandTotal.setText("£" + UtilityMethod.priceFormatter(totalSubAmout));
                txtViewDiscount.setText("£0.00");
            }
        }catch (Exception exDb) {

        }

        /** End total amount */
    }


    private void loadPaymentFragment() {
        Fragment fragment = null;
        fragment = new CheckoutFragment();
        Bundle args = new Bundle();
        args.putInt("selected_service", selectedServiceNo);
        args.putString("selected_policy_id", policyId);
        args.putString("selected_policy_time", policyTime);
        args.putInt("selected_offer", CartFragment.SELECTED_OFFER);
        args.putString("offer_id", CartFragment.offerId);
        args.putString("discount_id", CartFragment.discountId);
        args.putSerializable("all_offer_texts", allOfferTexts);

        if (CartFragment.SELECTED_OFFER == CartFragment.IS_DISCOUNT_SELECTED) {
            args.putString("grand_sub_total", txtViewGrandTotal.getText().toString());
            args.putString("discount_amount", txtViewDiscount.getText().toString());
            args.putString("total_amount", txtViewSubTotal.getText().toString());
            args.putString("offer_text", "");


        } else {
            args.putString("grand_sub_total", txtViewGrandTotal.getText().toString());
            args.putString("discount_amount", txtViewDiscount.getText().toString());
            args.putString("total_amount",txtViewSubTotal.getText().toString());
            args.putString("offer_text", txtViewOfferText.getText().toString());

        }

        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.addToBackStack(null);
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_left);
        ft.add(R.id.content_frame, fragment);
        ft.commit();
    }

    public void openTimePickerDialog () {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Pre-order Time");

        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[] { "10:00 AM", "10:15 AM", "10:30 AM", "10:45 AM", "11:00 AM", "11:30 AM" };
        modeAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);

        final Dialog dialog = builder.create();

        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        switch (v.getId()) {
            case R.id.imgBtnBack:
                break;
            case R.id.btnPlaceOrder:
                loadPaymentFragment();
                break;
            case R.id.btnPreOrder:
                openTimePickerDialog();
                break;
            case R.id.txtViewCancelOffer:
                if(!isDefaultDiscount) {
                    CartFragment.discountId = "";
                }
                allOfferTexts.remove(txtViewOfferText.getText().toString());
                txtViewGrandTotal.setText(txtViewSubTotal.getText().toString());
                txtViewDiscount.setText(getResources().getString(R.string.pound_sign) + "0.00");
                txtViewOfferText.setText("Offers available for you.");
                txtViewCancelOffer.setVisibility(View.GONE);
                offerCalculation ();
                break;
        }
    }

}
