package com.chefonline.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.adapter.OrderHistoryAdapter;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.datamodel.OrderHistoryData;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OrderHistoryFragment extends Fragment implements AdapterView.OnItemClickListener{
    public static String TAG = "OrderHistoryFragment";
    ProgressBar progressBar;
    ListView listViewOrderHistory;
    TextView txtViewCountRestaurant;
    ArrayList<OrderHistoryData> orderHistoryDatas = new ArrayList<>();
    private long mLastClickTime = 0;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //btnTabVoiceOrder.setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Settings");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().findViewById(R.id.toolbar_title1).setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Order History");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().findViewById(R.id.toolbar_title1).setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_order_history, container, false);
        setHasOptionsMenu(true);
        initView(rootView);
        callOrderHistoryApi();
        return rootView;
    }

    private void initView(View rootView) {
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        listViewOrderHistory = (ListView) rootView.findViewById(R.id.listViewRestaurants);
        txtViewCountRestaurant = (TextView) rootView.findViewById(R.id.txtViewCountRestaurant);
        listViewOrderHistory.setOnItemClickListener(this);
    }


    /** Call restaurant category item api*/
    ProgressDialog progress;
    private void callOrderHistoryApi() {
        final PreferenceUtil preferenceUtil = new PreferenceUtil(getActivity());
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "13");
                params.put("userid", preferenceUtil.getUserID());
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

        progress = new ProgressDialog(getActivity());
        progress.setMessage("Please wait....");
        //progress.show();
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progressBar.setVisibility(View.GONE);
                    new CustomToast(getActivity(), getResources().getString(R.string.server_response_error), "", false);
                }catch (Exception e) {
                    Log.e("After volley execution",e.getMessage());
                }

            }
        };
    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //progress.dismiss();
                try {
                    progressBar.setVisibility(View.GONE);
                    Log.e(TAG, "Order History Data**** " + response);

                    orderHistoryDatas = JsonParser.parseOrderHistoryData(response);
                    OrderHistoryAdapter orderHistoryAdapter = new OrderHistoryAdapter(getActivity(), orderHistoryDatas);
                    listViewOrderHistory.setAdapter(orderHistoryAdapter);
                    txtViewCountRestaurant.setText("You have " + orderHistoryDatas.size() + " previous order(s)");

                } catch (Exception e) {
                    try{
                        e.printStackTrace();
                        progressBar.setVisibility(View.GONE);
                    }
                    catch (Exception e1){
                        Log.e("After volley execution",e1.getMessage());
                    }
                }

            }

        };
    }

    private void loadOrderDetailsFragment(String restaurant_id, String order_id, String user_id, String confirmationStatus, String cardFee, String offerDesc) {
        android.app.Fragment fragment = null;
        fragment = new OrderDetailsFragment();
        Bundle args = new Bundle();
        args.putString("restaurant_id", restaurant_id);
        args.putString("confirmation_status", confirmationStatus);
        args.putString("order_id", order_id);
        args.putString("user_id", user_id);
        args.putString("card_fee", cardFee);
        args.putString("offer_desc", offerDesc);
        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.addToBackStack(null);
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.add(R.id.content_frame, fragment);
        ft.commit();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        String restaurant_id = orderHistoryDatas.get(position).getRestaurantId();
        String order_id = orderHistoryDatas.get(position).getOrderNo();
        String user_id = AppData.getInstance().getUserDatas().getUserId();
        String confirmationStatus = orderHistoryDatas.get(position).getPaymentStatus();
        String cardFee = orderHistoryDatas.get(position).getCardFee();
        String offerDesc = orderHistoryDatas.get(position).getOfferDesc();
        loadOrderDetailsFragment(restaurant_id, order_id, user_id, confirmationStatus, cardFee, offerDesc);
    }

}
