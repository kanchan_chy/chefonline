package com.chefonline.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.chefonline.customview.CustomToast;
import com.chefonline.datamodel.NetpayData;
import com.chefonline.online.NetPayActivity;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;

/**
 * Created by user on 9/3/2016.
 */
public class NetPayConfirmFragment extends Fragment implements CompoundButton.OnCheckedChangeListener{

    TextView txtPayTo;
    TextView txtDisplayedName;
    TextView txtPaymentType;
    TextView txtCardNumber;
    TextView txtExpiryDate;
    TextView txtBillingAddress;
    TextView txtNetpayCharge;
    TextView txtTotal;
    TextView txtGrandTotal;
    CheckBox checkBoxSaveCard;
    SwitchCompat switchSave;
    RelativeLayout relativeSave;

    public static boolean saveNetpay = false;
    public boolean isNew = false;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_netpay_confirm, container, false);
        initView(rootView);
        handleUiClick();
        return rootView;
    }

    private void initView(View view) {
        txtPayTo = (TextView) view.findViewById(R.id.txtPayTo);
        txtDisplayedName = (TextView) view.findViewById(R.id.txtDisplayedName);
        txtPaymentType = (TextView) view.findViewById(R.id.txtPaymentType);
        txtCardNumber = (TextView) view.findViewById(R.id.txtCardNumber);
        txtExpiryDate = (TextView) view.findViewById(R.id.txtExpiryDate);
        txtBillingAddress = (TextView) view.findViewById(R.id.txtBillingAddress);
        txtNetpayCharge = (TextView) view.findViewById(R.id.txtNetpayCharge);
        txtTotal = (TextView) view.findViewById(R.id.txtTotal);
        txtGrandTotal = (TextView) view.findViewById(R.id.txtGrandTotal);
        checkBoxSaveCard = (CheckBox) view.findViewById(R.id.checkBoxSaveCard);
        switchSave = (SwitchCompat) view.findViewById(R.id.switchSave);
        relativeSave = (RelativeLayout) view.findViewById(R.id.relativeSave);

        if(!isNew) {
            relativeSave.setVisibility(View.GONE);
            saveNetpay = true;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(this.isVisible()) {
            if(isVisibleToUser) {
                // Confirm fragment becomes visible
                setData();
            } else {
                // Confirm fragment becomes invisible
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    @Override
    public void setArguments(Bundle args) {
        isNew = args.getBoolean("is_new");
    }

    private void handleUiClick() {
        //checkBoxSaveCard.setOnCheckedChangeListener(this);
        switchSave.setOnCheckedChangeListener(this);
    }

    private void setData() {
        NetpayData netpayData = AppData.getInstance().getNetpayData();
        if(netpayData.getBillToCompany() != null && !"".equalsIgnoreCase(netpayData.getBillToCompany())) {
            txtPayTo.setText(netpayData.getBillToCompany());
        } else {
            txtPayTo.setText("Not Found");
        }
        if(!"".equalsIgnoreCase(NetPayActivity.paymentTypeName)) {
            txtPaymentType.setText(NetPayActivity.paymentTypeName.toUpperCase());
        } else {
            txtPaymentType.setText("Not Found");
        }
        if(!"".equalsIgnoreCase(netpayData.getCardNumber())) {
            txtCardNumber.setText(netpayData.getCardNumber());
        } else {
            txtCardNumber.setText("Not Found");
        }
        if(netpayData.getExpiryMonth()!= null && !"".equalsIgnoreCase(netpayData.getExpiryMonth()) && netpayData.getExpiryYear()!= null && !"".equalsIgnoreCase(netpayData.getExpiryYear())) {
            txtExpiryDate.setText(netpayData.getExpiryMonth() + "/" + netpayData.getExpiryYear());
        } else {
            txtExpiryDate.setText("Not Found");
        }
        String displayedName = "";
        if(!"".equalsIgnoreCase(netpayData.getTitle())) {
            displayedName += netpayData.getTitle() + " ";
        }
        if(!"".equalsIgnoreCase(netpayData.getFirstName())) {
            displayedName += netpayData.getFirstName() + " ";
        }
        if(!"".equalsIgnoreCase(netpayData.getMiddleName())) {
            displayedName += netpayData.getMiddleName() + " ";
        }
        if(!"".equalsIgnoreCase(netpayData.getLastName())) {
            displayedName += netpayData.getLastName();
        }
        if(!"".equalsIgnoreCase(displayedName)) {
            txtDisplayedName.setText(displayedName);
        } else {
            txtDisplayedName.setText("Not Found");
        }
        if(!"".equalsIgnoreCase(netpayData.getBillToAddress())) {
            txtBillingAddress.setText(netpayData.getBillToAddress());
        } else {
            txtBillingAddress.setText("Not Found");
        }
        if(!"".equalsIgnoreCase(AppData.getInstance().getOrderPlaceData().getCardFee())) {
            txtNetpayCharge.setText(getResources().getString(R.string.pound_sign) + AppData.getInstance().getOrderPlaceData().getCardFee());
        } else {
            txtNetpayCharge.setText(getResources().getString(R.string.pound_sign) + "0");
        }
        if(!"".equalsIgnoreCase(NetPayActivity.totalStr)) {
            txtTotal.setText(getResources().getString(R.string.pound_sign) + NetPayActivity.totalStr);
        } else {
            txtTotal.setText(getResources().getString(R.string.pound_sign) + "0");
        }
        if(!"".equalsIgnoreCase(AppData.getInstance().getNetpayData().getAmount())) {
            txtGrandTotal.setText(getResources().getString(R.string.pound_sign) + AppData.getInstance().getNetpayData().getAmount());
        } else {
            txtGrandTotal.setText(getResources().getString(R.string.pound_sign) + "0");
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        saveNetpay = isChecked;
    }

}
