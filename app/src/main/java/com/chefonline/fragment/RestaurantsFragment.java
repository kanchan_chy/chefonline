package com.chefonline.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chefonline.adapter.HotRestaurantsAdapter;
import com.chefonline.datamodel.HotRestaurantData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.modelinterface.RestaurantView;
import com.chefonline.online.MainActivity;
import com.chefonline.online.R;
import com.chefonline.online.ReservationActivity;
import com.chefonline.pattern.AppData;
import com.chefonline.presenter.RestaurantPresenter;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UtilityMethod;

import java.util.ArrayList;
import java.util.Locale;

public class RestaurantsFragment extends Fragment implements View.OnClickListener,  RestaurantView {
    //public static int SELECTED_BUTTON = 0;
    private String  key = null;
    private ListView listViewRestaurants;
    private TextView txtViewCountRestaurant;
    private Button btnSearchRestaurant, btnSearchCuisine;
    private LinearLayout linear_action_holder;
    private ProgressBar progressBar;
    private DataBaseUtil db;
    ArrayList<HotRestaurantData> hotRestaurantDataArrayList = new ArrayList<>();
    EditText textSearch;
    HotRestaurantsAdapter adapter;
    RelativeLayout toolbar_top_search;
    ImageButton imgBtnClose;
    View rootView;
    private long mLastClickTime = 0;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getActivity().findViewById(R.id.toolbar_title1).setVisibility(View.VISIBLE);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ConstantValues.SELECTED_BUTTON = 0;
    }

    @Override
    public void onResume() {
        super.onResume();
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Restaurants");
                ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getActivity().findViewById(R.id.toolbar_title1).setVisibility(View.GONE);
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_restaurants, container, false);
        setHasOptionsMenu(true);
        initView(rootView);
        getIntentValue();
        setUiItemListener(rootView);

        RestaurantPresenter restaurantPresenter = new RestaurantPresenter(this);
        restaurantPresenter.getRestaurantList(getActivity(), "6", key, UtilityMethod.getRealCriteriaString(ConstantValues.SELECTED_BUTTON));
        openDb();

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Restaurants");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().findViewById(R.id.toolbar_title1).setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        //inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getFragmentManager().popBackStack();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openDb() {
        db = new DataBaseUtil(getActivity());
        db.open();
    }

    public void initView(View rootView) {
        linear_action_holder = (LinearLayout) rootView.findViewById(R.id.linear_action_holder);
        toolbar_top_search = (RelativeLayout) rootView.findViewById(R.id.toolbar_top_search);
        imgBtnClose = (ImageButton) rootView.findViewById(R.id.imgBtnClose);
        txtViewCountRestaurant = (TextView) rootView.findViewById(R.id.txtViewCountRestaurant);
        btnSearchRestaurant = (Button) rootView.findViewById(R.id.btnSearchRestaurant);
        btnSearchCuisine = (Button) rootView.findViewById(R.id.btnSearchCuisine);
        listViewRestaurants = (ListView) rootView.findViewById(R.id.listViewRestaurants);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

    }

    private void setUiItemListener(View rootView) {
        //listViewRestaurants.setOnItemClickListener(this);
        btnSearchRestaurant.setOnClickListener(this);
        imgBtnClose.setOnClickListener(this);
        btnSearchCuisine.setOnClickListener(this);

        textSearch =(EditText) rootView.findViewById(R.id.searchEditText);
        textSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                try {
                    String text = textSearch.getText().toString().toLowerCase(Locale.getDefault());
                    if (text.length() == 0) {
                        adapter.filter(text);
                    } else {
                        adapter.filter(text);
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });

    }

    public void getIntentValue () {
        ConstantValues.SELECTED_BUTTON = getArguments().getInt("criteria");
        key = getArguments().getString("key");
    }

   private void loadDataToList(int size) {
       String label;
       if (size > 1) {
           label = "Restaurants";
       } else {
           label = "Restaurant";
       }

       txtViewCountRestaurant.setText(size + " " + label + " Found");

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void openNextUI(int position) {
        //AppData.getInstance().setRestaurantInfoData(hotRestaurantDataArrayList.get(position));
        if (ConstantValues.SELECTED_BUTTON == ConstantValues.RESERVATION) {
            //emptyCart();
            startActivity(new Intent(getActivity(), ReservationActivity.class));
        } else {
            loadCategoryFragment(position, Integer.toString(hotRestaurantDataArrayList.get(position).getRestaurantId()));
        }
    }

    private void emptyCart() {
        db.emptyCartData();
        MainActivity.txtViewBubble.setText("" + db.sumCartData());
    }

    /*@Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        AppData.getInstance().hideKeyboard(getActivity(), view);
        String restId = Integer.toString(hotRestaurantDataArrayList.get(position).getRestaurantId());
        if (db.sumCartData() == 0) {
            AppData.getInstance().setRestaurantInfoData(hotRestaurantDataArrayList.get(position));
            openNextUI(position);
            return;
        }

        if (db.checkRestaurant(restId) == 0) {
            openDialog(Integer.toString(hotRestaurantDataArrayList.get(position).getRestaurantId()), position);

        } else {
            openNextUI(position);
        }

    }*/

    private void openDialog(final String restaurant_id, final int position) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_restuarant_override);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }

                mLastClickTime = SystemClock.elapsedRealtime();

                if (ConstantValues.SELECTED_BUTTON == ConstantValues.RESERVATION) {
                    startActivity(new Intent(getActivity(), ReservationActivity.class));
                } else {
                    AppData.getInstance().setRestaurantInfoData(hotRestaurantDataArrayList.get(position));
                    loadCategoryFragment(position, restaurant_id);
                }

                emptyCart();
                dialog.dismiss();
            }

        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ImageButton btnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private void loadFragmentManager(Bundle args) {
        android.app.Fragment fragment = null;
        fragment = new TakeawayCategoryFragment();
        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.addToBackStack(null);
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.add(R.id.content_frame, fragment);
        ft.commit();

    }

    private void loadCategoryFragment(int position, String rest_id) {
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putInt("is_searched_list", 1);
        args.putInt("selected_policy", ConstantValues.SELECTED_BUTTON);
        args.putString("restaurant_id", rest_id);
        loadFragmentManager(args);

    }

    private void openCuisineListDialog(String name, String dishName) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_offers);

        TextView text = (TextView) dialog.findViewById(R.id.txtViewOptionName);
        text.setText(dishName);

        final ListView listView = (ListView) dialog.findViewById(R.id.listViewOptions);
        final ArrayAdapter<String> adapterCuisine = new ArrayAdapter<String>(getActivity(),  android.R.layout.simple_list_item_1, android.R.id.text1, ConstantValues.CUISINE_LIST);
        listView.setAdapter(adapterCuisine);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                if (position == 0) {
                    adapter.filterCuisine("");

                } else {
                    adapter.filterCuisine((String) listView.getItemAtPosition(position));
                }

                dialog.dismiss();
            }
        });

        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        Button btnContinue = (Button) dialog.findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSearchRestaurant:
                toolbar_top_search.setVisibility(View.VISIBLE);
                linear_action_holder.setVisibility(View.GONE);
                txtViewCountRestaurant.setVisibility(View.GONE);
                break;
            case R.id.btnSearchCuisine:
                openCuisineListDialog("", "Cuisine type");
                break;
            case R.id.imgBtnClose:
                textSearch.setText("");
                toolbar_top_search.setVisibility(View.GONE);
                linear_action_holder.setVisibility(View.VISIBLE);
                txtViewCountRestaurant.setVisibility(View.VISIBLE);
                AppData.getInstance().hideKeyboard(getActivity(), v);
                break;

        }
    }

    @Override
    public void setLoadingHide() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setListView(ArrayList<HotRestaurantData> hotRestaurantDataArrayList) {
        this.hotRestaurantDataArrayList = hotRestaurantDataArrayList;
        loadDataToList(hotRestaurantDataArrayList.size()); /* Set data to the list view */
        adapter = new HotRestaurantsAdapter(getActivity(), hotRestaurantDataArrayList, ConstantValues.SELECTED_BUTTON, ConstantValues.SEARCHING_RESULT);
        listViewRestaurants.setAdapter(adapter);
        linear_action_holder.setVisibility(View.VISIBLE);

        adapter.setmOnDataChangeListener(new HotRestaurantsAdapter.OnDataChangeListener() {
            @Override
            public void onDataChanged(int size) {
                loadDataToList(size);
            }
        });
    }

    @Override
    public void setEmptyMessage(String msg) {
        txtViewCountRestaurant.setText(msg);
    }


}

