package com.chefonline.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chefonline.customview.CustomToast;
import com.chefonline.customview.MonthYearPickerDialog;
import com.chefonline.customview.MsmEditText;
import com.chefonline.datamodel.NetpayData;
import com.chefonline.online.NetPayActivity;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;

import java.util.ArrayList;

/**
 * Created by user on 9/3/2016.
 */
public class NetPayPaymentFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener{

    LinearLayout linearPaymentMethodContainer;
    LinearLayout linearSelectedPaymentMethod = null;
    TextView txtErrorPaymentType;
    Button btnMonthYearPicker;
    MsmEditText edtCardNumber;
    MsmEditText edtCardCVV;
    MsmEditText edtCardExpDate;
    MsmEditText edtCardHolderName;
    ImageView imgViewPaymentMethod;
    Context context;

    String expMonth = "";
    String expYear = "";

    private int selectedPaymentType = -1;
    private ArrayList<Integer> paymentTypes;
    private int[] idPaymentTypeIcons = {R.drawable.master_card,
            R.drawable.visa,
            R.drawable.american_express,
            R.drawable.maestro,
            R.drawable.master_card_debit,
            R.drawable.visa_debit,
            R.drawable.visa_electron,
            R.drawable.diners_club_international};
    private String[] paymentTypeCodes = {"MCRD",
            "VISA",
            "AMEX",
            "MSTO",
            "MCDB",
            "VISAUK",
            "ELEC",
            "DINE"
    };
    private String[] paymentTypeNames = {"MasterCard",
            "Visa",
            "American Express",
            "Maestro",
            "MasterCard Debit",
            "Visa Debit UK",
            "Visa Electron",
            "Diners"
    };
    public static boolean checkPaymentValidation = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
     //   new CustomToast(getActivity(), "Payment View Created", "", true);
        View rootView = inflater.inflate(R.layout.fragment_netpay_payment, container, false);
        initView(rootView);
        handleUiClick();
        initData();
        addPaymentMethods();
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(this.isVisible()) {
            if(isVisibleToUser) {
                // Payment fragment becomes visible
                setUiData();
                if(checkPaymentValidation) {
                    checkPaymentValidation = false;
                    if(isValidated()) {

                    }
                }
            } else {
                // Payment fragment becomes invisible
                setPaymentData();
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        setUiData();
        if(checkPaymentValidation) {
            checkPaymentValidation = false;
            if(isValidated()) {

            }
        }
    }


    private void initView(View view) {
        linearPaymentMethodContainer = (LinearLayout) view.findViewById(R.id.linearPaymentMethodContainer);
        txtErrorPaymentType = (TextView) view.findViewById(R.id.txtErrorPaymentType);
        btnMonthYearPicker = (Button) view.findViewById(R.id.btnMonthYearPicker);
        edtCardNumber = (MsmEditText) view.findViewById(R.id.edtCardNumber);
        edtCardCVV = (MsmEditText) view.findViewById(R.id.edtCardCVV);
        edtCardExpDate = (MsmEditText) view.findViewById(R.id.edtCardExpDate);
        edtCardHolderName = (MsmEditText) view.findViewById(R.id.edtCardHolderName);
        imgViewPaymentMethod = (ImageView) view.findViewById(R.id.imgViewPaymentMethod);
        context = view.getContext();
    }

    private void handleUiClick() {
        btnMonthYearPicker.setOnClickListener(this);
    }

    private void initData() {
        paymentTypes = new ArrayList<>();
        paymentTypes.add(ConstantValues.PAYMENT_TYPE_MASTER_CARD);
        paymentTypes.add(ConstantValues.PAYMENT_TYPE_VISA);
        paymentTypes.add(ConstantValues.PAYMENT_TYPE_AMERICAN_EXPRESS);
        paymentTypes.add(ConstantValues.PAYMENT_TYPE_MAESTRO);
        paymentTypes.add(ConstantValues.PAYMENT_TYPE_MASTER_CARD_DEBIT);
        paymentTypes.add(ConstantValues.PAYMENT_TYPE_VISA_DEBIT);
        paymentTypes.add(ConstantValues.PAYMENT_TYPE_VISA_ELECTRON);
        paymentTypes.add(ConstantValues.PAYMENT_TYPE_DINERS_CLUB_INTERNATIONAL);

        if(!"".equalsIgnoreCase(NetPayActivity.paymentTypeName)) {
            for (int i = 0; i < paymentTypeNames.length; i++) {
                if(NetPayActivity.paymentTypeName.equalsIgnoreCase(paymentTypeNames[i])) {
                    selectedPaymentType = i;
                    break;
                }
            }
        }
    }

    private void setUiData() {
        NetpayData netpayData = AppData.getInstance().getNetpayData();
        edtCardNumber.setText(netpayData.getCardNumber());
        edtCardCVV.setText(netpayData.getSecurityCode());
        if(netpayData.getExpiryMonth()!= null && !"".equalsIgnoreCase(netpayData.getExpiryMonth()) && netpayData.getExpiryYear()!= null && !"".equalsIgnoreCase(netpayData.getExpiryYear())) {
            edtCardExpDate.setText(netpayData.getExpiryMonth() + "/" + netpayData.getExpiryYear());
            btnMonthYearPicker.setText(netpayData.getExpiryMonth() + "/" + netpayData.getExpiryYear());
            expMonth = netpayData.getExpiryMonth();
            expYear = netpayData.getExpiryYear();
        }
        edtCardHolderName.setText(netpayData.getFullName());
    }

    private void addPaymentMethods() {
        linearPaymentMethodContainer.removeAllViews();
        int size = 8;
        View[] views = new View[size];
        ImageView[] imgViewPaymentMethods = new ImageView[size];
        View[] viewSeparators = new View[size];
        LinearLayout[] linearPaymentItemHolders = new LinearLayout[size];
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        for (int i = 0; i < size; i++) {
            views[i] = inflater.inflate(R.layout.item_payment_method, null);
            imgViewPaymentMethods[i] = (ImageView) views[i].findViewById(R.id.imgViewPaymentMethod);
            viewSeparators[i] = views[i].findViewById(R.id.viewSeparator);
            imgViewPaymentMethods[i].setImageResource(idPaymentTypeIcons[paymentTypes.get(i)]);
            linearPaymentItemHolders[i] = (LinearLayout) views[i].findViewById(R.id.linearPaymentItemHolder);
            if(i == selectedPaymentType) {
                linearSelectedPaymentMethod = linearPaymentItemHolders[i];
                linearSelectedPaymentMethod.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.theme_red));
                imgViewPaymentMethod.setImageResource(idPaymentTypeIcons[selectedPaymentType]);
                imgViewPaymentMethod.setVisibility(View.VISIBLE);
                txtErrorPaymentType.setVisibility(View.GONE);
            }
            linearPaymentItemHolders[i].setOnClickListener(paymentMethodSelectionListener(i, linearPaymentItemHolders[i]));
            linearPaymentMethodContainer.addView(views[i]);
            if(i == (size - 1)) {
                viewSeparators[i].setVisibility(View.GONE);
            }
        }
    }

    private View.OnClickListener paymentMethodSelectionListener(final int position, final LinearLayout selectedLayout) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPaymentType = position;
                if(linearSelectedPaymentMethod != null) {
                    linearSelectedPaymentMethod.setBackgroundColor(Color.parseColor("#F7F8F8"));
                }
                linearSelectedPaymentMethod = selectedLayout;
                linearSelectedPaymentMethod.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.theme_red));
                imgViewPaymentMethod.setImageResource(idPaymentTypeIcons[selectedPaymentType]);
                imgViewPaymentMethod.setVisibility(View.VISIBLE);
                txtErrorPaymentType.setVisibility(View.GONE);
            }
        };
    }

    public boolean isValidated() {
        if(selectedPaymentType == -1) {
            txtErrorPaymentType.setText("Payment type is required.");
            txtErrorPaymentType.setVisibility(View.VISIBLE);
            return false;
        }
        if(edtCardNumber.getText().toString().trim().equalsIgnoreCase("")) {
            edtCardNumber.setErrorTextVisible(true);
            edtCardNumber.setErrorMsg("Card Number is required.");
            edtCardNumber.setTypingFocus();
            return false;
        }
        if(expMonth.equalsIgnoreCase("") || expYear.equalsIgnoreCase("")) {
            edtCardExpDate.setErrorTextVisible(true);
            edtCardExpDate.setErrorMsg("Card Expiration Date is required.");
            edtCardExpDate.setTypingFocus();
            return false;
        }
        if(edtCardCVV.getText().toString().trim().equalsIgnoreCase("")) {
            edtCardCVV.setErrorTextVisible(true);
            edtCardCVV.setErrorMsg("Security Code is required.");
            edtCardCVV.setTypingFocus();
            return false;
        }
        if(edtCardHolderName.getText().toString().trim().equalsIgnoreCase("")) {
            edtCardHolderName.setErrorTextVisible(true);
            edtCardHolderName.setErrorMsg("Card Holder Name is required.");
            edtCardHolderName.setTypingFocus();
            return false;
        }
        return true;
    }

    private boolean isPaymentDataValidated() {
        NetpayData netpayData = AppData.getInstance().getNetpayData();
        if("".equalsIgnoreCase(netpayData.getCardType())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getCardNumber())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getSecurityCode())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getExpiryMonth())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getExpiryYear())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getFullName())) {
            return false;
        }
        return true;
    }

    public void setPaymentData() {
        NetpayData netpayData = AppData.getInstance().getNetpayData();
        if(selectedPaymentType != -1) {
            NetPayActivity.paymentTypeName = paymentTypeNames[selectedPaymentType];
            netpayData.setCardType(paymentTypeCodes[selectedPaymentType]);
        }
        netpayData.setCardNumber(edtCardNumber.getText().toString().trim());
        netpayData.setSecurityCode(edtCardCVV.getText().toString().trim());
        netpayData.setExpiryMonth(expMonth);
        netpayData.setExpiryYear(expYear);
        netpayData.setFullName(edtCardHolderName.getText().toString().trim());
        AppData.getInstance().setNetpayData(netpayData);
        if(!isPaymentDataValidated()) {
            Intent intent = new Intent("validation_failed");
            intent.putExtra("index", 0);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
            return;
        }
    }

    private void showMonthAndYearPicker() {
        MonthYearPickerDialog pd = new MonthYearPickerDialog();
        pd.setMonthAndYearListener(this);
        pd.show(getFragmentManager(), "MonthYearPickerDialog");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if(monthOfYear < 10) {
            expMonth = "0" + monthOfYear;
        } else {
            expMonth = "" + monthOfYear;
        }
        year = year - 2000;
        if(year < 10) {
            expYear = "0" + year;
        } else {
            expYear = "" + year;
        }
        btnMonthYearPicker.setText(expMonth + "/" + expYear);
        edtCardExpDate.setText(expMonth + "/" + expYear);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnMonthYearPicker) {
            showMonthAndYearPicker();
        }
    }

}
