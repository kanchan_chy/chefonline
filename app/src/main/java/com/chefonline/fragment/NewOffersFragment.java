package com.chefonline.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.adapter.OffersRestaurantsAdapter;
import com.chefonline.customview.CustomToast;
import com.chefonline.datamodel.CollectionDeliveryData;
import com.chefonline.datamodel.HotRestaurantData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.gps.GPSTracker;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.online.R;
import com.chefonline.online.MainActivity;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.TimeManagement;
import com.chefonline.utility.UtilityMethod;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NewOffersFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener{
    private ListView listViewRestaurants;
    private TextView txtViewCountRestaurant,txtAvailability;
    private ProgressBar progressBar;
    private DataBaseUtil db;
    ArrayList<HotRestaurantData> hotRestaurantDataArrayList = new ArrayList<>();
    private long mLastClickTime = 0;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //btnTabVoiceOrder.setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getActivity().findViewById(R.id.toolbar_title1).setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().findViewById(R.id.toolbar_title1).setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_offers, container, false);
        setHasOptionsMenu(true);
        initView(rootView);
        setUiItemListener();
        loadData();
        callRestaurantListApi();
        return rootView;
    }

    private void loadData() {
        db = new DataBaseUtil(getActivity());
        db.open();
    }

    private void initView(View rootView) {
        txtViewCountRestaurant = (TextView) rootView.findViewById(R.id.txtViewCountRestaurant);
        txtAvailability=(TextView)rootView.findViewById(R.id.txtAvailability);
        listViewRestaurants = (ListView) rootView.findViewById(R.id.listViewRestaurants);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
    }

    private void setUiItemListener() {
        listViewRestaurants.setOnItemClickListener(this);

    }

    private void loadDataToList() {
        String label = null;
        int size = AppData.getInstance().getRestaurantDataArrayList().size();

        if (size > 1) {
            label = "Restaurants";
        } else {
            label = "Restaurant";
        }

        txtViewCountRestaurant.setText(size + " " + label + " Found");
        OffersRestaurantsAdapter adapter = new OffersRestaurantsAdapter(getActivity(),  AppData.getInstance().getRestaurantDataArrayList(), ConstantValues.FEATURED_RESULT);
        listViewRestaurants.setAdapter(adapter);

    }

    private void loadCategoryFragment(String rest_id , int position) {
      //  String collectionTime = AppData.getInstance().getRestaurantDataArrayList().get(position).getCollectionTime();
      //  String deliveryTime = AppData.getInstance().getRestaurantDataArrayList().get(position).getDeliveryTime();
        TimeManagement timeManagement = new TimeManagement();
        CollectionDeliveryData collectionDeliveryData = new CollectionDeliveryData();
        collectionDeliveryData = timeManagement.getCollectionDeliveryTIme(AppData.getInstance().getRestaurantDataArrayList().get(position));
        String collectionTime = collectionDeliveryData.getCollection();
        String deliveryTime = collectionDeliveryData.getDelivery();
        if(collectionTime == null) collectionTime = "";
        if(deliveryTime == null) deliveryTime = "";
        android.app.Fragment fragment = null;
        fragment = new TakeawayCategoryFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putInt("is_searched_list", 0);
        args.putInt("selected_policy", ConstantValues.SELECTED_BUTTON);
        args.putString("delivery_time", deliveryTime);
        args.putString("collection_time", collectionTime);
        args.putString("restaurant_id", rest_id);

        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.addToBackStack(null);
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.add(R.id.content_frame, fragment);
        ft.commit();
    }

    private void openDialog(final String restaurant_id, final int position) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_restuarant_override);
        // TextView text = (TextView) dialog.findViewById(R.id.txtViewOptionName);
        //text.setText(dishName);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        // if button is clicked, close the custom dialog
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppData.getInstance().setRestaurantInfoData(AppData.getInstance().getRestaurantDataArrayList().get(position));
                loadCategoryFragment(restaurant_id, position);
                db.emptyCartData();
                MainActivity.txtViewBubble.setText("" + db.sumCartData());
                dialog.dismiss();
            }

        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
            }
        });

        ImageButton btnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    /** Restaurant listing api */
    private void callRestaurantListApi() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = ConstantValues.BASE_API_URL;
        final GPSTracker tracker=new GPSTracker(getActivity());
        StringRequest myReq = new StringRequest(Request.Method.POST, url, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "17");
                params.put("latitude",String.valueOf(tracker.getLatitude()));
                params.put("longitude",String.valueOf(tracker.getLongitude()));
              //  params.put("latitude",String.valueOf(51.4190549));
               // params.put("longitude",String.valueOf(-0.3020312));
                Log.e("params", params.toString());
                return params;
            }
        };

        int socketTimeout = 60000;  //60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);
    }

    private void loadCategoryFragment(int position) {
      //  String collectionTime = AppData.getInstance().getRestaurantDataArrayList().get(position).getCollectionTime();
      //  String deliveryTime = AppData.getInstance().getRestaurantDataArrayList().get(position).getDeliveryTime();
        TimeManagement timeManagement = new TimeManagement();
        CollectionDeliveryData collectionDeliveryData = new CollectionDeliveryData();
        collectionDeliveryData = timeManagement.getCollectionDeliveryTIme(AppData.getInstance().getRestaurantDataArrayList().get(position));
        String collectionTime = collectionDeliveryData.getCollection();
        String deliveryTime = collectionDeliveryData.getDelivery();
        if(collectionTime == null) collectionTime = "";
        if(deliveryTime == null) deliveryTime = "";
        android.app.Fragment fragment = null;
        fragment = new TakeawayCategoryFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putInt("is_searched_list", 1);
        args.putInt("selected_policy", ConstantValues.SELECTED_BUTTON);
        args.putString("delivery_time", deliveryTime);
        args.putString("collection_time", collectionTime);
        args.putString("restaurant_id", Integer.toString(AppData.getInstance().getRestaurantDataArrayList().get(position).getRestaurantId()));

        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.addToBackStack(null);
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.add(R.id.content_frame, fragment);
        ft.commit();
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progressBar.setVisibility(View.GONE);
                    new CustomToast(getActivity(), "Server Response Error " + error.getMessage(), "", false);
                } catch (Exception e) {
                    Log.e("After volley execution",e.getMessage());
                }

            }
        };
    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i("Restaurant *** ", "** " + response);
                    progressBar.setVisibility(View.GONE);
                    GPSTracker gps = new GPSTracker(getActivity());
                    LatLng latLngCurrent = new LatLng(gps.getLatitude(), gps.getLongitude());

                    hotRestaurantDataArrayList =  JsonParser.getHotRestaurant(response, latLngCurrent);
                    if(hotRestaurantDataArrayList == null || hotRestaurantDataArrayList.size() <= 0){
                        txtAvailability.setVisibility(View.VISIBLE);

                    } else {
                        AppData.getInstance().setRestaurantDataArrayList(hotRestaurantDataArrayList);
                        loadDataToList(); /* Set data to the list view */
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        new CustomToast(getActivity(), "Parsing error encountered", "", false);
                    } catch (Exception e1) {
                        Log.e("After volley execution",e1.getMessage());
                    }
                }
            }

        };
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        String restId = Integer.toString(AppData.getInstance().getRestaurantDataArrayList().get(position).getRestaurantId());
        if (db.sumCartData() == 0) {
            AppData.getInstance().setRestaurantInfoData(AppData.getInstance().getRestaurantDataArrayList().get(position));
            loadCategoryFragment(position);
            return;
        }

        if (db.checkRestaurant(restId) == 0) {
            openDialog(Integer.toString(AppData.getInstance().getRestaurantDataArrayList().get(position).getRestaurantId()), position);

        } else {
            AppData.getInstance().setRestaurantInfoData(AppData.getInstance().getRestaurantDataArrayList().get(position));
            loadCategoryFragment(position);

        }
    }
}
