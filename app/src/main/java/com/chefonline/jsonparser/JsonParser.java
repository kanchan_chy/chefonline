package com.chefonline.jsonparser;

import android.util.Log;

import com.chefonline.datamodel.AlergensData;
import com.chefonline.datamodel.CategoryData;
import com.chefonline.datamodel.CuisineData;
import com.chefonline.datamodel.DiscountData;
import com.chefonline.datamodel.DishData;
import com.chefonline.datamodel.HotRestaurantData;
import com.chefonline.datamodel.LocationData;
import com.chefonline.datamodel.NewAddressData;
import com.chefonline.datamodel.OffersData;
import com.chefonline.datamodel.OpenEndTimeData;
import com.chefonline.datamodel.OptionData;
import com.chefonline.datamodel.OrderHistoryData;
import com.chefonline.datamodel.PizzaGroupData;
import com.chefonline.datamodel.PizzaGroupItemData;
import com.chefonline.datamodel.PlaceOrderResponseData;
import com.chefonline.datamodel.PolicyData;
import com.chefonline.datamodel.PostCodeData;
import com.chefonline.datamodel.PostcodeChargeData;
import com.chefonline.datamodel.ReservationData;
import com.chefonline.datamodel.ScheduleData;
import com.chefonline.datamodel.UserData;
import com.chefonline.pattern.AppData;
import com.google.android.gms.drive.internal.StringListResponse;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by masum on 10/03/2015.
 */
public class JsonParser {
    public static String TAG = "JsonParser";

    //API 2
    public static ArrayList<CategoryData> categoryParser(String response) {
        ArrayList<CategoryData> categoryList = new ArrayList<CategoryData>();
        //ArrayList<DishData> dishDatas = new ArrayList<>();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("app");
            int jsonAppArrayLength = jsonArray.length();

            for (int appCount = 0; appCount < jsonAppArrayLength; appCount++) {
                Log.i(TAG, "Cuisine Name *** " + jsonArray.getJSONObject(appCount).getString("Cuisine_name"));

                JSONArray jsonCategory = jsonArray.getJSONObject(appCount).getJSONArray("category");
                int catLength = jsonCategory.length();

                if (catLength == 0) {
                    return null;
                }

                for (int i = 0; i < catLength; i++) {
                    String name = jsonCategory.getJSONObject(i).getString("Category_Name");
                    Log.i("Category Name", "" + name);
                    CategoryData categoryData = new CategoryData();
                    categoryData.setCategoryName(name);

                    /* Dish parsing */
                    ArrayList<DishData> dishDatas = new ArrayList<>();
                    int itemLength = jsonCategory.getJSONObject(i).getJSONObject("Dish_Information").getJSONArray("ItemList").length();
                    JSONArray jsonArrayDishes = jsonCategory.getJSONObject(i).getJSONObject("Dish_Information").getJSONArray("ItemList");
                    for (int j = 0; j < itemLength; j++) {
                        DishData dishData = new DishData();
                        dishData.setDishId(jsonArrayDishes.getJSONObject(j).getString("Dish_id"));
                        dishData.setDishName(jsonArrayDishes.getJSONObject(j).getString("Dish_Name"));
                        dishData.setDishDescription(jsonArrayDishes.getJSONObject(j).getString("Dish_Description"));
                        dishData.setDishPrice(jsonArrayDishes.getJSONObject(j).getString("Dish_Price"));

                        ArrayList<AlergensData> alergensDatas = new ArrayList<>();

                        if (!jsonArrayDishes.getJSONObject(j).isNull("Dish_Allergens")) {
                            int alergensDataLength = jsonArrayDishes.getJSONObject(j).getJSONArray("Dish_Allergens").length();
                            JSONArray jsonAlergensArray = jsonArrayDishes.getJSONObject(j).getJSONArray("Dish_Allergens");

                            for (int k = 0; k < alergensDataLength; k++) {
                                AlergensData  alergensData = new AlergensData();
                                alergensData.setAlergensNo(jsonAlergensArray.get(k).toString());
                                alergensDatas.add(alergensData);
                            }
                        }

                        dishData.setAlergensDatas(alergensDatas);
                        //dishData.setDishAllergens(jsonArrayDishes.getJSONObject(j).getString("Dish_Allergens"));
                        /* Option parsing */
                        ArrayList<OptionData> optionDatas = new ArrayList<>();
                        if (!jsonArrayDishes.getJSONObject(j).isNull("options")) {
                            JSONArray jsonOptionsArray = jsonArrayDishes.getJSONObject(j).getJSONObject("options").getJSONArray("OptionList");
                            int optionLength = jsonOptionsArray.length();
                            for (int k = 0; k < optionLength; k++) {
                                OptionData optionData = new OptionData();
                                optionData.setOptionParentDishId(jsonOptionsArray.getJSONObject(k).getString("parent_dish_id"));
                                optionData.setOptionSelfId(jsonOptionsArray.getJSONObject(k).getString("self_id"));
                                optionData.setOptionName(jsonOptionsArray.getJSONObject(k).getString("option_name"));
                                optionData.setOptionDescription("Some description goes here");
                                optionData.setOptionPrice(jsonOptionsArray.getJSONObject(k).getString("option_price"));
                                Log.i("   -->> Option Name", "" + jsonOptionsArray.getJSONObject(k).getString("option_name"));
                                optionDatas.add(optionData);
                            }
                        }

                        dishData.setOptionDatas(optionDatas);
                        dishDatas.add(dishData);

                    }

                    categoryData.setDishDatas(dishDatas);
                    categoryList.add(categoryData);

                }

            }

        } catch (Exception e) {
            Log.i("Extension Error", "" + e.getMessage());
            e.getStackTrace();
            return null;
        }

        return categoryList;
    }

    public static ArrayList<CuisineData> cuisineParser(String response) {
        ArrayList<CuisineData> cuisineList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(response);
            if("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                JSONArray jCuisineList = jsonObject.getJSONObject("restaurant_info").getJSONArray("cuisine");
                if(jCuisineList != null && jCuisineList.length() > 0) {
                    for (int i = 0; i < jCuisineList.length(); i++) {
                        CuisineData cuisineData = new CuisineData();

                        JSONObject jCuisine = jCuisineList.getJSONObject(i);
                        cuisineData.setCuisineId(jCuisine.getString("cuisine_id"));
                        cuisineData.setCuisineName(jCuisine.getString("cuisine_name"));

                        ArrayList<CategoryData> categoryList = new ArrayList<>();
                        JSONArray jCategoryList = jCuisine.getJSONArray("category");
                        for (int j = 0; j < jCategoryList.length(); j++) {
                            CategoryData categoryData = new CategoryData();

                            JSONObject jCategory = jCategoryList.getJSONObject(j);
                            categoryData.setCategoryId(jCategory.getString("category_id"));
                            categoryData.setCategoryName(jCategory.getString("category_name"));

                            ArrayList<DishData> dishList = new ArrayList<>();
                            JSONArray jDishList = jCategory.getJSONArray("dish");
                            for (int k = 0; k < jDishList.length(); k++) {
                                DishData dishData = new DishData();

                                JSONObject jDish = jDishList.getJSONObject(k);
                                dishData.setDishId(jDish.getString("dish_id"));
                                dishData.setDishName(jDish.getString("dish_name"));
                                dishData.setDishDescription(jDish.getString("dish_description"));
                             //   dishData.setDishRating(jDish.getString("dish_rating"));
                             //   dishData.setDishTotalRating(jDish.getString("dish_total_rating"));
                                dishData.setDishSpiceLevel(jDish.getString("dish_spice_level"));

                              //  Log.e("before_allergense", "success");
                              //  dishData.setDishAllergens(jDish.getString("dish_allergens"));
                                ArrayList<AlergensData> alergensDatas = new ArrayList<>();
                                if(!jDish.get("dish_allergens").equals(null) && !jDish.get("dish_allergens").equals("")) {
                              //  if (!jDish.isNull("dish_allergens")) {
                                    int alergensDataLength = jDish.getJSONArray("dish_allergens").length();
                                    JSONArray jsonAlergensArray = jDish.getJSONArray("dish_allergens");

                                    for (int e = 0; e < alergensDataLength; e++) {
                                        AlergensData  alergensData = new AlergensData();
                                        alergensData.setAlergensNo(jsonAlergensArray.get(e).toString());
                                        alergensDatas.add(alergensData);
                                    }
                                }
                                dishData.setAlergensDatas(alergensDatas);
                           //     Log.e("after_allergense", "success");

                                dishData.setDishPrice(jDish.getString("dish_price"));
                                dishData.setDishAcmPrice(jDish.getString("dish_acm_price"));

                                ArrayList<OptionData> optionDatas = new ArrayList<>();
                                if (!jDish.isNull("option")) {
                                    JSONArray jsonOptionsArray = jDish.getJSONArray("option");
                                    int optionLength = jsonOptionsArray.length();
                                    for (int e = 0; e < optionLength; e++) {
                                        OptionData optionData = new OptionData();
                                        optionData.setOptionParentDishId(jsonOptionsArray.getJSONObject(e).getString("parent_dish_id"));
                                        optionData.setOptionSelfId(jsonOptionsArray.getJSONObject(e).getString("self_id"));
                                        optionData.setOptionName(jsonOptionsArray.getJSONObject(e).getString("option_name"));
                                        optionData.setOptionDescription(jsonOptionsArray.getJSONObject(e).getString("option_description"));
                                        optionData.setOptionPrice(jsonOptionsArray.getJSONObject(e).getString("option_price"));
                                        optionDatas.add(optionData);
                                    }
                                }
                                dishData.setOptionDatas(optionDatas);

                                dishData.setPizzaMenu(jDish.getBoolean("is_pizza_menu"));
                                if(dishData.isPizzaMenu()) {
                                    ArrayList<PizzaGroupData> pizzaGroupList = new ArrayList<>();

                                    JSONArray jPizzaList = jDish.getJSONArray("pizza_group");
                                    for (int e = 0; e < jPizzaList.length(); e++) {
                                        PizzaGroupData pizzaGroupData = new PizzaGroupData();

                                        pizzaGroupData.setPizzaGroupId(jPizzaList.getJSONObject(e).getInt("pizza_group_id"));
                                        pizzaGroupData.setMinSelection(jPizzaList.getJSONObject(e).getInt("min_selection"));
                                        pizzaGroupData.setMaxSelection(jPizzaList.getJSONObject(e).getInt("max_selection"));

                                        ArrayList<PizzaGroupItemData> pizzaGroupItemList = new ArrayList<>();

                                        JSONArray jPizzaItemList = jPizzaList.getJSONObject(e).getJSONArray("item");
                                        for (int f = 0; f < jPizzaItemList.length(); f++) {
                                            PizzaGroupItemData pizzaGroupItemData = new PizzaGroupItemData();

                                            pizzaGroupItemData.setPizzaGroupItemId(jPizzaItemList.getJSONObject(f).getString("pizza_group_item_id"));
                                            pizzaGroupItemData.setItemName(jPizzaItemList.getJSONObject(f).getString("item_name"));
                                            pizzaGroupItemData.setItemPrice(jPizzaItemList.getJSONObject(f).getString("item_price"));
                                            pizzaGroupItemData.setDefaultStatus(jPizzaItemList.getJSONObject(f).getInt("default_status"));

                                            pizzaGroupItemList.add(pizzaGroupItemData);
                                        }
                                        pizzaGroupData.setPizzaGroupItemList(pizzaGroupItemList);

                                        pizzaGroupList.add(pizzaGroupData);
                                    }
                                    dishData.setPizzaGroupList(pizzaGroupList);

                                }
                                dishList.add(dishData);
                            }
                            categoryData.setDishDatas(dishList);

                            categoryList.add(categoryData);
                        }
                        cuisineData.setCuisineCategories(categoryList);

                        cuisineList.add(cuisineData);
                    }

                }
            }
        } catch (Exception e) {
            Log.e("dish_parser", "" + e);
            return null;
        }

        return cuisineList;
    }

    public static String getPostCode(String response){
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("result");

            int length = jsonArray.length();
            for (int i = 0; i < 1 ; i++) {
                return jsonArray.getJSONObject(i).getString("postcode");
            }

        } catch (Exception e) {
            return "";
        }

        return "";
    }

    //API 44
    public static LocationData getRealIP(String response){
        Log.e("Get_IP", "****" + response);
        LocationData locationData = new LocationData();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            locationData.setStatus(jsonObject.getString("status"));
            locationData.setIpAddress(jsonObject.getJSONObject("details").getString("ip"));
            locationData.setCountryName(jsonObject.getJSONObject("details").getString("country"));
        } catch (Exception e) {
            return null;
        }

        return locationData;
    }

    //API 5, 6
    public static ArrayList<HotRestaurantData> getHotRestaurant(String response, LatLng latLngCurrent) throws JSONException{
        ArrayList<HotRestaurantData> hotRestaurantDataArrayList = new ArrayList<HotRestaurantData>();
            Log.i("CURRENT_LOCATION", "**** " + latLngCurrent.latitude + ", " + latLngCurrent.longitude);
            JSONObject jsonObject = new org.json.JSONObject(response);
        String status = jsonObject.getString("status");
        if(status.equals("Failed")) return hotRestaurantDataArrayList;
            JSONArray jsonArray = jsonObject.getJSONArray("app");
            int length = jsonArray.length();
            for (int i = 0; i < length; i++) {
                HotRestaurantData hotRestaurantData = new HotRestaurantData();
                hotRestaurantData.setRestaurantId(Integer.parseInt(jsonArray.getJSONObject(i).getString("rest_id")));
                hotRestaurantData.setRestaurantName(jsonArray.getJSONObject(i).getString("restaurant_name"));
                hotRestaurantData.setRestaurantLogo(jsonArray.getJSONObject(i).getString("logo"));
                hotRestaurantData.setPostCode(jsonArray.getJSONObject(i).getString("postcode"));
                hotRestaurantData.setRestaurantAddress1(jsonArray.getJSONObject(i).getString("address1"));
                hotRestaurantData.setRestaurantAddress2(jsonArray.getJSONObject(i).getString("address2"));
                hotRestaurantData.setRestaurantCity(jsonArray.getJSONObject(i).getString("city"));
                hotRestaurantData.setRestaurantLat(jsonArray.getJSONObject(i).getString("latitude"));
                hotRestaurantData.setRestaurantLon(jsonArray.getJSONObject(i).getString("longitude"));
                hotRestaurantData.setBusinessTel(jsonArray.getJSONObject(i).getString("business_tel"));
                hotRestaurantData.setRestaurantTelSingle(jsonArray.getJSONObject(i).getString("res_business_tel_for_call"));
                hotRestaurantData.setReservationTel(jsonArray.getJSONObject(i).getString("reservation_tel"));
                hotRestaurantData.setRestaurantPayPalEmail(jsonArray.getJSONObject(i).getString("paypal_email"));
                hotRestaurantData.setNetpayStatus(jsonArray.getJSONObject(i).getString("netpay"));

                if("1".equalsIgnoreCase(hotRestaurantData.getNetpayStatus())) {
                    hotRestaurantData.setNetpayChargeType(jsonArray.getJSONObject(i).getJSONObject("netpay_charge").getString("charge_type"));
                    hotRestaurantData.setNetpayCustomerCharge(jsonArray.getJSONObject(i).getJSONObject("netpay_charge").getString("customer_charge"));
                }

                hotRestaurantData.setComingSoon(jsonArray.getJSONObject(i).getString("chefonline_coming_soon"));

                hotRestaurantData.setRestaurantAcceptReservation(jsonArray.getJSONObject(i).getInt("accept_reservation"));
                hotRestaurantData.setRestaurantAcceptCollection(jsonArray.getJSONObject(i).getInt("accept_collection"));
                hotRestaurantData.setRestaurantAcceptDelivery(jsonArray.getJSONObject(i).getInt("accept_delivery"));

                if (!"".equalsIgnoreCase(jsonArray.getJSONObject(i).getString("distance")) && !"NONE".equalsIgnoreCase(jsonArray.getJSONObject(i).getString("distance"))) {
                    hotRestaurantData.setRestaurantDistance(jsonArray.getJSONObject(i).getString("distance"));
                }  else {
                    hotRestaurantData.setRestaurantDistance("0.0");
                }

                /** Start parsing opening time and end time*/
                ArrayList<ScheduleData> scheduleDatas = new ArrayList<>();
                if (!jsonArray.getJSONObject(i).isNull("restuarent_schedule")) {
                    JSONArray jsonArraySchedule = jsonArray.getJSONObject(i).getJSONObject("restuarent_schedule").getJSONArray("schedule");
                    int LengthJsonArraySchedule = jsonArraySchedule.length();
                    for (int j = 0; j < LengthJsonArraySchedule; j++) {
                        JSONArray jsonScheduleList = jsonArraySchedule.getJSONObject(j).getJSONArray("list");
                        ScheduleData scheduleData = new ScheduleData();
                        scheduleData.setDayName(jsonArraySchedule.getJSONObject(j).getString("day_name"));
                        scheduleData.setWeekDayId(jsonArraySchedule.getJSONObject(j).getString("weekday_id"));
                        ArrayList<OpenEndTimeData> openEndTimeDatas = new ArrayList<>();
                        ArrayList<OpenEndTimeData> reservationDatas = new ArrayList<>();
                        int jsonScheduleListLength = jsonScheduleList.length();
                        for (int k = 0; k < jsonScheduleListLength; k++) {
                            OpenEndTimeData openEndTimeData = new OpenEndTimeData();
                            openEndTimeData.setOpeningTime(jsonScheduleList.getJSONObject(k).getString("opening_time"));
                            openEndTimeData.setEndTime(jsonScheduleList.getJSONObject(k).getString("closing_time"));
                            openEndTimeData.setCollection(jsonScheduleList.getJSONObject(k).getString("Collection"));
                            openEndTimeData.setDelivery(jsonScheduleList.getJSONObject(k).getString("Delivery"));
                            openEndTimeData.setDeliveryPolicyId(jsonScheduleList.getJSONObject(k).getString("Delivery_policy_id"));
                            openEndTimeData.setCollectionPolicyId(jsonScheduleList.getJSONObject(k).getString("Collection_policy_id"));
                            openEndTimeData.setLastDeliverySubmission(jsonScheduleList.getJSONObject(k).getString("last_time_for_delivery_submit"));
                            openEndTimeData.setLastCollectionSubmission(jsonScheduleList.getJSONObject(k).getString("last_time_for_collection_submit"));
                            openEndTimeData.setShiftSerial(jsonScheduleList.getJSONObject(k).getInt("shift"));
                            openEndTimeData.setType(jsonScheduleList.getJSONObject(k).getString("type"));
                            openEndTimeData.setMinOrder(jsonScheduleList.getJSONObject(k).getString("min_order"));
                            openEndTimeData.setMinOrderCollection(jsonScheduleList.getJSONObject(k).getString("min_order_collection"));
                            openEndTimeData.setTimingFor(jsonScheduleList.getJSONObject(k).getString("timing_for"));
                            if(openEndTimeData.getType() != null && "3".equalsIgnoreCase(openEndTimeData.getType())) {
                                openEndTimeDatas.add(openEndTimeData);
                            } else {
                                reservationDatas.add(openEndTimeData);
                            }
                        }

                        scheduleData.setOpenEndTimeDatas(openEndTimeDatas);
                        scheduleData.setReservationDatas(reservationDatas);
                        scheduleDatas.add(scheduleData);
                    }

                }

                hotRestaurantData.setScheduleDatas(scheduleDatas);
                /* End parsing opening time and end time */

                /** Start parse cuisine array*/
                ArrayList<CuisineData> cuisineDatas = new ArrayList<>();
                if (!jsonArray.getJSONObject(i).isNull("available_cuisine")) {
                    JSONArray jsonArrayCuisine = jsonArray.getJSONObject(i).getJSONObject("available_cuisine").getJSONArray("cuisine");
                    int lengthCuisine = jsonArray.getJSONObject(i).getJSONObject("available_cuisine").getJSONArray("cuisine").length();
                    for (int j = 0; j < lengthCuisine; j++) {
                        CuisineData cuisineData = new CuisineData();
                        cuisineData.setCuisineName(jsonArrayCuisine.getJSONObject(j).getString("name"));
                        cuisineDatas.add(cuisineData);
                    }

                }
                hotRestaurantData.setCuisineList(cuisineDatas);
                /*End parse cuisine array*/

                /** Start parse discount array*/
                ArrayList<DiscountData> discountDatas = new ArrayList<>();

                if (jsonArray.getJSONObject(i).getJSONObject("discount").getInt("status") != 0) {
                    if (!jsonArray.getJSONObject(i).isNull("discount")) {
                        JSONArray jsonArrayDiscount = jsonArray.getJSONObject(i).getJSONObject("discount").getJSONArray("off");
                        int lengthDiscountArray = jsonArrayDiscount.length();

                        for (int j = 0; j < lengthDiscountArray; j++) {
                            DiscountData discountData = new DiscountData();
                            discountData.setDiscountId(jsonArrayDiscount.getJSONObject(j).getString("discount_id"));
                            discountData.setDiscountImage(jsonArrayDiscount.getJSONObject(j).getString("image"));
                            discountData.setRestaurantOrderPolicyId(jsonArrayDiscount.getJSONObject(j).getString("restaurant_order_policy_id"));
                            discountData.setDiscountTitle(jsonArrayDiscount.getJSONObject(j).getString("discount_title"));
                            discountData.setDiscountDescription(jsonArrayDiscount.getJSONObject(j).getString("discount_description"));
                            discountData.setDiscountAmount(jsonArrayDiscount.getJSONObject(j).getString("discount_amount"));
                            discountData.setDiscountType(jsonArrayDiscount.getJSONObject(j).getString("discount_type"));
                            discountData.setDiscountEligibleAmount(jsonArrayDiscount.getJSONObject(j).getString("eligible_amount"));
                            discountData.setDefaultStatus(jsonArrayDiscount.getJSONObject(j).getString("default"));
                            discountData.setActiveStatus(jsonArrayDiscount.getJSONObject(j).getInt("active"));
                            discountDatas.add(discountData);
                        }
                    }
                }
                hotRestaurantData.setDiscountList(discountDatas);
                /* End parse discount array*/

                /** Start parse policy data array */
                ArrayList<PolicyData> policyDatas = new ArrayList<>();
                if (!jsonArray.getJSONObject(i).isNull("order_policy")) {
                    JSONArray jsonArrayOrderPolicy = jsonArray.getJSONObject(i).getJSONObject("order_policy").getJSONArray("policy");
                    int lengthPolicyArray = jsonArray.getJSONObject(i).getJSONObject("order_policy").getJSONArray("policy").length();

                    for (int j = 0; j < lengthPolicyArray; j++) {
                        PolicyData policyData = new PolicyData();
                        policyData.setPolicyId(jsonArrayOrderPolicy.getJSONObject(j).getString("policy_id"));
                        policyData.setPolicyName(jsonArrayOrderPolicy.getJSONObject(j).getString("policy_name"));
                        policyData.setPolicyTime(jsonArrayOrderPolicy.getJSONObject(j).getString("policy_time"));
                        policyData.setMinOrder(jsonArrayOrderPolicy.getJSONObject(j).getString("min_order"));
                        policyData.setStatus(jsonArrayOrderPolicy.getJSONObject(j).getString("status"));
                        policyDatas.add(policyData);
                    }
                }
                hotRestaurantData.setPolicyList(policyDatas);
                /* End parse policy data array */


                /** Start Offers parsing */
                ArrayList<OffersData> offersDatas = new ArrayList<>();
                if (jsonArray.getJSONObject(i).getJSONObject("offer").getInt("status") !=0) {
                    JSONArray jsonArrayOffers = jsonArray.getJSONObject(i).getJSONObject("offer").getJSONArray("offer_list");
                    int jsonArrayOfferSize = jsonArrayOffers.length();
                    for (int j = 0; j < jsonArrayOfferSize; j++) {
                        OffersData offersData = new OffersData();
                        offersData.setOfferId(jsonArrayOffers.getJSONObject(j).getString("id"));
                        offersData.setOfferStatus(jsonArrayOffers.getJSONObject(j).getString("status"));
                        offersData.setRestaurantId(jsonArrayOffers.getJSONObject(j).getString("restaurant_id"));
                        offersData.setOfferDescription(jsonArrayOffers.getJSONObject(j).getString("description"));
                        offersData.setOfferTitle(jsonArrayOffers.getJSONObject(j).getString("offer_title"));
                        offersData.setOfferImage(jsonArrayOffers.getJSONObject(j).getString("image"));

                        if (!jsonArrayOffers.getJSONObject(j).isNull("thumb_name")) {
                            offersData.setOfferThumb(jsonArrayOffers.getJSONObject(j).getString("thumb_name"));
                        }

                        offersData.setOfferElegibleAmount(jsonArrayOffers.getJSONObject(j).getString("eligible_amount"));
                        offersData.setRestaurantOrderPolicyId(jsonArrayOffers.getJSONObject(j).getString("restaurant_order_policy_id"));
                        offersData.setOfferPosition(jsonArrayOffers.getJSONObject(j).getString("position"));
                        if (!jsonArrayOffers.getJSONObject(j).isNull("image_from")) {
                            offersData.setImageFrom(jsonArrayOffers.getJSONObject(j).getString("image_from"));
                        }
                        offersData.setDefaultStatus(jsonArrayOffers.getJSONObject(j).getString("default"));
                        offersData.setActiveStatus(jsonArrayOffers.getJSONObject(j).getInt("active"));
                        offersData.setOfferFor(jsonArrayOffers.getJSONObject(j).getString("offer_for"));
                        offersDatas.add(offersData);
                    }
                }

                hotRestaurantData.setOffersDatas(offersDatas);
                /** End Offers parsing */

                //Object added to main array List
                hotRestaurantDataArrayList.add(hotRestaurantData);
            }

        /*Collections.sort(hotRestaurantDataArrayList, new Comparator<HotRestaurantData>() {
            @Override
            public int compare(HotRestaurantData lhs, HotRestaurantData rhs) {
                return  Double.compare(lhs.getRestaurantDistance(), rhs.getRestaurantDistance());
            }
        });*/

        return hotRestaurantDataArrayList;
    }

    public static UserData parserUserData(String response) throws JSONException{
        UserData userData = new UserData();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            userData.setLoginStatus(jsonObject.getString("status"));

            if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                userData.setUserId(jsonObject.getJSONObject("UserDetails").getString("userid"));
                userData.setTitle(jsonObject.getJSONObject("UserDetails").getString("title"));
                userData.setFirstName(jsonObject.getJSONObject("UserDetails").getString("first_name"));
                userData.setLastName(jsonObject.getJSONObject("UserDetails").getString("last_name"));
                userData.setSurName(jsonObject.getJSONObject("UserDetails").getString("sur_name"));
                userData.setCity(jsonObject.getJSONObject("UserDetails").getString("town"));
                userData.setCountry(jsonObject.getJSONObject("UserDetails").getString("country"));
                userData.setPostCode(jsonObject.getJSONObject("UserDetails").getString("postcode"));
                userData.setMobileNo(jsonObject.getJSONObject("UserDetails").getString("mobile_no"));
                userData.setAddress1(jsonObject.getJSONObject("UserDetails").getString("address1"));
                userData.setAddress2(jsonObject.getJSONObject("UserDetails").getString("address2"));
                userData.setDob(jsonObject.getJSONObject("UserDetails").getString("date_of_birth"));
                userData.setDoa(jsonObject.getJSONObject("UserDetails").getString("date_of_anniversery"));
                userData.setTelNo(jsonObject.getJSONObject("UserDetails").getString("telephone_no"));
                userData.setTelNo(jsonObject.getJSONObject("UserDetails").getString("telephone_no"));
                userData.setEmailId(jsonObject.getJSONObject("UserDetails").getString("email"));
                userData.setGroupId(jsonObject.getJSONObject("UserDetails").getString("user_group_id"));
            }

        } catch (Exception e) {
            //throws JSONException;
        }

        return userData;
    }


    public static UserData parseRegistrationData(String response) throws JSONException{
        UserData userData = new UserData();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            userData.setLoginStatus(jsonObject.getString("status"));
            userData.setLoginMessage(jsonObject.getString("msg"));

            if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                userData.setUserId(jsonObject.getJSONObject("UserDetails").getString("userid"));
                userData.setTitle(jsonObject.getJSONObject("UserDetails").getString("title"));
                userData.setEmailId(jsonObject.getJSONObject("UserDetails").getString("email"));
                userData.setFirstName(jsonObject.getJSONObject("UserDetails").getString("first_name"));
                userData.setLastName(jsonObject.getJSONObject("UserDetails").getString("last_name"));
                userData.setAddress1(jsonObject.getJSONObject("UserDetails").getString("address1"));
                userData.setAddress2(jsonObject.getJSONObject("UserDetails").getString("address2"));
                userData.setPostCode(jsonObject.getJSONObject("UserDetails").getString("postcode"));
                userData.setMobileNo(jsonObject.getJSONObject("UserDetails").getString("mobile_no"));
                userData.setTelNo(jsonObject.getJSONObject("UserDetails").getString("telephone_no"));

                userData.setDob(jsonObject.getJSONObject("UserDetails").getString("date_of_birth"));
                userData.setDoa(jsonObject.getJSONObject("UserDetails").getString("date_of_anniversery"));
                userData.setCity(jsonObject.getJSONObject("UserDetails").getString("town"));
                userData.setCountry(jsonObject.getJSONObject("UserDetails").getString("country"));
            }

        } catch (Exception e) {
            //throws JSONException;
        }

        return userData;
    }


    public static ReservationData parseReservationData(String response) throws JSONException{
        ReservationData reservationData = new ReservationData();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            reservationData.setStatus(jsonObject.getString("status"));

            if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                reservationData.setMessage(jsonObject.getString("msg"));
            }

        } catch (Exception e) {
            //throws JSONException;
        }

        return reservationData;
    }

    public static int parseForgotPasswordData(String response) throws JSONException{

        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            JSONObject jsonAppObject = jsonObject.getJSONObject("app");
            jsonAppObject.getString("status");

        } catch (Exception e) {
            //throws JSONException;
        }

        return 1;
    }


    public static PlaceOrderResponseData parsePlaceOrderCompleteData(String response) throws JSONException{
        PlaceOrderResponseData placeOrderResponseData = new PlaceOrderResponseData();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            //JSONObject jsonAppObject = jsonObject.getJSONObject("app");
            if (jsonObject.getString("status").equalsIgnoreCase("Success")) {
                placeOrderResponseData.setStatus(1);
                return placeOrderResponseData;

            } else if (jsonObject.getString("status").equalsIgnoreCase("sms_sent")) {
                placeOrderResponseData.setStatus(2);
                placeOrderResponseData.setCardFee("0");
                placeOrderResponseData.setDeliveryCharge("0");
                if ("false".equalsIgnoreCase(jsonObject.getString("is_special_message_required"))) {
                    placeOrderResponseData.setIsSpecialMessageRequired("0");
                } else {
                    placeOrderResponseData.setIsSpecialMessageRequired("1");
                }

                if ("false".equalsIgnoreCase(jsonObject.getString("is_varification_required"))) {
                    placeOrderResponseData.setIsVarificationRequired("0");
                } else {
                    placeOrderResponseData.setIsVarificationRequired("1");
                }

                placeOrderResponseData.setCardFee("0");
                placeOrderResponseData.setUserAddressExtId("0");
                return placeOrderResponseData;

            }else if (jsonObject.getString("status").equalsIgnoreCase("Failed")) {
                placeOrderResponseData.setStatus(3);
                placeOrderResponseData.setCardFee("0");
                placeOrderResponseData.setDeliveryCharge("0");
                placeOrderResponseData.setIsSpecialMessageRequired("0");
                placeOrderResponseData.setIsVarificationRequired("0");
                placeOrderResponseData.setUserAddressExtId("0");
                return placeOrderResponseData;

            } else {
                placeOrderResponseData.setStatus(0);
                return placeOrderResponseData;

            }

        } catch (Exception e) {
            Log.e(TAG, "parsePlaceOrderCompleteData: " + e.getMessage());
            placeOrderResponseData.setStatus(0);
            return placeOrderResponseData;
        }

    }

    //ApI 25 parser
    public static List<String> parsePostCodeData(String response) throws JSONException{
        PostCodeData postCodeData = new PostCodeData();
        List<String> list;
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            JSONArray jsonAppArray = jsonObject.getJSONArray("app");
            //jsonAppArray.getJSONObject(0).getString("postcode_list");
            list = new ArrayList<String>(Arrays.asList(jsonAppArray.getJSONObject(0).getString("postcode_list").split(",")));
            Log.i(TAG, "POSTCODE" + list.toString());
            postCodeData.setPostCodeDatas(list);

        } catch (Exception e) {
            list = new ArrayList<>();
            list.add("No Postcode Found");
            postCodeData.setPostCodeDatas(list);
            //throws JSONException;
        }

        return postCodeData.getPostCodeDatas();
    }

    //ApI 48 parser
    public static List<String> parseAllPostCodes(String response) throws JSONException{
        PostCodeData postCodeData = new PostCodeData();
        List<String> list;
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            String status=jsonObject.getString("status");
            if(status.equals("Failed"))
            {
                list = new ArrayList<>();
                postCodeData.setPostCodeDatas(list);
            }
            else
            {
                JSONObject jCodes=jsonObject.getJSONObject("postcode_list");
                JSONArray jsonAppArray = jCodes.getJSONArray("district");
                list=new ArrayList<>();
                for(int i=0;i<jsonAppArray.length();i++)
                {
                    list.add(jsonAppArray.getString(i));
                }
                Log.i(TAG, "POSTCODE" + list.toString());
                postCodeData.setPostCodeDatas(list);
            }

        } catch (Exception e) {
            list = new ArrayList<>();
            postCodeData.setPostCodeDatas(list);
            //throws JSONException;
        }

        return postCodeData.getPostCodeDatas();
    }


    //ApI 62 parser
    public static ArrayList<NewAddressData> parseExtraAddresses(String response) throws JSONException{
        ArrayList<NewAddressData> list = new ArrayList<>();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            String status=jsonObject.getString("status");
            if(status.equals("Success"))
            {
                JSONArray jAddresses = jsonObject.getJSONArray("addersses");
                for(int i=0;i<jAddresses.length();i++)
                {
                    JSONObject jAdrObj = jAddresses.getJSONObject(i);
                    NewAddressData newAddressData = new NewAddressData();
                    newAddressData.setExtId(jAdrObj.getString("user_address_ext_id"));
                    newAddressData.setExtPostCode(jAdrObj.getString("user_address_ext_postcode"));
                    newAddressData.setExtAddress1(jAdrObj.getString("user_address_ext_line1"));
                    newAddressData.setExtAddress2(jAdrObj.getString("user_address_ext_line2"));
                    newAddressData.setExtTown(jAdrObj.getString("user_address_ext_town"));
                    newAddressData.setUserId(jAdrObj.getString("user_id"));
                    newAddressData.setIsDelivered(jAdrObj.getString("is_delivered"));
                    PostcodeChargeData postcodeChargeData = new PostcodeChargeData();
                    JSONObject jsonChargeData = jAdrObj.getJSONObject("user_postcode_charge");
                    postcodeChargeData.setZoneName(jsonChargeData.getString("zone_name"));
                    postcodeChargeData.setDeliveryChargePerMile(jsonChargeData.getString("delivery_charge_per_mile"));
                    postcodeChargeData.setDeliveryRadius(jsonChargeData.getString("delivery_radius"));
                    postcodeChargeData.setMinDeliveryCharge(jsonChargeData.getString("min_delivery_charge"));
                    postcodeChargeData.setDeliveryCharge(jsonChargeData.getString("delivery_charge"));
                    postcodeChargeData.setMinDeliveryAmount(jsonChargeData.getString("min_delivery_amount"));
                    newAddressData.setPostcodeChargeData(postcodeChargeData);
                    list.add(newAddressData);
                }
                Log.i(TAG, "ADDRESS" + list.toString());
            }

        } catch (Exception e) {

        }

        return list;
    }

    //ApI 48 parser
    public static boolean parsePostCodeVerify(String response) throws JSONException{
        PostCodeData postCodeData = new PostCodeData();
        List<String> list;
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            String status=jsonObject.getString("status");
            if(status.equals("Success")){
                return true;
            } else{
                return false;
            }

        } catch (Exception e) {
            return false;
        }

    }

    //ApI 28 parser
    public static String parseOrderUpdate(String response) throws JSONException{
        String result;
        String orderId;
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            result = jsonObject.getString("status");
            orderId = jsonObject.getString("order_ID");
        } catch (Exception e) {
            Log.i(TAG, "Order update " + e.getMessage());
            result = "Failed";
        }

        return result;
    }

    public static  ArrayList<OrderHistoryData> parseOrderHistoryData(String response) throws JSONException{
        ArrayList<OrderHistoryData> orderHistoryDatas = new ArrayList<>();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("orders");
            for (int i = 0; i < jsonArray.length(); i ++) {
                OrderHistoryData orderHistoryData = new OrderHistoryData();
                orderHistoryData.setOrderNo(jsonArray.getJSONObject(i).getString("order_no"));
                orderHistoryData.setRestaurantId(jsonArray.getJSONObject(i).getString("rest_id"));
                orderHistoryData.setLogo(jsonArray.getJSONObject(i).getString("logo"));
                orderHistoryData.setRestaurantName(jsonArray.getJSONObject(i).getString("restaurant_name"));
                orderHistoryData.setGrandTotal(jsonArray.getJSONObject(i).getString("grand_total"));
                orderHistoryData.setOrderDate(jsonArray.getJSONObject(i).getString("order_date"));
                orderHistoryData.setOfferDesc(jsonArray.getJSONObject(i).getString("offer_description"));
                orderHistoryData.setCardFee(jsonArray.getJSONObject(i).getString("card_fee"));
                orderHistoryData.setPaymentMethod(jsonArray.getJSONObject(i).getString("payment_method"));
                orderHistoryData.setPaymentStatus(jsonArray.getJSONObject(i).getString("payment_status"));
                orderHistoryData.setAddress1(jsonArray.getJSONObject(i).getString("address1"));
                orderHistoryData.setAddress2(jsonArray.getJSONObject(i).getString("address2"));
                orderHistoryData.setCity(jsonArray.getJSONObject(i).getString("city"));
                orderHistoryData.setPostCode(jsonArray.getJSONObject(i).getString("postcode"));
                orderHistoryData.setBusinessTel(jsonArray.getJSONObject(i).getString("business_tel"));
                //orderHistoryData.setPaypalEmail(jsonArray.getJSONObject(i).getString("paypal_email"));
                orderHistoryData.setLatitude(jsonArray.getJSONObject(i).getString("latitude"));
                orderHistoryData.setLongitude(jsonArray.getJSONObject(i).getString("longitude"));
                orderHistoryData.setAcceptReservation(jsonArray.getJSONObject(i).getString("accept_reservation"));

                ArrayList<ScheduleData> scheduleDatas = new ArrayList<>();
                orderHistoryData.setScheduleDatas(scheduleDatas);
                orderHistoryDatas.add(orderHistoryData);
            }
            //return orderHistoryDatas;
            AppData.getInstance().setOrderHistoryDatas(orderHistoryDatas);

        } catch (Exception e) {
            Log.e("Order History EXCEPTION", "**** " + e.getMessage());
        }

        return orderHistoryDatas;
    }

    //API 14
    public static ArrayList<HotRestaurantData> getRestaurantOrderDetails(String response, LatLng latLngCurrent) throws JSONException{
        ArrayList<HotRestaurantData> hotRestaurantDataArrayList = new ArrayList<HotRestaurantData>();
        Log.i("CURRENT_LOCATION", "**** " + latLngCurrent.latitude + ", " + latLngCurrent.longitude);
        JSONObject jsonObject = new org.json.JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("order");
        int length = jsonArray.length();
        for (int i = 0; i < length; i++) {
            HotRestaurantData hotRestaurantData = new HotRestaurantData();
            hotRestaurantData.setRestaurantId(Integer.parseInt(jsonArray.getJSONObject(i).getString("rest_id")));
            hotRestaurantData.setRestaurantName(jsonArray.getJSONObject(i).getString("restaurant_name"));
            hotRestaurantData.setRestaurantLogo(jsonArray.getJSONObject(i).getString("logo"));
            hotRestaurantData.setPostCode(jsonArray.getJSONObject(i).getString("postcode"));
            hotRestaurantData.setRestaurantAddress1(jsonArray.getJSONObject(i).getString("address1"));
            hotRestaurantData.setRestaurantAddress2(jsonArray.getJSONObject(i).getString("address2"));
            hotRestaurantData.setRestaurantCity(jsonArray.getJSONObject(i).getString("city"));
            hotRestaurantData.setOrderId(jsonArray.getJSONObject(i).getString("order_no"));
            hotRestaurantData.setBusinessTel(jsonArray.getJSONObject(i).getString("business_tel"));
            hotRestaurantData.setRestaurantTelSingle(jsonArray.getJSONObject(i).getString("res_business_tel_for_call"));
            hotRestaurantData.setReservationTel(jsonArray.getJSONObject(i).getString("reservation_tel"));
            hotRestaurantData.setRestaurantPayPalEmail(jsonArray.getJSONObject(i).getString("paypal_email"));
            hotRestaurantData.setNetpayStatus(jsonArray.getJSONObject(i).getString("netpay"));
            if("1".equalsIgnoreCase(hotRestaurantData.getNetpayStatus())) {
                hotRestaurantData.setNetpayChargeType(jsonArray.getJSONObject(i).getJSONObject("netpay_charge").getString("charge_type"));
                hotRestaurantData.setNetpayCustomerCharge(jsonArray.getJSONObject(i).getJSONObject("netpay_charge").getString("customer_charge"));
            }
            hotRestaurantData.setOrderDate(jsonArray.getJSONObject(i).getString("order_date"));
            hotRestaurantData.setDiscountAmount(jsonArray.getJSONObject(i).getString("discount_amount"));
            hotRestaurantData.setSubTotal(jsonArray.getJSONObject(i).getString("sub_total"));
            hotRestaurantData.setGrandTotal(jsonArray.getJSONObject(i).getString("grand_total"));
            hotRestaurantData.setRestaurantLat(jsonArray.getJSONObject(i).getString("latitude"));
            hotRestaurantData.setRestaurantLon(jsonArray.getJSONObject(i).getString("longitude"));
            hotRestaurantData.setRestaurantAcceptCollection(jsonArray.getJSONObject(i).getInt("accept_collection"));
            hotRestaurantData.setRestaurantAcceptDelivery(jsonArray.getJSONObject(i).getInt("accept_delivery"));
            hotRestaurantData.setDeliveryCharge(jsonArray.getJSONObject(i).getDouble("delivery_charge"));
            hotRestaurantData.setRestaurantAcceptReservation(jsonArray.getJSONObject(i).getInt("accept_reservation"));
            hotRestaurantData.setComingSoon(jsonArray.getJSONObject(i).getString("chefonline_coming_soon"));

            if (!"".equalsIgnoreCase(jsonArray.getJSONObject(i).getString("distance")) && !"NONE".equalsIgnoreCase(jsonArray.getJSONObject(i).getString("distance"))) {
                hotRestaurantData.setRestaurantDistance(jsonArray.getJSONObject(i).getString("distance"));
            } else {
                hotRestaurantData.setRestaurantDistance("0.0");
            }

            /** Start dish parsing */
           // JSONArray jsonArray1 = jsonArray.getJSONObject(i).getJSONObject("ordered_dish").getJSONArray("dish_choose");
            JSONArray jsonArray1 = jsonArray.getJSONObject(i).getJSONArray("ordered_dish");

            ArrayList<DishData> dishDatas = new ArrayList<>();
            for (int j = 0; j < jsonArray1.length() ; j++) {
                DishData dishData = new DishData();
                dishData.setDishName(jsonArray1.getJSONObject(j).getString("dish_name"));
                dishData.setDishId(jsonArray1.getJSONObject(j).getString("dish_id"));
                dishData.setDishPrice(jsonArray1.getJSONObject(j).getString("dish_price"));
                dishData.setDishQty(jsonArray1.getJSONObject(j).getString("quantity"));
                dishData.setPizzaMenu(jsonArray1.getJSONObject(j).getBoolean("is_pizza_menu"));
                ArrayList<PizzaGroupItemData> pizzaItemList = new ArrayList<>();
                if(dishData.isPizzaMenu()) {
                    if(jsonArray1.getJSONObject(j).get("pizza_group_item") != null && !"".equals(jsonArray1.getJSONObject(j).get("pizza_group_item")) && jsonArray1.getJSONObject(j).getJSONArray("pizza_group_item").length() > 0) {
                        for (int k = 0; k < jsonArray1.getJSONObject(j).getJSONArray("pizza_group_item").length(); k++) {
                            PizzaGroupItemData pizzaItemData = new PizzaGroupItemData();
                            pizzaItemData.setPizzaGroupItemId(jsonArray1.getJSONObject(j).getJSONArray("pizza_group_item").getJSONObject(k).getString("pizza_group_item_id"));
                            pizzaItemData.setItemPrice(jsonArray1.getJSONObject(j).getJSONArray("pizza_group_item").getJSONObject(k).getString("item_price"));
                            pizzaItemData.setItemName(jsonArray1.getJSONObject(j).getJSONArray("pizza_group_item").getJSONObject(k).getString("item_name"));
                            pizzaItemList.add(pizzaItemData);
                        }
                    }
                }
                dishData.setOrderedPizzaItemList(pizzaItemList);
                dishDatas.add(dishData);
            }
            hotRestaurantData.setDishDatas(dishDatas);
            /** End Dish parsing*/

            /** Start parsing opening time and end time*/
            ArrayList<ScheduleData> scheduleDatas = new ArrayList<>();
            if (!jsonArray.getJSONObject(i).isNull("restuarent_schedule")) {

                JSONArray jsonArraySchedule = jsonArray.getJSONObject(i).getJSONObject("restuarent_schedule").getJSONArray("schedule");
                int LengthJsonArraySchedule = jsonArraySchedule.length();
                for (int j = 0; j < LengthJsonArraySchedule; j++) {

                    JSONArray jsonScheduleList = jsonArraySchedule.getJSONObject(j).getJSONArray("list");

                    ScheduleData scheduleData = new ScheduleData();
                    scheduleData.setDayName(jsonArraySchedule.getJSONObject(j).getString("day_name"));
                    scheduleData.setWeekDayId(jsonArraySchedule.getJSONObject(j).getString("weekday_id"));
                    Log.i("DAY NAME J", "--- " + jsonArraySchedule.getJSONObject(j).getString("day_name"));

                    ArrayList<OpenEndTimeData> openEndTimeDatas = new ArrayList<>();
                    ArrayList<OpenEndTimeData> reservationDatas = new ArrayList<>();
                    int jsonScheduleListLength = jsonScheduleList.length();
                    for (int k = 0; k < jsonScheduleListLength; k++) {
                        OpenEndTimeData openEndTimeData = new OpenEndTimeData();
                        openEndTimeData.setOpeningTime(jsonScheduleList.getJSONObject(k).getString("opening_time"));
                        openEndTimeData.setEndTime(jsonScheduleList.getJSONObject(k).getString("closing_time"));
                        openEndTimeData.setCollection(jsonScheduleList.getJSONObject(k).getString("Collection"));
                        openEndTimeData.setDelivery(jsonScheduleList.getJSONObject(k).getString("Delivery"));
                        openEndTimeData.setDeliveryPolicyId(jsonScheduleList.getJSONObject(k).getString("Delivery_policy_id"));
                        openEndTimeData.setCollectionPolicyId(jsonScheduleList.getJSONObject(k).getString("Collection_policy_id"));
                        openEndTimeData.setLastDeliverySubmission(jsonScheduleList.getJSONObject(k).getString("last_time_for_delivery_submit"));
                        openEndTimeData.setLastCollectionSubmission(jsonScheduleList.getJSONObject(k).getString("last_time_for_collection_submit"));
                        openEndTimeData.setShiftSerial(jsonScheduleList.getJSONObject(k).getInt("shift"));
                        openEndTimeData.setType(jsonScheduleList.getJSONObject(k).getString("type"));
                        openEndTimeData.setMinOrder(jsonScheduleList.getJSONObject(k).getString("min_order"));
                        openEndTimeData.setMinOrderCollection(jsonScheduleList.getJSONObject(k).getString("min_order_collection"));
                        openEndTimeData.setTimingFor(jsonScheduleList.getJSONObject(k).getString("timing_for"));
                        //scheduleData.setOpenEndTimeDatas(openEndTimeData);
                        if(openEndTimeData.getType() != null && "3".equalsIgnoreCase(openEndTimeData.getType())) {
                            openEndTimeDatas.add(openEndTimeData);
                        } else {
                            reservationDatas.add(openEndTimeData);
                        }
                    }

                    scheduleData.setOpenEndTimeDatas(openEndTimeDatas);
                    scheduleData.setReservationDatas(reservationDatas);
                    scheduleDatas.add(scheduleData);
                }

            }

            hotRestaurantData.setScheduleDatas(scheduleDatas);
            /* End parsing opening time and end time */

            /** Start parse cuisine array*/
            ArrayList<CuisineData> cuisineDatas = new ArrayList<>();
            if (!jsonArray.getJSONObject(i).isNull("available_cuisine")) {
                JSONArray jsonArrayCuisine = jsonArray.getJSONObject(i).getJSONObject("available_cuisine").getJSONArray("cuisine");
                int lengthCuisine = jsonArray.getJSONObject(i).getJSONObject("available_cuisine").getJSONArray("cuisine").length();
                for (int j = 0; j < lengthCuisine; j++) {
                    CuisineData cuisineData = new CuisineData();
                    cuisineData.setCuisineName(jsonArrayCuisine.getJSONObject(j).getString("name"));
                    cuisineDatas.add(cuisineData);
                }

            }
            hotRestaurantData.setCuisineList(cuisineDatas);
                /*End parse cuisine array*/

            /** Start parse discount array*/
            ArrayList<DiscountData> discountDatas = new ArrayList<>();
            if (jsonArray.getJSONObject(i).getJSONObject("discount").getInt("status") !=0) {
                if (!jsonArray.getJSONObject(i).isNull("discount")) {
                    JSONArray jsonArrayDiscount = jsonArray.getJSONObject(i).getJSONObject("discount").getJSONArray("off");
                    int lengthDiscountArray = jsonArrayDiscount.length();

                    for (int j = 0; j < lengthDiscountArray; j++) {
                        DiscountData discountData = new DiscountData();
                        discountData.setDiscountId(jsonArrayDiscount.getJSONObject(j).getString("discount_id"));
                        discountData.setDiscountImage(jsonArrayDiscount.getJSONObject(j).getString("image"));
                        discountData.setRestaurantOrderPolicyId(jsonArrayDiscount.getJSONObject(j).getString("restaurant_order_policy_id"));
                        discountData.setDiscountTitle(jsonArrayDiscount.getJSONObject(j).getString("discount_title"));
                        discountData.setDiscountDescription(jsonArrayDiscount.getJSONObject(j).getString("discount_description"));
                        discountData.setDiscountAmount(jsonArrayDiscount.getJSONObject(j).getString("discount_amount"));
                        discountData.setDiscountType(jsonArrayDiscount.getJSONObject(j).getString("discount_type"));
                        discountData.setDiscountEligibleAmount(jsonArrayDiscount.getJSONObject(j).getString("eligible_amount"));
                        discountData.setDefaultStatus(jsonArrayDiscount.getJSONObject(j).getString("default"));
                        discountData.setActiveStatus(jsonArrayDiscount.getJSONObject(j).getInt("active"));
                        discountDatas.add(discountData);
                    }

                }
            }

            hotRestaurantData.setDiscountList(discountDatas);
            /* End parse discount array*/

            /** Start parse policy data array */
            ArrayList<PolicyData> policyDatas = new ArrayList<>();
                if (!jsonArray.getJSONObject(i).isNull("order_policy")) {
                    JSONArray jsonArrayOrderPolicy = jsonArray.getJSONObject(i).getJSONObject("order_policy").getJSONArray("policy");
                    int lengthPolicyArray = jsonArray.getJSONObject(i).getJSONObject("order_policy").getJSONArray("policy").length();

                    for (int j = 0; j < lengthPolicyArray; j++) {
                        PolicyData policyData = new PolicyData();
                        policyData.setPolicyId(jsonArrayOrderPolicy.getJSONObject(j).getString("policy_id"));
                        policyData.setPolicyName(jsonArrayOrderPolicy.getJSONObject(j).getString("policy_name"));
                        policyData.setPolicyTime(jsonArrayOrderPolicy.getJSONObject(j).getString("policy_time"));
                        policyData.setMinOrder(jsonArrayOrderPolicy.getJSONObject(j).getString("min_order"));
                        policyData.setStatus(jsonArrayOrderPolicy.getJSONObject(j).getString("status"));
                        policyDatas.add(policyData);
                    }
                }
            hotRestaurantData.setPolicyList(policyDatas);
            /** End parse policy data array */

            /** Start Offers parsing */
            ArrayList<OffersData> offersDatas = new ArrayList<>();
            if (jsonArray.getJSONObject(i).getJSONObject("offer").getInt("status") !=0) {
                JSONArray jsonArrayOffers = jsonArray.getJSONObject(i).getJSONObject("offer").getJSONArray("offer_list");
                int jsonArrayOfferSize = jsonArrayOffers.length();
                for (int j = 0; j < jsonArrayOfferSize; j++) {
                    OffersData offersData = new OffersData();
                    offersData.setOfferId(jsonArrayOffers.getJSONObject(j).getString("id"));
                    offersData.setOfferStatus(jsonArrayOffers.getJSONObject(j).getString("status"));
                    offersData.setRestaurantId(jsonArrayOffers.getJSONObject(j).getString("restaurant_id"));
                    offersData.setOfferDescription(jsonArrayOffers.getJSONObject(j).getString("description"));
                    offersData.setOfferTitle(jsonArrayOffers.getJSONObject(j).getString("offer_title"));
                    offersData.setOfferImage(jsonArrayOffers.getJSONObject(j).getString("image"));
                    offersData.setOfferThumb(jsonArrayOffers.getJSONObject(j).getString("thumb_name"));
                    offersData.setOfferElegibleAmount(jsonArrayOffers.getJSONObject(j).getString("eligible_amount"));
                    offersData.setRestaurantOrderPolicyId(jsonArrayOffers.getJSONObject(j).getString("restaurant_order_policy_id"));
                    offersData.setOfferPosition(jsonArrayOffers.getJSONObject(j).getString("position"));
                    if (!jsonArrayOffers.getJSONObject(j).isNull("image_from")) {
                        offersData.setImageFrom(jsonArrayOffers.getJSONObject(j).getString("image_from"));
                    }

                    offersData.setOfferFor(jsonArrayOffers.getJSONObject(j).getString("offer_for"));
                    offersData.setDefaultStatus(jsonArrayOffers.getJSONObject(j).getString("default"));
                    offersData.setActiveStatus(jsonArrayOffers.getJSONObject(j).getInt("active"));
                    offersDatas.add(offersData);
                }
            }

            hotRestaurantData.setOffersDatas(offersDatas);
            /** End Offers parsing */

            //Object added to main array List
            hotRestaurantDataArrayList.add(hotRestaurantData);
        }

        return hotRestaurantDataArrayList;
    }

    public static int parseOrderDetailsData(String response) throws JSONException{
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("order");

            ArrayList<OrderHistoryData> orderHistoryDatas = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i ++) {
                OrderHistoryData orderHistoryData = new OrderHistoryData();
                orderHistoryData.setOrderNo(jsonArray.getJSONObject(i).getString("order_no"));
                orderHistoryData.setRestaurantId(jsonArray.getJSONObject(i).getString("rest_id"));
                orderHistoryData.setRestaurantName(jsonArray.getJSONObject(i).getString("restaurant_name"));
                orderHistoryData.setGrandTotal(jsonArray.getJSONObject(i).getString("grand_total"));
                orderHistoryData.setOrderDate(jsonArray.getJSONObject(i).getString("order_date"));
                orderHistoryData.setPaymentMethod(jsonArray.getJSONObject(i).getString("payment_method"));
                orderHistoryData.setPaymentStatus(jsonArray.getJSONObject(i).getString("payment_status"));
                orderHistoryData.setAddress1(jsonArray.getJSONObject(i).getString("address1"));
                orderHistoryData.setAddress2(jsonArray.getJSONObject(i).getString("address2"));
                orderHistoryData.setCity(jsonArray.getJSONObject(i).getString("city"));
                orderHistoryData.setPostCode(jsonArray.getJSONObject(i).getString("postcode"));
                orderHistoryData.setLatitude(jsonArray.getJSONObject(i).getString("latitude"));
                orderHistoryData.setLongitude(jsonArray.getJSONObject(i).getString("longitude"));
               // orderHistoryData.setAcceptReservation(jsonArray.getJSONObject(i).getString("accept_reservation"));

                Log.i(TAG, "Rest_name " + jsonArray.getJSONObject(i).getString("restaurant_name"));
                Log.i(TAG, "Grand Total " + jsonArray.getJSONObject(i).getString("grand_total"));

                JSONArray jsonArray1 = jsonArray.getJSONObject(i).getJSONObject("ordered_dish").getJSONArray("dish_choose");

                ArrayList<DishData> dishDatas = new ArrayList<>();
                for (int j = 0; j < jsonArray1.length() ; j++) {
                    DishData dishData = new DishData();
                    dishData.setDishName(jsonArray1.getJSONObject(j).getString("dish_name"));
                    dishData.setDishId(jsonArray1.getJSONObject(j).getString("dish_id"));
                    dishData.setDishPrice(jsonArray1.getJSONObject(j).getString("dish_price"));
                    dishData.setDishQty(jsonArray1.getJSONObject(j).getString("quantity"));
                    dishDatas.add(dishData);
                }

                orderHistoryData.setDishDatas(dishDatas);
                orderHistoryDatas.add(orderHistoryData);
            }

            AppData.getInstance().setOrderHistoryDetails(orderHistoryDatas);

        } catch (Exception e) {
            //throws JSONException;
        }

        return 0;
    }


}
