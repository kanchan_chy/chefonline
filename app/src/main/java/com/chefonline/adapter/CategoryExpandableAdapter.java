package com.chefonline.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomBottomSingleActivityToast;
import com.chefonline.customview.CustomToast;
import com.chefonline.datamodel.CategoryData;
import com.chefonline.datamodel.DishData;
import com.chefonline.datamodel.OptionData;
import com.chefonline.datamodel.PizzaGroupData;
import com.chefonline.datamodel.PizzaGroupItemData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.online.R;
import com.chefonline.online.MainActivity;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.TimeManagement;
import com.chefonline.utility.UtilityMethod;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.chefonline.online.R.layout.row_categories;
import static com.chefonline.online.R.layout.row_category_dishes;

public class CategoryExpandableAdapter extends BaseExpandableListAdapter {
    private String restaurantId;
    private ArrayList<CategoryData> categoryList;
	private Activity context;
	private LayoutInflater mInflater;
    private DataBaseUtil db;
    private TextToSpeech ttobj;
    private boolean dialogOpened = false;
    private boolean dialogMoreOptionOpened = false;
    public static double pizzaItemPrice = 0.0;
    public static String pizzaItemIds = "";
    public static String pizzaItemNames = "";
    private long mLastClickTime = 0;

	public CategoryExpandableAdapter(Activity context, ArrayList<CategoryData> categoryList , String restaurantId) {
		this.restaurantId = restaurantId;
        this.categoryList = categoryList;
		this.context = context;
        this.db = new DataBaseUtil(context);
        this.db.open();
	}

    @Override
    public int getGroupCount() {
        return categoryList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return categoryList.get(groupPosition).getDishDatas().size();
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
        /*int len = super.getGroupCount();*/
        ExpandableListView list = (ExpandableListView) context.findViewById(R.id.lstViewCategory);
        for (int i = 0; i < categoryList.size(); i++) {
            if (i != groupPosition) {
                list.collapseGroup(i);
            }
        }

    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(row_categories, null);

            holder.txtViewCategoryName = (TextView) convertView.findViewById(R.id.txtViewCategoryName);
            holder.txtViewCuisineName = (TextView) convertView.findViewById(R.id.txtViewCuisineName);
            holder.relativeCuisine = (RelativeLayout) convertView.findViewById(R.id.relativeCuisine);
            holder.relativeCategory = (RelativeLayout) convertView.findViewById(R.id.relativeCategory);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(!categoryList.get(groupPosition).isCategory()) {
            holder.relativeCuisine.setVisibility(View.VISIBLE);
            holder.relativeCategory.setVisibility(View.GONE);
            holder.txtViewCuisineName.setText(categoryList.get(groupPosition).getCuisineName());
        } else {
            holder.relativeCuisine.setVisibility(View.GONE);
            holder.relativeCategory.setVisibility(View.VISIBLE);

            String categoryName = categoryList.get(groupPosition).getCategoryName();
            categoryName.replace("'", "\\'");
            holder.txtViewCategoryName.setText(categoryName);

            if (isExpanded) {
                holder.txtViewCategoryName.setTextColor(context.getResources().getColor(R.color.theme_red_dark));
            } else {
                holder.txtViewCategoryName.setTextColor(context.getResources().getColor(R.color.black));
            }

            final ImageView expandedImage = (ImageView) convertView.findViewById(R.id.imageViewIndicator);
            final int resId = isExpanded ? R.drawable.arrow : R.drawable.arrow_selected;
            expandedImage.setImageResource(resId);

        }

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ArrayList<DishData> child = categoryList.get(groupPosition).getDishDatas();
        final ViewHolder holder;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(row_category_dishes, null);

            holder.linearLayoutAllergensInfo = (LinearLayout) convertView.findViewById(R.id.linearLayout_alergens_info);
            holder.txtViewDescription = (TextView) convertView.findViewById(R.id.txtViewDescription);
            holder.txtViewDishName = (TextView) convertView.findViewById(R.id.txtViewDishName);
            holder.txtViewPrice = (TextView) convertView.findViewById(R.id.txtViewPrice);
            holder.imgBtnItemActions = (TextView) convertView.findViewById(R.id.btnItemActions);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // get the textView reference and set the value

        if ("".equals(child.get(childPosition).getDishDescription())) {
            holder.txtViewDescription.setVisibility(View.GONE);
        } else {
            holder.txtViewDescription.setText(child.get(childPosition).getDishDescription());
        }

        holder.txtViewDishName.setText(child.get(childPosition).getDishName());
        holder.txtViewPrice.setText("£" + child.get(childPosition).getDishPrice());

        final boolean isMoreOption;
        final boolean isPizzaMenu;

        if(child.get(childPosition).isPizzaMenu()) {
            isMoreOption = false;
            isPizzaMenu = true;
            holder.imgBtnItemActions.setBackgroundResource(R.drawable.round_red_button_selector);
            holder.imgBtnItemActions.setText("   OPTION   ");
        } else {
            isPizzaMenu = false;
            if (child.get(childPosition).getOptionDatas().size() == 0) {
                isMoreOption = false;
                holder.imgBtnItemActions.setBackgroundResource(R.drawable.round_red_button_selector);
                holder.imgBtnItemActions.setText("     ADD     ");
            } else {
                isMoreOption = true;
                holder.imgBtnItemActions.setBackgroundResource(R.drawable.round_red_button_selector);
                holder.imgBtnItemActions.setText("   OPTION   ");
            }
        }

        holder.imgBtnItemActions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if("1".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getComingSoon())) {
                    openDialog("This restaurant is coming soon. Ordering is not possible right now.","OK");
                    return;
                }

                TimeManagement timeManagement = new TimeManagement();
                if(!timeManagement.checkOrderingOpenStatus())
                {
                    if(!dialogOpened) openDialog("Online ordering is closed today. Ordering is not possible.","OK");
                    return;
                }

                if(timeManagement.isCollectionTimeOver() && timeManagement.isDeliveryTimeOver()) {
                    openDialog("Online ordering time is over today. Ordering is not possible.","OK");
                    return;
                }

                int amount = Integer.parseInt(MainActivity.txtViewBubble.getText().toString());
                if (amount >= 999) {
                    if(!dialogOpened) openDialog("You can not order more than 999 item.", "OK");
                    return;
                }

                if(!isPizzaMenu) {

                    if (isMoreOption) {
                        if(!dialogMoreOptionOpened) openDialog(child.get(childPosition).getOptionDatas(), child.get(childPosition).getDishName().toString());

                    } else {
                        HashMap<String, Integer> wordList = new HashMap<String, Integer>();
                        wordList =  db.checkExistItem(child.get(childPosition).getDishId(), "");

                        if (wordList.get("count").intValue() > 0) {
                            db.updateCartData(child.get(childPosition).getDishId(), "", wordList.get("qty").intValue() + 1);

                        } else {
                            db.insertCartData(restaurantId, child.get(childPosition).getDishId(), child.get(childPosition).getDishName().toString(), 1, child.get(childPosition).getDishPrice(), "", "");
                            ConstantValues.IS_CART_ADDED = true;

                        }

                        MainActivity.txtViewBubble.setText("" + (amount + 1));

                        String toastMessage = "Added to your cart successfully";

                        PreferenceUtil preferenceUtil = new PreferenceUtil(context);
                        if (preferenceUtil.getOnOffVoiceCommand() == 1) {
                            ttobj.speak(toastMessage, TextToSpeech.QUEUE_FLUSH, null);
                        }
                        new CustomBottomSingleActivityToast(context,child.get(childPosition).getDishName().toString() + " is added to your cart","",true);
                    }

                } else {
                   // new CustomToast(context, "Pizza menu clicked", "", true);
                    openPizzaDialog(groupPosition, childPosition);
                }


            }

        });

        // set the ClickListener to handle the click event on child item
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(context, "" + child.get(childPosition).getOptionDatas().size(), Toast.LENGTH_SHORT).show();
            }
        });


        /** Set allergens info, dynamically item create for showing info */
        holder.linearLayoutAllergensInfo.removeAllViews();

        TextView txtAlergensLabel = new TextView(context);
        //txtAlergensLabel.setText("");
        //txtAlergensLabel.setTextColor(context.getResources().getColor(R.color.white));
        holder.linearLayoutAllergensInfo.addView(txtAlergensLabel);

        //String alergens = child.get(childPosition).getDishAllergens();
        if (child.get(childPosition).getAlergensDatas() != null && child.get(childPosition).getAlergensDatas().size()> 0) {
          //  List<String> items = Arrays.asList(alergens.split("\\s*,\\s*"));

            for(int x = 0; x < child.get(childPosition).getAlergensDatas().size(); x++) {
                ImageView image = new ImageView(context);
                //image.setBackgroundColor(context.getResources().getColor(R.color.off_gray_dark));

                if (child.get(childPosition).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("1")) {
                    image.setImageResource(R.drawable.al_fish);

                } else if (child.get(childPosition).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("2")) {
                    image.setImageResource(R.drawable.al_penut);

                } else if (child.get(childPosition).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("3")) {
                    image.setImageResource(R.drawable.al_nuts);

                } else if (child.get(childPosition).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("4")) {
                    image.setImageResource(R.drawable.al_egg);

                } else if (child.get(childPosition).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("5")) {
                    image.setImageResource(R.drawable.al_milk);

                } else if (child.get(childPosition).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("6")) {
                    image.setImageResource(R.drawable.al_musterd);

                } else if (child.get(childPosition).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("7")) {
                    image.setImageResource(R.drawable.al_soya);

                } else if (child.get(childPosition).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("8")) {
                    image.setImageResource(R.drawable.al_crustaceans);

                } else if (child.get(childPosition).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("9")) {
                    image.setImageResource(R.drawable.al_seeds);

                } else if (child.get(childPosition).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("10")) {
                    image.setImageResource(R.drawable.al_gluten);
                }

                //image.setPadding(5, 5, 5, 5);
                LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                llp.setMargins(0, 0, 5, 0); // llp.setMargins(left, top, right, bottom);
                image.setLayoutParams(llp);

                holder.linearLayoutAllergensInfo.addView(image);
            }


            holder.linearLayoutAllergensInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(context, "" + child.get(childPosition).getOptionDatas().size(), Toast.LENGTH_SHORT).show();
                    openDialogAllergens();
                }
            });

        }




        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private void openDialog(ArrayList<OptionData> optionDatas1, String dishName) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_options);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                dialogMoreOptionOpened = true;
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialogMoreOptionOpened = false;
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialogMoreOptionOpened = false;
            }
        });

        TextView text = (TextView) dialog.findViewById(R.id.txtViewOptionName);
        text.setText(dishName);

        ListView listView = (ListView) dialog.findViewById(R.id.listViewOptions);
        OptionsAdapter adapter = new OptionsAdapter(context, optionDatas1, restaurantId, dishName);
        listView.setAdapter(adapter);

        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        Button btnContinue = (Button) dialog.findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    /*
    private boolean checkOrderingOpenStatus()
    {
        String currentDay = new SimpleDateFormat("EEEE").format(new Date());
        int shiftCount = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().size();
        boolean todayFound = false;
        for (int i = 0; i < shiftCount; i++) {
            if (AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getDayName().equalsIgnoreCase(currentDay)) {
                todayFound = true;
                if(AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas() == null || AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().size() == 0) {
                    return false;
                }
            }
        }
        if(todayFound == false) {
            return false;
        }
        return true;
    }  */


    private void openDialog(String message, String okButton) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_message);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(okButton);

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialogOpened = false;
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialogOpened = false;
            }
        });

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                dialogOpened = true;
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private void openDialogAllergens() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_allergens_key);

        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    private boolean isMinMaxForPizzaOk(int groupPos, final int childPos) {

        ArrayList<String> allItemIds = new ArrayList<>();
        String tempIds = pizzaItemIds;
        String id = "";
        for (int j = 0; j < tempIds.length(); j++) {
            if(tempIds.charAt(j) != ',') {
                id += tempIds.charAt(j);
            }
            if(j == (tempIds.length() - 1) || tempIds.charAt(j) == ',') {
                allItemIds.add(id);
                id = "";
            }
        }


        ArrayList<PizzaGroupData> pizzaGroupList = categoryList.get(groupPos).getDishDatas().get(childPos).getPizzaGroupList();
        for (int i = 0; i < pizzaGroupList.size(); i++) {
            PizzaGroupData pizzaGroupData = pizzaGroupList.get(i);
            int min = pizzaGroupData.getMinSelection();
            int max = pizzaGroupData.getMaxSelection();
            int count = 0;
            ArrayList<PizzaGroupItemData> pizzaItemList = pizzaGroupData.getPizzaGroupItemList();
            for (int j = 0; j < pizzaItemList.size(); j++ ) {
                if(allItemIds.contains(String.valueOf(pizzaItemList.get(j).getPizzaGroupItemId()))) {
                    count ++;
                }
            }
            if(count < min) {
                new CustomToast(context, "Please select at least " + min + " item from Pizza Group " + (i + 1), "", false);
                return false;
            }
            if(count > max) {
                new CustomToast(context, "You can select maximum " + max + " item from Pizza Group " + (i + 1), "", false);
                return false;
            }

        }
        return true;
    }


    private void openPizzaDialog(final int groupPos, final int childPos) {

        pizzaItemPrice = 0.0;
        pizzaItemIds = "";
        pizzaItemNames = "";
        final ArrayList<DishData> child = categoryList.get(groupPos).getDishDatas();

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_pizza);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                dialogMoreOptionOpened = true;
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialogMoreOptionOpened = false;
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialogMoreOptionOpened = false;
            }
        });

        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        Button btnAdd = (Button) dialog.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isMinMaxForPizzaOk(groupPos, childPos)) {
                    return;
                }

                dialog.dismiss();

                int amount = Integer.parseInt(MainActivity.txtViewBubble.getText().toString());
                HashMap<String, Integer> wordList = new HashMap<String, Integer>();
                wordList =  db.checkExistItem(child.get(childPos).getDishId(), pizzaItemIds);

                if (wordList.get("count").intValue() > 0) {
                    db.updateCartData(child.get(childPos).getDishId(), pizzaItemIds, wordList.get("qty").intValue() + 1);

                } else {
                    db.insertCartData(restaurantId, child.get(childPos).getDishId(), child.get(childPos).getDishName().toString(), 1, String.valueOf(pizzaItemPrice), pizzaItemIds, pizzaItemNames);
                    ConstantValues.IS_CART_ADDED = true;

                }

                MainActivity.txtViewBubble.setText("" + (amount + 1));

                String toastMessage = "Added to your cart successfully";

                PreferenceUtil preferenceUtil = new PreferenceUtil(context);
                if (preferenceUtil.getOnOffVoiceCommand() == 1) {
                    ttobj.speak(toastMessage, TextToSpeech.QUEUE_FLUSH, null);
                }
                new CustomBottomSingleActivityToast(context,child.get(childPos).getDishName().toString() + " is added to your cart","",true);

            }

        });

        TextView txtViewOptionName = (TextView) dialog.findViewById(R.id.txtViewOptionName);
        txtViewOptionName.setText(child.get(childPos).getDishName());

     /*   ListView listView = (ListView) dialog.findViewById(R.id.listViewOptions);
        OptionsAdapter adapter = new OptionsAdapter(context, optionDatas1, restaurantId, dishName);
        listView.setAdapter(adapter); */

        LinearLayout linearSelected = (LinearLayout) dialog.findViewById(R.id.linearSelected);
        LinearLayout linearSelectedContainer = (LinearLayout) dialog.findViewById(R.id.linearSelectedContainer);

        RecyclerView recyclerPizzaGroup = (RecyclerView) dialog.findViewById(R.id.recyclerPizzaGroup);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerPizzaGroup.setLayoutManager(layoutManager);

        PizzaGroupAdapter pizzaGroupAdapter = new PizzaGroupAdapter(context, child.get(childPos).getPizzaGroupList(), true, linearSelected, linearSelectedContainer, btnAdd);
        recyclerPizzaGroup.setAdapter(pizzaGroupAdapter);

        dialog.show();

    }



    static class ViewHolder {
        LinearLayout linearLayoutAllergensInfo;
        RelativeLayout relativeCuisine;
        RelativeLayout relativeCategory;
        TextView txtViewCuisineName;
		TextView txtViewCategoryName;
        TextView txtViewDescription;
        TextView txtViewDishName;
        TextView txtViewPrice;
        TextView imgBtnItemActions;
	}

}
