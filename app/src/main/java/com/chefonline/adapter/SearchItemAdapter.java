package com.chefonline.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomBottomSingleActivityToast;
import com.chefonline.datamodel.DishData;
import com.chefonline.datamodel.OptionData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.online.R;
import com.chefonline.online.MainActivity;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.TimeManagement;
import com.chefonline.utility.UtilityMethod;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static com.chefonline.online.R.layout.row_category_dishes;

public class SearchItemAdapter extends BaseAdapter {
    private String restaurantId = null;
    private ArrayList<DishData> dishDatas;
    private ArrayList<DishData> dishDatasTemp = new ArrayList<>();
	private Activity context;
	private LayoutInflater mInflater;
    private DataBaseUtil db;
	private TextToSpeech ttobj;
	public static double pizzaItemPrice = 0.0;
	public static String pizzaItemIds = "";
	public static String pizzaItemNames = "";
	private long mLastClickTime = 0;

	public SearchItemAdapter(Activity context, ArrayList<DishData> dishDatas, String restauranId) {
        this.restaurantId = restauranId;
        this.dishDatas = dishDatas;
		dishDatasTemp.addAll(dishDatas);
		this.context = context;
        this.db = new DataBaseUtil(context);
        this.db.open();

		ttobj = new TextToSpeech(context,
				new TextToSpeech.OnInitListener() {
					@Override
					public void onInit(int status) {
						if(status != TextToSpeech.ERROR){
							ttobj.setLanguage(Locale.UK);
						}
					}
				});



	}

	@Override
	public int getCount() {
		return this.dishDatas.size();
	}

	@Override
	public Object getItem(int position) {
		return this.dishDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		final ViewHolder holder;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(row_category_dishes, null);

			holder.linearLayoutAllergensInfo = (LinearLayout) convertView.findViewById(R.id.linearLayout_alergens_info);
			holder.txtViewDescription = (TextView) convertView.findViewById(R.id.txtViewDescription);
			holder.txtViewDishName = (TextView) convertView.findViewById(R.id.txtViewDishName);
			holder.txtViewPrice = (TextView) convertView.findViewById(R.id.txtViewPrice);
			holder.imgBtnItemActions = (TextView) convertView.findViewById(R.id.btnItemActions);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// get the textView reference and set the value
		holder.txtViewDescription.setText(dishDatas.get(position).getDishDescription());
		holder.txtViewDishName.setText(dishDatas.get(position).getDishName());
		holder.txtViewPrice.setText("£" + dishDatas.get(position).getDishPrice());

		final boolean isMoreOption;
		final boolean isPizzaMenu;

		if(dishDatas.get(position).isPizzaMenu()) {
			isMoreOption = false;
			isPizzaMenu = true;
			holder.imgBtnItemActions.setBackgroundResource(R.drawable.round_red_button_selector);
			holder.imgBtnItemActions.setText("   OPTION   ");
		} else {
			isPizzaMenu = false;
			if (dishDatas.get(position).getOptionDatas().size() == 0) {
				isMoreOption = false;
				holder.imgBtnItemActions.setBackgroundResource(R.drawable.round_red_button_selector);
				holder.imgBtnItemActions.setText("     ADD     ");
			} else {
				isMoreOption = true;
				holder.imgBtnItemActions.setBackgroundResource(R.drawable.round_red_button_selector);
				holder.imgBtnItemActions.setText("   OPTION   ");
			}
		}

		holder.imgBtnItemActions.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if("1".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getComingSoon())) {
					openDialog("This restaurant is coming soon. Ordering is not possible right now.","OK");
					return;
				}

				TimeManagement timeManagement = new TimeManagement();
				if(!timeManagement.checkOrderingOpenStatus())
				{
					openDialog("Online ordering is closed today. Ordering is not possible.","OK");
					return;
				}

				if(timeManagement.isCollectionTimeOver() && timeManagement.isDeliveryTimeOver()) {
					openDialog("Online ordering time is over today. Ordering is not possible.","OK");
					return;
				}

				int amount = Integer.parseInt(MainActivity.txtViewBubble.getText().toString());
				if (amount >= 999) {
					openDialog("You can not order more than 999 item.", "OK");
					return;
				}

				if(!isPizzaMenu) {

					if (isMoreOption) {
						openDialog(dishDatas.get(position).getOptionDatas(), dishDatas.get(position).getDishName().toString());

					} else {
						HashMap<String, Integer> wordList = new HashMap<String, Integer>();
						wordList =  db.checkExistItem(dishDatas.get(position).getDishId(), "");

						if (wordList.get("count").intValue() > 0) {
							db.updateCartData(dishDatas.get(position).getDishId(), "", wordList.get("qty").intValue() + 1);

						} else {
							db.insertCartData(restaurantId, dishDatas.get(position).getDishId(), dishDatas.get(position).getDishName().toString(), 1, dishDatas.get(position).getDishPrice(), "", "");
							ConstantValues.IS_CART_ADDED = true;

						}

						MainActivity.txtViewBubble.setText("" + (amount + 1));

						String toastMessage = "Added to your cart successfully";

						PreferenceUtil preferenceUtil = new PreferenceUtil(context);
						if (preferenceUtil.getOnOffVoiceCommand() == 1) {
							ttobj.speak(toastMessage, TextToSpeech.QUEUE_FLUSH, null);
						}
						// Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();

						new CustomBottomSingleActivityToast(context, dishDatas.get(position).getDishName().toString() + " is added to your cart","" , true);

					}

				} else {
					openPizzaDialog(position);
				}


			}

		});

		// set the ClickListener to handle the click event on child item
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//Toast.makeText(context, "" + child.get(childPosition).getOptionDatas().size(), Toast.LENGTH_SHORT).show();
			}
		});


		/** Set allergens info, dynamically item create for showing info */
		holder.linearLayoutAllergensInfo.removeAllViews();

		TextView txtAlergensLabel = new TextView(context);
		//txtAlergensLabel.setText("");
		//txtAlergensLabel.setTextColor(context.getResources().getColor(R.color.white));
		holder.linearLayoutAllergensInfo.addView(txtAlergensLabel);

		//String alergens = child.get(childPosition).getDishAllergens();
		int s = dishDatas.get(position).getAlergensDatas().size();
		Log.i("A SIZE", "" + s);
		if (dishDatas.get(position).getAlergensDatas() != null && dishDatas.get(position).getAlergensDatas().size()> 0) {

			for(int x = 0; x < dishDatas.get(position).getAlergensDatas().size(); x++) {
				ImageView image = new ImageView(context);
				//image.setBackgroundColor(context.getResources().getColor(R.color.off_gray_dark));

				if (dishDatas.get(position).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("1")) {
					image.setImageResource(R.drawable.al_fish);

				} else if (dishDatas.get(position).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("2")) {
					image.setImageResource(R.drawable.al_penut);

				} else if (dishDatas.get(position).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("3")) {
					image.setImageResource(R.drawable.al_nuts);

				} else if (dishDatas.get(position).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("4")) {
					image.setImageResource(R.drawable.al_egg);

				} else if (dishDatas.get(position).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("5")) {
					image.setImageResource(R.drawable.al_milk);

				} else if (dishDatas.get(position).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("6")) {
					image.setImageResource(R.drawable.al_musterd);

				} else if (dishDatas.get(position).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("7")) {
					image.setImageResource(R.drawable.al_soya);

				} else if (dishDatas.get(position).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("8")) {
					image.setImageResource(R.drawable.al_crustaceans);

				} else if (dishDatas.get(position).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("9")) {
					image.setImageResource(R.drawable.al_seeds);

				} else if (dishDatas.get(position).getAlergensDatas().get(x).getAlergensNo().equalsIgnoreCase("10")) {
					image.setImageResource(R.drawable.al_gluten);
				}

				//image.setPadding(5, 5, 5, 5);
				LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
				llp.setMargins(0, 0, 5, 0); // llp.setMargins(left, top, right, bottom);
				image.setLayoutParams(llp);

				holder.linearLayoutAllergensInfo.addView(image);
			}


			holder.linearLayoutAllergensInfo.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					//Toast.makeText(context, "" + child.get(childPosition).getOptionDatas().size(), Toast.LENGTH_SHORT).show();
					openDialogAllergens();
				}
			});

		}

		return convertView;
	}


	/*
	private boolean checkOrderingOpenStatus()
	{
		String currentDay = new SimpleDateFormat("EEEE").format(new Date());
		int shiftCount = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().size();
		boolean todayFound = false;
		for (int i = 0; i < shiftCount; i++) {
			if (AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getDayName().equalsIgnoreCase(currentDay)) {
				todayFound = true;
				if(AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas() == null || AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getOpenEndTimeDatas().size() == 0) {
					return false;
				}
			}
		}
		if(todayFound == false) {
			return false;
		}
		return true;
	}  */


	private void openDialog(String message, String okButton) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_message);
		TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
		ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
		txtViewPopupMessage.setText(message);
		Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
		btnAccept.setText(okButton);

		btnAccept.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		imgBtnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		dialog.show();
	}

	private void openDialog(ArrayList<OptionData> optionDatas1, String dishName) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_options);

		// set the custom dialog components - text, list view and Image button
		TextView text = (TextView) dialog.findViewById(R.id.txtViewOptionName);
		text.setText(dishName);

		ListView listView = (ListView) dialog.findViewById(R.id.listViewOptions);

		OptionsAdapter adapter = new OptionsAdapter(context, optionDatas1, restaurantId, dishName);
		listView.setAdapter(adapter);

		ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Button btnContinue = (Button) dialog.findViewById(R.id.btnContinue);
		btnContinue.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		dialog.show();
	}

	public void filter(String charText) {

		try {
			charText = charText.toLowerCase(Locale.getDefault());
			this.notifyDataSetChanged();
			// Separate the search resultfsdfdf
            /*String[] splited = charText.split("\\s+");
            if (splited.length > 1) {
                charText = splited[0];
            }*/

			Log.i("Text", "getting.. ** " + charText);
			//ArrayList<DishData> dishDatas1 = new ArrayList<>();
			//dishDatas1 = dishDatas;
			this.dishDatas.clear();

			if (charText.length() == 0) {
				//this.dishDatas.addAll(dishDatasTemp);

			} else {
				for (int i = 0; i < dishDatasTemp.size(); i++) {
					//Log.i("Text", "Print... ** " + dishDatasTemp.get(i).getDishName());
					if (dishDatasTemp.get(i).getDishName().toLowerCase(Locale.getDefault()).contains(charText)) {
						Log.i("Text", "Match.. ** " + dishDatasTemp.get(i).getDishName());
						dishDatas.add(dishDatasTemp.get(i));
					}
				}

				this.notifyDataSetChanged();

			}

		} catch (Exception e) {

		}

	}

	private void openDialogAllergens() {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.dialog_allergens_key);

		ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
		dialogButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
		btnAccept.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		dialog.show();
	}





	private void openPizzaDialog(final int position) {

		pizzaItemPrice = 0.0;
		pizzaItemIds = "";
		pizzaItemNames = "";

		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_pizza);

		ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
		dialogButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Button btnAdd = (Button) dialog.findViewById(R.id.btnAdd);
		btnAdd.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();

				int amount = Integer.parseInt(MainActivity.txtViewBubble.getText().toString());
				HashMap<String, Integer> wordList = new HashMap<String, Integer>();
				wordList =  db.checkExistItem(dishDatas.get(position).getDishId(), pizzaItemIds);

				if (wordList.get("count").intValue() > 0) {
					db.updateCartData(dishDatas.get(position).getDishId(), pizzaItemIds, wordList.get("qty").intValue() + 1);

				} else {
					db.insertCartData(restaurantId, dishDatas.get(position).getDishId(), dishDatas.get(position).getDishName().toString(), 1, String.valueOf(pizzaItemPrice), pizzaItemIds, pizzaItemNames);
					ConstantValues.IS_CART_ADDED = true;

				}

				MainActivity.txtViewBubble.setText("" + (amount + 1));

				String toastMessage = "Added to your cart successfully";

				PreferenceUtil preferenceUtil = new PreferenceUtil(context);
				if (preferenceUtil.getOnOffVoiceCommand() == 1) {
					ttobj.speak(toastMessage, TextToSpeech.QUEUE_FLUSH, null);
				}
				new CustomBottomSingleActivityToast(context,dishDatas.get(position).getDishName().toString() + " is added to your cart","",true);

			}

		});

		TextView txtViewOptionName = (TextView) dialog.findViewById(R.id.txtViewOptionName);
		txtViewOptionName.setText(dishDatas.get(position).getDishName());

     /*   ListView listView = (ListView) dialog.findViewById(R.id.listViewOptions);
        OptionsAdapter adapter = new OptionsAdapter(context, optionDatas1, restaurantId, dishName);
        listView.setAdapter(adapter); */

		LinearLayout linearSelected = (LinearLayout) dialog.findViewById(R.id.linearSelected);
		LinearLayout linearSelectedContainer = (LinearLayout) dialog.findViewById(R.id.linearSelectedContainer);

		RecyclerView recyclerPizzaGroup = (RecyclerView) dialog.findViewById(R.id.recyclerPizzaGroup);
		LinearLayoutManager layoutManager = new LinearLayoutManager(context);
		recyclerPizzaGroup.setLayoutManager(layoutManager);

		PizzaGroupAdapter pizzaGroupAdapter = new PizzaGroupAdapter(context, dishDatas.get(position).getPizzaGroupList(), false, linearSelected, linearSelectedContainer, btnAdd);
		recyclerPizzaGroup.setAdapter(pizzaGroupAdapter);

		dialog.show();

	}




	//Static UI
	static class ViewHolder {
		LinearLayout linearLayoutAllergensInfo;
        TextView txtViewPrice;
		LinearLayout linearLayoutAlergensInfo;
		TextView txtViewDescription;
		TextView txtViewDishName;
		TextView imgBtnItemActions;

	}

}
