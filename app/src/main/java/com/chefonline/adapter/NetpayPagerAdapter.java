package com.chefonline.adapter;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;

import com.chefonline.fragment.NetPayBillingAddressFragment;
import com.chefonline.fragment.NetPayConfirmFragment;
import com.chefonline.fragment.NetPayPaymentFragment;

import java.lang.CharSequence;import java.lang.Override;import java.lang.String;import java.util.List;

/**
 * Created by Edwin on 15/02/2015.
 */
public class NetpayPagerAdapter extends FragmentPagerAdapter {
    LayoutInflater inflater;
    Context mContext;
    List<String> strings;
    boolean isNew;
    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public NetpayPagerAdapter(Context context, FragmentManager fm, CharSequence mTitles[], boolean isNew) {
        super(fm);
        this.Titles = mTitles;
        this.isNew = isNew;
        this.mContext = context;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if(position == 0) // if the position is 0 we are returning the First tab
        {
            NetPayPaymentFragment fragment = new NetPayPaymentFragment();
            return fragment;
        } else if(position == 1) {
            NetPayBillingAddressFragment fragment = new NetPayBillingAddressFragment();
            return fragment;
        } else {
            Bundle bundle = new Bundle();
            bundle.putBoolean("is_new", isNew);
            NetPayConfirmFragment fragment = new NetPayConfirmFragment();
            fragment.setArguments(bundle);
            return fragment;
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {
       return Titles[position];
        //return MainActivity.relativeSizeSpan(Titles[position], position, mContext);
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return Titles.length;
    }


}