package com.chefonline.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.datamodel.NetpayData;
import com.chefonline.datamodel.PizzaGroupData;
import com.chefonline.datamodel.PizzaGroupItemData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.online.NetPayActivity;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;

import java.util.ArrayList;

public class PizzaGroupAdapter extends RecyclerView.Adapter<PizzaGroupAdapter.ViewHolder> {

    private ArrayList<PizzaGroupData> pizzaGroupList;
	private Activity context;
	private ArrayList<RadioButton> prevSelectedRadioButton;
	private ArrayList<String> selectedItemNames;
	private ArrayList<String> selectedItemIds;
	private ArrayList<String> selectedItemPrices;
	private double totalPrice;
	private boolean isFromCategoryExpandableAdapter;
	private LinearLayout linearSelected;
	private LinearLayout linearSelectedContainer;
	private Button btnAdd;

	public PizzaGroupAdapter(Activity context, ArrayList<PizzaGroupData> pizzaGroupList, boolean isFromCategoryExpandableAdapter, LinearLayout linearSelected, LinearLayout linearSelectedContainer, Button btnAdd) {
		this.context = context;
		this.pizzaGroupList = pizzaGroupList;
		this.isFromCategoryExpandableAdapter = isFromCategoryExpandableAdapter;
		this.linearSelected = linearSelected;
		this.linearSelectedContainer = linearSelectedContainer;
		this.btnAdd = btnAdd;
		totalPrice = 0.0;
		prevSelectedRadioButton = new ArrayList<>();
		selectedItemNames = new ArrayList<>();
		selectedItemIds = new ArrayList<>();
		selectedItemPrices = new ArrayList<>();
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.row_pizza_group, parent, false);
		ViewHolder holder = new ViewHolder(view);
		return holder;
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		holder.txtViewPizzaGroup.setText("Pizza Group " + (position + 1));
		holder.txtViewMin.setText("" + pizzaGroupList.get(position).getMinSelection());
		holder.txtViewMax.setText("" + pizzaGroupList.get(position).getMaxSelection());
		addPizzaItem(holder.linearPizzaItemContainer, position);
	}

	@Override
	public int getItemCount() {
		return pizzaGroupList.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
		TextView txtViewPizzaGroup;
		TextView txtViewMin;
		TextView txtViewMax;
		LinearLayout linearPizzaItemContainer;

		public ViewHolder(View itemView) {
			super(itemView);
			txtViewPizzaGroup = (TextView) itemView.findViewById(R.id.txtViewPizzaGroup);
			txtViewMin = (TextView) itemView.findViewById(R.id.txtViewMin);
			txtViewMax = (TextView) itemView.findViewById(R.id.txtViewMax);
			linearPizzaItemContainer = (LinearLayout) itemView.findViewById(R.id.linearPizzaItemContainer);
		//	relativeRowHolder = (RelativeLayout) itemView.findViewById(R.id.relativeRowHolder);
		//	relativeRowHolder.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if(v.getId() == R.id.relativeRowHolder) {
				int position = getAdapterPosition();
			}
		}
	}


	private void addSelectedItems() {
		int size = selectedItemNames.size();
		linearSelectedContainer.removeAllViews();
		View[] views = new View[size];
		TextView[] txtViewSelectedItems = new TextView[size];
		TextView[] txtViewPluses = new TextView[size];
		LayoutInflater inflater = LayoutInflater.from(context);
		for (int i = 0; i < size; i++) {
			views[i] = inflater.inflate(R.layout.column_selected_topping, null);
			txtViewSelectedItems[i] = (TextView) views[i].findViewById(R.id.txtViewSelectedItem);
			txtViewPluses[i] = (TextView) views[i].findViewById(R.id.txtViewPlus);
			txtViewSelectedItems[i].setText(selectedItemNames.get(i));
			if(i == (size - 1)) {
				txtViewPluses[i].setVisibility(View.GONE);
			}
			linearSelectedContainer.addView(views[i]);
		}
		linearSelected.setVisibility(View.VISIBLE);
	}


	private void checkIfSelectedItemExists() {
		if(selectedItemIds.size() > 0) {
			addSelectedItems();
		} else {
			linearSelectedContainer.removeAllViews();
			linearSelected.setVisibility(View.GONE);
		}

		String tempIds = "";
		String tempNames = "";
		for (int i = 0; i < selectedItemIds.size(); i++) {
			if(i == 0) {
				tempIds += selectedItemIds.get(i);
				tempNames += selectedItemNames.get(i);
			} else {
				tempIds += "," + selectedItemIds.get(i);
				tempNames += "," + selectedItemNames.get(i);
			}
		}
		if(isFromCategoryExpandableAdapter) {
			CategoryExpandableAdapter.pizzaItemIds = tempIds;
			CategoryExpandableAdapter.pizzaItemNames = tempNames;
		} else {
			SearchItemAdapter.pizzaItemIds = tempIds;
			SearchItemAdapter.pizzaItemNames = tempNames;
		}

	}


	private CompoundButton.OnCheckedChangeListener customCheckChangeListener(final int groupPos, final int itemPos, final boolean isMaxOne) {
		return new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
				ArrayList<PizzaGroupItemData> pizzaItemList = pizzaGroupList.get(groupPos).getPizzaGroupItemList();
				if(checked) {
				//	if(!selectedItemIds.contains(String.valueOf(pizzaGroupList.get(groupPos).getPizzaGroupItemList().get(itemPos).getPizzaGroupItemId()))) {
						if(isMaxOne) {
							if(prevSelectedRadioButton.get(groupPos) != null) {
								prevSelectedRadioButton.get(groupPos).setChecked(false);
							}
							prevSelectedRadioButton.set(groupPos, (RadioButton) compoundButton);
						}
						selectedItemNames.add(pizzaGroupList.get(groupPos).getPizzaGroupItemList().get(itemPos).getItemName());
						selectedItemIds.add(String.valueOf(pizzaGroupList.get(groupPos).getPizzaGroupItemList().get(itemPos).getPizzaGroupItemId()));
						selectedItemPrices.add(String.valueOf(pizzaGroupList.get(groupPos).getPizzaGroupItemList().get(itemPos).getItemPrice()));
						totalPrice = totalPrice + Double.valueOf(pizzaGroupList.get(groupPos).getPizzaGroupItemList().get(itemPos).getItemPrice());
					//}
				} else {
					if(selectedItemIds.contains(String.valueOf(pizzaGroupList.get(groupPos).getPizzaGroupItemList().get(itemPos).getPizzaGroupItemId()))) {
						int index = selectedItemIds.indexOf(String.valueOf(pizzaGroupList.get(groupPos).getPizzaGroupItemList().get(itemPos).getPizzaGroupItemId()));
						totalPrice = totalPrice - Double.valueOf(selectedItemPrices.get(index));
						selectedItemIds.remove(index);
						selectedItemNames.remove(index);
						selectedItemPrices.remove(index);
					}
				}
				if(isFromCategoryExpandableAdapter) {
					CategoryExpandableAdapter.pizzaItemPrice = totalPrice;
				} else {
					SearchItemAdapter.pizzaItemPrice = totalPrice;
				}
				btnAdd.setText("Add - " + totalPrice);
				checkIfSelectedItemExists();

			}
		};
	}

	private void addPizzaItem(LinearLayout linearContainer, int pos) {
		ArrayList<PizzaGroupItemData> pizzaItemList = pizzaGroupList.get(pos).getPizzaGroupItemList();
		boolean isMaxOne;
		if(pizzaGroupList.get(pos).getMaxSelection() > 1) {
			isMaxOne = false;
		} else {
			isMaxOne = true;
		}
		RadioButton radioButton = null;
		prevSelectedRadioButton.add(radioButton);


		int size = pizzaItemList.size();
		linearContainer.removeAllViews();
		View[] views = new View[size];
		TextView[] txtViewItemNames = new TextView[size];
		TextView[] txtViewItemPrices = new TextView[size];
		CheckBox[] checkBoxItems = new CheckBox[size];
		RadioButton[] radioButtonItems = new RadioButton[size];
		LayoutInflater inflater = LayoutInflater.from(context);
		for (int i = 0; i < size; i++) {
			views[i] = inflater.inflate(R.layout.row_pizza_item, null);
			txtViewItemNames[i] = (TextView) views[i].findViewById(R.id.txtViewItemName);
			txtViewItemPrices[i] = (TextView) views[i].findViewById(R.id.txtViewItemPrice);
			checkBoxItems[i] = (CheckBox) views[i].findViewById(R.id.checkBoxItem);
			radioButtonItems[i] = (RadioButton) views[i].findViewById(R.id.radioButtonItem);
			if(isMaxOne) {
				checkBoxItems[i].setVisibility(View.GONE);
				radioButtonItems[i].setVisibility(View.VISIBLE);
				radioButtonItems[i].setOnCheckedChangeListener(customCheckChangeListener(pos, i, isMaxOne));
			} else {
				radioButtonItems[i].setVisibility(View.GONE);
				checkBoxItems[i].setVisibility(View.VISIBLE);
				checkBoxItems[i].setOnCheckedChangeListener(customCheckChangeListener(pos, i, isMaxOne));
			}
			txtViewItemNames[i].setText(pizzaItemList.get(i).getItemName());
			txtViewItemPrices[i].setText("£" + pizzaItemList.get(i).getItemPrice());
			if(pizzaItemList.get(i).getDefaultStatus() == 1) {
				if(isMaxOne) {
					radioButtonItems[i].setChecked(true);
				//	radioButtonItems[i].setEnabled(false);
				} else {
					checkBoxItems[i].setChecked(true);
				//	checkBoxItems[i].setEnabled(false);
				}
			}
			linearContainer.addView(views[i]);
		}
	}



}
