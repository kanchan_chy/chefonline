package com.chefonline.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chefonline.datamodel.OffersData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.online.R;
import com.chefonline.utility.UnsafeOkHttpClient;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import okhttp3.OkHttpClient;

public class OffersDialogAdapter extends BaseAdapter {
    private String  dishName = null;
    private String restaurantId = null;
	private String restauranId = null;
    private ArrayList<OffersData> optionDataArrayList;
	private Context context;
	private LayoutInflater mInflater;
    private DataBaseUtil db;
	private TextToSpeech ttobj;
	private Picasso.Builder builder;
	private Picasso picasso;

	public OffersDialogAdapter(Context context, ArrayList<OffersData> optionDatas, String restauranId, String dishName) {
        this.dishName = dishName;
        this.restauranId = restauranId;
        this.optionDataArrayList = optionDatas;
		this.context = context;
        this.db = new DataBaseUtil(context);
        this.db.open();

		ttobj = new TextToSpeech(context,
				new TextToSpeech.OnInitListener() {
					@Override
					public void onInit(int status) {
						if(status != TextToSpeech.ERROR){
							ttobj.setLanguage(Locale.UK);
						}
					}
				});

		try {
			builder = new Picasso.Builder(context);
			OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient(context);
			builder.downloader(new OkHttp3Downloader(okHttpClient));
			picasso = builder.build();
		} catch (Exception e) {

		}

	}

	@Override
	public int getCount() {
		return this.optionDataArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.optionDataArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		final ViewHolder holder;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
            holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.row_offers_dialog, null);
			holder.txtViewOptionName = (TextView) convertView.findViewById(R.id.txtViewOptionName);
			holder.txtViewPrice = (TextView) convertView.findViewById(R.id.txtViewPrice);
			holder.txtViewSubText = (TextView) convertView.findViewById(R.id.txtViewSubText);
			holder.imagOfferLogo = (ImageView) convertView.findViewById(R.id.imageOfferLogo);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.txtViewOptionName.setText(optionDataArrayList.get(position).getOfferTitle());
		holder.txtViewPrice.setText("£ " + optionDataArrayList.get(position).getOfferElegibleAmount());
		holder.txtViewSubText.setText("Eligible amount is £" + optionDataArrayList.get(position).getOfferElegibleAmount());

		try {
			picasso.load(optionDataArrayList.get(position).getOfferImage()).error(R.drawable.placeholder).noFade().fit().into(holder.imagOfferLogo);
		} catch (Exception e) {
			Log.e("Image Loader", "" + e);
		}


		return convertView;
	}

	//Static dssg
	static class ViewHolder {
		TextView txtViewOptionName;
        TextView txtViewPrice;
        TextView txtViewSubText;
        TextView txtViewReview;
		ImageView imagOfferLogo;
        Button btnDiscount;

	}

}
