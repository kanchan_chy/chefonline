package com.chefonline.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.datamodel.NetpayData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.online.NetPayActivity;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;

import java.util.ArrayList;

public class ManagePaymentMethodsAdapter extends RecyclerView.Adapter<ManagePaymentMethodsAdapter.ViewHolder> {

    private ArrayList<NetpayData> netpayDatas;
	private Activity context;
	private LayoutInflater mInflater;
	private int[] idPaymentTypeIcons = {R.drawable.master_card,
			R.drawable.visa,
			R.drawable.american_express,
			R.drawable.maestro,
			R.drawable.master_card_debit,
			R.drawable.visa_debit,
			R.drawable.visa_electron,
			R.drawable.diners_club_international};
	private String[] paymentTypeCodes = {"MCRD",
			"VISA",
			"AMEX",
			"MSTO",
			"MCDB",
			"VISAUK",
			"ELEC",
			"DINE"
	};
	private String[] paymentTypeNames = {"MasterCard",
			"Visa",
			"American Express",
			"Maestro",
			"MasterCard Debit",
			"Visa Debit UK",
			"Visa Electron",
			"Diners"
	};


	public ManagePaymentMethodsAdapter(Activity context, ArrayList<NetpayData> netpayDatas) {
		this.context = context;
		this.netpayDatas = netpayDatas;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.row_manage_payment_method, parent, false);
		ViewHolder holder = new ViewHolder(view);
		return holder;
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		int paymentTypePos = -1;
		holder.txtTitle.setText(netpayDatas.get(position).getCardNumber());
		for (int i = 0; i < paymentTypeCodes.length; i++) {
			if(paymentTypeCodes[i].equalsIgnoreCase(netpayDatas.get(position).getCardType())) {
				paymentTypePos = i;
				break;
			}
		}
		if(paymentTypePos != -1) {
			holder.imgViewPaymentMethod.setImageResource(idPaymentTypeIcons[paymentTypePos]);
		}
	}

	@Override
	public int getItemCount() {
		return netpayDatas.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
		TextView txtTitle;
		ImageView imgViewPaymentMethod;
		RelativeLayout relativeRowHolder;

		public ViewHolder(View itemView) {
			super(itemView);
			txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
			imgViewPaymentMethod = (ImageView) itemView.findViewById(R.id.imgViewPaymentMethod);
			relativeRowHolder = (RelativeLayout) itemView.findViewById(R.id.relativeRowHolder);
			relativeRowHolder.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if(v.getId() == R.id.relativeRowHolder) {
				int position = getAdapterPosition();
				for (int i = 0; i < paymentTypeCodes.length; i++) {
					if(paymentTypeCodes[i].equalsIgnoreCase(netpayDatas.get(position).getCardType())) {
						NetPayActivity.paymentTypeName = paymentTypeNames[i];
						break;
					}
				}
				NetpayData netpayData = netpayDatas.get(position);
				//	netpayData.setAmount(AppData.getInstance().getOrderPlaceData().getGrandTotal().substring(1));
				//	netpayData.setBillToCompany("ChefOnline");
				AppData.getInstance().setNetpayData(netpayData);
				Intent intent = new Intent(context, NetPayActivity.class);
				intent.putExtra("is_new", false);
				context.startActivity(intent);
				context.finish();
			}
		}
	}


	private void openDialog(String message, String okButton, String cancelButton, final int position) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_restuarant_override);
		TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
		ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
		txtViewPopupMessage.setText(message);
		Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
		btnAccept.setText(okButton);

		// if button is clicked, close the custom dialog
		btnAccept.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				deleteCard(position);
			}

		});

		imgBtnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
		btnCancel.setText(cancelButton);
		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		dialog.show();
	}


	private void deleteCard(int position) {
		try {
			PreferenceUtil preferenceUtil = new PreferenceUtil(context);
			DataBaseUtil db = new DataBaseUtil(context);
			db.open();
			int flag = db.deleteNetPayUserData(preferenceUtil.getUserID(), netpayDatas.get(position).getCardNumber());
			db.close();
			netpayDatas.remove(position);
			notifyItemRemoved(position);
			new CustomToast(context, "Card is deleted", "", true);
		} catch (Exception e) {
			new CustomToast(context, "Card can't be deleted", "", false);
		}
	}


	public void remove(int position) {
		notifyDataSetChanged();
		openDialog("Do you want to delete the card?", "DELETE", "CANCEL", position);
	}


}
