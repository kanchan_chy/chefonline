package com.chefonline.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chefonline.datamodel.DishData;
import com.chefonline.datamodel.OrderHistoryData;
import com.chefonline.datamodel.PizzaGroupItemData;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.UtilityMethod;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class OrderHistoryDetailsAdapter extends BaseAdapter {

	private ArrayList<DishData> orderHistoryList;
	private Context context;
	private LayoutInflater mInflater;

	public OrderHistoryDetailsAdapter(Context context, ArrayList<DishData> restaurantList) {
		this.orderHistoryList = restaurantList;
		this.context = context;
	}

	@Override
	public int getCount() {
		return this.orderHistoryList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.orderHistoryList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolder holder;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
            holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.row_order_history_details, null);

			holder.txtViewDishName = (TextView) convertView.findViewById(R.id.txtViewDishName);
			holder.txtViewPizzaItem = (TextView) convertView.findViewById(R.id.txtViewPizzaItem);
			holder.txtViewQty = (TextView) convertView.findViewById(R.id.txtViewQty);
			holder.imgViewLogo = (ImageView) convertView.findViewById(R.id.imgViewLogo);
			holder.txtViewGrandTotal = (TextView) convertView.findViewById(R.id.txtViewTotal);
			holder.txtViewDetails = (TextView) convertView.findViewById(R.id.txtViewDetails);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.txtViewQty.setText(orderHistoryList.get(position).getDishQty() + " x ");
		holder.txtViewDishName.setText(orderHistoryList.get(position).getDishName());
		holder.txtViewDetails.setText(orderHistoryList.get(position).getDishDescription());
		//holder.txtViewAddress.setText(orderHistoryList.get(position).getAddress1());

		String tempNames = "";
		double totalPrice = 0.0;
		if(orderHistoryList.get(position).isPizzaMenu()) {
			ArrayList<PizzaGroupItemData> pizzaItemList = orderHistoryList.get(position).getOrderedPizzaItemList();
			for (int j = 0; j < pizzaItemList.size(); j++) {
				if(j == 0) {
					tempNames += pizzaItemList.get(j).getItemName();
				} else {
					tempNames += ", " + pizzaItemList.get(j).getItemName();
				}
				totalPrice += Double.valueOf(pizzaItemList.get(j).getItemPrice());
			}
		}

		if("".equalsIgnoreCase(tempNames)) {
			holder.txtViewPizzaItem.setVisibility(View.GONE);
			double realPrice = (Double.parseDouble(orderHistoryList.get(position).getDishQty()) * Double.parseDouble(orderHistoryList.get(position).getDishPrice()));
			/*DecimalFormat df = new DecimalFormat("#.##");
			realPrice = Double.valueOf(df.format(realPrice));*/
			holder.txtViewGrandTotal.setText(context.getResources().getString(R.string.pound_sign) + UtilityMethod.priceFormatter(realPrice));
		} else {
			holder.txtViewPizzaItem.setText(tempNames);
			holder.txtViewPizzaItem.setVisibility(View.VISIBLE);
			double realPrice = (Double.parseDouble(orderHistoryList.get(position).getDishQty()) * totalPrice);
			holder.txtViewGrandTotal.setText(context.getResources().getString(R.string.pound_sign) + UtilityMethod.priceFormatter(realPrice));
		}
		
		return convertView;
	}

	static class ViewHolder {
		TextView txtViewDishName;
		TextView txtViewPizzaItem;
		TextView txtViewQty;
        TextView txtViewAddress;
        TextView txtViewGrandTotal;
        TextView txtViewDetails;
        ImageView imgViewLogo;
        Button btnDiscount;

	}

}
