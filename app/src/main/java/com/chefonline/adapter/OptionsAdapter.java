package com.chefonline.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomBottomSingleActivityToast;
import com.chefonline.datamodel.OptionData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.online.R;
import com.chefonline.online.MainActivity;
import com.chefonline.utility.ConstantValues;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class OptionsAdapter extends BaseAdapter {
    private String  dishName = null;
    private String restaurantId = null;
	private String restauranId = null;
    private ArrayList<OptionData> optionDataArrayList;
	private Activity context;
	private LayoutInflater mInflater;
    private DataBaseUtil db;
	private TextToSpeech ttobj;
	private long mLastClickTime = 0;

	public OptionsAdapter(Activity context, ArrayList<OptionData> optionDatas, String restauranId, String dishName) {
        this.dishName = dishName;
        this.restauranId = restauranId;
        this.optionDataArrayList = optionDatas;
		this.context = context;
        this.db = new DataBaseUtil(context);
        this.db.open();

		ttobj = new TextToSpeech(context,
				new TextToSpeech.OnInitListener() {
					@Override
					public void onInit(int status) {
						if(status != TextToSpeech.ERROR){
							ttobj.setLanguage(Locale.UK);
						}
					}
				});
	}

	@Override
	public int getCount() {
		return this.optionDataArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.optionDataArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		final ViewHolder holder;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
            holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.row_options, null);
			holder.txtViewOptionName = (TextView) convertView.findViewById(R.id.txtViewOptionName);
			holder.txtViewPrice = (TextView) convertView.findViewById(R.id.txtViewPrice);
			//holder.imgBtnAdd = (ImageButton) convertView.findViewById(R.id.imgBtnAdd);
			holder.relativeOption = (RelativeLayout) convertView.findViewById(R.id.relativeOption);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.txtViewOptionName.setText(optionDataArrayList.get(position).getOptionName());
		holder.txtViewPrice.setText("£ " + optionDataArrayList.get(position).getOptionPrice());

        holder.relativeOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				int amount = Integer.parseInt(MainActivity.txtViewBubble.getText().toString());
				if (amount >= 999) {
					openDialog("You can not order more than 999 item.", "OK");
					return;
				}

                HashMap<String, Integer> cartMap = new HashMap<String, Integer>();
                cartMap =  db.checkExistItem(optionDataArrayList.get(position).getOptionSelfId(), "");

                if (cartMap.get("count").intValue() > 0) {
                    db.updateCartData(optionDataArrayList.get(position).getOptionSelfId(), "", cartMap.get("qty").intValue() + 1);

                } else {
                    db.insertCartData(restauranId, optionDataArrayList.get(position).getOptionSelfId(), dishName + "" + optionDataArrayList.get(position).getOptionName().toString(), 1, optionDataArrayList.get(position).getOptionPrice(), "", "");
                    ConstantValues.IS_CART_ADDED = true;
                }

                //final TextView txtViewBubble = (TextView) context.findViewById(R.id.txtViewBubble);

                MainActivity.txtViewBubble.setText("" + (amount + 1));
                for (int i = 0; i < db.fetchCartData().size(); i++) {
                    Log.i("CART_DATA_OPTION ", "=> " + db.fetchCartData().get(i).getItemName() + " Qty => " + db.fetchCartData().get(i).getItemQty());
                }

				String toastMessage = "Added to your cart successfully";
				PreferenceUtil preferenceUtil = new PreferenceUtil(context);
				if (preferenceUtil.getOnOffVoiceCommand() == 1) {
					ttobj.speak(toastMessage, TextToSpeech.QUEUE_FLUSH, null);
				}
				new CustomBottomSingleActivityToast(context,optionDataArrayList.get(position).getOptionName() + " is added to your cart","",true);

            }
        });

		return convertView;
	}


	private void openDialog(String message, String okButton) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_message);
		TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
		ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
		txtViewPopupMessage.setText(message);
		Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
		btnAccept.setText(okButton);

		btnAccept.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		imgBtnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		dialog.show();
	}

	//Static dssg
	static class ViewHolder {
		TextView txtViewOptionName;
        TextView txtViewPrice;
        TextView txtViewDistance;
        TextView txtViewReview;
        //ImageButton imgBtnAdd;
        Button btnDiscount;
		RelativeLayout relativeOption;

	}

}
