package com.chefonline.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.chefonline.customview.ChefOnlineTextView;
import com.chefonline.datamodel.CartData;
import com.chefonline.datamodel.OfferListData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.fragment.CartFragment;
import com.chefonline.fragment.OffersCalculationFragment;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.UtilityMethod;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class SelectionOfferAdapter extends BaseAdapter {
    private ArrayList<OfferListData> optionDataArrayList;
    private ArrayList<OfferListData> optionDataTempArrayList = new ArrayList<>();
	private ArrayList<String> receivedOffersId = new ArrayList<>();
	private Activity context;
	private LayoutInflater mInflater;
    private DataBaseUtil db;
	private long mLastClickTime = 0;

	public SelectionOfferAdapter(Activity context, ArrayList<OfferListData> optionDatas, ArrayList<String> receivedOffersId) {
        this.optionDataArrayList = optionDatas;
		this.receivedOffersId = receivedOffersId;
        this.optionDataTempArrayList.addAll(optionDatas);
		this.context = context;
        this.db = new DataBaseUtil(context);
        this.db.open();
	}

	@Override
	public int getCount() {
		return this.optionDataArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.optionDataArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		final ViewHolder holder;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
            holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.row_offers, null);
			holder.txtViewOptionName = (TextView) convertView.findViewById(R.id.txtViewOptionName);
			holder.txtViewPrice = (TextView) convertView.findViewById(R.id.txtViewPrice);
			holder.txtEligibility = (ChefOnlineTextView) convertView.findViewById(R.id.txtEligibility);
			holder.imgBtnAdd = (Button) convertView.findViewById(R.id.imgBtnAdd);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.txtViewOptionName.setText(optionDataArrayList.get(position).getOfferText());
		holder.txtEligibility.setText(String.valueOf("Eligible amount is £" + optionDataArrayList.get(position).getEligibleAmount()));

        holder.imgBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				if (optionDataArrayList.get(position).getOfferCode().equalsIgnoreCase("1")) {
					CartFragment.SELECTED_OFFER = CartFragment.IS_DISCOUNT_SELECTED;
					OffersCalculationFragment.txtViewOfferText.setText(optionDataArrayList.get(position).getOfferText());
					calculateTotalAmount(optionDataArrayList.get(position).getPolicyId(), optionDataArrayList.get(position).getOfferCode(), optionDataArrayList.get(position).getOfferId());
					CartFragment.discountId = optionDataArrayList.get(position).getOfferId();
					optionDataArrayList.clear();
					SelectionOfferAdapter.this.notifyDataSetChanged();
				} else {
					CartFragment.SELECTED_OFFER = CartFragment.IS_OFFER_SELECTED;
					OffersCalculationFragment.txtViewOfferText.setText(optionDataArrayList.get(position).getOfferText());
					calculateTotalAmount(optionDataArrayList.get(position).getPolicyId(), optionDataArrayList.get(position).getOfferCode(), optionDataArrayList.get(position).getOfferId());
					//CartFragment.offerId = optionDataArrayList.get(position).getOfferId();
					OffersCalculationFragment.allOfferTexts.add(optionDataArrayList.get(position).getOfferText());
					receivedOffersId.add(optionDataArrayList.get(position).getOfferId());
					try {
						JSONArray jsonArray = new JSONArray();
						for (int i = 0; i < receivedOffersId.size(); i++) {
							JSONObject jObj = new JSONObject();
							jObj.put("offer_id", optionDataArrayList.get(position).getOfferId());
							jsonArray.put(jObj);
						}
						CartFragment.offerId = jsonArray.toString();
					} catch (Exception e) {
						CartFragment.offerId = "";
					}
					optionDataArrayList.clear();
					/*optionDataArrayList.addAll(optionDataTempArrayList);
					optionDataArrayList.remove(position);*/
					SelectionOfferAdapter.this.notifyDataSetChanged();
				}

				OffersCalculationFragment.txtViewCancelOffer.setVisibility(View.VISIBLE);
				OffersCalculationFragment.txtViewOfferText.setVisibility(View.VISIBLE);
				//CartFragment.dialog.dismiss();
                //Toast.makeText(context, optionDataArrayList.get(position).getOptionName() + " is added to your cart", Toast.LENGTH_SHORT).show();

            }
        });

		return convertView;
	}

	//Static dssg
	static class ViewHolder {
		TextView txtViewOptionName;
        TextView txtViewPrice;
		ChefOnlineTextView txtEligibility;
        Button imgBtnAdd;
        Button btnDiscount;

	}

	private void calculateTotalAmount(String policyId, String offerCode, String offerId) {
		/** Calculate Total Amount */
		Double totalSubAmout = 0.00;
		ArrayList<CartData> cartDataForTotal = new ArrayList<>();
		DataBaseUtil dataBaseUtil = new DataBaseUtil(context);
		cartDataForTotal = db.fetchCartData();
		for (int i = 0; i < cartDataForTotal.size(); i++) {
			totalSubAmout += (cartDataForTotal.get(i).getItemQty() * cartDataForTotal.get(i).getPrice());
		}

		TextView txtViewSubTotal = (TextView)  this.context.findViewById(R.id.txtViewSubTotal1);
		txtViewSubTotal.setText("£" + UtilityMethod.priceFormatter(totalSubAmout));

		TextView txtViewTotal = (TextView) this.context.findViewById(R.id.txtViewTotal1);
		TextView txtViewDiscount = (TextView) this.context.findViewById(R.id.txtViewDiscount1);

		try {
			// Offer code 1 when discount selected
			if (offerCode.equalsIgnoreCase("1")) {
				OffersCalculationFragment.SELECTED_OFFER = 1;
				for (int i = 0; i < AppData.getInstance().getRestaurantInfoData().getDiscountList().size(); i++) {
					if (offerId.equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountId())) {
                        //Log.i("OFFER_ID", "Selected " + offerId);
						//if (totalSubAmout >  Integer.parseInt(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountEligibleAmount())) {
							if ("Percentage".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountType())) {
								Double discount = Double.parseDouble(UtilityMethod.priceFormatter((totalSubAmout * Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount()) / 100)));
								txtViewDiscount.setText("£" +  UtilityMethod.priceFormatter(discount));
								txtViewTotal.setText("£" + UtilityMethod.priceFormatter((Double.parseDouble(UtilityMethod.priceFormatter(totalSubAmout)) - discount)));
                                Log.i("OFFER_ID_IN", "Selected " + offerId);
                            } else {
								txtViewDiscount.setText("£" +  UtilityMethod.priceFormatter(Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount())));
								txtViewTotal.setText("£" + UtilityMethod.priceFormatter(totalSubAmout - Double.parseDouble(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount())));
							}

						/*} else {
							txtViewTotal.setText("£" + String.format("%.2f", totalSubAmout));
							txtViewDiscount.setText("£0.0");
						}*/
						break;
					}
				}

			} else {
				OffersCalculationFragment.SELECTED_OFFER = 2;
			//	txtViewTotal.setText("£" + UtilityMethod.priceFormatter(totalSubAmout));
			//	txtViewDiscount.setText("£0.00");
			}

		} catch (Exception e) {
			txtViewTotal.setText("£" + UtilityMethod.priceFormatter(totalSubAmout));
			txtViewDiscount.setText("£0.00");
		}

		/** End total amount */
	}

}
