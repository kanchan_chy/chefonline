package com.chefonline.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.customview.CustomToast;
import com.chefonline.datamodel.NewAddressData;
import com.chefonline.fragment.CheckoutFragment;
import com.chefonline.online.AddAddressActivity;
import com.chefonline.online.R;
import com.chefonline.utility.ConstantValues;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddressDialogAdapter extends BaseAdapter {
    private ArrayList<NewAddressData> addressList;
	private Activity context;
	private LayoutInflater mInflater;
	int selectedPos;
	RadioButton radioDeleted;


	private long mLastClickTime = 0;


	public AddressDialogAdapter(Activity context, ArrayList<NewAddressData> addressList, int selectedPos) {
		this.context = context;
        this.addressList = addressList;
		this.selectedPos = selectedPos;
		this.radioDeleted = null;
	}

	@Override
	public int getCount() {
		return this.addressList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.addressList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		final ViewHolder holder;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
            holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.row_address, null);
			holder.txtPostcode = (TextView) convertView.findViewById(R.id.txtPostcode);
			holder.txtAddress = (TextView) convertView.findViewById(R.id.txtAddress);
			holder.radioPostcode = (RadioButton) convertView.findViewById(R.id.radioPostcode);
			holder.imgViewEdit = (ImageView) convertView.findViewById(R.id.imgViewEdit);
			holder.imgViewDelete = (ImageView) convertView.findViewById(R.id.imgViewdelete);
			holder.txtAvailable = (TextView) convertView.findViewById(R.id.txtAvailable);
			holder.relativeOption = (RelativeLayout) convertView.findViewById(R.id.relativeOption);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if("1".equalsIgnoreCase(addressList.get(position).getIsDelivered())) {
			holder.txtAvailable.setText("Available");
			holder.txtAvailable.setTextColor(context.getResources().getColor(R.color.dark_green));
		} else if("3".equalsIgnoreCase(addressList.get(position).getIsDelivered())) {
			holder.txtAvailable.setText("Default");
			holder.txtAvailable.setTextColor(context.getResources().getColor(R.color.off_gray_dark));
		} else {
			holder.txtAvailable.setText("Not Available");
			holder.txtAvailable.setTextColor(context.getResources().getColor(R.color.theme_red));
		}

		holder.txtPostcode.setText(addressList.get(position).getExtPostCode());
		String address = addressList.get(position).getExtAddress1();
		if(addressList.get(position).getExtAddress2() != null && !addressList.get(position).getExtAddress2().equalsIgnoreCase("")) {
			address += ", " + addressList.get(position).getExtAddress2();
		}
		holder.txtAddress.setText(address);

		if(position == 0) {
			holder.imgViewEdit.setVisibility(View.GONE);
			holder.imgViewDelete.setVisibility(View.GONE);
			if(position != selectedPos) {
				holder.radioPostcode.setChecked(false);
			}
		}

		holder.imgViewEdit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CheckoutFragment.dismissAddressDialog();
				Intent intent = new Intent(context, AddAddressActivity.class);
				intent.putExtra("ext_id", addressList.get(position).getExtId());
				intent.putExtra("ext_postcode", addressList.get(position).getExtPostCode());
				intent.putExtra("ext_addr1", addressList.get(position).getExtAddress1());
				intent.putExtra("ext_addr2", addressList.get(position).getExtAddress2());
				intent.putExtra("ext_town", addressList.get(position).getExtTown());
				context.startActivity(intent);
			}
		});


		holder.imgViewDelete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				radioDeleted = holder.radioPostcode;
				showDeleteDialog(position);
			}
		});

			holder.radioPostcode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isChecked) {
						if(position != selectedPos) {
							Handler handler = new Handler();
							handler.postDelayed(new Runnable() {
								@Override
								public void run() {
									CheckoutFragment.changeAddressData(position);
									CheckoutFragment.dismissAddressDialog();
								}
							}, 300);
						}
					}
				}
			});

			holder.relativeOption.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					holder.radioPostcode.setChecked(true);
				}
			});

		if(position == selectedPos) {
			holder.radioPostcode.setChecked(true);
		}

		return convertView;
	}

	//Static dssg
	static class ViewHolder {
		TextView txtPostcode, txtAddress, txtAvailable;
		RadioButton radioPostcode;
		ImageView imgViewEdit, imgViewDelete;
		RelativeLayout relativeOption;
	}


	private void showDeleteDialog(final int pos) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(R.layout.dialog_restuarant_override);
		TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
		ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
		txtViewPopupMessage.setText("Are you sure you want to delete this address?");
		Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
		btnAccept.setText("Delete");

		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				radioDeleted = null;
			}
		});


		// if button is clicked, close the custom dialog
		btnAccept.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
					mLastClickTime = SystemClock.elapsedRealtime();
					return;
				}
				else {
					mLastClickTime = SystemClock.elapsedRealtime();
				}
				dialog.dismiss();
				callDeleteAddress(pos);
			}

		});

		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
		btnCancel.setText("Cancel");
		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				radioDeleted = null;
				dialog.dismiss();
			}

		});

		dialog.show();
	}



	ProgressDialog progress;
	private void callDeleteAddress(final int pos) {
		RequestQueue queue = Volley.newRequestQueue(context);
		StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(pos), createMyReqErrorListener()) {
			protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put("funId", "63");
				params.put("user_id", addressList.get(pos).getUserId());
				params.put("user_address_ext_id", addressList.get(pos).getExtId());
				return params;
			}
		};

		int socketTimeout = 30000;//30 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		myReq.setRetryPolicy(policy);
		queue.add(myReq);
		progress = new ProgressDialog(context);
		progress.setMessage("Please wait....");
		progress.show();
	}



	private Response.ErrorListener createMyReqErrorListener() {
		return new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				try {
					progress.dismiss();
					String x = error.getMessage();
					new CustomToast(context, "Sorry, deletion was not possible.", "", false);
				}
				catch (Exception e) {
					Log.e("After volley execution", e.getMessage());
				}
			}
		};
	}

	private Response.Listener<String> createMyReqSuccessListener(final int pos) {
		return new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				try {
					progress.dismiss();
					Log.i("Delete_response", "****" + response);

					JSONObject jsonObject = new JSONObject(response);
					if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
						selectedPos = 0;
						if(radioDeleted != null) {
							radioDeleted.setChecked(false);
							radioDeleted = null;
						}
						addressList.remove(pos);
						CheckoutFragment.changeAddressData(0);
						notifyDataSetChanged();
						new CustomToast(context, jsonObject.getString("message"), "", true);
					} else {
						new CustomToast(context, "Sorry, deletion was not possible.", "", false);
					}

				} catch (Exception e) {
					try {
						e.printStackTrace();
						new CustomToast(context, "Sorry, deletion was not possible.", "", false);
					}
					catch (Exception e1){
					}
				}

			}

		};
	}



}
