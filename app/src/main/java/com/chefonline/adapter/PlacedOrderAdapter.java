package com.chefonline.adapter;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.chefonline.datamodel.CartData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.fragment.CartFragment;
import com.chefonline.online.R;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.UtilityMethod;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class PlacedOrderAdapter extends BaseAdapter {

	private ArrayList<CartData> cartDatas;
	public Activity context = null;
	private Fragment fragmentContext = null;
	private LayoutInflater mInflater = null;
    private DataBaseUtil db;
    ArrayList<CartData> cartDataForTotal = new ArrayList<>();

	public PlacedOrderAdapter(Activity context, Fragment fragmentContext, ArrayList<CartData> cartDataArrayList) {
		this.cartDatas = cartDataArrayList;
		this.context = context;
        this.fragmentContext = fragmentContext;
        this.db = new DataBaseUtil(context);
        this.db.open();
        //calculateTotalAmount();
	}

	@Override
	public int getCount() {
		return this.cartDatas.size();
	}

	@Override
	public Object getItem(int position) {
		return this.cartDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		final ViewHolder holder;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
            holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.row_placed_order_item, null);
			holder.txtViewItemName = (TextView) convertView.findViewById(R.id.txtViewItemName);
            holder.txtViewPizzaItem = (TextView) convertView.findViewById(R.id.txtViewPizzaItem);
			holder.txtViewPrice = (TextView) convertView.findViewById(R.id.txtViewPrice);
			holder.txtViewQty = (TextView) convertView.findViewById(R.id.txtViewQty);
			holder.imgBtnClose = (ImageButton) convertView.findViewById(R.id.imgBtnClose);
			holder.btnAddCart = (Button) convertView.findViewById(R.id.btnAddCart);
			holder.btnRemoveCart = (Button) convertView.findViewById(R.id.btnRemoveCart);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

        holder.txtViewItemName.setText(cartDatas.get(position).getItemName());

        if(cartDatas.get(position).getItemPizzaNames() == null || "".equalsIgnoreCase(cartDatas.get(position).getItemPizzaNames())) {
            holder.txtViewPizzaItem.setVisibility(View.GONE);
        } else {
            String tempNames = cartDatas.get(position).getItemPizzaNames();
            String allNames = "";
            String name = "";
            for (int j = 0; j < tempNames.length(); j++) {
                if(tempNames.charAt(j) != ',') {
                    name += tempNames.charAt(j);
                }
                if(j == (tempNames.length() - 1) || tempNames.charAt(j) == ',') {
                    if("".equalsIgnoreCase(allNames)) {
                        allNames += name;
                    } else {
                        allNames += ", " + name;
                    }
                    name = "";
                }
            }
            holder.txtViewPizzaItem.setText(allNames);
            holder.txtViewPizzaItem.setVisibility(View.VISIBLE);
        }

		holder.txtViewQty.setText("" + cartDatas.get(position).getItemQty());


        double realPrice = (cartDatas.get(position).getPrice() * cartDatas.get(position).getItemQty());
       // DecimalFormat df = new DecimalFormat("#.##");
       // realPrice = Double.valueOf(df.format(realPrice));

		holder.txtViewPrice.setText("£" + UtilityMethod.priceFormatter(realPrice));
        //holder.btnAddCart.setText("" + cartDatas.get(position).getPrice());

        /** Calculate Total Amount */
        /** End total amount */

        final float price;
        price = (cartDatas.get(position).getPrice());

        /*holder.btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = 0;
                qty = qty + Integer.parseInt(holder.txtViewQty.getText().toString());
                qty += 1;
                holder.txtViewQty.setText("" + qty);
                db.updateCartData(Integer.toString(cartDatas.get(position).getItemId()), qty);

                //double time = 200.3456;
                double realPrice = (price * qty);
                DecimalFormat df = new DecimalFormat("#.##");
                realPrice = Double.valueOf(df.format(realPrice));

               // System.out.println(time); // 200.35

                holder.txtViewPrice.setText("£" + realPrice);

                *//** Calculate Total Amount *//*
                calculateTotalAmount();
                *//** End total amount *//*

                for (int i = 0; i < db.fetchCartData().size(); i++) {
                    //Log.i("CART_DATA_OPTION ", "=> " + db.fetchCartData().get(i).getItemName() + " Qty => " + db.fetchCartData().get(i).getItemQty());
                }

            }
        });*/

        /*holder.btnRemoveCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(holder.txtViewQty.getText().toString().trim()) > 0) {

                    int qty = 0;
                    qty = qty + Integer.parseInt(holder.txtViewQty.getText().toString());
                    qty -= 1;

                    Log.i("SINGLE PRICE ", "=> " + price);
                    Log.i("QTY ", "=> " + (qty));
                    Log.i("Total ", "=> " + (price*(qty)));

                    holder.txtViewQty.setText("" + qty);
                    db.updateCartData(Integer.toString(cartDatas.get(position).getItemId()), (Integer.parseInt(holder.txtViewQty.getText().toString())));

                    double time = (price * qty);
                    DecimalFormat df = new DecimalFormat("#.##");
                    time = Double.valueOf(df.format(time));

                    holder.txtViewPrice.setText("£" + time);

                    *//** Calculate Total Amount *//*
                    calculateTotalAmount();
                    *//** End total amount *//*
                    for (int i = 0; i < db.fetchCartData().size(); i++) {
                        //Log.i("CART_DATA_OPTION  REM", "=> " + db.fetchCartData().get(i).getItemName() + " Qty => " + db.fetchCartData().get(i).getItemQty() + " ID=> " + db.fetchCartData().get(i).getItemId());
                    }
                }

            }
        });*/

        /*holder.imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("CART_DATA_OPTION  DEL", "=> " + cartDatas.get(position).getItemId());

                db.deleteCartData(Integer.toString(cartDatas.get(position).getItemId()));
                cartDatas.remove(position);
                calculateTotalAmount();
                PlacedOrderAdapter.this.notifyDataSetChanged();

                for (int i = 0; i < db.fetchCartData().size(); i++) {
                    Log.i("CART_DATA_OPTION  DEL", "=> " + db.fetchCartData().get(i).getItemName() + " Qty => " + db.fetchCartData().get(i).getItemQty() + " ID=> " + db.fetchCartData().get(i).getItemId());
                }
            }
        });*/

		return convertView;
	}

	static class ViewHolder {
		TextView txtViewItemName;
        TextView txtViewPizzaItem;
        TextView txtViewPrice;
        TextView txtViewQty;
        ImageButton imgBtnClose;
        Button btnRemoveCart;
        Button btnAddCart;
        Button btnDiscount;

	}

    private void calculateTotalAmount() {
        /** Calculate Total Amount */
        Double totalSubAmout = 0.0;
        cartDataForTotal = db.fetchCartData();
        for (int i = 0; i < cartDataForTotal.size(); i++) {
            totalSubAmout += (cartDataForTotal.get(i).getItemQty() * cartDataForTotal.get(i).getPrice());
        }

        TextView txtViewSubTotal = (TextView)  this.context.findViewById(R.id.txtViewSubTotal);
        txtViewSubTotal.setText("£" + String.format("%.2f", totalSubAmout));

        TextView txtViewTotal = (TextView) this.context.findViewById(R.id.txtViewTotal);
        //txtViewTotal.setText("£" + String.format("%.2f", totalSubAmout));

        TextView txtViewDiscount = (TextView) this.context.findViewById(R.id.txtViewDiscount);

        try {
            if (CartFragment.SELECTED_OFFER == CartFragment.IS_DISCOUNT_SELECTED) {
                for (int i = 0; i < AppData.getInstance().getRestaurantInfoData().getDiscountList().size(); i++) {
                    if (CartFragment.policyId.equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getRestaurantOrderPolicyId())) {

                        if (totalSubAmout >  Integer.parseInt(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountEligibleAmount())) {
                            if ("Percentage".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountType())) {
                                txtViewDiscount.setText("£" +  String.format("%.2f", (totalSubAmout * Float.parseFloat(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount()) / 100)));
                                txtViewTotal.setText("£" + String.format("%.2f", totalSubAmout - (totalSubAmout * Float.parseFloat(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount()) / 100)));

                            } else {
                                txtViewDiscount.setText("£" +  String.format("%.2f", (totalSubAmout * Float.parseFloat(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount()) / 100)));
                                txtViewTotal.setText("£" + String.format("%.2f",  (totalSubAmout - Float.parseFloat(AppData.getInstance().getRestaurantInfoData().getDiscountList().get(i).getDiscountAmount()))));

                            }

                        } else {
                            txtViewTotal.setText("£" + String.format("%.2f", totalSubAmout));
                            txtViewDiscount.setText("£ 0.00");
                        }
                        break;
                    }
                }

            } else {
                txtViewTotal.setText("£" + String.format("%.2f", totalSubAmout));
                txtViewDiscount.setText("£ 0.00");
            }

        } catch (Exception e) {
            txtViewTotal.setText("£" + String.format("%.2f", totalSubAmout));
            txtViewDiscount.setText("£ 0.00");
        }

        /** End total amount */
    }

}
