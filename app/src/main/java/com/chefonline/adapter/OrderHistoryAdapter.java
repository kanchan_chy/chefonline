package com.chefonline.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chefonline.datamodel.OrderHistoryData;
import com.chefonline.online.R;
import com.chefonline.utility.UnsafeOkHttpClient;
import com.custom.dateform.DateFormatter;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;

public class OrderHistoryAdapter extends BaseAdapter {

	private ArrayList<OrderHistoryData> orderHistoryList;
	private Context context;
	private LayoutInflater mInflater;
	private Picasso.Builder builder;
	private Picasso picasso;

	public OrderHistoryAdapter(Context context, ArrayList<OrderHistoryData> restaurantList) {
		this.orderHistoryList = restaurantList;
		this.context = context;

		try {
			builder = new Picasso.Builder(context);
			OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient(context);
			builder.downloader(new OkHttp3Downloader(okHttpClient));
			picasso = builder.build();
		} catch (Exception e) {

		}
	}

	@Override
	public int getCount() {
		return this.orderHistoryList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.orderHistoryList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolder holder;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
            holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.row_order_history, null);

			holder.txtViewRestaurantName = (TextView) convertView.findViewById(R.id.txtViewRestaurantName);
			holder.imgViewLogo = (ImageView) convertView.findViewById(R.id.imgViewLogo);
			holder.txtViewConfirmation = (TextView) convertView.findViewById(R.id.txtViewConfirmation);
			holder.txtViewGrandTotal = (TextView) convertView.findViewById(R.id.txtViewGrandTotal);
			holder.txtViewDate = (TextView) convertView.findViewById(R.id.txtViewDate);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.txtViewRestaurantName.setText(orderHistoryList.get(position).getRestaurantName());
		holder.txtViewGrandTotal.setText(context.getResources().getString(R.string.pound_sign) + orderHistoryList.get(position).getGrandTotal());

		DateFormatter dateFormatter = new DateFormatter();
		try {
			holder.txtViewDate.setText(dateFormatter.doFormat(orderHistoryList.get(position).getOrderDate(), DateFormatter.DD_MM_YYYY2, DateFormatter.MMM_dd_YYYY));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		holder.txtViewConfirmation.setText(orderHistoryList.get(position).getPaymentStatus());

		try {
			picasso.load(orderHistoryList.get(position).getLogo()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).noFade().fit().into(holder.imgViewLogo);
		} catch (Exception e) {
			Log.e("Image Loader", "" + e);
		}

		return convertView;
	}

	static class ViewHolder {
		TextView txtViewRestaurantName;
        TextView txtViewConfirmation;
        TextView txtViewGrandTotal;
        TextView txtViewDate;
        ImageView imgViewLogo;

	}

}
