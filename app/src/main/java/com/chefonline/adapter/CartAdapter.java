package com.chefonline.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.sax.StartElementListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.chefonline.datamodel.CartData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.online.R;
import com.chefonline.utility.UtilityMethod;

import org.json.JSONObject;

import java.util.ArrayList;

public class CartAdapter extends BaseAdapter {

	private ArrayList<CartData> cartDatas;
	public Activity context = null;
	private Fragment fragmentContext = null;
	private LayoutInflater mInflater = null;
    private DataBaseUtil db;
    ArrayList<CartData> cartDataForTotal = new ArrayList<>();
    private long mLastClickTime = 0;

	public CartAdapter(Activity context, Fragment fragmentContext, ArrayList<CartData> cartDataArrayList) {
		this.cartDatas = cartDataArrayList;
		this.context = context;
        this.fragmentContext = fragmentContext;
        this.db = new DataBaseUtil(context);
        this.db.open();
	}

	@Override
	public int getCount() {
		return this.cartDatas.size();
	}

	@Override
	public Object getItem(int position) {
		return this.cartDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		final ViewHolder holder;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
            holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.row_cart, null);
			holder.txtViewItemName = (TextView) convertView.findViewById(R.id.txtViewItemName);
            holder.txtViewPizzaItem = (TextView) convertView.findViewById(R.id.txtViewPizzaItem);
			holder.txtViewPrice = (TextView) convertView.findViewById(R.id.txtViewPrice);
			holder.txtViewQty = (TextView) convertView.findViewById(R.id.txtViewQty);
			holder.imgBtnClose = (ImageButton) convertView.findViewById(R.id.imgBtnClose);
			holder.btnAddCart = (Button) convertView.findViewById(R.id.btnAddCart);
			holder.btnRemoveCart = (Button) convertView.findViewById(R.id.btnRemoveCart);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

        holder.txtViewItemName.setText(cartDatas.get(position).getItemName());

        if(cartDatas.get(position).getItemPizzaNames() == null || "".equalsIgnoreCase(cartDatas.get(position).getItemPizzaNames())) {
            holder.txtViewPizzaItem.setVisibility(View.GONE);
        } else {
            String tempNames = cartDatas.get(position).getItemPizzaNames();
            String allNames = "";
            String name = "";
            for (int j = 0; j < tempNames.length(); j++) {
                if(tempNames.charAt(j) != ',') {
                    name += tempNames.charAt(j);
                }
                if(j == (tempNames.length() - 1) || tempNames.charAt(j) == ',') {
                    if("".equalsIgnoreCase(allNames)) {
                        allNames += name;
                    } else {
                        allNames += ", " + name;
                    }
                    name = "";
                }
            }
            holder.txtViewPizzaItem.setText(allNames);
            holder.txtViewPizzaItem.setVisibility(View.VISIBLE);
        }

		holder.txtViewQty.setText("" + cartDatas.get(position).getItemQty());

        double realPrice = (cartDatas.get(position).getPrice() * cartDatas.get(position).getItemQty());

		holder.txtViewPrice.setText("£" + UtilityMethod.priceFormatter(realPrice));

        final float price;
        price = (cartDatas.get(position).getPrice());

        holder.btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (db.sumCartData() >= 999) {
                    openDialog("You can not order more than 999 item.", "OK");
                    return;
                }

                int qty = 0;
                qty = qty + Integer.parseInt(holder.txtViewQty.getText().toString());
                qty += 1;

                cartDatas.get(position).setItemQty(qty);
                holder.txtViewQty.setText("" + qty);
                db.updateCartData(Integer.toString(cartDatas.get(position).getItemId()), cartDatas.get(position).getItemPizzaIds(), qty);

                double realPrice = (price * qty);
                holder.txtViewPrice.setText("£" + UtilityMethod.priceFormatter(realPrice));

                /** Calculate Total Amount */
                calculateTotalAmount();
                /** End total amount */

                for (int i = 0; i < db.fetchCartData().size(); i++) {
                    //Log.i("CART_DATA_OPTION ", "=> " + db.fetchCartData().get(i).getItemName() + " Qty => " + db.fetchCartData().get(i).getItemQty());
                }

            }
        });

        holder.btnRemoveCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(holder.txtViewQty.getText().toString().trim()) > 1) {
                    int qty = 0;
                    qty = qty + Integer.parseInt(holder.txtViewQty.getText().toString());
                    qty -= 1;
                    cartDatas.get(position).setItemQty(qty);
                    /*Log.i("SINGLE PRICE ", "=> " + price);
                    Log.i("QTY ", "=> " + (qty));
                    Log.i("Total ", "=> " + (price*(qty)));*/

                    holder.txtViewQty.setText("" + qty);
                    db.updateCartData(Integer.toString(cartDatas.get(position).getItemId()), cartDatas.get(position).getItemPizzaIds(), (Integer.parseInt(holder.txtViewQty.getText().toString())));

                    double realPrice = (price * qty);
                    holder.txtViewPrice.setText("£" + UtilityMethod.priceFormatter(realPrice));

                    /** Calculate Total Amount */
                    calculateTotalAmount();
                    /** End total amount */

                } else {
                    removeItem(Integer.toString(cartDatas.get(position).getItemId()), cartDatas.get(position).getItemPizzaIds(), position);
                }

            }
        });

        holder.imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeItem(Integer.toString(cartDatas.get(position).getItemId()), cartDatas.get(position).getItemPizzaIds(), position);
            }
        });

		return convertView;
	}

    private void openDialog(String message, String okButton) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_message);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(okButton);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

	static class ViewHolder {
		TextView txtViewItemName;
        TextView txtViewPizzaItem;
        TextView txtViewPrice;
        TextView txtViewQty;
        ImageButton imgBtnClose;
        Button btnRemoveCart;
        Button btnAddCart;
        Button btnDiscount;

	}

    private void removeItem(String itemId, String itemPizzaIds, int position) {
        db.deleteCartData(itemId, itemPizzaIds);
        cartDatas.remove(position);
        calculateTotalAmount();
        CartAdapter.this.notifyDataSetChanged();
    }

    private void calculateTotalAmount() {
        /** Calculate Total Amount */
        Double totalSubAmount = 0.0;
        cartDataForTotal = db.fetchCartData();
        for (int i = 0; i < cartDataForTotal.size(); i++) {
            totalSubAmount += (cartDataForTotal.get(i).getItemQty() * cartDataForTotal.get(i).getPrice());
        }

        TextView txtViewSubTotal = (TextView)  this.context.findViewById(R.id.txtViewSubTotal);
        txtViewSubTotal.setText("£" + UtilityMethod.priceFormatter(totalSubAmount));

        TextView txtViewTotal = (TextView) this.context.findViewById(R.id.txtViewTotal);
        TextView txtViewDiscount = (TextView) this.context.findViewById(R.id.txtViewDiscount);
        txtViewTotal.setText("£" + UtilityMethod.priceFormatter(totalSubAmount));

    }

}
