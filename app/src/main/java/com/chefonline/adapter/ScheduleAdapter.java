package com.chefonline.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.chefonline.datamodel.ScheduleData;
import com.chefonline.online.R;

import java.util.ArrayList;

public class ScheduleAdapter extends BaseAdapter {

	private ArrayList<ScheduleData> scheduleDatas;
	private Context context;
	private LayoutInflater mInflater;

	public ScheduleAdapter(Context context, ArrayList<ScheduleData> scheduleDatas1) {
		this.scheduleDatas = scheduleDatas1;
		this.context = context;
	}

	@Override
	public int getCount() {
		return this.scheduleDatas.size();
	}

	@Override
	public Object getItem(int position) {
		return this.scheduleDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolder holder;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
            holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.row_time_schedule, null);

			holder.txtViewDayName = (TextView) convertView.findViewById(R.id.txtViewDayName);
			holder.txtViewTime = (TextView) convertView.findViewById(R.id.txtViewTime);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.txtViewDayName.setText(scheduleDatas.get(position).getDayName());

		if(scheduleDatas.get(position).getOpenEndTimeDatas() != null){
			if(scheduleDatas.get(position).getOpenEndTimeDatas().size() > 0){
				String data = "";
				data = scheduleDatas.get(position).getOpenEndTimeDatas().get(0).getOpeningTime() + " - " + scheduleDatas.get(position).getOpenEndTimeDatas().get(0).getEndTime();

				if (scheduleDatas.get(position).getOpenEndTimeDatas().size() > 1) {
					data += "\n" + scheduleDatas.get(position).getOpenEndTimeDatas().get(1).getOpeningTime() + " - " + scheduleDatas.get(position).getOpenEndTimeDatas().get(1).getEndTime();
				}

				holder.txtViewTime.setText("" + data);
			}
			else {
				holder.txtViewTime.setText("CLOSED");
			}
		}
        else {
			holder.txtViewTime.setText("CLOSED");
		}
		return convertView;
	}

	static class ViewHolder {
		TextView txtViewDayName;
        TextView txtViewTime;
	}

}
