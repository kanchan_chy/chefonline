package com.chefonline.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.datamodel.CollectionDeliveryData;
import com.chefonline.datamodel.HotRestaurantData;
import com.chefonline.datamodel.OffersData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.fragment.TakeawayCategoryFragment;
import com.chefonline.online.MainActivity;
import com.chefonline.online.R;
import com.chefonline.online.ReservationActivity;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.TimeManagement;
import com.chefonline.utility.UnsafeOkHttpClient;
import com.chefonline.utility.UtilityMethod;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Cache;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.UrlConnectionDownloader;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Call;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


public class HotRestaurantsAdapter extends BaseAdapter {
    private static String TAG = "HotRestaurantsAdapter";
    private int orderPolicy;
    private int listType;
    private ArrayList<HotRestaurantData> restaurantList;
    private ArrayList<HotRestaurantData> restaurantListTemp = new ArrayList<>();
    private Context context;
    private LayoutInflater mInflater;
    private LocationManager manager;
    private long mLastClickTime = 0;
    private DataBaseUtil db;
    private Picasso.Builder builder;
    private Picasso picasso;

    public HotRestaurantsAdapter(Context context, ArrayList<HotRestaurantData> restaurantList, int orderPolicy, int listType) {
        this.restaurantList = restaurantList;
        restaurantListTemp.addAll(restaurantList);
        this.orderPolicy = orderPolicy;
        this.listType = listType;
        this.context = context;
        this.manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        db = new DataBaseUtil(context);

        try {
            builder = new Picasso.Builder(context);
            OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient(context);
            builder.downloader(new OkHttp3Downloader(okHttpClient));
            picasso = builder.build();
        } catch (Exception e) {

        }

    }

    @Override
    public int getCount() {
        return this.restaurantList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.restaurantList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup arg2) {
        final ViewHolder holder;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.row_features_restaurant, null);

            holder.txtViewRestaurantName = (TextView) convertView.findViewById(R.id.txtViewRestaurantName);
            holder.relativeRestaurantViewHolder = (RelativeLayout) convertView.findViewById(R.id.relativeRestaurantViewHolder);
            holder.imgViewLogo = (ImageView) convertView.findViewById(R.id.imgViewLogo);
            holder.imgViewOrder = (ImageView) convertView.findViewById(R.id.imgViewCollection);
            holder.imgViewDelivery = (ImageView) convertView.findViewById(R.id.imgViewDelivery);
            holder.imgViewCollection = (ImageView) convertView.findViewById(R.id.imgViewReservation);
            holder.txtViewCuisineName = (TextView) convertView.findViewById(R.id.txtViewCuisineName);
            holder.txtViewDistance = (TextView) convertView.findViewById(R.id.txtViewDistance);
            holder.txtViewDiscount = (TextView) convertView.findViewById(R.id.txtViewDiscount);
          //  holder.textViewClosed = (TextView) convertView.findViewById(R.id.textViewClosed);
            holder.linearClosed = (LinearLayout) convertView.findViewById(R.id.linearClosed);
            holder.linear_service_holder = (LinearLayout) convertView.findViewById(R.id.linear_service_holder);
            holder.relative_holder_offers = (RelativeLayout) convertView.findViewById(R.id.relative_holder_offers);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtViewRestaurantName.setText(restaurantList.get(position).getRestaurantName());

        if("1".equalsIgnoreCase(restaurantList.get(position).getComingSoon())) {
            holder.linearClosed.setVisibility(View.VISIBLE);
        }
        else {
            holder.linearClosed.setVisibility(View.GONE);
        }

        String cuisineName = "";
        if (restaurantList.get(position).getCuisineList().size() > 0) {
            for (int i = 0; i < restaurantList.get(position).getCuisineList().size(); i++) {
                cuisineName += ", " + restaurantList.get(position).getCuisineList().get(i).getCuisineName();
            }
            //stringList.add(position, cuisineName);
            restaurantList.get(position).setRestaurantConcatedCuisineName(cuisineName);
            holder.txtViewCuisineName.setText(cuisineName.substring(1).trim());
        }

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            holder.txtViewDistance.setText("");
            //return;
        } else {
            if (restaurantList.get(position).getRestaurantDistance().equalsIgnoreCase("0.0")) {
                holder.txtViewDistance.setText("");

            } else {
                //holder.txtViewDistance.setText("" + String.format("%.01f", restaurantList.get(position).getRestaurantDistance()) + " miles ");
                holder.txtViewDistance.setText("" + restaurantList.get(position).getRestaurantDistance());
            }
        }

        holder.relativeRestaurantViewHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                AppData.getInstance().hideKeyboard(context, v);
                String restId = Integer.toString(restaurantList.get(position).getRestaurantId());
                db.open();
                if (db.sumCartData() == 0) {
                    AppData.getInstance().setRestaurantInfoData(restaurantList.get(position));
                    openNextUI(position);
                    return;
                }

                if (db.checkRestaurant(restId) == 0) {
                    openDialog(Integer.toString(restaurantList.get(position).getRestaurantId()), position);

                } else {
                    openNextUI(position);
                }

                db.close();
            }
        });


        float value = 10 * context.getResources().getDisplayMetrics().density;
        float value1 = 8 * context.getResources().getDisplayMetrics().density;

        if (restaurantList.get(position).getDiscountList() != null && restaurantList.get(position).getOffersDatas() != null) {
            holder.relative_holder_offers.removeAllViews();
            if (restaurantList.get(position).getDiscountList().size() > 0 || restaurantList.get(position).getOffersDatas().size() > 0) {
                TextView txtViewDiscount = new TextView(context);
                txtViewDiscount.setText("OFFER");
                Typeface typeface = Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Medium.ttf");
                txtViewDiscount.setTypeface(typeface);
                txtViewDiscount.setPadding((int) value, (int) value1,(int) value, (int) value1);
                txtViewDiscount.setGravity(Gravity.CENTER);
                txtViewDiscount.setSingleLine();
                txtViewDiscount.setTextColor(context.getResources().getColor(R.color.theme_red));
                txtViewDiscount.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                txtViewDiscount.setMaxLines(1);
                txtViewDiscount.setEllipsize(TextUtils.TruncateAt.END);
                int width = (int) context.getResources().getDimension(R.dimen.fixed_hundred);
                LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                llp.setMargins(10, 0, 0, 0); //llp.setMargins(left, top, right, bottom);
                txtViewDiscount.setLayoutParams(llp);
                holder.relative_holder_offers.addView(txtViewDiscount);
                holder.relative_holder_offers.setBackgroundResource(R.drawable.round_cat_holder_default);

                holder.relative_holder_offers.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                      //  UtilityMethod.openDialog(restaurantList.get(position), "AVAILABLE OFFERS", context);
                        openOfferDialog("AVAILABLE OFFERS", position);
                    }
                });

            }

        } else {

        }


        TimeManagement timeManagement = new TimeManagement();
        if(timeManagement.checkRestaurantOpenStatus(restaurantList.get(position))) {
            String collection = "", delivery = "";
            CollectionDeliveryData collectionDeliveryData = new CollectionDeliveryData();
            collectionDeliveryData = timeManagement.getCollectionDeliveryTIme(restaurantList.get(position));
            restaurantList.get(position).setDeliveryTime(collectionDeliveryData.getDelivery());
            restaurantList.get(position).setCollectionTime(collectionDeliveryData.getCollection());
            collection = collectionDeliveryData.getCollection();
            delivery = collectionDeliveryData.getDelivery();

            holder.linear_service_holder.removeAllViews();
            if (orderPolicy == ConstantValues.NO_POLICY_BUTTON_SELECTION) {
                if (restaurantList.get(position).getRestaurantAcceptReservation() == 1) {
                    ImageView image = new ImageView(context);
                    image.setImageResource(R.drawable.reservation);
                    image.setBackgroundResource(R.drawable.round_gray_shape_default);
                    image.setPadding(5, 5, 5, 5);
                    LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    llp.setMargins(0, 0, 15, 0); // llp.setMargins(left, top, right, bottom);
                    image.setLayoutParams(llp);
                    holder.linear_service_holder.addView(image);
                }

                if (restaurantList.get(position).getRestaurantAcceptCollection() == 1) {
                    if (collection != null && !collection.equalsIgnoreCase("")) {
                        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View v = vi.inflate(R.layout.custom_time_text_label, null);
                        TextView textView = (TextView) v.findViewById(R.id.textView);
                        ImageView image = (ImageView) v.findViewById(R.id.imageView);
                        image.setImageResource(R.drawable.collection);
             /*   for (int i = 0; i < restaurantList.get(position).getPolicyList().size(); i++) {
                    if ("Collection".equalsIgnoreCase(restaurantList.get(position).getPolicyList().get(i).getPolicyName())) {
                        textView.setText(restaurantList.get(position).getPolicyList().get(i).getPolicyTime());
                    }
                }  */
                        textView.setText(collection);
                        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        llp.setMargins(0, 0, 15, 0); // llp.setMargins(left, top, right, bottom);
                        v.setLayoutParams(llp);
                        holder.linear_service_holder.addView(v);

                    }
                }

                if (restaurantList.get(position).getRestaurantAcceptDelivery() == 1) {
                    if (delivery != null && !delivery.equalsIgnoreCase("")) {
                        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View v = vi.inflate(R.layout.custom_time_text_label, null);
                        // fill in any details dynamically here
                        TextView textView = (TextView) v.findViewById(R.id.textView);
                        ImageView image = (ImageView) v.findViewById(R.id.imageView);
                        image.setImageResource(R.drawable.delivery);
                        textView.setText(delivery);
                        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        llp.setMargins(0, 0, 15, 0); // llp.setMargins(left, top, right, bottom);
                        v.setLayoutParams(llp);
                        holder.linear_service_holder.addView(v);
                    }
                }

            } else if (orderPolicy == ConstantValues.RESERVATION) {
                //holder.linear_service_holder.removeAllViews();
                if (restaurantList.get(position).getRestaurantAcceptReservation() == 1) {
                    ImageView image = new ImageView(context);
                    image.setImageResource(R.drawable.reservation);
                    image.setBackgroundResource(R.drawable.round_gray_shape_default);
                    image.setPadding(5, 5, 5, 5);
                    LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    llp.setMargins(0, 0, 15, 0); // llp.setMargins(left, top, right, bottom);
                    image.setLayoutParams(llp);
                    holder.linear_service_holder.addView(image);
                }

            } else if (orderPolicy == ConstantValues.COLLECTION) {
                if (restaurantList.get(position).getRestaurantAcceptCollection() == 1) {
                    if (collection != null && !collection.equalsIgnoreCase("")) {
                        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View v = vi.inflate(R.layout.custom_time_text_label, null);
                        TextView textView = (TextView) v.findViewById(R.id.textView);
                        ImageView image = (ImageView) v.findViewById(R.id.imageView);
                        image.setImageResource(R.drawable.collection);
                        textView.setText(collection);
                        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        llp.setMargins(0, 0, 15, 0); // llp.setMargins(left, top, right, bottom);
                        v.setLayoutParams(llp);
                        holder.linear_service_holder.addView(v);
                    }
                }

            } else if (orderPolicy == ConstantValues.DELIVERY) {
                    if (restaurantList.get(position).getRestaurantAcceptDelivery() == 1) {
                        if (delivery != null && !delivery.equalsIgnoreCase("")) {
                            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View v = vi.inflate(R.layout.custom_time_text_label, null);

                            // fill in any details dynamically here
                            TextView textView = (TextView) v.findViewById(R.id.textView);
                            ImageView image = (ImageView) v.findViewById(R.id.imageView);
                            image.setImageResource(R.drawable.delivery);
                            textView.setText(delivery); // new added
                            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            llp.setMargins(0, 0, 15, 0); // llp.setMargins(left, top, right, bottom);
                            v.setLayoutParams(llp);
                            holder.linear_service_holder.addView(v);
                        }
                }
            }
        } else {
            holder.linear_service_holder.removeAllViews();
            TextView txtViewClosed = new TextView(context);
            txtViewClosed.setText("CLOSED");
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
            txtViewClosed.setTypeface(typeface);
            txtViewClosed.setBackgroundResource(R.drawable.round_gray_button_selected);
            txtViewClosed.setPadding(8, 2, 8, 2);
            txtViewClosed.setGravity(Gravity.CENTER);
            txtViewClosed.setSingleLine();
            txtViewClosed.setTextColor(context.getResources().getColor(R.color.white));
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            llp.setMargins(0, 0, 15, 0); // llp.setMargins(left, top, right, bottom);
            txtViewClosed.setLayoutParams(llp);
            holder.linear_service_holder.addView(txtViewClosed);
        }

        try {
            picasso.load(restaurantList.get(position).getRestaurantLogo().trim()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).noFade().fit().into(holder.imgViewLogo);
        } catch (Exception e) {
            Log.e("Image Loader", "" + e);
        }

        return convertView;
    }

/*
    private boolean checkRestaurantOpenStatus(int pos)
    {
        String currentDay = new SimpleDateFormat("EEEE").format(new Date());
        int shiftCount = restaurantList.get(pos).getScheduleDatas().size();
        boolean todayFound = false;
        for (int i = 0; i < shiftCount; i++) {
            if (restaurantList.get(pos).getScheduleDatas().get(i).getDayName().equalsIgnoreCase(currentDay)) {
                todayFound = true;
                if(restaurantList.get(pos).getScheduleDatas().get(i).getOpenEndTimeDatas() == null || restaurantList.get(pos).getScheduleDatas().get(i).getOpenEndTimeDatas().size() == 0) {
                    return false;
                }
            }
        }
        if(todayFound == false) {
            return false;
        }
        return true;
    }  */


    public void filter(String charText) {
        try {
            this.notifyDataSetChanged();
            charText = charText.replaceAll("[,]", "").toLowerCase().trim();
            Log.i("Text", "getting.. ** " + charText);
            this.restaurantList.clear();
            if (charText.length() == 0) {
                this.restaurantList.addAll(restaurantListTemp);
                //this.notifyDataSetChanged();

            } else {
                for (int i = 0; i < restaurantListTemp.size(); i++) {
                    //Log.i("Text", "Print... ** " + dishDatasTemp.get(i).getDishName());
                    if (restaurantListTemp.get(i).getRestaurantName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        Log.i("Text", "Match.. ** " + restaurantListTemp.get(i).getRestaurantName());
                        restaurantList.add(restaurantListTemp.get(i));
                    }
                }

                doCounterRestaurantActions();
                this.notifyDataSetChanged();

            }

        } catch (Exception e) {
            Log.e("Error adapter", "" + e.getMessage());
            this.notifyDataSetChanged();
            doCounterRestaurantActions();
        } finally {
            this.notifyDataSetChanged();
            doCounterRestaurantActions();
        }

    }

    public void filterCuisine(String charText) {
        charText = charText.replaceAll("[,]", "").toLowerCase().trim();
        try {
            this.notifyDataSetChanged();

            Log.i("Text", "getting.. ** " + charText);
            this.restaurantList.clear();

                if (charText == null) {

                } else {
                    charText = charText.toLowerCase();
                    if (charText.length() == 0) {
                        this.restaurantList.addAll(restaurantListTemp);
                    } else {
                        for (int i = 0; i < restaurantListTemp.size(); i++) {
                            if (restaurantListTemp.get(i).getRestaurantConcatedCuisineName().toLowerCase().trim().contains(charText.toString())) {
                                Log.i("Text", "Match.. ** " + restaurantListTemp.get(i).getRestaurantConcatedCuisineName());
                                restaurantList.add(restaurantListTemp.get(i));
                                //doCounterRestaurantActions();
                            }
                        }

                    }
                }

                //this.notifyDataSetChanged();

            Log.i("Notify", "Test....");

        } catch (Exception e) {
            Log.e("Error adapter", "" + e.getMessage());
            this.notifyDataSetChanged();
            doCounterRestaurantActions();

        } finally {
            this.notifyDataSetChanged();
            doCounterRestaurantActions();

        }

    }

    OnDataChangeListener mOnDataChangeListener;

    private void doCounterRestaurantActions() {
        if(mOnDataChangeListener != null){
            mOnDataChangeListener.onDataChanged(restaurantList.size());
        }
    }

    public void setmOnDataChangeListener(OnDataChangeListener onDataChangeListener) {
        this.mOnDataChangeListener = onDataChangeListener;
    }

    public interface OnDataChangeListener{
        void onDataChanged(int size);
    }

    public void openNextUI(int position) {
        //AppData.getInstance().setRestaurantInfoData(hotRestaurantDataArrayList.get(position));
        if (ConstantValues.SELECTED_BUTTON == ConstantValues.RESERVATION) {
            //emptyCart();
            context.startActivity(new Intent(context, ReservationActivity.class));
        } else {
            loadCategoryFragment(position, Integer.toString(restaurantList.get(position).getRestaurantId()));
        }
    }

    private void loadCategoryFragment(int position, String rest_id) {
       // String collectionTime = restaurantList.get(position).getCollectionTime();
       // String deliveryTime = restaurantList.get(position).getDeliveryTime();
        TimeManagement timeManagement = new TimeManagement();
        CollectionDeliveryData collectionDeliveryData = new CollectionDeliveryData();
        collectionDeliveryData = timeManagement.getCollectionDeliveryTIme(restaurantList.get(position));
        String collectionTime = collectionDeliveryData.getCollection();
        String deliveryTime = collectionDeliveryData.getDelivery();
        if(collectionTime == null) collectionTime = "";
        if(deliveryTime == null) deliveryTime = "";
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putInt("is_searched_list", listType);
        args.putInt("selected_policy", ConstantValues.SELECTED_BUTTON);
        args.putString("delivery_time", deliveryTime);
        args.putString("collection_time", collectionTime);
        args.putString("restaurant_id", rest_id);
        loadFragmentManager(args);

    }

    private void loadFragmentManager(Bundle args) {
        android.app.Fragment fragment = null;
        fragment = new TakeawayCategoryFragment();
        fragment.setArguments(args);
        FragmentManager frgManager = ((Activity) context).getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.addToBackStack(null);
        //ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.add(R.id.content_frame, fragment);
        ft.commit();

    }

    private void openDialog(final String restaurant_id, final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_restuarant_override);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConstantValues.SELECTED_BUTTON == ConstantValues.RESERVATION) {
                    context.startActivity(new Intent(context, ReservationActivity.class));
                } else {
                    AppData.getInstance().setRestaurantInfoData(restaurantList.get(position));
                    loadCategoryFragment(position, restaurant_id);
                }

                emptyCart();
                dialog.dismiss();
            }

        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ImageButton btnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    private void openOfferDialog(String dishName, final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_offers);

        TextView text = (TextView) dialog.findViewById(R.id.txtViewOptionName);
        text.setText(dishName);

        ListView listView = (ListView) dialog.findViewById(R.id.listViewOptions);

        ArrayList<OffersData> optionDatas = new ArrayList<>();

        for (int i = 0; i < restaurantList.get(position).getDiscountList().size() ; i++) {
            OffersData offersData = new OffersData();
            offersData.setOfferTitle(restaurantList.get(position).getDiscountList().get(i).getDiscountTitle());
            offersData.setOfferElegibleAmount(restaurantList.get(position).getDiscountList().get(i).getDiscountEligibleAmount());
            offersData.setOfferImage(restaurantList.get(position).getDiscountList().get(i).getDiscountImage());
            optionDatas.add(offersData);
        }

        optionDatas.addAll(restaurantList.get(position).getOffersDatas());

        OffersDialogAdapter adapter = new OffersDialogAdapter(context, optionDatas, "", dishName);
        listView.setAdapter(adapter);

        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        Button btnContinue = (Button) dialog.findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

                dialog.dismiss();
                String restId = Integer.toString(restaurantList.get(position).getRestaurantId());
                db.open();
                if (db.sumCartData() == 0) {
                    AppData.getInstance().setRestaurantInfoData(restaurantList.get(position));
                    openNextUI(position);
                    return;
                }

                if (db.checkRestaurant(restId) == 0) {
                    openDialog(Integer.toString(restaurantList.get(position).getRestaurantId()), position);

                } else {
                    openNextUI(position);
                }

                db.close();

            }
        });

        dialog.show();
    }


    private void emptyCart() {
        db.open();
        db.emptyCartData();
        MainActivity.txtViewBubble.setText("" + db.sumCartData());
        db.close();
    }

    static class ViewHolder {
        TextView txtViewRestaurantName;
        TextView txtViewCuisineName;
        TextView txtViewDistance;
        ImageView imgViewOrder;
        ImageView imgViewDelivery;
        ImageView imgViewCollection;
        ImageView imgViewLogo;
        TextView txtViewDiscount;
        //TextView textViewClosed;
        LinearLayout linearClosed;
        LinearLayout linear_service_holder;
        RelativeLayout relative_holder_offers;
        RelativeLayout relativeRestaurantViewHolder;

    }

}
