package com.chefonline.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.chefonline.online.R;

import java.util.ArrayList;
import java.util.List;

public class TitlesDialogAdapter extends BaseAdapter {
    private String[] titles;
	private Activity context;
	private LayoutInflater mInflater;


	public TitlesDialogAdapter(Activity context, String[] titles) {
		this.context = context;
        this.titles=titles;
	}

	@Override
	public int getCount() {
		return this.titles.length;
	}

	@Override
	public Object getItem(int position) {
		return this.titles[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		final ViewHolder holder;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
            holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.row_post_code_list, null);
			holder.txtPostCode = (TextView) convertView.findViewById(R.id.txtPostCode);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.txtPostCode.setText(titles[position]);

		return convertView;
	}

	//Static dssg
	static class ViewHolder {
		TextView txtPostCode;
	}




}
