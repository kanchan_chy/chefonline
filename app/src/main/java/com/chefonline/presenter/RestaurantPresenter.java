package com.chefonline.presenter;

import android.content.Context;
import android.util.Log;

import com.chefonline.datamodel.HotRestaurantData;
import com.chefonline.gps.GPSTracker;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.modelinterface.RequestCompleteInterface;
import com.chefonline.modelinterface.RestaurantView;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by masum on 29/11/2015.
 */
public class RestaurantPresenter {
    RestaurantView restaurantView;
    public RestaurantPresenter(RestaurantView restaurantView) {
        this.restaurantView = restaurantView;
    }

    public void getRestaurantList(final Context context, String f, String keyword, String orderType) {
        new AsyncTaskRestaurants(context, f, keyword, orderType, new RequestCompleteInterface() {
            @Override
            public void onCompleted(String result) {
                Log.e("Search_response", result);
                restaurantView.setLoadingHide();
                GPSTracker gps = new GPSTracker(context);
                LatLng latLngCurrent = new LatLng(gps.getLatitude(), gps.getLongitude());
                try {
                    ArrayList<HotRestaurantData> hotRestaurantDataArrayList =  JsonParser.getHotRestaurant(result, latLngCurrent);
                    if(hotRestaurantDataArrayList.size() > 0) {
                        restaurantView.setListView(hotRestaurantDataArrayList);
                    } else {
                        restaurantView.setEmptyMessage("Sorry, No restaurant found");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    try{
                        restaurantView.setEmptyMessage("Sorry, No restaurant found");
                    }
                    catch (Exception e1){

                    }
                }
            }

            @Override
            public void onErrorCompleted(String result) {
                try {
                    restaurantView.setEmptyMessage(result);
                    restaurantView.setLoadingHide();
                }
                catch (Exception e){

                }
            }

        });
    }


}
