package com.chefonline.presenter;

import android.content.Context;
import android.util.Log;

import com.android.volley.VolleyError;
import com.chefonline.modelinterface.NetPayView;
import com.chefonline.modelinterface.VolleyApiInterface;
import com.chefonline.volleyapicalls.PlaceOrderNewApiCallback;

import org.json.JSONObject;

/**
 * Created by masum on 29/11/2015.
 */
public class NetPayPresenter {
    public static String TAG = "NetPayPresenter";
    NetPayView netPayView;

    public NetPayPresenter(NetPayView netPayView) {
        this.netPayView = netPayView;
    }

    public void onClickPayNetpay(final Context context, final String paymentOption) {
        netPayView.setLoadingVisible();
        new PlaceOrderNewApiCallback(context, paymentOption, new VolleyApiInterface() {

            @Override
            public void onRequestSuccess(String response) {
                try {
                    Log.e("placeorder_new", response);
                    netPayView.setLoadingHide();
                    JSONObject jsonObject = new JSONObject(response);
                    if("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        netPayView.setOrderId(jsonObject.getString("order_ID"));
                        netPayView.setTransactionId(jsonObject.getString("transaction_id"));
                        netPayView.orderCompleted(true);
                    } else {
                        if(jsonObject.getBoolean("neyPayStatus")) {
                            netPayView.showToast("Sorry, payment was not completed successfully. Please order again.", "", false);
                        } else {
                            netPayView.showToast("Sorry, order can't be completed", "", false);
                        }
                    }
                } catch (Exception e) {
                    try {
                        netPayView.setLoadingHide();
                        netPayView.showToast("Something went wrong", "", false);
                    }catch (Exception e1) {
                    }
                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    netPayView.setLoadingHide();
                    netPayView.showToast("Something went wrong", "", false);
                }catch (Exception e) {
                }
            }

        });

    }



}
