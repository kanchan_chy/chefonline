package com.chefonline.presenter;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.modelinterface.RequestCompleteInterface;
import com.chefonline.utility.ConstantValues;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by masum on 29/11/2015.
 */
public class AsyncTaskRestaurants {
    RequestCompleteInterface requestCompleteInterface;

    AsyncTaskRestaurants(Context context, final String f, final String keyword, final String orderType, RequestCompleteInterface requestCompleteInterface) {
        this.requestCompleteInterface = requestCompleteInterface;

        RequestQueue queue = Volley.newRequestQueue(context);
        String url = ConstantValues.BASE_API_URL;

        StringRequest myReq = new StringRequest(Request.Method.POST, url, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", f);
                params.put("searchText", keyword);
                params.put("ordertype", orderType);
                return params;
            }
        };

        int socketTimeout = 60000;  //60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                requestCompleteInterface.onCompleted(response);
            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                requestCompleteInterface.onErrorCompleted("Sorry, No restaurant found");
            }
        };
    }

}
