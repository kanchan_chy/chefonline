package com.chefonline.online;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.adapter.PostCodesDialogAdapter;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.customview.MsmEditText;
import com.chefonline.fragment.CheckoutFragment;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UtilityMethod;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddAddressActivity extends AppCompatActivity implements View.OnClickListener {
    private static String TAG = "AddAddressActivity";
    private MsmEditText editTextPostCode;
    private MsmEditText editTextAddress1;
    private MsmEditText editTextAddress2;
    private MsmEditText editTextTown;
    private Button btnSave, btnAvailablePostcodes;
    Toolbar toolBar;
    PreferenceUtil preferenceUtil;
    private long mLastClickTime = 0;

    List<String> postCodeList = new ArrayList<>();

    String userExtId = "", extPostcode = "", extAddr1 = "", extAddr2 = "", extTown = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        initView();
        setUiListener();
        loadData();
    }

    private void setUiListener() {
        btnSave.setOnClickListener(this);
        btnAvailablePostcodes.setOnClickListener(this);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void initView() {

        toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        preferenceUtil = new PreferenceUtil(this);

        editTextPostCode = (MsmEditText) findViewById(R.id.editTextPostCode);
        editTextAddress1 = (MsmEditText) findViewById(R.id.editTextAddress1);
        editTextAddress2 = (MsmEditText) findViewById(R.id.editTextAddress2);
        editTextTown = (MsmEditText) findViewById(R.id.editTextTown);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnAvailablePostcodes = (Button) findViewById(R.id.btnAvailablePostcodes);

    }


    private void loadData() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        userExtId = getIntent().getExtras().getString("ext_id");

        if(!userExtId.equalsIgnoreCase("")) {
            getSupportActionBar().setTitle("Update Address");
            extPostcode = getIntent().getExtras().getString("ext_postcode");
            extAddr1 = getIntent().getExtras().getString("ext_addr1");
            extAddr2 = getIntent().getExtras().getString("ext_addr2");
            extTown = getIntent().getExtras().getString("ext_town");
            btnSave.setText("Update Address");
           // getAddressDetails();
            editTextPostCode.setText(extPostcode);
            editTextAddress1.setText(extAddr1);
            editTextAddress2.setText(extAddr2);
            editTextTown.setText(extTown);
        } else {
            getSupportActionBar().setTitle("Add New Address");
        }

        callGetPostCodeApi();
    }

    ProgressDialog progress;
    private void callAddAddress() {
        RequestQueue queue = Volley.newRequestQueue(AddAddressActivity.this);
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "60");
                params.put("user_id", preferenceUtil.getUserID());
                params.put("address1", editTextAddress1.getText().toString().trim());
                params.put("address2", editTextAddress2.getText().toString().trim());
                params.put("town", editTextTown.getText().toString().trim());
                params.put("postcode", editTextPostCode.getText().toString().trim());
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);
        progress = new ProgressDialog(AddAddressActivity.this);
        progress.setMessage("Please wait....");
        progress.show();
    }

    private void callUpdateAddress() {
        RequestQueue queue = Volley.newRequestQueue(AddAddressActivity.this);
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "61");
                params.put("user_address_ext_id", userExtId);
                params.put("user_id", preferenceUtil.getUserID());
                params.put("address1", editTextAddress1.getText().toString().trim());
                params.put("address2", editTextAddress2.getText().toString().trim());
                params.put("town", editTextTown.getText().toString().trim());
                params.put("postcode", editTextPostCode.getText().toString().trim());
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);
        progress = new ProgressDialog(AddAddressActivity.this);
        progress.setMessage("Please wait....");
        progress.show();
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                    String x = error.getMessage();
                    new CustomToast(AddAddressActivity.this, "Something went wrong, please try again.", "", false);
                }
                catch (Exception e) {
                    Log.e("After volley execution",e.getMessage());
                }
            }
        };
    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progress.dismiss();
                    Log.i(TAG, "****" + response);

                    JSONObject jsonObject = new JSONObject(response);
                    if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        new CustomToast(AddAddressActivity.this, jsonObject.getString("message"), "", true);
                        CheckoutFragment.defaultPostCode = editTextPostCode.getText().toString().trim();
                        CheckoutFragment.defaultAddr1 = editTextAddress1.getText().toString().trim();
                        CheckoutFragment.defaultAddr2 = editTextAddress2.getText().toString().trim();
                        CheckoutFragment.defaultTown = editTextTown.getText().toString().trim();
                        finish();
                    } else {
                        new CustomToast(AddAddressActivity.this, "Submission failed", "", false);
                    }

                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                        new CustomToast(AddAddressActivity.this, "Something went wrong, please try again.", "", false);
                    }
                    catch (Exception e1){
                        Log.e("After volley execution",e1.getMessage());
                    }
                }

            }

        };
    }


    private void getAddressDetails() {
        RequestQueue queue = Volley.newRequestQueue(AddAddressActivity.this);
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createAddressDetailsSuccessListener(), createAddressDetailsErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "62");
                params.put("user_id", preferenceUtil.getUserID());
                params.put("user_address_ext_id", userExtId);
                params.put("rest_id", String.valueOf(AppData.getInstance().getRestaurantInfoData().getRestaurantId()));
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);
        progress = new ProgressDialog(AddAddressActivity.this);
        progress.setMessage("Please wait....");
        progress.show();
    }


    private Response.ErrorListener createAddressDetailsErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                    String x = error.getMessage();
                    new CustomToast(AddAddressActivity.this, "Something went wrong.", "", false);
                }
                catch (Exception e) {
                    Log.e("After volley execution",e.getMessage());
                }
            }
        };
    }

    private Response.Listener<String> createAddressDetailsSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i(TAG, "****" + response);
                    progress.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        JSONArray jAddresses = jsonObject.getJSONArray("addersses");
                        if(jAddresses.length() > 0) {
                            JSONObject jAdrObject = jAddresses.getJSONObject(0);
                            editTextPostCode.setText(jAdrObject.getString("user_address_ext_postcode"));
                            editTextAddress1.setText(jAdrObject.getString("user_address_ext_line1"));
                            editTextAddress2.setText(jAdrObject.getString("user_address_ext_line2"));
                            editTextTown.setText(jAdrObject.getString("user_address_ext_town"));
                        } else {
                            new CustomToast(AddAddressActivity.this, "Address details not found.", "", false);
                        }
                    } else {
                        new CustomToast(AddAddressActivity.this, "Something went wrong.", "", false);
                    }

                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                        new CustomToast(AddAddressActivity.this, "Something went wrong.", "", false);
                    }
                    catch (Exception e1){
                        Log.e("After volley execution",e1.getMessage());
                    }
                }

            }

        };
    }


    /** Call Get Postcode api */
    //ProgressDialog progress;
    private void callGetPostCodeApi() {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createGetPostcodeSuccessListener(), createGetPostcodeErrorListener()) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "25");
                params.put("rest_id", String.valueOf(AppData.getInstance().getRestaurantInfoData().getRestaurantId()));
                return params;
            }
        };

        int socketTimeout = 30000;  //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

        progress = new ProgressDialog(this);
        progress.setMessage("Please wait....");
        progress.show();
    }


    private Response.Listener<String> createGetPostcodeSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progress.dismiss();
                    Log.e("post_code_response", response);
                    postCodeList = new ArrayList<>();
                    postCodeList = JsonParser.parseAllPostCodes(response);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "Postcode api parsing *** " + e.getMessage());
                }

            }

        };
    }

    private Response.ErrorListener createGetPostcodeErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                    postCodeList = new ArrayList<>();
                }
                catch (Exception e) {
                    Log.e(TAG, "After volley request *** " + e.getMessage());
                }
            }
        };
    }


    private boolean inputValidation() {
        if (editTextPostCode.getText().toString().trim().equalsIgnoreCase("") || !UtilityMethod.postcodeValidation(editTextPostCode.getText().toString().trim().toUpperCase())) {
            editTextPostCode.setErrorTextVisible(true);
            editTextPostCode.setErrorMsg("Invalid post code.");
            editTextPostCode.setTypingFocus();
            return false;
        }

        /*if (UtilityMethod.isWhiteSpaces(editTextPrevPassword.getText().toString())) {
            editTextPrevPassword.setErrorTextVisible(true);
            editTextPrevPassword.setErrorMsg("White Space is not accepted.");
            editTextPrevPassword.setTypingFocus();
            return false;
        }*/

        if (editTextAddress1.getText().toString().trim().equalsIgnoreCase("")) {
            editTextAddress1.setErrorTextVisible(true);
            editTextAddress1.setErrorMsg("Address1 required.");
            editTextAddress1.setTypingFocus();
            return false;
        }

        if (editTextTown.getText().toString().trim().equalsIgnoreCase("")) {
            editTextTown.setErrorTextVisible(true);
            editTextTown.setErrorMsg("Town required.");
            editTextTown.setTypingFocus();
            return false;
        }

        return true;
    }


    private void openWarning(String message, String okButton) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_message);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(okButton);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    private void showPostcodeListDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_post_code_list);
        dialog.setCancelable(true);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        final ImageButton imgBtnSearch = (ImageButton) dialog.findViewById(R.id.imgBtnSearch);
        final EditText edtSearchDialog = (EditText) dialog.findViewById(R.id.edtSearchDialog);
        ListView listViewPostcodes = (ListView)dialog.findViewById(R.id.listViewOptions);

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        imgBtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtSearchDialog.setText("");
            }
        });


        final PostCodesDialogAdapter adapter = new PostCodesDialogAdapter(this, postCodeList);
        listViewPostcodes.setAdapter(adapter);

        listViewPostcodes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String tempPostcode = postCodeList.get(position);
                editTextPostCode.setText(tempPostcode);
                dialog.dismiss();
            }
        });

        edtSearchDialog.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 0) {
                    imgBtnSearch.setVisibility(View.VISIBLE);
                } else {
                    imgBtnSearch.setVisibility(View.GONE);
                }
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialog.show();
    }



    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        switch (v.getId()){
            case R.id.btnSave:
                if (inputValidation()) {
                    if(!userExtId.equalsIgnoreCase("")) {
                        callUpdateAddress();
                    } else {
                        callAddAddress();
                    }
                }

                break;
            case R.id.btnAvailablePostcodes:
                if(postCodeList == null || postCodeList.size() < 1) {
                    openWarning("No postcode is available", "OK");
                } else {
                    showPostcodeListDialog();
                }

        }
    }
}
