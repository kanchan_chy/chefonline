package com.chefonline.online;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chefonline.adapter.NetpayPagerAdapter;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.customview.SlidingTabLayout;
import com.chefonline.datamodel.NetpayData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.fragment.NetPayBillingAddressFragment;
import com.chefonline.fragment.NetPayConfirmFragment;
import com.chefonline.fragment.NetPayPaymentFragment;
import com.chefonline.fragment.PaymentFragment;
import com.chefonline.modelinterface.NetPayView;
import com.chefonline.pattern.AppData;
import com.chefonline.presenter.NetPayPresenter;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UtilityMethod;

import java.util.ArrayList;

/**
 * Created by user on 9/3/2016.
 */
public class NetPayActivity extends AppCompatActivity implements View.OnClickListener, NetPayView{

    Toolbar toolBar;
    SlidingTabLayout tabs;
    ViewPager pager;
    Button btnNext;
    TextView txtTotal;
    ProgressDialog progressDialog;
    NetpayPagerAdapter netpayPagerAdapter;
    NetPayPresenter netPayPresenter;
    PreferenceUtil preferenceUtil;

    CharSequence titles[] = {"PAYMENT", "BILLING ADDRESS", "CONFIRM"};
    public int currentTabPos = 0;
    public int previousTabPos = 0;
    public static String paymentTypeName = "";
    private String orderId = "";
    private String transactionId = "";
    public static String totalStr = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_netpay);
        saveActivity();
        initView();
        uiClickHandler();
        initData();
        setNetPayData();
    }


    @Override
    protected void onDestroy() {
//        unregisterReceiver(receiverValidationFailed);
        AppData.getInstance().getOrderPlaceData().setGrandTotal(getResources().getString(R.string.pound_sign) + totalStr);
        paymentTypeName = "";
        NetPayConfirmFragment.saveNetpay = false;
        ArrayList<Activity> activities = AppData.getInstance().getSavedActivities();
        if(activities.contains((Activity)this)) {
            activities.remove((Activity) this);
        }
        AppData.getInstance().setSavedActivities(activities);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        AppData.getInstance().getOrderPlaceData().setGrandTotal(getResources().getString(R.string.pound_sign) + totalStr);
        Intent intent = new Intent(NetPayActivity.this, ManageNetpayAccountActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void saveActivity() {
        ArrayList<Activity> activities = AppData.getInstance().getSavedActivities();
        activities.add(this);
        AppData.getInstance().setSavedActivities(activities);
    }


    private void initView() {
        toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle("CHECKOUT");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtTotal = (TextView) findViewById(R.id.txtTotal);
        btnNext = (Button) findViewById(R.id.btnNext);
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.pager);

        netpayPagerAdapter = new NetpayPagerAdapter(this, getSupportFragmentManager(), titles, getIntent().getExtras().getBoolean("is_new"));
        pager.setAdapter(netpayPagerAdapter);
        tabs.setViewPager(pager);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return ContextCompat.getColor(NetPayActivity.this, R.color.white);
            }

            @Override
            public int getDividerColor(int position) {
                //return getResources().getColor(R.color.yellow);
                return 0;
            }
        });

    }


    private void uiClickHandler() {
        btnNext.setOnClickListener(this);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                handlePageChange(position, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


    }


    private void handlePageChange(int tabPosition, boolean checkValidation) {
        previousTabPos = currentTabPos;
        currentTabPos = tabPosition;
        tabs.changeTabTextColor(tabPosition);
        if(currentTabPos == 0) {
            btnNext.setText("NEXT - BILLING ADDRESS");
        } else if(currentTabPos == 1) {
            btnNext.setText("NEXT - CONFIRM");
        } else if(currentTabPos == 2) {
            btnNext.setText("PAY £" + AppData.getInstance().getNetpayData().getAmount());
        }

        if(currentTabPos == 2 && previousTabPos == 0) {
            if(isPaymentDataValidated()) {
                if(!isBillingDataValidated()) {
                    new CustomToast(this, "Please fill all mandatory fields", "", false);
                    NetPayBillingAddressFragment.checkBillingValidation = true;
                    pager.setCurrentItem(1);
                }
            }
        }

    }


    private void initData() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiverValidationFailed, new IntentFilter("validation_failed"));

        preferenceUtil = new PreferenceUtil(this);
      //  txtTotal.setText(AppData.getInstance().getOrderPlaceData().getGrandTotal().substring(1));
        netPayPresenter = new NetPayPresenter(this);
        handlePageChange(0, false);
    }

    BroadcastReceiver receiverValidationFailed = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int index = intent.getExtras().getInt("index");
            if(currentTabPos > index) {
                if(index == 0) {
                    NetPayPaymentFragment.checkPaymentValidation = true;
                } else if(index == 1) {
                    NetPayBillingAddressFragment.checkBillingValidation = true;
                }
                new CustomToast(NetPayActivity.this, "Please fill all mandatory fields", "", false);
                pager.setCurrentItem(index);
            }
        }
    };

    private boolean isPaymentDataValidated() {
        NetpayData netpayData = AppData.getInstance().getNetpayData();
        if("".equalsIgnoreCase(netpayData.getCardType())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getCardNumber())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getSecurityCode())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getExpiryMonth())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getExpiryYear())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getFullName())) {
            return false;
        }
        return true;
    }

    private boolean isBillingDataValidated() {
        NetpayData netpayData = AppData.getInstance().getNetpayData();
        if("".equalsIgnoreCase(netpayData.getTitle())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getFirstName())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getLastName())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getBillToAddress())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getBillToTownCity())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getBillToCounty())) {
            return false;
        }
        if("".equalsIgnoreCase(netpayData.getBillToCountry())) {
            return false;
        }
        return true;
    }

    private boolean isCardExists(DataBaseUtil dataBaseUtil) {
        ArrayList<NetpayData> netpayDatas = dataBaseUtil.fetchNetpayUserData(preferenceUtil.getUserID());
        for(int i = 0; i < netpayDatas.size(); i++) {
            String tempCardNumber = netpayDatas.get(i).getCardNumber();
            if(AppData.getInstance().getNetpayData().getCardNumber().equals(tempCardNumber)) {
                return true;
            }
        }
        return false;
    }

    private void proceedToPayment() {
        if(NetPayConfirmFragment.saveNetpay) {
            try {
                DataBaseUtil db = new DataBaseUtil(this);
                db.open();
                if(isCardExists(db)) {
                    db.updateNetpayUserData(AppData.getInstance().getNetpayData(), preferenceUtil.getUserID());
                } else {
                    db.insertNetpayUserData(AppData.getInstance().getNetpayData(), preferenceUtil.getUserID());
                }
                db.close();
            } catch (Exception e) {
                Log.e("save_netpay", "" + e);
            }
        }
        try {
            netPayPresenter.onClickPayNetpay(this, ConstantValues.PAYMENT_NETPAY);
        } catch (Exception e) {
            Log.e("call_netpay_purchase", "" + e);
        }
    }


    public void openConfirmationActivity() {
        String paymentOption = "";
        paymentOption = "NetPay";
        Intent intent = new Intent(this, OrderConfirmationActivity.class);
        intent.putExtra("transaction_id", transactionId);
        intent.putExtra("order_id", orderId);
        intent.putExtra("payment_option", paymentOption);
        intent.putExtra("address", AppData.getInstance().getOrderPlaceData().getAddress());
        intent.putExtra("delivery_option", AppData.getInstance().getOrderPlaceData().getDeliveryCharge());
        intent.putExtra("delivery_charge", AppData.getInstance().getOrderPlaceData().getDeliveryCharge());
        intent.putExtra("offer_text", AppData.getInstance().getOrderPlaceData().getOfferText());
        intent.putExtra("card_fee", AppData.getInstance().getOrderPlaceData().getCardFee());
        intent.putStringArrayListExtra("all_offer_texts", AppData.getInstance().getOrderPlaceData().getAllOfferTexts());
        startActivity(intent);

        transactionId = "";
        orderId = "";
    }


    private void openDialogConfirmPayment(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_restuarant_override);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        //imgBtnClose.setVisibility(View.INVISIBLE);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(okButton);
        btnAccept.setBackgroundResource(R.drawable.round_red_button_selector);

        // if button is clicked, close the custom dialog
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                proceedToPayment();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setText(cancelButton);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    private void setNetPayData() {
        totalStr = AppData.getInstance().getOrderPlaceData().getGrandTotal().substring(1);
        if(totalStr != null && !totalStr.equalsIgnoreCase("")) {
            if(AppData.getInstance().getRestaurantInfoData().getNetpayCustomerCharge() != null && !"".equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getNetpayCustomerCharge())) {
                String chargeStr = AppData.getInstance().getRestaurantInfoData().getNetpayCustomerCharge();
                if(ConstantValues.CHARGE_TYPE_ACTUAL.equalsIgnoreCase(AppData.getInstance().getRestaurantInfoData().getNetpayChargeType())) {
                    chargeStr = UtilityMethod.priceFormatter(Double.valueOf(chargeStr));
                    AppData.getInstance().getOrderPlaceData().setCardFee(chargeStr);
                    double totalTemp = Double.valueOf(totalStr) + Double.valueOf(chargeStr);
                    String grandTotal = UtilityMethod.priceFormatter(totalTemp);
                    AppData.getInstance().getOrderPlaceData().setGrandTotal(getResources().getString(R.string.pound_sign) + grandTotal);
                } else {
                    double chargeTemp = (Double)((Double.valueOf(totalStr)*Double.valueOf(chargeStr))/(Double)100.0);
                    chargeStr = UtilityMethod.priceFormatter(chargeTemp);
                    AppData.getInstance().getOrderPlaceData().setCardFee(chargeStr);
                    double totalTemp = Double.valueOf(totalStr) + Double.valueOf(chargeStr);
                    String grandTotal = UtilityMethod.priceFormatter(totalTemp);
                    AppData.getInstance().getOrderPlaceData().setGrandTotal(getResources().getString(R.string.pound_sign) + grandTotal);
                }
            }
        }
        AppData.getInstance().getNetpayData().setAmount(AppData.getInstance().getOrderPlaceData().getGrandTotal().substring(1));
        AppData.getInstance().getNetpayData().setBillToCompany("ChefOnline");
    }


    @Override
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public void orderCompleted(boolean isSuccess) {
        if(isSuccess) {
            openConfirmationActivity();
            ArrayList<Activity> tempActivities = new ArrayList<>();
            ArrayList<Activity> activities = AppData.getInstance().getSavedActivities();
            AppData.getInstance().setSavedActivities(tempActivities);
            for(int i = 0; i < activities.size(); i++) {
                activities.get(i).finish();
            }
        }
    }

    @Override
    public void setLoadingVisible() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait ...");
        progressDialog.show();
    }

    @Override
    public void setLoadingHide() {
        if(progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showToast(String message, String subMessage, boolean isButtonOk) {
        new CustomToast(this, message, subMessage, isButtonOk);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnNext) {
            if(currentTabPos == 0 || currentTabPos == 1) {
                pager.setCurrentItem(currentTabPos + 1);
            } else {
             /*   if(!isPaymentDataValidated()){
                    new CustomToast(this, "Please fill all mandatory fields", "", false);
                    NetPayPaymentFragment.checkPaymentValidation = true;
                    pager.setCurrentItem(0);
                    return;
                }
                if(!isBillingDataValidated()){
                    new CustomToast(this, "Please fill all mandatory fields", "", false);
                    NetPayBillingAddressFragment.checkBillingValidation = true;
                    pager.setCurrentItem(1);
                    return;
                }  */
                openDialogConfirmPayment("Do you wish to confirm your payment?", "Confirm", "Cancel");
            }
        }
    }

}
