package com.chefonline.online;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.chefonline.adapter.ManagePaymentMethodsAdapter;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.MoveTouchHelper;
import com.chefonline.datamodel.NetpayData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UtilityMethod;

import java.util.ArrayList;

/**
 * Created by user on 9/4/2016.
 */
public class ManageNetpayAccountActivity extends AppCompatActivity implements View.OnClickListener{

    Toolbar toolBar;
    TextView txtAddNew;
    TextView txtTotal;
    RecyclerView recyclerPaymentMethods;
    private ManagePaymentMethodsAdapter adapter;
    ArrayList<NetpayData> netpayDatas;
    DataBaseUtil db;
    PreferenceUtil preferenceUtil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_netpay_account);
        saveActivity();
        initView();
        handleUiClick();
        initData();
        setAdapter();
        setTouchHelper();
    }

    /*
    @Override
    protected void onResume() {
        super.onResume();
        int prevCount = adapter.getItemCount();
        initData();
        if(netpayDatas.size() > prevCount || netpayDatas.size() < prevCount) {
            setAdapter();
            setTouchHelper();
        }
    }  */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        ArrayList<Activity> activities = AppData.getInstance().getSavedActivities();
        if(activities.contains((Activity)this)) {
            activities.remove((Activity) this);
        }
        AppData.getInstance().setSavedActivities(activities);
        super.onDestroy();
    }

    private void saveActivity() {
        ArrayList<Activity> activities = AppData.getInstance().getSavedActivities();
        activities.add(this);
        AppData.getInstance().setSavedActivities(activities);
    }

    private void initView() {
        toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle("Payment Method");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtTotal = (TextView) findViewById(R.id.txtTotal);
        txtAddNew = (TextView) findViewById(R.id.txtAddNew);
        recyclerPaymentMethods = (RecyclerView) findViewById(R.id.recyclePaymentMethods);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerPaymentMethods.setLayoutManager(linearLayoutManager);
    }

    private void handleUiClick() {
        txtAddNew.setOnClickListener(this);
    }

    private void initData() {
        preferenceUtil = new PreferenceUtil(this);
       // txtTotal.setText(AppData.getInstance().getOrderPlaceData().getGrandTotal().substring(1));

        try {
            db = new DataBaseUtil(this);
            db.open();
        } catch (Exception e) {
            Log.e("open db", "" + e);
        }

        try {
            netpayDatas = db.fetchNetpayUserData(preferenceUtil.getUserID());
            db.close();
        } catch (Exception e) {
            Log.e("get_netpay_data", "" + e);
        }
    }

    /*
    private void initNetpayData() {
        NetpayData netpayData = AppData.getInstance().getNetpayData();
        netpayData.setAmount(AppData.getInstance().getOrderPlaceData().getGrandTotal().substring(1));
        netpayData.setBillToCompany("ChefOnline");
        AppData.getInstance().setNetpayData(netpayData);
    }  */

    private void setAdapter() {
        adapter = new ManagePaymentMethodsAdapter(this, netpayDatas);
        recyclerPaymentMethods.setAdapter(adapter);
    }

    private void setTouchHelper() {
        ItemTouchHelper.Callback callback = new MoveTouchHelper(adapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(recyclerPaymentMethods);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.txtAddNew) {
          //  netpayData.setAmount(AppData.getInstance().getOrderPlaceData().getGrandTotal().substring(1));
         //   netpayData.setBillToCompany("ChefOnline");
            NetpayData netpayData = new NetpayData();
            AppData.getInstance().setNetpayData(netpayData);
            Intent intent = new Intent(ManageNetpayAccountActivity.this, NetPayActivity.class);
            intent.putExtra("is_new", true);
            startActivity(intent);
            finish();
        }
    }

}
