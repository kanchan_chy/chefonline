package com.chefonline.online;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.chefonline.adapter.PlacedOrderAdapter;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.datamodel.CartData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.UtilityMethod;

import java.util.ArrayList;

public class OrderConfirmationActivity extends Activity implements View.OnClickListener {
    private TextView textViewPostCode, txtViewName, txtViewOrderId, txtViewOrderDate, textViewDetails, txtViewOption, textViewOrderType, txtViewMobile, textViewTotal, txtViewMsg, txtViewOffer;
    private ImageButton imgBtnBack;
    private ListView listViewOrderDetails;
    private View headerView;
    private DataBaseUtil db;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirmation);
        initView();
        setUiListener();
        loadData();
    }

    private void loadData() {
        db = new DataBaseUtil(this);
        db.open();

        ArrayList<CartData> cartDatas = new ArrayList<>();
        cartDatas = db.fetchCartData();
        txtViewOrderId.setText("ORDER ID: " + getIntent().getExtras().getString("order_id"));
        txtViewOrderDate.setText("" + UtilityMethod.formattedCurrentDate());

        PreferenceUtil preferenceUtil = new PreferenceUtil(this);
        if (getIntent().getExtras().getString("delivery_option").equalsIgnoreCase("Delivery")) {
            txtViewName.setText(preferenceUtil.getUserFirstName() + " " + preferenceUtil.getUserLastName());
            if(AppData.getInstance().getOrderPlaceData().getPostCode() != null && !"".equalsIgnoreCase(AppData.getInstance().getOrderPlaceData().getPostCode())) {
                textViewPostCode.setText(AppData.getInstance().getOrderPlaceData().getPostCode());
            } else {
                textViewPostCode.setVisibility(View.GONE);
            }
            if(AppData.getInstance().getOrderPlaceData().getAddress() != null && !"".equalsIgnoreCase(AppData.getInstance().getOrderPlaceData().getAddress())) {
                textViewDetails.setText(AppData.getInstance().getOrderPlaceData().getAddress());
            } else {
                textViewDetails.setVisibility(View.GONE);
            }
            textViewOrderType.setText("Delivery");
            txtViewOption.setText("" + getIntent().getExtras().getString("payment_option"));
            if(AppData.getInstance().getOrderPlaceData().getMobile() != null && !"".equalsIgnoreCase(AppData.getInstance().getOrderPlaceData().getMobile())) {
                txtViewMobile.setText("" + AppData.getInstance().getOrderPlaceData().getMobile());
            } else {
                txtViewMobile.setVisibility(View.GONE);
            }
            textViewTotal.setText("Total: " + getIntent().getExtras().getString("total_amount"));

        } else {
            txtViewName.setText(AppData.getInstance().getRestaurantInfoData().getRestaurantName());
            textViewPostCode.setText(AppData.getInstance().getOrderPlaceData().getPostCode());
            textViewDetails.setText(AppData.getInstance().getOrderPlaceData().getAddress());
            textViewOrderType.setText("Collection");
            txtViewOption.setText("" + getIntent().getExtras().getString("payment_option"));
            txtViewMobile.setText("" + AppData.getInstance().getOrderPlaceData().getMobile());
            textViewTotal.setText("Total: " + getIntent().getExtras().getString("total_amount"));

        }

        txtViewMsg.setText("Thank you for your order with " + AppData.getInstance().getRestaurantInfoData().getRestaurantName() + ", an email has been sent with your order details.");

        listViewOrderDetails.addHeaderView(headerView, null, false);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View footerView  = inflater.inflate(R.layout.footer_order_history_details, null);
        TextView txtViewSubTotal = (TextView) footerView.findViewById(R.id.txtViewSubTotal);
        TextView txtViewDiscount = (TextView) footerView.findViewById(R.id.txtViewDiscount);
        TextView txtViewDeliveryChargeAmount = (TextView) footerView.findViewById(R.id.textViewDeliveryChargeAmount);
        txtViewOffer = (TextView) footerView.findViewById(R.id.txtViewOffer);
        TextView txtViewCardFee = (TextView) footerView.findViewById(R.id.textViewCardFee);
        TextView textViewGrandTotal = (TextView) footerView.findViewById(R.id.textViewGrandTotal);

        //textViewDiscountAmount.setVisibility(View.GONE);
        //textViewGrandTotal.setVisibility(View.GONE);

        txtViewSubTotal.setText("Sub Total: " + AppData.getInstance().getOrderPlaceData().getTotalAmount());
        txtViewDiscount.setText("Discount: " + getResources().getString(R.string.pound_sign) + AppData.getInstance().getOrderPlaceData().getDiscount());
        txtViewDeliveryChargeAmount.setText("Delivery Charge: " + getResources().getString(R.string.pound_sign)+getIntent().getExtras().getString("delivery_charge").toString().trim());
        txtViewCardFee.setText("NetPay Charge: " + getResources().getString(R.string.pound_sign) + getIntent().getExtras().getString("card_fee").toString().trim());
        textViewGrandTotal.setText("Grand Total: " + AppData.getInstance().getOrderPlaceData().getGrandTotal());

       // txtViewOffer.setText("*** " + getIntent().getExtras().getString("offer_text") + " ***");
        ArrayList<String> allOfferTexts = getIntent().getStringArrayListExtra("all_offer_texts");
        if(allOfferTexts != null && allOfferTexts.size() > 0) {
            if(allOfferTexts.size() > 1) {
                String tempOfferText = "*** ";
                for (int i = 0; i < allOfferTexts.size(); i++) {
                    if(i == 0) {
                        tempOfferText += allOfferTexts.get(i);
                    } else {
                        tempOfferText += ", " + allOfferTexts.get(i);
                    }
                }
                tempOfferText += " ***";
                txtViewOffer.setText(tempOfferText);
            } else {
                txtViewOffer.setText("*** " + allOfferTexts.get(0) + " ***");
            }
        }

        if(getIntent().getExtras().getString("delivery_charge") != null && !"".equalsIgnoreCase(getIntent().getExtras().getString("delivery_charge"))) {
            if (Double.parseDouble(getIntent().getExtras().getString("delivery_charge").toString().trim()) > 0) {
                txtViewDeliveryChargeAmount.setVisibility(View.VISIBLE);
            } else {
                txtViewDeliveryChargeAmount.setVisibility(View.GONE);
            }
        }else {
            txtViewDeliveryChargeAmount.setVisibility(View.GONE);
        }

        if(getIntent().getExtras().getString("card_fee") != null && !"".equalsIgnoreCase(getIntent().getExtras().getString("card_fee"))) {
            if (Double.parseDouble(getIntent().getExtras().getString("card_fee").toString().trim()) > 0) {
                txtViewCardFee.setVisibility(View.VISIBLE);
            } else {
                txtViewCardFee.setVisibility(View.GONE);
            }
        } else {
            txtViewCardFee.setVisibility(View.GONE);
        }

        if(AppData.getInstance().getOrderPlaceData().getDiscount() != null && !"".equalsIgnoreCase(AppData.getInstance().getOrderPlaceData().getDiscount())) {
            if (Double.parseDouble(AppData.getInstance().getOrderPlaceData().getDiscount().toString()) > 0 ) {
                txtViewDiscount.setVisibility(View.VISIBLE);
            } else {
                txtViewDiscount.setVisibility(View.GONE);
            }
        } else {
            txtViewDiscount.setVisibility(View.GONE);
        }

        //When discount and delivery charge is not available
        if(AppData.getInstance().getOrderPlaceData().getTotalAmount().toString().equalsIgnoreCase(AppData.getInstance().getOrderPlaceData().getGrandTotal())) {
            txtViewSubTotal.setVisibility(View.VISIBLE);
            txtViewSubTotal.setText("Total: " + AppData.getInstance().getOrderPlaceData().getGrandTotal());

            txtViewDiscount.setVisibility(View.GONE);
            txtViewDeliveryChargeAmount.setVisibility(View.GONE);
            textViewGrandTotal.setVisibility(View.GONE);
        }

    /*    if (getIntent().getExtras().getString("offer_text").equalsIgnoreCase("") || getIntent().getExtras().getString("offer_text") == null) {
            txtViewOffer.setVisibility(View.GONE);
        }

        if (getIntent().getExtras().getString("offer_text").contains("Offers available for you")) {
            txtViewOffer.setVisibility(View.GONE);
        }  */
        if(allOfferTexts == null || allOfferTexts.size() == 0) {
            txtViewOffer.setVisibility(View.GONE);
        }

        listViewOrderDetails.addFooterView(footerView);

        PlacedOrderAdapter cartAdapter = new PlacedOrderAdapter(this, null, cartDatas);
        listViewOrderDetails.setAdapter(cartAdapter);
    }

    private void setUiListener() {
        imgBtnBack.setOnClickListener(this);
    }

    private void initView() {
        LayoutInflater inflater = LayoutInflater.from(this);
        headerView = inflater.inflate(R.layout.header_order_confirmation, null);
        txtViewMsg = (TextView) headerView.findViewById(R.id.txtViewMsg);
        txtViewOrderId = (TextView) headerView.findViewById(R.id.txtViewOrderId);
        txtViewName = (TextView) headerView.findViewById(R.id.txtViewName);
        textViewPostCode = (TextView) headerView.findViewById(R.id.textViewPostCode);
        txtViewOrderDate = (TextView) headerView.findViewById(R.id.txtViewOrderDate);
        textViewDetails = (TextView) headerView.findViewById(R.id.textViewDetails);
        txtViewOption = (TextView) headerView.findViewById(R.id.txtViewOption);
        textViewOrderType = (TextView) headerView.findViewById(R.id.txtViewOrderType);
        txtViewMobile = (TextView) headerView.findViewById(R.id.txtViewMobile);
        textViewTotal = (TextView) headerView.findViewById(R.id.textViewTotal);
        listViewOrderDetails = (ListView) findViewById(R.id.listViewOrderDetails);
        imgBtnBack = (ImageButton) findViewById(R.id.imgBtnBack);
    }

    @Override
    protected void onDestroy() {
        db.emptyCartData();
        MainActivity.txtViewBubble.setText("0");
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBtnBack:
                onBackPressed();
            break;
        }
    }
}
