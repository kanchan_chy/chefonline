package com.chefonline.online;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.chefonline.datamodel.CartData;
import com.chefonline.datamodel.PizzaGroupItemData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.fragment.CartFragment;
import com.chefonline.fragment.CheckoutFragment;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;

import java.util.ArrayList;

public class OrderHostActivity extends AppCompatActivity implements View.OnClickListener {
    private static String TAG = "SearchActivity";
    private String restaurantId = "";
    ArrayList<CartData> cartDatas;
    private Button btnPlaceOrder;
    private ImageButton imgBtnBack;
    private RelativeLayout relative_bottom;
    private long mLastClickTime = 0;

    @TargetApi(21)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_host);
        saveActivity();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Order");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initView();

        try {
            // condition approved when reorder is occurred
            if (getIntent().getExtras().getInt(ConstantValues.NAVIGATION_KEY, 0) == ConstantValues.FRAGMENT_CATEGORY) {
                ConstantValues.FRAGMENT_CATEGORY = 0;
                ConstantValues.IS_REORDER = true;

                restaurantId = getIntent().getExtras().getString("restaurant_id");
                DataBaseUtil db;
                db = new DataBaseUtil(this);
                db.open();
                db.emptyCartData();

                //Reordered item is added to cart
                for (int i = 0; i < AppData.getInstance().getRestaurantInfoData().getDishDatas().size(); i++) {

                    String tempIds = "";
                    String tempNames = "";
                    double pizzaItemPrice = 0.0;

                    if(AppData.getInstance().getRestaurantInfoData().getDishDatas().get(i).isPizzaMenu()) {
                        ArrayList<PizzaGroupItemData> pizzaItemList = AppData.getInstance().getRestaurantInfoData().getDishDatas().get(i).getOrderedPizzaItemList();
                        for (int j = 0; j < pizzaItemList.size(); j++) {
                            if(j == 0) {
                                tempIds += pizzaItemList.get(j).getPizzaGroupItemId();
                                tempNames += pizzaItemList.get(j).getItemName();
                            } else {
                                tempIds += "," + pizzaItemList.get(j).getPizzaGroupItemId();
                                tempNames += "," + pizzaItemList.get(j).getItemName();
                            }
                            pizzaItemPrice += Double.valueOf(pizzaItemList.get(j).getItemPrice());
                        }
                    }

                    db.insertCartData(restaurantId,
                            AppData.getInstance().getRestaurantInfoData().getDishDatas().get(i).getDishId(),
                            AppData.getInstance().getRestaurantInfoData().getDishDatas().get(i).getDishName(),
                            Integer.parseInt(AppData.getInstance().getRestaurantInfoData().getDishDatas().get(i).getDishQty()),
                            String.valueOf(pizzaItemPrice),
                            tempIds,
                            tempNames
                    );
                }
                //loadCategoryFragment2(0);
            } else {

            }
        } catch (Exception e) {
            Log.e(TAG, "" + e.getMessage());
        }

        loadCartFragment();

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    @Override
    protected void onDestroy() {
        ArrayList<Activity> activities = AppData.getInstance().getSavedActivities();
        if(activities.contains((Activity)this)) {
            activities.remove((Activity)this);
        }
        AppData.getInstance().setSavedActivities(activities);
        super.onDestroy();
    }

    private void saveActivity() {
        ArrayList<Activity> activities = AppData.getInstance().getSavedActivities();
        activities.add(this);
        AppData.getInstance().setSavedActivities(activities);
    }


    private void initView() {
        DataBaseUtil db = new DataBaseUtil(this);
        db.open();

        cartDatas = new ArrayList<>();
        cartDatas = db.fetchCartData();

        relative_bottom = (RelativeLayout) findViewById(R.id.relative_bottom);
    }

    private void loadCartFragment() {
        android.app.Fragment fragment = null;
        fragment = new CartFragment();
        Bundle args = new Bundle();
        args.putInt("isVoiceOrder", 1);
        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        // ft.addToBackStack(null);
        ft.add(R.id.content_frame, fragment);
        ft.commit();
    }

    private void loadOrderSummaryFragment() {
        android.app.Fragment fragment = null;
        fragment = new CheckoutFragment();
        Bundle args = new Bundle();
        args.putInt("selected_service", CheckoutFragment.selectedServiceNO);
        args.putString("selected_policy_id", CheckoutFragment.policyId);
        args.putString("selected_policy_time", CheckoutFragment.policyTime);
        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        ft.add(R.id.content_frame, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPlaceOrder:
                break;
            case R.id.imgBtnBack:
                AppData.getInstance().hideKeyboard(this, v);
                onBackPressed();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        FragmentManager manager = getFragmentManager();
        int count = manager.getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            manager.popBackStack();
        }
    }

    @Override
    protected void onResume() {
        //getSupportActionBar().setTitle("Order");
        if (ConstantValues.NAVIGATION_TO == ConstantValues.FRAGMENT_CART) {
            loadOrderSummaryFragment();
            ConstantValues.NAVIGATION_TO = 0;

        } else if (ConstantValues.NAVIGATION_TO == ConstantValues.ACTIVITY_SETTINGS) {
            ConstantValues.NAVIGATION_TO = 0;

        } else if (ConstantValues.NAVIGATION_TO == ConstantValues.ACTIVITY_RESERVATION) {
            ConstantValues.NAVIGATION_TO = 0;

        } else {
            ConstantValues.NAVIGATION_TO = 0;
        }

        super.onResume();
    }

}
