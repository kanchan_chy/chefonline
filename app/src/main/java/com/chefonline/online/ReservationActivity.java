package com.chefonline.online;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.adapter.TitlesDialogAdapter;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.ChefOnlineTextView;
import com.chefonline.customview.CustomToast;
import com.chefonline.customview.EditTextWithoutPadding;
import com.chefonline.customview.MsmEditText;
import com.chefonline.datamodel.LocationData;
import com.chefonline.datamodel.ReservationData;
import com.chefonline.gps.GPSTracker;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.modelinterface.PlaceOrderView;
import com.chefonline.pattern.AppData;
import com.chefonline.presenter.PlaceOrderPresenter;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.TimeManagement;
import com.chefonline.utility.UtilityMethod;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ReservationActivity extends AppCompatActivity implements View.OnClickListener, PlaceOrderView {
    Date selectedDate = null;
    Date currentDate = null;
    Date previousDate = null;

    String OPENING_TIME_ONE = null;
    String CLOSING_TIME_ONE = null;

    String OPENING_TIME_TWO = null;
    String CLOSING_TIME_TWO = null;

    String mIP = "";
    String mobileNo = "";
    String teleNo = "";
    String title="";
    String[] allTitles;

    TextView txtViewDateWarning;
    TextView txtViewTimeWarning;
    MsmEditText editTextTitle;
    MsmEditText editTextFirstName;
    MsmEditText editTextLastName;
    MsmEditText editTextEmail;
    MsmEditText editTextDate;
    MsmEditText editTextTime;
    MsmEditText editTextGuestNo;
    EditTextWithoutPadding editTextMobileNo;
    EditTextWithoutPadding editTextTeleNo;
    EditText editTextSpecialRequest;
    Button btnTitle;
    Spinner spinnerMobileNo;
    Spinner spinnerTeleNo;
    LinearLayout linear_layout_mandatory;
    ChefOnlineTextView txtIp;

    private ProgressDialog pDialog;

    Button   btnReserve;
  //  ImageButton imgBtnBack;
    Button imageBtnDatePicker, imageButtonTime;
    Toolbar toolBar;
    List<String> timeArrayList = new ArrayList<>();

    boolean ifSelectedTodayDate = false;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);
        intiView();
        setUiListener();

        PlaceOrderPresenter placeOrderPresenter = new PlaceOrderPresenter(this);
        placeOrderPresenter.getRealIPAddress(getApplicationContext(), "44");

        setDateTimeField();
        setDateTimeField();

        setViewData();

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void setViewData() {
        ArrayAdapter<CharSequence> adapterSpinnerMobileNo = ArrayAdapter.createFromResource(this,R.array.cnt_codes, R.layout.spinner_item);
        adapterSpinnerMobileNo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMobileNo.setAdapter(adapterSpinnerMobileNo);
        spinnerTeleNo.setAdapter(adapterSpinnerMobileNo);

        PreferenceUtil preferenceUtil = new PreferenceUtil(this);
        if (preferenceUtil.getLogInStatus() == 1) {
            title = preferenceUtil.getUserTitle();
            if(title == null || "".equalsIgnoreCase(title) || "null".equalsIgnoreCase(title)) {
                title = "";
            } else {
                btnTitle.setText(title);
                editTextTitle.setText(title);
            }
            editTextFirstName.setText(preferenceUtil.getUserFirstName());
            editTextLastName.setText(preferenceUtil.getUserLastName());
            editTextEmail.setText(preferenceUtil.getUserEmail());
            String tempMobileNo = preferenceUtil.getUserMobile();
            String[] cntCodesAll = getResources().getStringArray(R.array.cnt_codes);
            int selectedPosMobile = 0;
            if(tempMobileNo != null && cntCodesAll != null && cntCodesAll.length > 0) {
                for(int i = 0; i < cntCodesAll.length; i++) {
                    String tempCode = cntCodesAll[i];
                    tempCode = tempCode.substring(tempCode.indexOf("(")+1, tempCode.indexOf(")"));
                    if(tempMobileNo.startsWith(tempCode)){
                        selectedPosMobile = i;
                        tempMobileNo = tempMobileNo.substring(tempCode.length());
                    }
                }
            }
            editTextMobileNo.setText(tempMobileNo);
            spinnerMobileNo.setSelection(selectedPosMobile);
            editTextFirstName.setEnable(false);
            editTextLastName.setEnable(false);
            editTextEmail.setEnable(false);
        }
    }

    private void setUiListener() {
        btnReserve.setOnClickListener(this);
        imageBtnDatePicker.setOnClickListener(this);
        imageButtonTime.setOnClickListener(this);
        btnTitle.setOnClickListener(this);
    }

    private void intiView() {

        toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle("Reservation");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pDialog = new ProgressDialog(this);
        pDialog.setCanceledOnTouchOutside(false);

        linear_layout_mandatory = (LinearLayout) findViewById(R.id.linear_layout_mandatory);

        txtViewDateWarning = (TextView) findViewById(R.id.txtViewDateWarning);
        txtViewTimeWarning = (TextView) findViewById(R.id.txtViewTimeWarning);

        editTextTitle = (MsmEditText) findViewById(R.id.editTextTitle);
        editTextFirstName = (MsmEditText) findViewById(R.id.editTextFirstName);
        editTextLastName = (MsmEditText) findViewById(R.id.editTextLastName);
        editTextEmail = (MsmEditText) findViewById(R.id.editTextEmail);
        editTextMobileNo = (EditTextWithoutPadding) findViewById(R.id.editTextMobileNo);
        editTextTeleNo = (EditTextWithoutPadding) findViewById(R.id.editTextTeleNo);
        btnTitle = (Button) findViewById(R.id.btnTitle);
        spinnerMobileNo = (Spinner) findViewById(R.id.spinnerMobileNo);
        spinnerTeleNo = (Spinner) findViewById(R.id.spinnerTeleNo);

        editTextDate = (MsmEditText) findViewById(R.id.editTextDate);
        editTextDate.setEnable(false);
        editTextTime = (MsmEditText) findViewById(R.id.editTextTime);
        editTextTime.setEnable(false);

        editTextGuestNo = (MsmEditText) findViewById(R.id.editTextGuestNo);
        editTextGuestNo.setMaxLength(3);
        editTextSpecialRequest = (EditText) findViewById(R.id.editTextSpecialRequest);

        btnReserve = (Button) findViewById(R.id.btnReserve);
        imageBtnDatePicker = (Button) findViewById(R.id.imageBtnDatePicker);
        imageButtonTime = (Button) findViewById(R.id.imageButtonTime);

        txtIp=(ChefOnlineTextView)findViewById(R.id.txtIp);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_reservation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            //get.setDisplayHomeAsUpEnabled(false);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    ProgressDialog progress;
    private void callReservationApi() {
        RequestQueue queue = Volley.newRequestQueue(ReservationActivity.this);
        final GPSTracker gps = new GPSTracker(this);
        String url = ConstantValues.BASE_API_URL;

        StringRequest myReq = new StringRequest(Request.Method.POST, url, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "75");
                params.put("rest_id", Integer.toString(AppData.getInstance().getRestaurantInfoData().getRestaurantId()));
                params.put("title", title);
                params.put("first_name", editTextFirstName.getText().toString().trim());
                params.put("last_name", editTextLastName.getText().toString().trim());
                params.put("email", editTextEmail.getText().toString().trim());
                params.put("mobile_no", mobileNo);
                params.put("telephone", teleNo);
                params.put("reservation_date", editTextDate.getText().toString().trim());
                params.put("reservation_time", editTextTime.getText().toString().trim());
                params.put("guest", editTextGuestNo.getText().toString().trim());
                params.put("special_request", editTextSpecialRequest.getText().toString().trim());
                params.put("ip_address", mIP);
                params.put("platform", "2");
                Log.i("Reservation", "****" + params.toString());
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

        progress = new ProgressDialog(ReservationActivity.this);
        progress.setMessage("Please wait....");
        progress.show();
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                    btnReserve.setEnabled(true);
                    new CustomToast(ReservationActivity.this, "Server Response Error" + error.getMessage(),"" , false);
                }
                catch (Exception e){
                    Log.e("After volley execution",e.getMessage());
                }
            }
        };
    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progress.dismiss();
                    btnReserve.setEnabled(true);
                    Log.i("Reservation Response", "****" + response);

                    ReservationData reservationData = new ReservationData();
                    reservationData = JsonParser.parseReservationData(response);

                    if ("Success".equalsIgnoreCase(reservationData.getStatus())) {
                        new CustomToast(ReservationActivity.this, reservationData.getMessage(),"" , true);
                        startActivity(new Intent(ReservationActivity.this, ReservationConfirmActivity.class));
                        finish();
                    } else {
                        new CustomToast(ReservationActivity.this, reservationData.getMessage(),"" , false);
                    }

                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                        new CustomToast(ReservationActivity.this, "Parsing error encountered.","" , false);
                    }
                    catch (Exception e1){
                        Log.e("After volley execution",e1.getMessage());
                    }
                }

            }

        };
    }

    private List<String> getTimeList(Date date) {
        timeArrayList = null;
        int timeListLength = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
        String selectedDate = sdf.format(date);

        int shiftCount = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().size();
        for (int i = 0; i < shiftCount; i++) {
            if (AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getDayName().equalsIgnoreCase(selectedDate)) {
                Log.i("DAY NAME", "--- " + selectedDate);
                timeListLength = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getReservationDatas().size();
                for (int j = 0; j < timeListLength; j++) {
                    if (j == 0) {
                        OPENING_TIME_ONE = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getReservationDatas().get(j).getOpeningTime();
                        CLOSING_TIME_ONE = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getReservationDatas().get(j).getEndTime();
                        timeArrayList = TimeManagement.makeTimeListForReservation(OPENING_TIME_ONE, CLOSING_TIME_ONE, 0);
                    }

                    if (j == 1) {
                        OPENING_TIME_TWO = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getReservationDatas().get(j).getOpeningTime();
                        CLOSING_TIME_TWO = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getReservationDatas().get(j).getEndTime();

                        /** Check when user on gap of two shift  */
                        if (TimeManagement.isUserMiddleOnShift(CLOSING_TIME_ONE, OPENING_TIME_TWO)) {
                            Log.i("is middle", "true");

                            if (ifSelectedTodayDate) {
                                timeArrayList.clear();
                            }
                        }

                        timeArrayList.addAll(TimeManagement.makeTimeListForReservation(OPENING_TIME_TWO, CLOSING_TIME_TWO, 0));

                    }


                }
            }
        }

        return timeArrayList;
    }

    /** Pre order time picker dialog */
    public void openTimePickerDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Reservation Time");

        ListView modeList = new ListView(this);
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, timeArrayList);
        modeList.setAdapter(modeAdapter);
        modeAdapter.notifyDataSetChanged();
        builder.setView(modeList);

        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,View view,int position,long id) {
                txtViewTimeWarning.setVisibility(View.GONE);
                editTextTime.setText(timeArrayList.get(position));
                imageButtonTime.setText(timeArrayList.get(position));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showDatePickerDialog(View v) {
        fromDatePickerDialog.show();
    }

    private boolean checkReservationOpenStatus(String selectedDay)
    {
        String currentDay = selectedDay;
        int shiftCount = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().size();
        boolean todayFound = false;
        for (int i = 0; i < shiftCount; i++) {
            if (AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getDayName().equalsIgnoreCase(currentDay)) {
                todayFound = true;
                if(AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getReservationDatas() == null || AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getReservationDatas().size() == 0) {
                    return false;
                }
            }
        }
        if(todayFound == false) {
            return false;
        }
        return true;
    }

    private boolean isReservationTimeOver(Date tempSelectedDate) {
        String currentDay = new SimpleDateFormat("EEEE").format(tempSelectedDate);
        String selectedDateStr = new SimpleDateFormat("dd-MM-yyyy").format(tempSelectedDate);
        String todayStr = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        if(!todayStr.equalsIgnoreCase(selectedDateStr)) {
            return false;
        }
        int shiftCount = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().size();
        for (int i = 0; i < shiftCount; i++) {
            if (AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getDayName().equalsIgnoreCase(currentDay)) {
                if(AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getReservationDatas() != null && AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getReservationDatas().size() > 0) {
                    for( int k = 0; k < AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getReservationDatas().size(); k++) {
                        String openingTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getReservationDatas().get(k).getOpeningTime();
                        String closingTime = AppData.getInstance().getRestaurantInfoData().getScheduleDatas().get(i).getReservationDatas().get(k).getEndTime();
                        boolean isOver = isShiftTimeOver(0, openingTime, closingTime);
                        if(!isOver) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    private boolean isShiftTimeOver(int lastTime, String openingTimeStr1, String closingTimeStr1) {
        TimeManagement timeManagement = new TimeManagement();
        String openingTimeStr = timeManagement.getTimeFrom12To24(openingTimeStr1);
        String closingTimeStr = timeManagement.getTimeFrom12To24(closingTimeStr1);
        int extraDay = 0;
        lastTime = 0 - lastTime;
        //SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf24 = new SimpleDateFormat("HH:mm");
        String currentDateTimeStr = sdf24.format(new Date());
        try {
            Date currentDateTime = sdf24.parse(currentDateTimeStr);
            Date openingTime = sdf24.parse(openingTimeStr);
            Date closingTime = sdf24.parse(closingTimeStr);
            if(openingTime.after(closingTime)) {
                extraDay = 1;
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(closingTime);
            cal.add(Calendar.DATE, extraDay);
            cal.add(Calendar.MINUTE, lastTime);
            String newTimeStr = sdf24.format(cal.getTime());
            Date newTime = sdf24.parse(newTimeStr);
            if(!currentDateTime.before(newTime)) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    private void setDateTimeField() {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                try {
                    selectedDate = dateFormatter.parse(dateFormatter.format(newDate.getTime()));
                    currentDate = dateFormatter.parse(dateFormatter.format(new Date().getTime()));
                } catch (ParseException e) {
                }

                if(dateFormatter.format(newDate.getTime()).equalsIgnoreCase(dateFormatter.format(new Date().getTime()))) {
                    ifSelectedTodayDate = true;
                } else {
                    ifSelectedTodayDate = false;
                }

                if (selectedDate.before(currentDate)) {
                    imageBtnDatePicker.setText("");
                    editTextDate.setText("");
                    selectedDate = null;
                    openDialog("Please select today or upcoming date.", "OK");
                    return;
                }

                if(!checkReservationOpenStatus(new SimpleDateFormat("EEEE").format(selectedDate))) {
                    imageBtnDatePicker.setText("");
                    editTextDate.setText("");
                    selectedDate = null;
                    openDialog("Sorry, reservation is closed on that day.", "OK");
                    return;
                }

                if(isReservationTimeOver(selectedDate)) {
                    imageBtnDatePicker.setText("");
                    editTextDate.setText("");
                    selectedDate = null;
                    openDialog("Sorry, reservation time is over today. Select an upcoming date.", "OK");
                    return;
                }

                if(previousDate != null && selectedDate != null && !(dateFormatter.format(selectedDate.getTime()).equalsIgnoreCase(dateFormatter.format(previousDate.getTime())))) {
                    editTextTime.setText("");
                    imageButtonTime.setText("");
                    txtViewTimeWarning.setVisibility(View.GONE);
                }

                txtViewDateWarning.setVisibility(View.GONE);
                editTextDate.setText(dateFormatter.format(newDate.getTime()));
                imageBtnDatePicker.setText(dateFormatter.format(newDate.getTime()));
                Log.e("Date_setting", "done");
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    }

    private boolean  validation() {
        if (editTextTitle.getText().toString().trim().equalsIgnoreCase("")) {
            editTextTitle.setErrorTextVisible(true);
            editTextTitle.setErrorMsg("Title is required.");
            editTextTitle.setTypingFocus();
            return false;
        }
        if (editTextFirstName.getText().toString().trim().equalsIgnoreCase("")) {
            editTextFirstName.setErrorTextVisible(true);
            editTextFirstName.setErrorMsg("First Name is required.");
            editTextFirstName.setTypingFocus();
            return false;
        }

        if (editTextLastName.getText().toString().trim().equalsIgnoreCase("")) {
            editTextLastName.setErrorTextVisible(true);
            editTextLastName.setErrorMsg("Last Name is required.");
            editTextLastName.setTypingFocus();
            return false;
        }

        if (editTextEmail.getText().toString().trim().equalsIgnoreCase("")) {
            editTextEmail.setErrorTextVisible(true);
            editTextEmail.setErrorMsg("Email is required.");
            editTextEmail.setTypingFocus();
            return false;
        }

        if (!UtilityMethod.isEmailValid(editTextEmail.getText().toString().trim())) {
            editTextEmail.setErrorTextVisible(true);
            editTextEmail.setErrorMsg("Email is not valid.");
            editTextEmail.setTypingFocus();
            return false;
        }

        if (editTextMobileNo.getText().toString().trim().equalsIgnoreCase("")) {
            editTextMobileNo.setErrorTextVisible(true);
            editTextMobileNo.setErrorMsg("Mobile No is required.");
            editTextMobileNo.setTypingFocus();
            return false;
        }

        String baseNo = editTextMobileNo.getText().toString().trim();
        String cntAndCode = spinnerMobileNo.getSelectedItem().toString();
        if(cntAndCode != null && ("UK (+44)".equalsIgnoreCase(cntAndCode) || "Bangladesh (+880)".equalsIgnoreCase(cntAndCode))) {
            if(baseNo.length() == 11 && baseNo.startsWith("0")) {
                baseNo = baseNo.substring(1);
            }
        }

        String code = cntAndCode.substring(cntAndCode.indexOf("(")+1, cntAndCode.indexOf(")"));
        mobileNo = code + baseNo;

        if(cntAndCode != null && ("UK (+44)".equalsIgnoreCase(cntAndCode) || "Bangladesh (+880)".equalsIgnoreCase(cntAndCode))) {
            if (!UtilityMethod.isMobileNoValid(mobileNo)) {
                editTextMobileNo.setErrorTextVisible(true);
                editTextMobileNo.setErrorMsg("Please provide valid mobile number.");
                editTextMobileNo.setTypingFocus();
                return false;
            }
        }

        if(editTextTeleNo.getText().toString() == null || editTextTeleNo.getText().toString().trim().equalsIgnoreCase("")) {
            teleNo = "";
        } else {
            String baseNo1 = editTextTeleNo.getText().toString().trim();
            String cntAndCode1 = spinnerTeleNo.getSelectedItem().toString();
            String code1 = cntAndCode1.substring(cntAndCode1.indexOf("(")+1, cntAndCode1.indexOf(")"));
            teleNo = code1 + baseNo1;
        }

        if (editTextGuestNo.getText().toString().trim().equalsIgnoreCase("")) {
            editTextGuestNo.setErrorTextVisible(true);
            editTextGuestNo.setErrorMsg("Guest number required.");
            editTextGuestNo.setTypingFocus();
            return false;
        }

        if (!UtilityMethod.isNumberValid(editTextGuestNo.getText().toString().trim())) {
            editTextGuestNo.setErrorTextVisible(true);
            editTextGuestNo.setErrorMsg("Invalid guest number.");
            editTextGuestNo.setTypingFocus();
            return false;
        }

        if(Integer.valueOf(editTextGuestNo.getText().toString().trim()) < 1) {
            editTextGuestNo.setErrorTextVisible(true);
            editTextGuestNo.setErrorMsg("Invalid guest number.");
            editTextGuestNo.setTypingFocus();
            return false;
        }

        if (editTextDate.getText().toString().trim().equalsIgnoreCase("")) {
            txtViewDateWarning.setVisibility(View.VISIBLE);
            editTextDate.setErrorTextVisible(true);
            editTextDate.setErrorMsg("Reservation Date is required.");
            editTextDate.setTypingFocus();
            return false;
        }

        if (editTextTime.getText().toString().trim().equalsIgnoreCase("")) {
            txtViewTimeWarning.setVisibility(View.VISIBLE);
            editTextTime.setErrorTextVisible(true);
            editTextTime.setErrorMsg("Reservation time is required.");
            editTextTime.setTypingFocus();
            return false;
        }

        String regexStr = "^[0-9]*$";
        if(!editTextGuestNo.getText().toString().trim().matches(regexStr)){
            editTextGuestNo.setErrorTextVisible(true);
            editTextGuestNo.setErrorMsg("Invalid guest number.");
            editTextGuestNo.setTypingFocus();
            return false;
        }

        return true;
    }


    private void openDialog(String message, String okButton) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_message);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(okButton);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnReserve:
                AppData.getInstance().hideKeyboard(this, v);

                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }

                if (validation()) {
                    btnReserve.setEnabled(false);
                    callReservationApi();
                }
                break;
            case R.id.imageBtnDatePicker:
                AppData.getInstance().hideKeyboard(this, v);
                previousDate = selectedDate;
                fromDatePickerDialog.show();
                break;
            case R.id.imageButtonTime:
                AppData.getInstance().hideKeyboard(this, v);
                if (editTextDate.getText().toString().trim().equalsIgnoreCase("")) {
                    txtViewDateWarning.setVisibility(View.VISIBLE);
                    editTextDate.setErrorTextVisible(true);
                    editTextDate.setErrorMsg("Reservation Date is required.");
                    editTextDate.setTypingFocus();
                    return;
                }

                if (!ifSelectedTodayDate) {
                    getTimeList(selectedDate);
                } else {
                    getTimeList(new Date());
                    timeArrayList = TimeManagement.reGenerateTimeList(timeArrayList);
                }

                if (timeArrayList != null && timeArrayList.size() > 0) {
                    openTimePickerDialog();
                } else {
                    openDialog("Sorry, reservation time is over today.", "OK");
                }

                break;
            case R.id.btnTitle:
                showTitleListDialog();
                break;
        }
    }

    @Override
    public void setLoadingVisible() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait....");
        pDialog.show();
    }

    @Override
    public void setLoadingHide() {
        if (pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    public void showToast(String message, String subMessage, boolean isButtonOk) {

    }

    @Override
    public void setLocationData(LocationData locationData) {
        mIP=locationData.getIpAddress()==null ? "" : locationData.getIpAddress();
        if(!mIP.equals("")){
            txtIp.setText("Your IP Address " + mIP + ", We Use This IP For Reservation and Verification Purpose");
            txtIp.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void visiblePayPalButton() {

    }

    @Override
    public void verifyCode(String code, boolean isPaypal) {

    }

    @Override
    public void orderCompleted(String orderId, String transactionId, boolean isPaypal) {

    }

    @Override
    public void paymentStatusUpdated(boolean isSuccess) {

    }

    @Override
    public void onVerificationFailed(boolean isPaypal) {

    }

    @Override
    public void retryPayment(boolean isPaypal) {

    }

    @Override
    public void isNetpayChargeFound(boolean isFound) {

    }


    private void showTitleListDialog()
    {
        final Dialog dialog = new Dialog(ReservationActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_title_list);

        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);

        ListView listView = (ListView) dialog.findViewById(R.id.listViewOptions);
        allTitles=getResources().getStringArray(R.array.titles);
        final TitlesDialogAdapter adapter = new TitlesDialogAdapter(ReservationActivity.this, allTitles);
        listView.setAdapter(adapter);

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                title = allTitles[position];
                if (title != null && !"".equalsIgnoreCase(title)) {
                    btnTitle.setText(title);
                    editTextTitle.setText(title);
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}
