package com.chefonline.online;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chefonline.pattern.AppData;


public class ReservationConfirmActivity extends AppCompatActivity implements OnClickListener{

    private TextView txtViewMobile;
    private LinearLayout linearMobile;
    Toolbar toolBar;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_confirm);
        intiView();
    }

    private void intiView() {

        toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle("Confirm Reservation");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtViewMobile = (TextView) findViewById(R.id.txtViewMobile);
        linearMobile = (LinearLayout) findViewById(R.id.linearMobile);

        linearMobile.setOnClickListener(this);

        String mobileNo = AppData.getInstance().getRestaurantInfoData().getReservationTel().trim();
        txtViewMobile.setText(mobileNo);
    }

    private String getSpaceLessNumber(String mNumber){
        String tempNumber = "";
        for (int i=0; i<mNumber.length(); i++) {
            if(mNumber.charAt(i) != ' ') {
                tempNumber += mNumber.charAt(i);
            }
        }
        return tempNumber;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            //get.setDisplayHomeAsUpEnabled(false);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void showWarningDialog(String message)
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_message);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    private void makePhoneCall(String phNumber) {
        boolean noProblem = false;
        TelephonyManager telMgr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                noProblem=false;
                showWarningDialog("SIM card not found. Please insert SIM card.");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                noProblem=false;
                showWarningDialog("Network problem encountered. Please check network settings.");
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                noProblem=false;
                showWarningDialog("Sorry! Call is not possible");
                break;
            case TelephonyManager.SIM_STATE_READY:
                noProblem = true;
                break;
        }
        if(noProblem){
            try{
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phNumber));
                startActivity(callIntent);
            }
            catch (Exception e){
                showWarningDialog("Unable to deliver the call. "+ e.getMessage());
            }
        }
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.linearMobile) {
            String mobileNo = AppData.getInstance().getRestaurantInfoData().getReservationTel().trim();
            makePhoneCall(getSpaceLessNumber(mobileNo));
        }
    }

}
