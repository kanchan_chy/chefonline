package com.chefonline.online;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.StyleSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.customview.MsmEditText;
import com.chefonline.datamodel.UserData;
import com.chefonline.gps.GPSTracker;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UtilityMethod;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

public class RegistrationNextActivity extends AppCompatActivity implements View.OnClickListener{
    private static String TAG = "RegistrationActivity";
    MsmEditText editTextDob ;
    MsmEditText editTextAnniversary;
    MsmEditText editTextPostCode;
    MsmEditText editTextAddress1;
    MsmEditText editTextAddress2;
    MsmEditText editTextCity;
    MsmEditText editTextCountry;
    MsmEditText editTextPassword;
    MsmEditText editTextPasswordConfirm;
    TextView textViewRegMessage;
    TextView textViewPrivacy;
    Toolbar toolBar;

    Button btnSubmit;
    PreferenceUtil preferenceUtil;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_next);
        initView();
        setUiListener();
        setTermsOfUseUnderline();
        setLinkingOverActivity(textViewRegMessage, "Terms & Condition", "http://smartrestaurantsolutions.com/mobileapi-v2/chefonline_page/terms_conditions.html");
        setLinkingOverActivity(textViewPrivacy, "Privacy Policy", "http://smartrestaurantsolutions.com/mobileapi-v2/chefonline_page/privacy_policy.html");
        setLinkingOverActivity(textViewPrivacy, "Cookies Policy", "http://smartrestaurantsolutions.com/mobileapi-v2/chefonline_page/cookie_policy.html");
        Log.i("Get name dob", " " + getIntent().getExtras().getString("dob"));
        Log.i("Get name doa", " " + getIntent().getExtras().getString("doa"));

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    private void setUiListener() {
        btnSubmit.setOnClickListener(this);

    }

    private void initView() {

        toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle("Registration");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

     preferenceUtil = new PreferenceUtil(this);
     editTextPostCode = (MsmEditText) findViewById(R.id.editTextPostCode);
     editTextAddress1 = (MsmEditText) findViewById(R.id.editTextAddress1);
     editTextAddress2 = (MsmEditText) findViewById(R.id.editTextAddress2);
     editTextCity = (MsmEditText) findViewById(R.id.editTextCity);
     editTextCountry = (MsmEditText) findViewById(R.id.editTextCountry);
     editTextPassword = (MsmEditText) findViewById(R.id.editTextPassword);
     editTextPasswordConfirm = (MsmEditText) findViewById(R.id.editTextPasswordConfirm);

     btnSubmit = (Button) findViewById(R.id.btnSubmit);
     textViewRegMessage = (TextView) findViewById(R.id.textViewRegMessage);
     textViewPrivacy = (TextView) findViewById(R.id.textViewPrivacy);

    }

    private void setLinkingOverActivity(TextView view, String pattern, String URL) {
        Pattern myPattern = Pattern.compile(pattern);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        String newActivityURL = "chefonline://com.chefonline.online.WebViewActivity#" + URL + "#";
        Linkify.addLinks(view, myPattern, newActivityURL);
        view.setLinkTextColor(this.getResources().getColor(R.color.black));

    }

    private void setTermsOfUseUnderline() {
        SpannableString content = new SpannableString(textViewRegMessage.getText().toString());
        content.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 42, textViewRegMessage.getText().toString().length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        textViewRegMessage.setText(content);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    private void setDateTimeField() {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                Date strDate = null;
                try {
                    strDate = dateFormatter.parse(dateFormatter.format(newDate.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (new Date().before(strDate)) {
                    new CustomToast(RegistrationNextActivity.this, "Can not be greater than current date.", "", false);
                } else {
                    editTextDob.setText(dateFormatter.format(newDate.getTime()));
                }

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


    }

    //Set anniversary date picker
    private void setDateTimeField(int pickerNo) {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editTextAnniversary.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    }

    ProgressDialog progress;
    private void callRegistrationApi() {
        RequestQueue queue = Volley.newRequestQueue(RegistrationNextActivity.this);
        final GPSTracker gps = new GPSTracker(RegistrationNextActivity.this);
        String url = ConstantValues.BASE_API_URL;

        StringRequest myReq = new StringRequest(Request.Method.POST, url, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "8");
                params.put("title", getIntent().getExtras().getString("title"));
                params.put("fname", getIntent().getExtras().getString("first_name"));
                params.put("lname", getIntent().getExtras().getString("last_name"));
                params.put("email", getIntent().getExtras().getString("email"));
                params.put("mobile_no", getIntent().getExtras().getString("mobile_no"));
                params.put("telephone_no", getIntent().getExtras().getString("tel_no"));
                params.put("dob_date", getIntent().getExtras().getString("dob"));
                params.put("doa", getIntent().getExtras().getString("doa"));
                params.put("postcode", editTextPostCode.getText().toString().trim());
                params.put("address1", editTextAddress1.getText().toString().trim());
                params.put("address2", editTextAddress2.getText().toString().trim());
                params.put("city", editTextCity.getText().toString().trim());
                params.put("country", editTextCountry.getText().toString().trim());
                params.put("password", editTextPassword.getText().toString().trim());
                params.put("ip_address", getIntent().getExtras().getString("ip"));
                params.put("platform", "2");
                Log.i("Reg Param", "" + params.toString());
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);
        progress = new ProgressDialog(this);
        progress.setMessage("Please wait....");
        progress.show();
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try{
                    progress.dismiss();
                    new CustomToast(RegistrationNextActivity.this, "Server Response Error.", "", false);
                }
                catch (Exception e) {
                    Log.e("After volley execution",e.getMessage());
                }
            }
        };
    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progress.dismiss();

                    Log.e("Response Reg", "" + response);
                    UserData userData = new UserData();
                    userData = JsonParser.parseRegistrationData(response);
                     if ("Success".equalsIgnoreCase(userData.getLoginStatus())) {
                         preferenceUtil.setLogInStatus(1); // set login status 1
                         preferenceUtil.setUserID(userData.getUserId());
                         preferenceUtil.setUserTitle(userData.getTitle());
                         preferenceUtil.setUserFirstName(userData.getFirstName());
                         preferenceUtil.setUserSurName(userData.getSurName());
                         preferenceUtil.setUserLastName(userData.getLastName());
                         preferenceUtil.setUserAddress1(userData.getAddress1());
                         preferenceUtil.setUserAddress2(userData.getAddress2());
                         preferenceUtil.setUserMobile(userData.getMobileNo());
                         preferenceUtil.setUserEmail(userData.getEmailId());
                         preferenceUtil.setUserDob(userData.getDob());
                         preferenceUtil.setUserDoa(userData.getDoa());
                         preferenceUtil.setUserGroupId(userData.getGroupId());
                         preferenceUtil.setUserPostCode(userData.getPostCode());
                         preferenceUtil.setUserTel(userData.getTelNo());
                         preferenceUtil.setUserTown(userData.getCity());
                         preferenceUtil.setUserCountry(userData.getCountry());

                         new CustomToast(RegistrationNextActivity.this, userData.getLoginMessage(), "", false);
                         if(RegistrationActivity.instance != null) {
                             try {
                                 RegistrationActivity.instance.finish();
                             } catch (Exception e) {}
                         }

                         finish();
                         /*finish();
                         new CustomToast(RegistrationNextActivity.this, userData.getLoginMessage(), "", false);*/
                     } else {
                         new CustomToast(RegistrationNextActivity.this, "You are already registered.", "", false);
                     }

                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                        new CustomToast(RegistrationNextActivity.this, "Sorry, something went wrong.", "", false);
                    }
                    catch (Exception e1){
                        Log.e("After volley execution",e1.getMessage());
                    }
                }

            }

        };
    }

    private boolean  validation() {

        if (editTextPostCode.getText().toString().trim().equalsIgnoreCase("")) {
            editTextPostCode.setErrorTextVisible(true);
            editTextPostCode.setErrorMsg("Your Postcode is required.");
            editTextPostCode.setTypingFocus();
            return false;
        }

        if (!UtilityMethod.postcodeValidation(editTextPostCode.getText().toString().trim().toUpperCase())) {
            editTextPostCode.setErrorTextVisible(true);
            editTextPostCode.setErrorMsg("Your Postcode is invalid.");
            editTextPostCode.setTypingFocus();
            return false;
        }

        if(editTextCountry.getText().toString().trim().equalsIgnoreCase(""))
        {
            editTextCountry.setErrorTextVisible(true);
            editTextCountry.setErrorMsg("Your Country is required.");
            editTextCountry.setTypingFocus();
            return false;
        }

        if(editTextCity.getText().toString().trim().equalsIgnoreCase(""))
        {
            editTextCity.setErrorTextVisible(true);
            editTextCity.setErrorMsg("Your City is required.");
            editTextCity.setTypingFocus();
            return false;
        }

        if (editTextAddress1.getText().toString().trim().equalsIgnoreCase("")) {
            editTextAddress1.setErrorTextVisible(true);
            editTextAddress1.setErrorMsg("Your Address1 is required.");
            editTextAddress1.setTypingFocus();
            return false;
        }

        if (UtilityMethod.isWhiteSpaces(editTextPassword.getText().toString())) {
            editTextPassword.setErrorTextVisible(true);
            editTextPassword.setErrorMsg("White Space is not accepted.");
            editTextPassword.setTypingFocus();
            return false;
        }

        if (editTextPassword.getText().toString().length() < 6 || editTextPassword.getText().toString().length() > 16) {
            editTextPassword.setErrorTextVisible(true);
            editTextPassword.setErrorMsg("Password should be between 6 to 16 character.");
            editTextPassword.setTypingFocus();
            return false;
        }

        if (editTextPassword.getText().toString().trim().equalsIgnoreCase("")) {
            editTextPassword.setErrorTextVisible(true);
            editTextPassword.setErrorMsg("Your password is required.");
            editTextPassword.setTypingFocus();
            return false;
        }

        if (UtilityMethod.isWhiteSpaces(editTextPasswordConfirm.getText().toString())) {
            editTextPasswordConfirm.setErrorTextVisible(true);
            editTextPasswordConfirm.setErrorMsg("White Space is not accepted.");
            editTextPasswordConfirm.setTypingFocus();
            return false;
        }

        if (editTextPasswordConfirm.getText().trim().equalsIgnoreCase("")) {
            editTextPasswordConfirm.setErrorTextVisible(true);
            editTextPasswordConfirm.setErrorMsg("Your confirm password is required.");
            editTextPasswordConfirm.setTypingFocus();
            return false;
        }

        if (!editTextPasswordConfirm.getText().trim().equalsIgnoreCase(editTextPassword.getText().toString().trim())) {
            editTextPasswordConfirm.setErrorTextVisible(true);
            editTextPasswordConfirm.setErrorMsg("Password mismatch.");
            editTextPasswordConfirm.setTypingFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSubmit:
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                editTextPassword.setTypingFocus();
                if (validation()) {
                    callRegistrationApi();
                }
                break;
            case R.id.imageViewDob:
                setDateTimeField();
                fromDatePickerDialog.show();
                break;
            case R.id.imageViewAnnivDate:
                setDateTimeField(1);
                fromDatePickerDialog.show();
                break;

        }
    }

}
