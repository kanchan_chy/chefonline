package com.chefonline.online;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import com.chefonline.customview.CustomToast;


public class WebViewActivity extends ActionBarActivity implements OnClickListener{
    private TextView toolbarTitle;
    private ImageButton imgBtnBack;
    private static final String TAG = "WebViewActivity";
    private ProgressDialog progressBar;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        intiView();
        loadData();

    }

    private void loadWebView(String URL) {
        WebView webview= (WebView) findViewById(R.id.webView);

        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        progressBar = ProgressDialog.show(this, "Please wait", "Loading...");
        progressBar.setCanceledOnTouchOutside(true);


        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i("WebView", "Processing webview url click...");
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                Log.i(TAG, "Finished loading URL: " +url);
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e("Error page load", "Error: " + description);
                new CustomToast(WebViewActivity.this, "Oops! " + description,"" , false);
                alertDialog.setTitle("Error");
                alertDialog.setMessage(description);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                alertDialog.show();
            }
        });

        webview.loadUrl(URL);
    }

    private void intiView() {
        imgBtnBack = (ImageButton) findViewById(R.id.imgBtnBack);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        imgBtnBack.setOnClickListener(this);

    }

    private void loadData() {
        if (getIntent().getData() != null) {
            Log.e("in if", "-----");
            Uri uri = getIntent().getData();
            String URL = uri.toString().split("#")[1];
            String tagName = uri.toString().split("#")[2];
            Log.i("Total Tag", "-----" + uri.toString());
            Log.i("Tag", "-----" + tagName);
            Log.i("URL", "-----" + URL);
            toolbarTitle.setText(tagName);
            loadWebView(URL);

        } else if (getIntent().getExtras() != null) {
           // Log.e("I am in else if with tag name", "----" + tagName);
            //tagName = getIntent().getExtras().getString("HASH_TAG");
            //callForumsListApi();

        } else {
            //Log.e("in else else else", "-----" + tagName);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reservation_confirm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBtnBack:
                onBackPressed();
                break;
        }
    }
}
