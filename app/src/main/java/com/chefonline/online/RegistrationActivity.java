package com.chefonline.online;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.chefonline.adapter.TitlesDialogAdapter;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.MsmEditText;
import com.chefonline.datamodel.LocationData;
import com.chefonline.modelinterface.PlaceOrderView;
import com.chefonline.presenter.PlaceOrderPresenter;
import com.chefonline.utility.UtilityMethod;

import java.util.ArrayList;
import java.util.List;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, PlaceOrderView{
    private static String TAG = "RegistrationActivity";
    MsmEditText editTextFirstName;
    MsmEditText editTextLastName;
    MsmEditText editTextEmail;
    MsmEditText editTextMobileNo;
    MsmEditText editTextTel;
    Toolbar toolBar;

    TextView txtIp;
    private ProgressDialog pDialog;

    Spinner spinnerMonthDOB;
    Spinner spinnerMonthDOA;
    Spinner spinnerDateDOB;
    Spinner spinnerDateDOA;

    Button btnSubmit,btnTitle;
    PreferenceUtil preferenceUtil;

    String mIP="";
    String title="";
    String[] allTitles;

    public static RegistrationActivity instance = null;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_registration_new);
        initView();
        setUiListener();

        PlaceOrderPresenter placeOrderPresenter = new PlaceOrderPresenter(this);
        placeOrderPresenter.getRealIPAddress(getApplicationContext(), "44");

    }

    @Override
    public void finish() {
        super.finish();
        instance = null;
    }

    private void setUiListener() {
        btnSubmit.setOnClickListener(this);
        btnTitle.setOnClickListener(this);

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void initView() {

        toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle("Registration");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pDialog = new ProgressDialog(this);
        pDialog.setCanceledOnTouchOutside(false);

        preferenceUtil = new PreferenceUtil(this);
        editTextFirstName = (MsmEditText) findViewById(R.id.editTextFirstName);
        editTextLastName = (MsmEditText) findViewById(R.id.editTextLastName);
        editTextEmail = (MsmEditText) findViewById(R.id.editTextEmail);
        editTextMobileNo = (MsmEditText) findViewById(R.id.editTextMobileNo);
        editTextTel = (MsmEditText) findViewById(R.id.editTextTel);

        txtIp=(TextView)findViewById(R.id.txtIp);

        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnTitle = (Button) findViewById(R.id.btnTitle);

        spinnerDateDOB= (Spinner) findViewById(R.id.spinnerDateDOB);
        spinnerDateDOA = (Spinner) findViewById(R.id.spinnerDateDOA);

        spinnerMonthDOB = (Spinner) findViewById(R.id.spinnerMonthDOB);
        spinnerMonthDOB.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapterSpinnerMonthDOB = ArrayAdapter.createFromResource(this,R.array.months, R.layout.spinner_item);
        adapterSpinnerMonthDOB.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMonthDOB.setAdapter(adapterSpinnerMonthDOB);

        spinnerMonthDOA = (Spinner) findViewById(R.id.spinnerMonthDOA);
        spinnerMonthDOA.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapterSpinnerMonthDOA = ArrayAdapter.createFromResource(this,R.array.months, R.layout.spinner_item);
        adapterSpinnerMonthDOA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMonthDOA.setAdapter(adapterSpinnerMonthDOA);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean  validation() {
        if (editTextFirstName.getText().toString().trim().equalsIgnoreCase("")) {
            editTextFirstName.setErrorTextVisible(true);
            editTextFirstName.setErrorMsg("Your First Name is required.");
            editTextFirstName.setTypingFocus();
            return false;
        }

        if (editTextLastName.getText().toString().trim().equalsIgnoreCase("")) {
            editTextLastName.setErrorTextVisible(true);
            editTextLastName.setErrorMsg("Your Last Name is required.");
            editTextLastName.setTypingFocus();
            return false;
        }

        if (editTextEmail.getText().toString().trim().equalsIgnoreCase("")) {
            editTextEmail.setErrorTextVisible(true);
            editTextEmail.setErrorMsg("Your Email is required.");
            editTextEmail.setTypingFocus();
            return false;
        }

        if (editTextMobileNo.getText().toString().trim().equalsIgnoreCase("")) {
            editTextMobileNo.setErrorTextVisible(true);
            editTextMobileNo.setErrorMsg("Your Mobile no is required.");
            editTextMobileNo.setTypingFocus();
            return false;
        }

        if (!UtilityMethod.isEmailValid(editTextEmail.getText().toString().trim())) {
            editTextEmail.setErrorTextVisible(true);
            editTextEmail.setErrorMsg("Your Email is not valid.");
            editTextEmail.setTypingFocus();
            return false;
        }

        if (!UtilityMethod.isMobileNoValid(editTextMobileNo.getText().toString().trim())) {
            editTextMobileNo.setErrorTextVisible(true);
            editTextMobileNo.setErrorMsg("Invalid mobile number.");
            editTextMobileNo.setTypingFocus();
            return false;
        }

        if (editTextMobileNo.getText().toString().trim().equalsIgnoreCase("")) {
            editTextMobileNo.setErrorTextVisible(true);
            editTextMobileNo.setErrorMsg("Mobile no is required.");
            editTextMobileNo.setTypingFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSubmit:
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                if (validation()) {
                    String dob;
                    String doa;

                    if (spinnerMonthDOB.getSelectedItem().toString().equalsIgnoreCase("Month") ||
                            spinnerDateDOB.getSelectedItem().toString().equalsIgnoreCase("Date")) {
                        dob = "";
                    } else {
                        dob = UtilityMethod.getNoOfMonth(spinnerMonthDOB.getSelectedItem().toString()) + "-" + spinnerDateDOB.getSelectedItem().toString();
                    }

                    if (spinnerMonthDOA.getSelectedItem().toString().equalsIgnoreCase("Month") ||
                            spinnerDateDOA.getSelectedItem().toString().equalsIgnoreCase("Date")) {
                        doa = "";
                    } else {
                        doa = UtilityMethod.getNoOfMonth(spinnerMonthDOA.getSelectedItem().toString()) + "-" + spinnerDateDOA.getSelectedItem().toString();
                    }
                    Intent intent = new Intent(this, RegistrationNextActivity.class);
                    intent.putExtra("first_name", editTextFirstName.getText().toString().trim());
                    intent.putExtra("last_name", editTextLastName.getText().toString().trim());
                    intent.putExtra("email", editTextEmail.getText().toString().trim());
                    intent.putExtra("mobile_no", editTextMobileNo.getText().toString().trim());
                    intent.putExtra("tel_no", editTextTel.getText().toString().trim());
                    intent.putExtra("title", title);
                    intent.putExtra("dob", dob);
                    intent.putExtra("doa", doa);
                    intent.putExtra("ip", mIP);
                    startActivity(intent);
                }

                break;
            case R.id.btnTitle:
                showTitleListDialog();
                break;
            case R.id.imageViewDob:
                break;
            case R.id.imageViewAnnivDate:
                break;

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        List<String> words = new ArrayList<>();
        words.add("Date");
        if (position != 0) {
            int count = UtilityMethod.daysInMonth(parent.getItemAtPosition(position).toString());
            for (int i = 1; i <= count; i++) {
                if (i <= 9) {
                    words.add("0" + i);
                } else {
                    words.add("" + i);
                }
            }

        }

        switch (parent.getId()) {
            case R.id.spinnerMonthDOB:
                if (words.size() == 0) {
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.planets_array, R.layout.spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerDateDOB.setAdapter(adapter);
                } else {
                    ArrayAdapter<String> adapter;
                    adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, words);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerDateDOB.setAdapter(adapter);
                }
                break;
            case R.id.spinnerMonthDOA:
                if (words.size() == 0) {
                    ArrayAdapter<CharSequence> adapterSpinnerDateDOA = ArrayAdapter.createFromResource(this, R.array.planets_array, R.layout.spinner_item);
                    adapterSpinnerDateDOA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerDateDOA.setAdapter(adapterSpinnerDateDOA);
                } else {
                    ArrayAdapter<String> adapterSpinnerDateDOA = new ArrayAdapter<String>(this, R.layout.spinner_item, words);
                    adapterSpinnerDateDOA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerDateDOA.setAdapter(adapterSpinnerDateDOA);
                }

                break;

        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void showTitleListDialog()
    {
        final Dialog dialog = new Dialog(RegistrationActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_title_list);

        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);

        ListView listView = (ListView) dialog.findViewById(R.id.listViewOptions);
        allTitles=getResources().getStringArray(R.array.titles);
        final TitlesDialogAdapter adapter = new TitlesDialogAdapter(RegistrationActivity.this, allTitles);
        listView.setAdapter(adapter);

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                title=allTitles[position];
                btnTitle.setText(title);
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @Override
    public void setLoadingVisible() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait....");
        pDialog.show();
    }

    @Override
    public void setLoadingHide() {
        if (pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    public void showToast(String message, String subMessage, boolean isButtonOk) {

    }

    @Override
    public void setLocationData(LocationData locationData) {
        mIP=locationData.getIpAddress()==null ? "" : locationData.getIpAddress();
        if(!mIP.equals("")){
            txtIp.setText("Your IP Address "+mIP+", We Use This IP For Registration and Verification Purpose");
            txtIp.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void visiblePayPalButton() {

    }

    @Override
    public void verifyCode(String code, boolean isPaypal) {

    }

    @Override
    public void orderCompleted(String orderId, String transactionId, boolean isPaypal) {

    }

    @Override
    public void paymentStatusUpdated(boolean isSuccess) {

    }

    @Override
    public void onVerificationFailed(boolean isPaypal) {

    }

    @Override
    public void retryPayment(boolean isPaypal) {

    }

    @Override
    public void isNetpayChargeFound(boolean isFound) {

    }
}
