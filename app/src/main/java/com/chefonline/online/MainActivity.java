package com.chefonline.online;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.fragment.NewOffersFragment;
import com.chefonline.fragment.SearchFragment;
import com.chefonline.fragment.SettingsFragment;
import com.chefonline.fragment.TakeawayCategoryFragment;
import com.chefonline.utility.ConstantValues;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static String TAG = "SearchActivity";
    public int CURRENT_FRAGMENT_NO = 0;
    private String restaurantId = "";
    private String collectionTime = "";
    private String deliveryTime = "";
    private int selectedPolicy = 0;
    public static ImageButton imgBtnBack, imgBtnBackSearch;
    private Button btnTabSearch, btnTabSettings, btnTabOffers, btnTabOrder, btnTabVoiceOrder;
    public static TextView txtViewBubble;
    private DataBaseUtil db;
    private long mLastClickTime = 0;

    @TargetApi(21)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initView();
        initTabView();
        loadData();
        loadDirectCategoryFragment(); // when user select restaurant from order history

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.findItem(R.id.action_live).setVisible(false);
        //invalidateOptionsMenu();
        /*notifCount = (Button) count.findViewById(R.id.notif_count);
        notifCount.setText(String.valueOf(mNotifCount));*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            //get.setDisplayHomeAsUpEnabled(false);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    private void loadDirectCategoryFragment() {
        try {
            if (getIntent().getExtras().getInt(ConstantValues.NAVIGATION_KEY, 0) == ConstantValues.FRAGMENT_CATEGORY) {
                restaurantId = getIntent().getExtras().getString("restaurant_id");
                selectedPolicy = getIntent().getExtras().getInt("selected_policy");
                collectionTime = getIntent().getExtras().getString("collection_time");
                deliveryTime = getIntent().getExtras().getString("delivery_time");
                makeTabSelection(1);
                ConstantValues.SELECTED_BUTTON = selectedPolicy;
                loadCategoryFragmentAlone(0);
                ///imgBtnBack.setVisibility(View.INVISIBLE);
            }
        } catch (Exception e) {
            makeTabSelection(1);
            loadSearchFragment();
            Log.e(TAG, "" + e.getMessage());
        }
    }

    private void initTabView() {
        btnTabSearch = (Button) findViewById(R.id.btnTabSearch);
        btnTabSettings = (Button) findViewById(R.id.btnTabSettings);
        btnTabOrder = (Button) findViewById(R.id.btnTabOrder);
        btnTabOffers = (Button) findViewById(R.id.btnTabOffers);
        txtViewBubble = (TextView) findViewById(R.id.txtViewBubble);

        btnTabSearch.setOnClickListener(this);
        btnTabSettings.setOnClickListener(this);
        btnTabOffers.setOnClickListener(this);
        btnTabOrder.setOnClickListener(this);
    }

    public void initView() {
        //toolBarLayout = (RelativeLayout) findViewById(R.id.toolbar_top);

    }

    /***
     * Set adapter to hot listview  **/
    private void loadData() {
        db = new DataBaseUtil(this);

    }

    private void makeTabSelection(int itemNo) {
        if (itemNo == 1) {
            btnTabSearch.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.search, 0, 0);
            btnTabOffers.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.offers_selected, 0, 0);
            btnTabSettings.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.setting_selected, 0, 0);
            btnTabOrder.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cart_white_selected, 0, 0);
            btnTabSearch.setTextColor(getResources().getColor(R.color.white));
            btnTabOffers.setTextColor(getResources().getColor(R.color.selected_off_white));
            btnTabSettings.setTextColor(getResources().getColor(R.color.selected_off_white));
            btnTabOrder.setTextColor(getResources().getColor(R.color.selected_off_white));

        } else if (itemNo == 2) {
            btnTabSearch.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.search_selected, 0, 0);
            btnTabOffers.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.offers_selected, 0, 0);
            btnTabSettings.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.setting, 0, 0);
            btnTabOrder.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cart_white_selected, 0, 0);
            btnTabSearch.setTextColor(getResources().getColor(R.color.selected_off_white));
            btnTabOffers.setTextColor(getResources().getColor(R.color.selected_off_white));
            btnTabSettings.setTextColor(getResources().getColor(R.color.white));
            btnTabOrder.setTextColor(getResources().getColor(R.color.selected_off_white));

        } else if (itemNo == 3) {
            btnTabSearch.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.search_selected, 0, 0);
            btnTabOffers.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.offers, 0, 0);
            btnTabSettings.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.setting_selected, 0, 0);
            btnTabOrder.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cart_white_selected, 0, 0);
            btnTabSearch.setTextColor(getResources().getColor(R.color.selected_off_white));
            btnTabOffers.setTextColor(getResources().getColor(R.color.white));
            btnTabSettings.setTextColor(getResources().getColor(R.color.selected_off_white));
            btnTabOrder.setTextColor(getResources().getColor(R.color.selected_off_white));
        } else if (itemNo == 4) {
            btnTabSearch.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.search_selected, 0, 0);
            btnTabOffers.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.offers_selected, 0, 0);
            btnTabSettings.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.setting_selected, 0, 0);
            btnTabOrder.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cart_white, 0, 0);
            btnTabSearch.setTextColor(getResources().getColor(R.color.selected_off_white));
            btnTabOffers.setTextColor(getResources().getColor(R.color.selected_off_white));
            btnTabSettings.setTextColor(getResources().getColor(R.color.selected_off_white));
            btnTabOrder.setTextColor(getResources().getColor(R.color.white));
        }
    }

    private void loadSearchFragment() {
        //this.findViewById(R.id.toolbar_title1).setVisibility(View.GONE);
        getSupportActionBar().setTitle("");
        this.findViewById(R.id.toolbar_title1).setVisibility(View.VISIBLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        CURRENT_FRAGMENT_NO = ConstantValues.FRAGMENT_SEARCH;
        android.app.Fragment fragment = null;
        fragment = new SearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        int count = frgManager.getBackStackEntryCount();
        for(int i = 0; i < count; ++i) {
            frgManager.popBackStackImmediate();
        }
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        //ft.addToBackStack(null);
       // ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_left);
        ft.replace(R.id.content_frame, fragment, "SEARCH_FRAGMENT");
        ft.commit();
    }

    private void loadSettingsFragment() {
        //this.findViewById(R.id.toolbar_title1).setVisibility(View.GONE);
        CURRENT_FRAGMENT_NO = ConstantValues.FRAGMENT_SETTINGS;
        android.app.Fragment fragment = null;
        fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        int count = frgManager.getBackStackEntryCount();
        for(int i = 0; i < count; ++i) {
            frgManager.popBackStackImmediate();
        }
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        //ft.addToBackStack(null);
        //ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }

    private void loadOffersFragment() {
        //this.findViewById(R.id.toolbar_title1).setVisibility(View.GONE);
        CURRENT_FRAGMENT_NO = ConstantValues.FRAGMENT_OFFERS;
        android.app.Fragment fragment = null;
        fragment = new NewOffersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        int count = frgManager.getBackStackEntryCount();
        for(int i = 0; i < count; ++i) {
            frgManager.popBackStackImmediate();
        }
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        //ft.addToBackStack(null);
        //ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }

    private void loadCategoryFragmentAlone(int position) {
        //this.findViewById(R.id.toolbar_title1).setVisibility(View.GONE);
        CURRENT_FRAGMENT_NO = ConstantValues.FRAGMENT_CATEGORY;
        android.app.Fragment fragment = null;
        fragment = new TakeawayCategoryFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putInt("is_searched_list", 3);
        args.putString("restaurant_id", restaurantId);
        args.putInt("selected_policy", selectedPolicy);
        args.putString("delivery_time", deliveryTime);
        args.putString("collection_time", collectionTime);

        fragment.setArguments(args);
        FragmentManager frgManager = getFragmentManager();
        int count = frgManager.getBackStackEntryCount();
        for(int i = 0; i < count; ++i) {
            frgManager.popBackStackImmediate();
        }
        android.app.FragmentTransaction ft = frgManager.beginTransaction();
        //ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_left);
        ft.add(R.id.content_frame, fragment);
        ft.commit();
    }

    private void openDialog(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_restuarant_override);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        ImageButton imgBtnClose = (ImageButton) dialog.findViewById(R.id.imgBtnClose);
        txtViewPopupMessage.setText(message);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(okButton);

        // if button is clicked, close the custom dialog
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.open();
                db.emptyCartData();
                txtViewBubble.setText("" + db.sumCartData());
                dialog.dismiss();
                db.close();
                finish();
            }

        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setText(cancelButton);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getFragmentManager();
        int count = manager.getBackStackEntryCount();
        Log.i(TAG, "Found fragment: " + count);

        if (count == 1) {
            ConstantValues.SELECTED_BUTTON = 0;
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            this.findViewById(R.id.toolbar_title1).setVisibility(View.VISIBLE);

            if (CURRENT_FRAGMENT_NO == ConstantValues.FRAGMENT_SEARCH) {
               manager.popBackStack(); return;
            }

        }

        if(count == 0) {
            db.open();
            if (CURRENT_FRAGMENT_NO != ConstantValues.FRAGMENT_SEARCH) {
                makeTabSelection(1);
                loadSearchFragment();
                return;
            } else if (db.sumCartData() > 0) {
                openDialog("Are you sure want to quit now?", "YES", "NO");
                return;
            }
            db.close();
            super.onBackPressed();

        } else{
            hideKeyboard();
            //super.onBackPressed();
            manager.popBackStack();
        }
    }

    @Override
    protected void onDestroy() {
        ConstantValues.SELECTED_BUTTON = 0;
        super.onDestroy();
        //AppData.getInstance().getHotRestaurantDataArrayList().clear();
    }

    @Override
    protected void onResume() {
        db.open();
        try {
            txtViewBubble.setText("" + db.sumCartData());
            PreferenceUtil preferenceUtil = new PreferenceUtil(this);
            if (preferenceUtil.getLogInStatus() == 1) {
                SettingsFragment.btnLogin.setText("Logout");
                SettingsFragment.btnLogin.setCompoundDrawablesWithIntrinsicBounds(R.drawable.logout, 0, 0, 0);
                SettingsFragment.btnMyAccount.setVisibility(View.VISIBLE);
                SettingsFragment.btnResetPassword.setVisibility(View.VISIBLE);
                SettingsFragment.btnOrderHistory.setVisibility(View.VISIBLE);
            } else {
                SettingsFragment.btnLogin.setText("Login");
                SettingsFragment.btnLogin.setCompoundDrawablesWithIntrinsicBounds(R.drawable.login, 0, 0, 0);
                SettingsFragment.btnMyAccount.setVisibility(View.GONE);
                SettingsFragment.btnResetPassword.setVisibility(View.GONE);
                SettingsFragment.btnOrderHistory.setVisibility(View.GONE);
            }

        } catch (Exception e) {

        }

        db.close();
        super.onResume();
    }

    @TargetApi (21)
    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        switch (v.getId()) {
            case R.id.btnTabSearch:
                makeTabSelection(1);
                loadSearchFragment();
                break;

            case R.id.btnTabSettings:
                makeTabSelection(2);
                loadSettingsFragment();
                break;

            case R.id.btnTabOrder:
                db.open();
                if (db.sumCartData() > 0) {
                   // makeTabSelection(4);
                    ConstantValues.IS_REORDER = false;
                    Intent intentCart = new Intent(this, OrderHostActivity.class);
                    startActivity(intentCart);
                } else {
                    new CustomToast(this, "Your cart is empty.","" , false);
                }
                db.close();
                break;

            case R.id.btnTabOffers:
                makeTabSelection(3);
                loadOffersFragment();
                break;

            case R.id.imgBtnBack:
                onBackPressed();
                break;

        }
    }

}

