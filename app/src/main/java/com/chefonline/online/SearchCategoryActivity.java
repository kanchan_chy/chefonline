package com.chefonline.online;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.adapter.SearchItemAdapter;
import com.chefonline.datamodel.CartData;
import com.chefonline.datamodel.DishData;
import com.chefonline.db.DataBaseUtil;
import com.chefonline.pattern.AppData;
import com.chefonline.utility.ConstantValues;
import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by masum on 07/05/2015.
 */
public class SearchCategoryActivity extends ActionBarActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static String TAG = "SearchCategoryFragment";
    public int position = 0;
    public int is_searched_list = 0;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    private TextView txtViewCategoryName;
    public static EditText editTextSearch;
    public static  TextView txtViewBubble;
    private ListView lstViewCategorySearched;
    private FloatingGroupExpandableListView mListViewCategory;
    private ImageButton imgBtnCatMenu, imgBtnBack, imgBtnSearch;
    private RelativeLayout rltvMenuBtnHolder;
    private LinearLayout linear_menu;
    private View headerView;
    private ProgressBar progressBar;

    private SearchItemAdapter searhItemAdapter;
    private DataBaseUtil db;
    View rootView;
    private ArrayList<DishData> dishNewData = new ArrayList<>();
    private long mLastClickTime = 0;

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "ON CREATE VIEW");
        setContentView(R.layout.fragment_category_search);
        getIntentValue();
        initView(rootView);
        instantiateObject();
        setUiItemListener();
        loadData();
    }



    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }



    private void instantiateObject() {
    }

    private void setUiItemListener() {
        imgBtnSearch.setOnClickListener(this);

        lstViewCategorySearched.setOnItemClickListener(this);
        mListViewCategory.setOnItemClickListener(this);

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                String text = editTextSearch.getText().toString().toLowerCase(Locale.getDefault());
                Log.i("Text", "** " + text);

                if (text.length() == 0) {
                    //this.dishDatas.addAll(dishDatasTemp);
                    dishNewData = AppData.getInstance().getDishDatas();
                    Log.e("SEARCHED ITEM GET", "SIZE***** " + AppData.getInstance().getDishDatas().size());
                    searhItemAdapter.notifyDataSetChanged();
                    lstViewCategorySearched.setAdapter(searhItemAdapter);

                } else {
                    searhItemAdapter.filter(text);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });

    }

    private void loadData() {
        this.db = new DataBaseUtil(this);
        this.db.open();
        ArrayList<CartData> cartDatas = new ArrayList<>();
        AppData.getInstance().setCartDatas(cartDatas);
        /** End cart data fetching */

        lstViewCategorySearched.invalidateViews();
        ConstantValues.DISH_DATA = AppData.getInstance().getDishDatas();
        dishNewData = AppData.getInstance().getDishDatas();
        Log.e("SEARCHED ITEM GET", "SIZE***** " + AppData.getInstance().getDishDatas().size());
        Log.e(TAG, "Restaurant id ***** " + Integer.toString(AppData.getInstance().getRestaurantInfoData().getRestaurantId()));
        searhItemAdapter = new SearchItemAdapter(this, AppData.getInstance().getDishDatas(), Integer.toString(AppData.getInstance().getRestaurantInfoData().getRestaurantId()));
        lstViewCategorySearched.setAdapter(searhItemAdapter);

        try {
            editTextSearch.setText(getIntent().getExtras().getString("result"));
            searhItemAdapter.filter(getIntent().getExtras().getString("result"));
        } catch (Exception e) {

        }

       // callDishApi();

    }
    /** Init view method*/
    public void initView(View rootView) {
        txtViewCategoryName = (TextView) findViewById(R.id.txtViewCategoryName);
        txtViewBubble = (TextView) findViewById(R.id.txtViewBubble);
        imgBtnBack = (ImageButton) findViewById(R.id.imgBtnBack);
        imgBtnSearch = (ImageButton)findViewById(R.id.imgBtnSearch);
        editTextSearch = (EditText) findViewById(R.id.editTextSearch);

        lstViewCategorySearched = (ListView) findViewById(R.id.lstViewCategorySearched1);

        mListViewCategory = (FloatingGroupExpandableListView) findViewById(R.id.lstViewCategory);
        /** Instantiate header item of expandable item list view */
        LayoutInflater inflater = (LayoutInflater)  getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        headerView = inflater.inflate(R.layout.header_dishes_list, null);

        //headerView = this.getLayoutInflater().inflate(R.layout.header_dishes_list, null);
        mListViewCategory.addHeaderView(headerView);
        //lstViewCategorySearched.addHeaderView(headerView);

        View footerView =  getLayoutInflater().inflate(R.layout.footer_fake, null);
        mListViewCategory.addFooterView(footerView);
        mListViewCategory.setVisibility(View.GONE);

        lstViewCategorySearched.setVisibility(View.VISIBLE);
        lstViewCategorySearched.addFooterView(footerView);
        /** End header item load */

        rltvMenuBtnHolder = (RelativeLayout) findViewById(R.id.relative_top_menu);
        linear_menu = (LinearLayout) findViewById(R.id.linear_menu);
        rltvMenuBtnHolder.setVisibility(View.VISIBLE);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        /** search view load */
        mListViewCategory.setVisibility(View.GONE);
        lstViewCategorySearched.setVisibility(View.VISIBLE);
        imgBtnSearch.setBackgroundResource(R.drawable.close_search);
        editTextSearch.setVisibility(View.VISIBLE);
        linear_menu.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        /* End Search view */
    }

    public String getIntentValue() {
        //position = getIntent().getExtras().getInt("position");
        //is_searched_list = getIntent().getExtras().getInt("is_searched_list");
        String x = getIntent().getExtras().getString("result");
        Log.i("Restaurant_Id ", "***** " + x);
        return x;
    }

    @Override
    public void onResume() {
        //txtViewBubble.setText("" + db.sumCartData());
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBtnSearch:
                onBackPressed();
                editTextSearch.setText("");
                AppData.getInstance().hideKeyboard(this, v);
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //lastClickedPosition = position;
    }

}