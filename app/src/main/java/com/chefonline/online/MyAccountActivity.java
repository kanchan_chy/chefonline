package com.chefonline.online;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.adapter.TitlesDialogAdapter;
import com.chefonline.com.chefonline.sharedpref.PreferenceUtil;
import com.chefonline.customview.CustomToast;
import com.chefonline.customview.MsmEditText;
import com.chefonline.datamodel.UserData;
import com.chefonline.jsonparser.JsonParser;
import com.chefonline.utility.ConstantValues;
import com.chefonline.utility.UtilityMethod;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MyAccountActivity extends AppCompatActivity implements View.OnClickListener{
    private static String TAG = "MyAccountFragment";
    MsmEditText editTextFirstName;
    MsmEditText editTextLastName;
    MsmEditText editTextEmail;
    MsmEditText editTextMobileNo;
    MsmEditText editTextTel;
    MsmEditText editTextDob ;
    MsmEditText editTextAnniv;
    MsmEditText editTextPostCode;
    MsmEditText editTextAddress1;
    MsmEditText editTextAddress2;
    MsmEditText editTextCity;
    MsmEditText editTextCountry;
    MsmEditText editTextPassword;
    Button btnSubmit;
    Button btnTitle;
    ImageView imageViewBtnDob;
    ImageView imageViewBtnDoa;
    TextView txtAnniversaryMessage;
    TextView txtDobMessage;
    Toolbar toolBar;

    PreferenceUtil preferenceUtil;

    Spinner spinnerMonthDOB;
    Spinner spinnerMonthDOA;
    Spinner spinnerDateDOB;
    Spinner spinnerDateDOA;
    LinearLayout linearDOASpinnerHolder;
    LinearLayout linearDOBSpinnerHolder;
    String dob;
    String doa;
    String title = "";
    private long mLastClickTime = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_my_account);
        initView();
        setUiListener();
        onItemClickListen();
        loadView();

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setUiListener() {
        btnSubmit.setOnClickListener(this);
        btnTitle.setOnClickListener(this);
        //imageViewBtnDob.setOnClickListener(this);
        //imageViewBtnDoa.setOnClickListener(this);
        editTextDob.setOnClickListener(this);
        editTextAnniv.setOnClickListener(this);

        editTextDob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    setDateTimeField();
                    fromDatePickerDialog.show();

                }
                return true;
            }
        });

    }

    private void loadView() {
        editTextFirstName.setText(preferenceUtil.getUserFirstName());
        editTextLastName.setText(preferenceUtil.getUserLastName());
        editTextCity.setText(preferenceUtil.getUserTown());
        editTextAddress1.setText(preferenceUtil.getUserAddress());
        editTextAddress2.setText(preferenceUtil.getUserAddress2());
        editTextEmail.setText(preferenceUtil.getUserEmail());
        editTextCountry.setText(preferenceUtil.getUserCountry());
        editTextMobileNo.setText(preferenceUtil.getUserMobile());
        editTextPostCode.setText(preferenceUtil.getUserPostCode());
        editTextTel.setText(preferenceUtil.getUserTel());

        editTextMobileNo.setEnable(false);

        linearDOBSpinnerHolder = (LinearLayout) findViewById(R.id.linearDOBSpinnerHolder);
        linearDOASpinnerHolder = (LinearLayout) findViewById(R.id.linearDOASpinnerHolder);

        title = preferenceUtil.getUserTitle();
        if(!title.equals("")) {
            btnTitle.setText(title);
        }

        if (preferenceUtil.getUserDob().contains("-")) {
            linearDOBSpinnerHolder.setVisibility(View.GONE);
            txtDobMessage.setVisibility(View.GONE);
            dob = preferenceUtil.getUserDob().split("-")[0] + "-" + preferenceUtil.getUserDob().split("-")[1];
            editTextDob.setText(UtilityMethod.getNameOfMonth(preferenceUtil.getUserDob().split("-")[0]) + ", " + preferenceUtil.getUserDob().split("-")[1]);
        } else {
            editTextDob.setVisibility(View.GONE);
            linearDOBSpinnerHolder.setVisibility(View.VISIBLE);
        }

        if (preferenceUtil.getUserDoa().contains("-")) {
            doa = preferenceUtil.getUserDoa().split("-")[0] + "-" + preferenceUtil.getUserDoa().split("-")[1];
            editTextAnniv.setText(UtilityMethod.getNameOfMonth(preferenceUtil.getUserDoa().split("-")[0]) + ", " + preferenceUtil.getUserDoa().split("-")[1]);
            linearDOASpinnerHolder.setVisibility(View.GONE);
            txtAnniversaryMessage.setVisibility(View.GONE);

        } else {
            editTextAnniv.setVisibility(View.GONE);
            linearDOASpinnerHolder.setVisibility(View.VISIBLE);

        }

    }

    private void showTitleListDialog()
    {
        final Dialog dialog = new Dialog(MyAccountActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_title_list);

        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.imgBtnClose);

        ListView listView = (ListView) dialog.findViewById(R.id.listViewOptions);
        final String[] allTitles=getResources().getStringArray(R.array.titles);
        final TitlesDialogAdapter adapter = new TitlesDialogAdapter(MyAccountActivity.this, allTitles);
        listView.setAdapter(adapter);

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                title=allTitles[position];
                btnTitle.setText(title);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void onItemClickListen() {
        spinnerMonthDOB = (Spinner) findViewById(R.id.spinnerMonthDOB);
        spinnerDateDOB= (Spinner) findViewById(R.id.spinnerDateDOB);
        spinnerMonthDOA = (Spinner) findViewById(R.id.spinnerMonthDOA);
        spinnerDateDOA = (Spinner) findViewById(R.id.spinnerDateDOA);

        ArrayAdapter<CharSequence> adapterSpinnerMonthDOB = ArrayAdapter.createFromResource(this,R.array.months, R.layout.spinner_item);
        adapterSpinnerMonthDOB.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMonthDOB.setAdapter(adapterSpinnerMonthDOB);
        spinnerMonthDOB.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }

                List<String> words = new ArrayList<>();
                words.add("Date");

                if (position != 0) {
                    int count = UtilityMethod.daysInMonth(parent.getItemAtPosition(position).toString());
                    for (int i = 1; i <= count; i++) {
                        if (i <= 9) {
                            words.add("0" + i);
                        } else {
                            words.add("" + i);
                        }
                    }

                }

                if (words.size() == 0) {
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(MyAccountActivity.this, R.array.planets_array, R.layout.spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerDateDOB.setAdapter(adapter);
                } else {
                    ArrayAdapter<String> adapter;
                    adapter = new ArrayAdapter<String>(MyAccountActivity.this, R.layout.spinner_item, words);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerDateDOB.setAdapter(adapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<CharSequence> adapterSpinnerMonthDOA = ArrayAdapter.createFromResource(this,R.array.months, R.layout.spinner_item);
        adapterSpinnerMonthDOA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMonthDOA.setAdapter(adapterSpinnerMonthDOA);

        spinnerMonthDOA.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                List<String> words = new ArrayList<>();
                words.add("Date");
                if (position != 0) {
                    int count = UtilityMethod.daysInMonth(parent.getItemAtPosition(position).toString());
                    for (int i = 1; i <= count; i++) {
                        if (i <= 9) {
                            words.add("0" + i);
                        } else {
                            words.add("" + i);
                        }
                    }

                }

                if (words.size() == 0) {
                    ArrayAdapter<CharSequence> adapterSpinnerDateDOA = ArrayAdapter.createFromResource(MyAccountActivity.this, R.array.planets_array, R.layout.spinner_item);
                    adapterSpinnerDateDOA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerDateDOA.setAdapter(adapterSpinnerDateDOA);
                } else {
                    ArrayAdapter<String> adapterSpinnerDateDOA = new ArrayAdapter<String>(MyAccountActivity.this, R.layout.spinner_item, words);
                    adapterSpinnerDateDOA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerDateDOA.setAdapter(adapterSpinnerDateDOA);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initView() {

        toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle("My Account");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        preferenceUtil = new PreferenceUtil(this);
        editTextFirstName = (MsmEditText) findViewById(R.id.editTextFirstName);
        editTextLastName = (MsmEditText) findViewById(R.id.editTextLastName);
        editTextEmail = (MsmEditText) findViewById(R.id.editTextEmail);
        editTextEmail.setEnable(false);
        editTextEmail.setFocusableInTouchMode(false);
        editTextEmail.setFocusable(false);
        editTextEmail.setFocusableInTouchMode(true);
        editTextEmail.setFocusable(true);

        editTextMobileNo = (MsmEditText) findViewById(R.id.editTextMobileNo);
        editTextTel = (MsmEditText) findViewById(R.id.editTextTel);
        editTextDob = (MsmEditText) findViewById(R.id.editTextDob);
        editTextAnniv = (MsmEditText) findViewById(R.id.editTextAnniv);
        editTextPostCode = (MsmEditText) findViewById(R.id.editTextPostCode);
        editTextAddress1 = (MsmEditText) findViewById(R.id.editTextAddress1);
        editTextAddress2 = (MsmEditText) findViewById(R.id.editTextAddress2);
        editTextCity = (MsmEditText) findViewById(R.id.editTextCity);
        editTextCountry = (MsmEditText) findViewById(R.id.editTextCountry);

        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnTitle = (Button) findViewById(R.id.btnTitle);
        imageViewBtnDob = (ImageView) findViewById(R.id.imageViewDob);
        imageViewBtnDoa = (ImageView) findViewById(R.id.imageViewAnnivDate);

        txtAnniversaryMessage = (TextView) findViewById(R.id.txtViewAnnivMessage);
        txtDobMessage = (TextView) findViewById(R.id.txtViewDobMessage);

        editTextDob.setEnable(false);
        editTextAnniv.setEnable(false);

    }

    ProgressDialog progress;
    private void callUpdateProfilesApi() {
        if (linearDOBSpinnerHolder.getVisibility() == View.VISIBLE) {
            if (spinnerMonthDOB.getSelectedItem().toString().equalsIgnoreCase("Month")
                    && spinnerDateDOB.getSelectedItem().toString().equalsIgnoreCase("Date")) {
                dob = "";
            } else {
                dob = UtilityMethod.getNoOfMonth(spinnerMonthDOB.getSelectedItem().toString()) + "-" + spinnerDateDOB.getSelectedItem().toString();
            }
        } else {

            //dob = editTextDob.getText();
        }

        if (linearDOASpinnerHolder.getVisibility() == View.VISIBLE) {
            if (spinnerMonthDOA.getSelectedItem() == null || spinnerDateDOA.getSelectedItem() == null || (spinnerMonthDOA.getSelectedItem().toString().equalsIgnoreCase("Month")
                    && spinnerDateDOA.getSelectedItem().toString().equalsIgnoreCase("Date"))) {
                doa = "";
            }  else {
                doa = UtilityMethod.getNoOfMonth(spinnerMonthDOA.getSelectedItem().toString()) + "-" + spinnerDateDOA.getSelectedItem().toString();
            }

        } else {
            //doa = editTextAnniv.getText();
        }

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "15");
                params.put("userid", preferenceUtil.getUserID());
                params.put("title", title);
                params.put("fname", editTextFirstName.getText().toString().trim());
                params.put("lname", editTextLastName.getText().toString().trim());
                params.put("email", editTextEmail.getText().toString().trim());
                params.put("mobile_no", editTextMobileNo.getText().toString().trim());
                params.put("telephone_no", editTextTel.getText().toString().trim());
                params.put("dob_date", dob);
                params.put("doa", doa);
                params.put("postcode", editTextPostCode.getText().toString().trim());
                params.put("address1", editTextAddress1.getText().toString().trim());
                params.put("address2", editTextAddress2.getText().toString().trim());
                params.put("city", editTextCity.getText().toString().trim());
                params.put("country", editTextCountry.getText().toString().trim());
                Log.i("REGISTRATION", "" + params.toString());
                return params;
            }

        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);
        progress = new ProgressDialog(this);
        progress.setMessage("Please wait....");
        progress.show();
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                    String x = error.getMessage();
                    new CustomToast(MyAccountActivity.this,"Server Response Error.", "", false);
                }
                catch (Exception e){
                    Log.e("After volley execution",e.getMessage());
                }
            }
        };
    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progress.dismiss();
                    Log.i(TAG, "****" + response);

                    UserData userData = new UserData();
                    userData = JsonParser.parseRegistrationData(response);
                    if ("Success".equalsIgnoreCase(userData.getLoginStatus())) {
                        preferenceUtil.setUserTitle(title);
                        preferenceUtil.setUserFirstName(editTextFirstName.getText().toString());
                        preferenceUtil.setUserSurName("");
                        preferenceUtil.setUserLastName(editTextLastName.getText().toString());
                        preferenceUtil.setUserAddress1(editTextAddress1.getText().toString());
                        preferenceUtil.setUserAddress2(editTextAddress2.getText().toString());
                        preferenceUtil.setUserMobile(editTextMobileNo.getText().toString().trim());
                        preferenceUtil.setUserEmail(editTextEmail.getText().toString().trim());
                        preferenceUtil.setUserCountry(editTextCountry.getText().toString());
                        preferenceUtil.setUserDob(dob);
                        preferenceUtil.setUserDoa(doa);
                        preferenceUtil.setUserGroupId(userData.getGroupId());
                        preferenceUtil.setUserPostCode(editTextPostCode.getText().toString().trim());
                        preferenceUtil.setUserTel(editTextTel.getText().toString().trim());
                        preferenceUtil.setUserTown(editTextCity.getText().toString().trim());
                        new CustomToast(MyAccountActivity.this, "Your profile has been updated.", "", true);
                        onBackPressed();
                    } else {
                        new CustomToast(MyAccountActivity.this, "Sorry, update process error has encountered.", "", false);
                    }

                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                        new CustomToast(MyAccountActivity.this, "Parsing error encountered.", "", false);
                    } catch (Exception e1) {
                        Log.e("After volley execution",e1.getMessage());
                    }

                }
            }
        };
    }

    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    private void setDateTimeField() {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);
        Calendar newCalendar = Calendar.getInstance();

        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                Date strDate = null;
                try {
                    strDate = dateFormatter.parse(dateFormatter.format(newDate.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (new Date().before(strDate)) {
                    new CustomToast(MyAccountActivity.this, "Can not be greater than current date.", "", false);
                } else {
                    editTextDob.setText(dateFormatter.format(newDate.getTime()));
                }

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


    }

    private void setDateTimeField(int pickerNo) {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editTextAnniv.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


    }

    private boolean  validation() {
        if (editTextFirstName.getText().toString().trim().equalsIgnoreCase("")) {
                editTextFirstName.setErrorTextVisible(true);
                editTextFirstName.setErrorMsg("Your First Name is required.");
                editTextFirstName.setTypingFocus();
                return false;
        }

        if (editTextLastName.getText().toString().trim().equalsIgnoreCase("")) {
            editTextLastName.setErrorTextVisible(true);
            editTextLastName.setErrorMsg("Your Last Name is required.");
            editTextLastName.setTypingFocus();
            return false;
        }

        if (editTextEmail.getText().toString().trim().equalsIgnoreCase("")) {
            editTextEmail.setErrorTextVisible(true);
            editTextEmail.setErrorMsg("Your Email is required.");
            editTextEmail.setTypingFocus();
            return false;
        }

        if (!UtilityMethod.isEmailValid(editTextEmail.getText().toString().trim())) {
            editTextEmail.setErrorTextVisible(true);
            editTextEmail.setErrorMsg("Your Email is not valid.");
            editTextEmail.setTypingFocus();
            return false;
        }

        if (editTextMobileNo.getText().toString().trim().equalsIgnoreCase("")) {
            editTextMobileNo.setErrorTextVisible(true);
            editTextMobileNo.setErrorMsg("Your Mobile no is required.");
            editTextMobileNo.setTypingFocus();
            return false;
        }

        if (editTextCity.getText().toString().trim().equalsIgnoreCase("")) {
            editTextCity.setErrorTextVisible(true);
            editTextCity.setErrorMsg("Your City is required.");
            editTextCity.setTypingFocus();
            return false;
        }

        if (editTextCountry.getText().toString().trim().equalsIgnoreCase("")) {
            editTextCountry.setErrorTextVisible(true);
            editTextCountry.setErrorMsg("Your Country is required.");
            editTextCountry.setTypingFocus();
            return false;
        }

        if (!UtilityMethod.isMobileNoValid(editTextMobileNo.getText().toString().trim())) {
            editTextMobileNo.setErrorTextVisible(true);
            editTextMobileNo.setErrorMsg("Please provide valid mobile number.");
            editTextMobileNo.setTypingFocus();
            return false;
        }

        if (editTextPostCode.getText().toString().trim().equalsIgnoreCase("")) {
            editTextPostCode.setErrorTextVisible(true);
            editTextPostCode.setErrorMsg("Your Postcode is required.");
            editTextPostCode.setTypingFocus();
            return false;
        }

        if (!UtilityMethod.postcodeValidation(editTextPostCode.getText().toString().trim())) {
            editTextPostCode.setErrorTextVisible(true);
            editTextPostCode.setErrorMsg("Your Postcode is invalid.");
            editTextPostCode.setTypingFocus();
            return false;
        }

        if (editTextAddress1.getText().toString().trim().equalsIgnoreCase("")) {
            editTextAddress1.setErrorTextVisible(true);
            editTextAddress1.setErrorMsg("Your Address1 is required.");
            editTextAddress1.setTypingFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                if (SystemClock.elapsedRealtime() - mLastClickTime < 500){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                editTextCity.setTypingFocus();
                if(validation()){
                    callUpdateProfilesApi();
                }
                break;
            case R.id.btnTitle:
                showTitleListDialog();
                break;
            case R.id.imageViewDob:
                setDateTimeField();
                fromDatePickerDialog.show();
                break;
            case R.id.imageViewAnnivDate:
                setDateTimeField(1);
                fromDatePickerDialog.show();
                break;
            case R.id.editTextDob:
                setDateTimeField();
                fromDatePickerDialog.show();
                break;
            case R.id.editTextAnniv:
                setDateTimeField(1);
                fromDatePickerDialog.show();
                break;
        }
    }
}
